# coding=utf-8
"""

"""

from pathlib import Path
from Orange.data import FileFormat

from .pyopticslab import units as u
from .pyopticslab.generic import Surface, Profile
from pylost.utils.methods import minimal_processing


class PylostReader(FileFormat):
    """
     generic class for pylost data reader
    """
    DATATYPE = 'unknown'  # 'heights' or 'slopes'
    NDIM = 'unknown'      # '1D' or '2D'

    @staticmethod
    def read(filepath):
        """ return data, motor position, additional infos """
        return [], [], {}

    @staticmethod
    def export(filepath, data):
        """"""

    def loadfile(self, filepath):
        try:
            data, motors, extras = self.read(filepath)
            return self._checkdata(data, motors, extras)
        except Exception as e:
            raise e

    @staticmethod
    def _data_to_dict(coords:list, values:list, coords_unit_str:str, values_unit_str:str):
        """"""
        return {'coords':coords,
                'values':values,
                'coords_unit':u.UNITS[coords_unit_str],
                'values_unit':u.UNITS[values_unit_str]
                }

    @staticmethod
    def _motor_to_dict(x_pos:float = None, y_pos:float = None, unit_str:str = 'mm'):
        """"""
        if not x_pos:
            x_pos = 0
        if not y_pos:
            y_pos = 0
        return {'x':x_pos, 'y':y_pos, 'unit':u.UNITS.get(unit_str, u.mm)}

    def _checkdata(self, data, motors, extras=None, postprocessing=True):
        is_dataset = True
        if not isinstance(data, (list, tuple)):
            is_dataset = False
            data = [data, ]
            motors = [motors, ]
        # basic conditions
        if self.DATATYPE not in ('height', 'slope', 'heights', 'slopes', 'any'):
            raise ValueError(f'Invalid DATATYPE attribute in the plugin "{self.__class__}".')
        if self.NDIM not in ('1D', '2D'):
            raise ValueError(f'Invalid NDIM attribute in the plugin "{self.__class__}".')
        if not data:
            raise ValueError(f'The plugin "{self.__class__}" has returned an empty dataset.')
        if len(data) != len(motors):
            raise ValueError(f'The plugin "{self.__class__}" has returned different size of data and motors.')
        # convert data in Surface/Profile classes if not already
        datalist = []
        for i, data_tuple in enumerate(zip(data, motors)):
            item, motor = data_tuple
            if isinstance(item, dict):
                try:
                    filename = Path(self.filename).name
                    source = filename + f'[#{str(i)}]' if is_dataset else filename
                    units = {'coords': item['coords_unit'], 'values': item['values_unit'],
                             'angle': u.urad, 'length': u.mm, 'radius': u.km, 'pixel': u.mm}
                    cls = Profile if self.NDIM == '1D' else Surface
                    item = cls(coords=item['coords'], values=item['values'], units=units, source=source)
                    if postprocessing:
                        minimal_processing(item)
                except KeyError:
                    raise KeyError(f'Did the plugin "{self.__class__}" called the method "_data_to_dict" ?')
                try:
                    if motor is not None:
                        # convert motors position to same unit as coordinates
                        coords_unit = item.coords_unit
                        motor['x'] = coords_unit(motor['x'], motor['unit']) if motor['x'] else 0
                        motor['y'] = coords_unit(motor['y'], motor['unit']) if motor['y'] else 0
                        motor['unit'] = coords_unit
                except ValueError:
                    raise KeyError(f'Did the plugin "{self.__class__}" called the method "_motor_to_dict" ?')

            # initialize radius calc
            _, _ = item.radius, item.radius_sag

            datalist.append(item)
        return datalist, motors, extras
