# coding=utf-8

from pylost import VERBOSE

from .readers import PylostReader

from .pyopticslab import units as u
from .pyopticslab.generic import Surface, Profile, deepcopy

# builtin readers
from .esrf import *
from .diamond import *
from .others import *
from .hdf5_formats import *

# load user data readers
from importlib import import_module
from inspect import getmembers, isclass
from pylost.user.settings import READERS
if READERS.exists():
    try:
        module = import_module('data_readers')
        if VERBOSE:
            print(f"    loading plugins: {module.__name__}")
        for readername, readerclass in getmembers(module, isclass):
            if issubclass(readerclass, PylostReader) and readerclass is not PylostReader:
                locals()[readername] = readerclass
                if VERBOSE:
                    print(f"      reader '{readername}' loaded.")
    except Exception as exception:
        print(f'error when importing plugins.data_readers: \'{exception}\'')

# parse readers (builtin or user) to get all the extensions specific to known instruments
PYLOST_FILE_EXTENSIONS = []
for reader_object in locals().copy().values():
    if not isclass(reader_object):
        continue
    if issubclass(reader_object, PylostReader) and reader_object is not PylostReader:
        for ext in reader_object.EXTENSIONS:
            if ext not in PYLOST_FILE_EXTENSIONS:
                PYLOST_FILE_EXTENSIONS.append(ext)


class StitchingDataset:
    """
     convert the dataset into a specific class
    """
    def __init__(self,
                 dataset:[list[Surface, Profile], Surface, Profile],
                 motors:[list, tuple] = None,
                 center_coordinates=True,
                 **kwargs):
        if not isinstance(dataset, list):
            dataset = [dataset, ]
        # self.reader = None  # custom method to read the file
        self.scans = dataset
        self.motors = deepcopy(motors)
        self.extras = kwargs.get('extra', None)
        if 'attributes_only' in dataset:
            return
        if center_coordinates:
            self.center_coordinates()
        self.unify_units()
        self.stitched = None
        self.reference = None
        self.detector_size = self.shape if dataset else None
        self.data_offsets = [0, 0]
        self._it = 0
        self.comment = ('', )
        self.history = ('', )
        self._heights = None
        self._slopes = None
        self.title = kwargs.get('title', 'no title')
        self.sender = kwargs.get('sender', None)
        self.folder = kwargs.get('folder', '')
        self.noise_data = False

    def __len__(self):
        """can be test as list"""
        return len(self.scans)

    def __iter__(self):
        """can be used as iterator"""
        self._it = 0
        return self

    def __next__(self):
        """can be used as iterator"""
        try:
            item = self.scans[self._it]
        except IndexError:
            raise StopIteration()
        self._it += 1
        return item

    def __repr__(self):
        if self.is_stitched:
            length = self.stitched.length
            length_unit = str(self.stitched.coords_unit)
            return f'Stitched data: {length[0]:.3f}{length_unit} x {length[1]:.3f} {length_unit}'
        size = self.size
        length_unit = str(self.scans[0].coords_unit)
        if self.is_1D:
            string = f'{int(size[0])}  x  {size[1]:.3f} {length_unit}'
        else:
            string = f'{int(size[0])}  x  [{size[1]:.3f} {length_unit} x {size[2]:.3f} {length_unit}]'
        # if len(self.scans) == 1:
        #     string = string.split('x', 1)[-1]
        return f'Dataset: ' + string

    def summarize(self):
        if self.title:
            return self.title
        return self.__repr__
        # return 'StitchingDataset'

    def set_sender(self, widget):
        self.sender = widget

    def set_title(self, title):
        self.title = str(title)

    @property
    def kind(self):
        return self.scans[0].kind

    @property
    def pixel_size(self):
        return self.scans[0].pixel_size

    @property
    def is_heights(self):
        return self.scans[0].is_heights

    @property
    def is_slopes(self):
        return self.scans[0].is_slopes

    @property
    def is_1D(self):
        return self.scans[0].is_1D

    @property
    def is_2D(self):
        return self.scans[0].is_2D

    @property
    def is_oversampled(self):
        return self.scans[0].is_oversampled

    @property
    def mean_step(self):
        if self.is_oversampled:
            return self.scans[0].mean_step
        return self.scans[0].pixel_size

    @property
    def shape(self):
        shape = self.get_valid_mask().shape
        if self.is_1D:
            if self.is_stitched:
                return 1, self.stitched.shape[0]
            return len(self.scans), shape[0]
        if self.is_stitched:
            return 1, self.stitched.shape[0], self.stitched.shape[1]
        return len(self.scans), shape[0], shape[1]

    @property
    def size(self):
        if self.is_1D:
            if self.is_stitched:
                return 1, self.stitched.length
            lengths = [scan.length for scan in self.scans]
            return len(self.scans), max(lengths)
        if self.is_stitched:
            lengths_x, lengths_y = self.stitched.length
            return 1, lengths_x, lengths_y
        lengths = [scan.length for scan in self.scans]
        lengths_x, lengths_y = zip(*lengths)
        return len(self.scans), max(lengths_x), max(lengths_y)

    @property
    def units(self):
        return self.scans[0].units

    @property
    def coords_unit(self):
        return self.scans[0].coords_unit

    @property
    def values_unit(self):
        return self.scans[0].values_unit

    @property
    def radius_unit(self):
        return self.scans[0].radius_unit

    # def get_statistics(self, scan_idx=0):
    #     if self.is_stitched:
    #         return Statistics(self.stitched)
    #     return Statistics(self.scans[scan_idx])

    def get_slopes(self):
        """ return a list of slopes data on each axis """
        if self.is_slopes:
            return self
        elif self.is_heights:
            if self._slopes is not None:
                return self._slopes
            # derivate once for all
            self._slopes = []
            data = self.stitched if self.stitched else self.scans[0]
            if self.is_1D:
                self._slopes = StitchingDataset([data.derivative()], title=self.title + ' (slopes)')
            else:
                dx, dy = data.derivative()
                self._slopes = [StitchingDataset(dx, title=self.title + ' (dx)'),
                                StitchingDataset(dy, title=self.title + ' (dy)')]
            return self._slopes

    def get_heights(self):
        """ return a list of heights data on each axis """
        if self.is_heights:
            return self
        elif self.is_slopes:
            if self._heights is not None:
                return self._heights
            # integrate once for all
            self._heights = []
            data = self.stitched if self.stitched else self.scans[0]
            self._heights = [StitchingDataset(data.integral(copy=False, create_initial=False)),
                             StitchingDataset(data.integral(copy=False, create_initial=False))]
            return self._heights

    def set_motors(self, motors:np.ndarray, unit_str='mm'):
        shape = motors.shape
        if shape[0] != len(self.scans) or shape[1] != 2:
            raise ValueError('invalid motors array')
        x_pos = motors[:, 0]
        y_pos = motors[:, 1]
        unit = u.UNITS.get(unit_str, u.mm)
        self.motors = []
        for x, y in zip(x_pos, y_pos):
            self.motors.append({'x':x, 'y':y, 'unit':unit})

    def motors_to_list(self):
        """"""
        x_pos = []
        y_pos = []
        if self.motors:
            for motor in self.motors:
                x_pos.append(motor['x'])
                y_pos.append(motor['y'])
        return x_pos, y_pos

    def motors_to_pixel_shift(self, motors:[tuple, list]):
        """motors as tuple of list"""
        x_pos, y_pos = motors
        step_pix = (np.array(x_pos) - min(x_pos)) / self.pixel_size[0], (np.array(y_pos) - min(y_pos)) / self.pixel_size[1]
        return np.rint(step_pix).astype(int)

    def get_motors_steps(self):
        if not self.motors:
            return [0, 0]
        x_pos, y_pos = self.motors_to_list()
        return np.mean(np.diff(x_pos)), np.mean(np.diff(y_pos))

    def set_motors_from_steps(self, shift_x, shift_y, unit_str='mm'):
        # in coordinates unit
        pos = np.arange(0, len(self))
        # unit = self.coords_unit
        self.set_motors(np.array([pos * shift_x, pos * shift_y]).T, str(unit_str))
        # self.add_history_entry(f'motors position changed with constant steps [{shift_x:.3f} {unit_str}, '
        #                        f'{shift_y:.3f} {unit_str}]')

    def add_comment_entry(self):
        """"""

    def add_history_entry(self, entry:str):
        """"""
        self.history = self.history + (entry,)

    @property
    def is_stitched(self):
        """check if stitched data is present"""
        return self.stitched is not None

    @property
    def has_reference(self):
        """check if reference is present"""
        return self.reference is not None

    @property
    def has_motors(self):
        """check if motors positions are present"""
        if self.motors:
            return True
        return False

    def set_stitched_data(self, stitched:[Surface, Profile]):
        """add stitched data to the class"""
        self.stitched = stitched

    def set_reference_extracted(self, reference:[Surface, Profile]):
        """add reference data to the class"""
        self.reference = reference

    def copypatches(self, update_roc=True):
        """return a copy of the scans"""
        return [scan.duplicate(update_roc) for scan in self.scans]

    def copystitched(self, update_roc=True):
        """return a copy of the stiched data if present, else None"""
        if self.is_stitched:
            return self.stitched.duplicate(update_roc)
        return None

    def copymotors(self):
        """return a copy of the scans"""
        if self.has_motors:
            return deepcopy(self.motors)
        return None

    def copy_attributes_only(self):
        skip = ('scans', 'stitched', 'reference', '_heights', '_slopes', 'units', 'motors')
        new = StitchingDataset(['attributes_only'])
        for attr_name, value in self.__dict__.items():
            if attr_name in skip:
                setattr(new, attr_name, None)
                continue
            setattr(new, attr_name, value)
        new.unit = deepcopy(self.units)
        new.motors = self.copymotors()
        return new

    def copydataset(self):
        """return a copy of the dataset. Only copy stitched data if present, leaving the scans as is"""
        skip = ('reference', '_heights', '_slopes', 'units', 'motors')
        if self.is_stitched:
            new = StitchingDataset(self.scans)
            new.set_stitched_data(self.copystitched())
            skip = skip + ('stitched', )
        else:
            new = StitchingDataset(self.copypatches())
            skip = skip + ('scans', )
        new.set_reference_extracted(self.reference) # should we add comment/history ?
        for attr_name, value in self.__dict__.items():
            if attr_name in skip:
                continue
            setattr(new, attr_name, value)
        new.unit = deepcopy(self.units)
        new.motors = self.copymotors()
        new.data_offsets = [0, 0]
        return new

    @staticmethod
    def export_to_pylost_format(self, filepath:[str, Path]):
        """ includes all data (scans, stitch result, reference, history) """
        # TODO

    def get_valid_mask(self):
        shape_max = np.max([scan.shape for scan in self.scans], axis=0)
        if self.is_1D:
            for scan in self.scans:
                coords = np.linspace(0, scan.pixel_size * len(scan.coords), num=shape_max[0], endpoint=False)
                coords -= np.nanmean(coords)
                x = np.linspace(0, scan.pixel_size * shape_max[0], num=shape_max[0], endpoint=False)
                x -= np.nanmean(x)
                slc = x.searchsorted([np.nanmin(coords), np.nanmax(coords)])
                y = np.full_like(x, np.nan)
                y[slc[0]:slc[1] + 1] = scan.values
                scan.coords = x
                scan.values = y
        valid_mask = np.zeros(shape_max)
        try:
            for data in self.scans:
                valid_mask = np.logical_or(valid_mask, data.valid_mask)
        except ValueError as e:
            raise ValueError('Dataset could not be constructed with subaperture size mismatch', e)
        if not np.max(valid_mask):
            raise ValueError('empty dataset')
        return valid_mask

    def Mx_std_canvas_size(self):

        cn_list = [[s.header['cn_org_x'],
                    s.header['cn_org_y'],
                    s.header['cn_org_x'] + s.header['cn_width'],
                    s.header['cn_org_y'] + s.header['cn_height']] for s in self.scans]

        cn_list = list(zip(*cn_list))

        ox_min = np.amin(cn_list[0])
        oy_min = np.amin(cn_list[1])
        ex_max = np.amax(cn_list[2])
        ey_max = np.amax(cn_list[3])

        width_max = ex_max - ox_min
        height_max = ey_max - oy_min

        for s in self.scans:

            ox = s.header['cn_org_x'] - ox_min
            oy = s.header['cn_org_y'] - oy_min
            wx = s.header['cn_width']
            wy = s.header['cn_height']

            h = np.full((width_max,height_max), np.nan)
            try:
                h[ox:ox + wx,oy:oy + wy] = s.values
                s.values = np.array(h)
            except Exception:
                h[ox:ox + wx,oy:oy + wy] = np.transpose(s.values)
                s.values = np.transpose(np.array(h))

            s.header['cn_org_x'] = ox_min
            s.header['cn_org_y'] = oy_min
            s.header['cn_width'] = width_max
            s.header['cn_height'] = height_max

            value_shape = s.values.shape
            s.coords[0] = np.linspace(0, value_shape[0] * s.pixel_size[0],
                        num=value_shape[0], endpoint=False, dtype=np.float64)
            s.coords[1] = np.linspace(0, value_shape[1] * s.pixel_size[1],
                        num=value_shape[1], endpoint=False, dtype=np.float64)

        self.center_coordinates()

    def shrinkdata(self):
        """find the minimum common size of list of scans, removing useless np.nan values on the border
        return the X/Y offsets keeping track of a possible shift of the data
        """
        valid_mask = self.get_valid_mask()
        if self.is_1D: # TODO: dont shrink profile data ?
            return
        valid_x = np.full((valid_mask.shape[0]), False)
        arg_x = np.argwhere(valid_mask.any(axis=1))[:, 0]
        valid_x[slice(arg_x[0], arg_x[-1] + 1)] = True
        valid_y = np.full((valid_mask.shape[1]), False)
        arg_y = np.argwhere(valid_mask.any(axis=0))[:, 0]
        valid_y[slice(arg_y[0], arg_y[-1] + 1)] = True
        for data in self.scans:
            data.detector_size = data.values.shape
            data.values = data.values[valid_x, :]
            data.values = data.values[:, valid_y]
            data.coords[0] = data.coords[0][valid_x]
            data.coords[1] = data.coords[1][valid_y]
            data.center_coordinates()

        offset_x = arg_x[0] + (arg_x[-1] - arg_x[0]) / 2 - (len(valid_x) - 1) / 2
        offset_y = arg_y[0] + (arg_y[-1] - arg_y[0]) / 2 - (len(valid_y) - 1) / 2
        self.data_offsets = [offset_x, offset_y]

        if self.is_stitched:
            self.stitched.remove_invalid_lines()

    def center_coordinates(self):
        for scan in self.scans:
            scan.center_coordinates()

    def unify_units(self):
        if not self.scans:
            return

        coords_pv, values_rms, roc = 0.0, 0.0, 0.0
        new_coords_unit = self.coords_unit
        new_values_unit = u.urad
        new_radius_unit = u.m

        for scan in self.scans:
            if not str(self.coords_unit) in ('pix', 'unitless'):
                coords_pv = max(coords_pv, np.nanmax(u.m(scan.length, scan.coords_unit)))
            values_rms = max(values_rms, scan.rms)
            if scan.radius is not None:
                roc = max(roc, scan.radius)

        if not str(self.coords_unit) == 'pix':
            _, new_coords_unit = u.m.auto(coords_pv)
            if str(new_coords_unit) == 'm': # force millimeter
                new_coords_unit = u.mm

        if not str(self.values_unit) == 'unitless':
            _, new_values_unit = self.scans[0].values_unit.auto(values_rms)
            # limit lower units
            factor = new_values_unit.SI_unit(1, new_values_unit)
            if self.is_slopes and factor < 1e-6:
                new_values_unit = u.urad
            if self.is_heights and factor < 1e-9:
                new_values_unit = u.nm

        if roc != 0.0:
            _, new_radius_unit = self.radius_unit.auto(roc)

        # apply common unit
        for scan in self.scans:
            if not str(self.coords_unit) == 'pix':
                scan.change_coords_unit(new_coords_unit)
            if not str(self.values_unit) == 'unitless':
                scan.change_values_unit(new_values_unit)
            scan.change_radius_unit(new_radius_unit)

        if self.motors is not None:
            for motor in self.motors:
                motor['x'] = new_coords_unit(motor['x'], motor['unit'])
                motor['y'] = new_coords_unit(motor['y'], motor['unit'])
                motor['unit'] = new_coords_unit

        return {'coords':new_coords_unit, 'values':new_values_unit}

    def masking(self, rectangle):
        """return a copy of the dataset masked using pixel coordinates given by rectangle (origin/size)
        (masked the scans by default but only stiched data if present)"""
        origin, size = rectangle
        xrangearray = [origin[0], origin[0] + size[0]]
        yrangearray = [origin[1], origin[1] + size[1]]
        masked = self.copydataset()
        dataset = [masked.stitched] if masked.is_stitched else masked.scans
        for scan in dataset:
            scan.subarray_pixel(xrangearray=xrangearray, yrangearray=yrangearray, apply=True)
            if len(scan.values) == 0:
                return None
            scan.mean_removal()
            scan._roc_x = None
            scan._roc_y = None
        return masked

    def change_pixel_size(self, new_sampling:[float, list[float], tuple[float]]):
        if self.is_1D:
            new_sampling = new_sampling[0] if not isinstance(new_sampling, float) else new_sampling
            [scan.change_pixel_size(new_sampling) for scan in self.scans]
        else:
            [scan.change_pixel_size(*new_sampling) for scan in self.scans]
        # unit = self.coords_unit
        # self.add_history_entry(f'sampling changed to [X: {new_sampling[0]:.6f} {unit}, Y: {new_sampling[1]:.6f} {unit}]')
