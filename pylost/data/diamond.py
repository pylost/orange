# coding=utf-8
"""
accepted unit string in "set_coords" and "set_values":
    - heights
        'm', 'km', 'cm', 'mm', 'um', 'nm', 'A', 'pm', 'inch'
    - slopes
        'rad', 'mrad', 'urad', 'nrad'
"""

from pylost.data.readers import PylostReader
from .pyopticslab.zygo import MxData

import pandas as pd
from os import path

class MxReader(PylostReader):
    """File reader for Mx datx file format"""
    EXTENSIONS = ('.datx', '.DATX')
    DESCRIPTION = 'Mx DATX file reader'
    PRIORITY = 1
    DATATYPE = 'heights'
    NDIM = '2D'

    @staticmethod
    def read(filepath):
        def _readHDXmotors(file_path):
            try:
                abs_path = path.split(path.splitext(file_path)[0])[0]
                file_name = path.split(path.splitext(file_path)[0])[1]
                split_name = file_name.split('-P')
                aux_name = ''

                if len(split_name) > 2:
                    for i in range(len(split_name) - 1):
                        if not i:
                            aux_name = split_name[i]
                        else:
                            aux_name = aux_name + '-P' + split_name[i]
                else:
                    aux_name = split_name[0]

                motor_df_path = aux_name[:-1] + 'data.csv'
                sub_aperture_number = int(file_name.split('-P')[-1][:])
                motor_df = pd.read_csv(abs_path + '//' + motor_df_path)
                motor_x_pos = motor_df[motor_df['Step_Count_X'] == sub_aperture_number - 1]['Motor_X_Pos'].values[0]
                motor_y_pos = motor_df[motor_df['Step_Count_X'] == sub_aperture_number - 1]['Motor_Y_Pos'].values[0]
            except Exception:
                motor_x_pos = None
                motor_y_pos = None

            return motor_x_pos, motor_y_pos

        try:
            # data as Surface object
            data = MxData.read(filepath)
            # read positions from .csv file

            unit = 'mm'
            x_pos, y_pos = _readHDXmotors(filepath)

            if x_pos is not None:
                motors = PylostReader._motor_to_dict(x_pos, y_pos, unit)
                return data, motors, None
            else:
                return data, None, None
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def export(filepath, data):
        try:
            MxData.writeDatxFile(filepath, data.values, data.pixel_size, data.units)
            # custom_attrs = {
            #     'Stitching': {'Instrument':'AT+',
            #                   'Final Length':{'Value':283.512, 'Unit':'mm'},
            #                   'Overlap':{'Value':80.35, 'Unit':'pct'},
            #                   'Constant Step':{'Value':3.929, 'Unit':'mm'},
            #                   'Current Position':{'Value':-125.465, 'Unit':'mm'}},
            #     'Temperatures': {'Probe1':20.5134, 'Probe2':21.6645, 'Probe3':20.4665, 'Unit':'celsius'},
            #     'AC_measurement': {'Rx':1.2354, 'Ry':0.3146, 'Unit':'urad'},
            #     'Motors': {'Axmo':{'Value':864521, 'Unit':'step'},
            #                'rot_huber':{'Value':99.465, 'Unit':'urad'}},
            # }
            # MxData.add_custom_attributes(filepath, custom_attrs)
        except Exception as e:
            raise Exception(e)
