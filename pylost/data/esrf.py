# coding=utf-8
"""
accepted unit string in "set_coords" and "set_values":
    - heights
        'm', 'km', 'cm', 'mm', 'um', 'nm', 'A', 'pm', 'inch'
    - slopes
        'rad', 'mrad', 'urad', 'nrad'
"""

from pylost.data.readers import PylostReader

from .pyopticslab.generic import Profile, Surface, u
from .pyopticslab.zygo import MetroProData
from .pyopticslab.veeco import OpdData
from .pyopticslab.ltp import LTPdata
from .pyopticslab.misc import HeightProfile
from .pyopticslab.keyence import KeyenceData
from .pyopticslab.haso import SlopesSurface


class HGT_Reader(PylostReader):
    """File reader for Veeco OPD file format"""
    EXTENSIONS = ('.hgt', '.HGT')
    DESCRIPTION = 'ESRF height profile reader'
    PRIORITY = 4
    DATATYPE = 'heights'
    NDIM = '1D'

    @staticmethod
    def read(filepath):
        try:
            item = HeightProfile.read(filepath)
            motors = None
            return item, motors, {'header':item.header}
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def export(filepath, data:Profile):
        try:
            Profile.writeESRFfileformat(filepath, data.coords, data.values, data.units, slp=False)
        except Exception as e:
            raise Exception(e)


class SLP_Reader(PylostReader):
    """File reader for Veeco OPD file format"""
    EXTENSIONS = ('.slp', '.slp2', '.SLP', '.SLP2')
    DESCRIPTION = 'ESRF LTP file reader'
    PRIORITY = 3
    DATATYPE = 'slopes'
    NDIM = '1D'

    @staticmethod
    def read(filepath):
        try:
            item = LTPdata.read(filepath)
            motors = None
            return item, motors, {'header':item.header}
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def export(filepath, data:Profile):
        try:
            Profile.writeESRFfileformat(filepath, data.coords, data.values, data.units, slp=True)
        except Exception as e:
            raise Exception(e)


class KeyenceVK4Reader(PylostReader):
    """File reader for Veeco OPD file format"""
    EXTENSIONS = ('.vk4', '.VK4')
    DESCRIPTION = 'Keyence vk4 file reader'
    PRIORITY = 2
    DATATYPE = 'heights'
    NDIM = '2D'

    @staticmethod
    def read(filepath):
        try:
            item = KeyenceData.read(filepath)
            return item, None, {'header':item.header}
        except Exception as e:
            raise Exception(e)


class VeecoReader(PylostReader):
    """File reader for Veeco OPD file format"""
    EXTENSIONS = ('.opd', '.OPD')
    DESCRIPTION = 'Veeco OPD file reader'
    PRIORITY = 2
    DATATYPE = 'heights'
    NDIM = '2D'

    @staticmethod
    def read(filepath):
        try:
            item = OpdData.read(filepath)
            x_pos = item.header.get('StageX', None)
            y_pos = item.header.get('StageY', None)
            motors = PylostReader._motor_to_dict(x_pos, y_pos, 'mm')
            # TEST
            # item = PylostReader._data_to_dict(item.coords, item.values, str(item.coords_unit), str(item.values_unit))
            # items = [(item, motors) for i in range(3)]
            # item, motors = list(zip(*items))
            # motors = list(motors)
            # motors[1] = None
            return item, motors, {'header':item.header}
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def export(filepath, data:Surface, **kwargs):
        try:
            OpdData.writefile(filepath, u.nm(data.values, data.values_unit), data._pixel_res[0] * 1e3,
                              stages=kwargs.get('stages', (0.0, 0.0)), title='Created from Pylost')
        except Exception as e:
            raise Exception(e)


class MetroproReader(PylostReader):
    """File reader for Metropro dat file format"""
    EXTENSIONS = ('.dat', '.DAT')
    DESCRIPTION = 'Metropro DAT file reader'
    PRIORITY = 1
    DATATYPE = 'heights'
    NDIM = '2D'

    @staticmethod
    def read(filepath):
        def _parse_comment(surface):
            """find motors positions of others infos in the comment (ESRF tweak)"""
            try:
                comment = surface.header['comment'].split(' ')
                infos = {}
                for line in comment:
                    line = line.strip()
                    if 'P=' in line:
                        infos['position_idx'] = int(line.split('=')[-1])
                    elif 'coord=' in line:  # only one axis for the moment
                        infos['encoder_position'] = {}
                        infos['encoder_position']['axis'] = line.split('-')[0]
                        reverse = -1 if infos['encoder_position']['axis'] == 'Y' else 1
                        infos['encoder_position']['value'] = float(line.split('=')[-1]) * reverse
                        infos['encoder_position']['unit'] = 'mm'
                    elif 'shift=' in line:
                        infos['step'] = {}
                        infos['step']['axis'] = line.split('-')[0]
                        infos['step']['value'] = float(line.split('=')[-1])
                        infos['step']['unit'] = 'mm'
                    elif 'TF' in line:
                        infos['TF'] = line
                    elif 'M=' in line:
                        string = line.split('=')[-1]
                        infos['rectangular_mask'] = [float(dim) for dim in string.split('x')]
                        infos['step']['unit'] = 'mm'
                return infos
            except Exception:
                return None

        try:
            # data as Surface object
            data = MetroProData.read(filepath)
            # read positions from comment
            x_pos = []
            y_pos = []
            unit = 'mm'
            misc_infos = _parse_comment(data)
            if misc_infos.get('encoder_position', None):
                encoder = misc_infos.get('encoder_position', None)
                if 'X' in encoder['axis']:
                    x_pos = -encoder['value']  # reverse direction
                elif 'Y' in encoder['axis']:
                    y_pos = -encoder['value']  # reverse direction
                unit = encoder['unit']
            motors = PylostReader._motor_to_dict(x_pos, y_pos, unit)
            return data, motors, {'header':data.header}
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def export(filepath, data:Surface):
        try:
            data.writeMetroprofile(filepath, data.values, data.pixel_size, data.units)
        except Exception as e:
            raise Exception(e)


class Haso_Reader(PylostReader):
    """File reader for Veeco OPD file format"""
    EXTENSIONS = ('.has', '.HAS')
    DESCRIPTION = 'Imagine Optics HASO file reader'
    PRIORITY = 3
    DATATYPE = 'slopes'
    NDIM = '2D'

    @staticmethod
    def read(filepath):
        try:
            item = SlopesSurface.read(filepath)
            motors = None
            return item, motors, {'header':item.header}
        except Exception as e:
            raise Exception(e)
