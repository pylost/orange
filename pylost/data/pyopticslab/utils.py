# -*- coding: utf-8 -*-
""""""

import sys
import datetime as dt
from pathlib import Path
import matplotlib.pyplot as plt
from .gui import DialogBoxSimple


#
# ------------Display------------
#
def plot_array(nparray):
    """Set a matplotlib plot with simple numpy array."""
    plt.plot(*nparray.T)


def plot_df(ltpdata, colnames=None):
    """Use the pandas dataframe plot method, without legend.
    All the columns of a dataframe by default.
    """
    if colnames is None:
        colnames = [item[0] for item in ltpdata.dataframe.items()]
    axes = ltpdata.dataframe.plot(y=colnames, use_index=True, legend=False, )
    axes.set_xlabel('')

#
# ------------File system------------
#
def checkextension(filepath, extension:[str, tuple[str]] = None):
    """Check if another file has the same name with a different extension.

        Use string.endswith() function.

        Return a boolean.

        Parameters :
            'filepath' : 'Path' object from pathlib.

            'extension' : string or sequence of string.
                          'None' object returns always True.
                          (default None)
    """
    if extension is None:
        return True
    if isinstance(extension, str):
        return filepath.name.endswith(formatextension(extension))
    try:
        # noinspection PyTypeChecker
        for suffix in tuple(extension):
            if filepath.name.endswith(formatextension(suffix)):
                return True
    except TypeError:
        pass
    return False


def choosefile(startpath=None,
               filters=None):
    """Launch a simple dialog box.

        Folder and files are always displayed, filters can be applied.

        Return a 'Path' object of a file (pathlib).

        Parameters :
            'startpath' : 'Path' object from pathlib.
                          'None' object sets the working folder as starting folder.
                          (default None)

            'filters' : tuple ('desc', 'e') or a list of tuples.
                        'None' object means no filters   ('All Files','*')
                        (default None)
    """
    return DialogBoxSimple().filedialog(title='Choose a file',
                                        startpath=startpath,
                                        filters=filters
                                        )


def choosefolder(startpath=None, showfiles=False):
    """Launch a simple dialog box.

        Folder and files can be displayed, no filters.

        Return a 'Path' object of a folder (pathlib).

        Parameters :
            'startpath' : 'Path' object from pathlib.
                          'None' object sets the working folder as starting folder.
                          (default None)

            'showfiles' : boolean   (default False)
    """
    return DialogBoxSimple().folderdialog(title='Choose a folder',
                                          startpath=startpath,
                                          showfiles=showfiles
                                          )


def currentdir():
    """Return the current path (pathlib) of the caller."""
    return Path.cwd()


def formatextension(ext):
    """Add a dot if not present or change None in empty string.

        Return a new string with dot prefix.

        Parameters :
            'ext' : string   'txt' or '.txt'.
    """
    if ext:
        if '.' not in ext[0]:
            return f'.{ext}'
    return ''


def isempty(itempath):
    """Check if a file or a folder is empty.

        Return a boolean.

        Parameters :
            'itempath' : 'Path' object from pathlib.
    """
    return itempath.stat().st_size == 0


def pathfromparts(parts):
    """Build path from PurePath.parts.

        Returns a 'Path' object from pathlib.

        Parameters :
            'parts' : sequence containing the parts of the item (pathlib).
    """
    return Path('/'.join(parts))


def relativepath(absolutepath, rootpath):
    """Create relative path of the absolute path from a root path.

        Return a 'Path' object from pathlib.

        Parameters :
            'absolutepath' : 'Path' object from pathlib.

            'rootpath' : 'Path' object from pathlib.
"""
    return pathfromparts(absolutepath.parts[len(rootpath.parts):])


def renamefilesinfolder(folder, search, replace, extension=None):
    """Check and rename files in a specific folder according to the filter.
            (file extension excluded)

        Only use the first extension if array of extensions is provided.

        Return nothing.

        Parameters :
            'folder' : 'Path' object of the folder checked (pathlib).

            'search' : string to be replaced.

            'replace' : replacement string.

            'extension' : string object for file extension filtering.
                          (default None)
    """
    if extension:
        if not isinstance(extension, str):
            extension = extension[0]
    for file in _yieldpaths(folder, search, extension, False, True):
        if file.exists:
            new_path = Path(file.parent, file.name.replace(search, replace))
            file.rename(new_path)


def folderstructure(searchpath:[Path],
                    key_word=None,
                    extension=None,
                    recursive=True,
                    empty=True):
    """List files in a folder, recusively or not, according to the filters.
            (file extension excluded)

        Returns a nested dictionnary of the objects inside searchpath.
        Folders as dictionnaries and files as string representing relative path.

        Parameters :
            'searchpath' : 'Path' object of the starting folder (pathlib).
                           'None' object sets the working folder as starting folder.
                           (default None)

            'key_word' : string for file name filtering.
                         (default None)

            'extension' : string for file extension filtering.
                          (default None)

            'recursive' : string object for file extension filtering.
                          (default False)
    """
    if not isinstance(searchpath, Path):
        searchpath = Path(searchpath)

    if searchpath.is_file():
        searchpath = searchpath.parent

    tree_dict = {}
    base_len = len(searchpath.parts)
    for item in _yieldpaths(searchpath=searchpath,
                            key_word=key_word,
                            extension=extension,
                            recursive=recursive,
                            empty=empty):
        if item.is_file():
            temp = tree_dict
            parts = item.parts[base_len:]
            for part in parts[:-1]:
                if len(part) > 0:
                    temp = temp.setdefault(part, {})
            temp[parts[-1]] = '/'.join(parts)
    return tree_dict


def folder_dictview(searchpath:[str, Path], key_word=None, extension=None,
                recursive=True, empty=True, depth=None):
    if not isinstance(searchpath, Path):
        searchpath = Path(searchpath)
    if searchpath.is_file():
        searchpath = searchpath.parent

    def build_branch(d, key, stat):
        dico = {}
        if isinstance(d, str):
            dico[key] = stat
        else:
            dico[key] = d
        return dico

    def update_tree(tree, branch):
        for key, value in branch.items():
            if isinstance(tree, dict):
                if key in tree.keys():
                    update_tree(tree[key], value)
                    return
                tree.update(branch)

    tree_dict = {}
    size = len(searchpath.parts)
    for file in searchfiles(searchpath, key_word, extension, recursive, empty, depth):
        relative = ''
        # stat = file.stat()
        parts = file.parts[size:]
        for part in parts[::-1]:
            relative = build_branch(relative, part, file)
        update_tree(tree_dict, relative)

    return tree_dict


def searchfiles(searchpath=None, key_word=None, extension=None, recursive=False,
                empty=True, depth=None):
    """Search files in a folder, recusively or not, according to the filters.
            (file extension excluded)

        Yield an iteration sequence of 'Path' objects from pathlib.

        Parameters :
            'searchpath' : 'Path' object of the starting folder (pathlib).
                           'None' object sets the working folder as starting folder.
                           (default None)

            'key_word' : string for file name filtering.
                         (default None)

            'extension' : string for file extension filtering.
                          (default None)

            'recursive' : string object for file extension filtering.
                          (default False)

            'empty' : skip empty element in the iteration sequence.
                      (default True)

            'depth' : stop iteration over depth.
                      (default None)
    """
    if searchpath is not None:
        if not isinstance(searchpath, Path):
            searchpath = Path(searchpath)
        if searchpath.is_file():
            searchpath = searchpath.parent
        if depth is not None:
            level = depth
            for item in _yieldpaths(searchpath, None, None, False, empty):
                if item.is_file():
                    level = depth
                    if key_word is not None:
                        if key_word not in item.stem:
                            continue
                    if extension is not None:
                        if not item.suffix.endswith(extension):
                            continue
                    yield item
                if item.is_dir() and level > 0:
                    level = level - 1
                    for deeper in searchfiles(item, key_word, extension,
                                              False, empty, level):
                        yield deeper
                    level = level + 1
        else:
            for element in _yieldpaths(searchpath, key_word, extension,
                                       recursive, empty):
                yield element

# --------private functions--------
def _yieldpaths(searchpath, key_word=None,  extension=None,
                recursive=True, empty=True):
    """Generator using glob or rglob generators from pathlib."""
    if key_word:
        key_word = f'*{key_word}*'
    else:
        key_word = '*'
    if extension:
        key_word = f'{key_word}{formatextension(extension)}'
    func = 'glob'
    if recursive:
        func = f'r{func}'
    for item in getattr(searchpath, func)(key_word):
        if not empty:
            if isempty(item):
                continue
        yield item

#
# ------------Archive------------
#
def list_entries(archive, key_word=None, extension=None):
    """List the entries inside an archive according to the filters.

        Based ont the libarchive package.

        Yield an iteration sequence of memory entries.

        Parameters :
            'archive' : 'Path' object of the archive (pathlib).

            'key_word' : string for entry name filtering.
                         (default None)

            'extension' : string or tuple of strings for entry extension filtering.
                          (default None)
    """
    import py7zr
    with py7zr.SevenZipFile(archive, mode='r') as z:
        entries = z.readall()
        for sourcename, entry in entries.items():
            if key_word in sourcename:
                if sourcename.endswith(extension):
                    buffer = entry.read().decode('latin1')
                    yield sourcename, buffer
    return

#
# ------------Excel------------
#
def colnum_string(colnum, rel=0):
    """Format an column number as a letter in A1 reference style.

        Return a alphabetical string in relative or absolute convention.

        Parameters :
            'colnum' : int.

            'rel' : specify the presence or not of absolute prefix/suffix.
                        0 : no prefix/suffix  'AB'
                        1 : '$' prefix  '$AB'
                        2 : '$' suffix  'AB$'
                        3 : '$' prefix  '$AB$'
                    (default '0)
    """
    string = ''
    while colnum > 0:
        colnum, remainder = divmod(colnum - 1, 26)
        string = chr(65 + remainder) + string
    if rel == 1:
        string = '$' + string
    if rel == 2:
        string = string + '$'
    if rel == 3:
        string = '$' + string + '$'
    return string

def dt2excel(datetime):
    """Return a datetime object as an excel time value."""
    delta = datetime - dt.datetime(1899, 12, 30)
    return delta.days + delta.seconds/86400

def excel2dt(exceltime):
    """Return an excel time value as a datetime object."""
    days, fracday = divmod(exceltime, 1)
    delta = dt.timedelta(days=days, seconds=round(fracday, 5)*86400)
    return delta + dt.datetime(1899, 12, 30)

def excel_array(start, end, rel=0):
    """Format as A1 reference style from array of cells.

        Return a A1 reference style string.

        Parameters :
            'start' : sequence of int for the starting position.

            'end' : sequence of int for the ending position.

            'rel' : specify the presence or not of absolute prefix/suffix.
                        0 : no prefix/suffix  'AB'
                        1 : '$' prefix  '$AB'
                        2 : '$' suffix  'AB$'
                        3 : '$' prefix  '$AB$'
                    (default '0)
    """
    return f'{excel_cell(*start, rel)}:{excel_cell(*end, rel)}'


def excel_cell(row, column, rel=0):
    """Format as A1 reference style from cell coordinates.

        Return a A1 reference style string.

        Parameters :
            'row' : int

            'column' : int

            'rel' : specify the presence or not of absolute prefix/suffix.
                        0 : no prefix/suffix  'AB'
                        1 : '$' prefix  '$AB'
                        2 : '$' suffix  'AB$'
                        3 : '$' prefix  '$AB$'
                    (default '0)
    """
    return colnum_string(int(column+1), rel) + str(int(row+1))

#
# ------------Misc------------
#
def currentscriptfolder(obj):
    """Return the current Path (pathlib) of the calling script as object."""
    from os import path
    return Path(path.dirname(obj))


def moduleexists(module_name):
    """Check if the module name is already in use in the environment."""
    return module_name.lower() in sys.modules


def print_colored(string, color='RESET', end='\n'):
    """Add color with colorama package."""
    from colorama import Fore#, Back, Style,
    print(f'{getattr(Fore, color.upper())}{string}', end=end)


"""
            ------------Colors------------
            class AnsiFore(AnsiCodes):
                BLACK           = 30
                RED             = 31
                GREEN           = 32
                YELLOW          = 33
                BLUE            = 34
                MAGENTA         = 35
                CYAN            = 36
                WHITE           = 37
                RESET           = 39

            These are fairly well supported, but not part of the standard.
                LIGHTBLACK_EX   = 90
                LIGHTRED_EX     = 91
                LIGHTGREEN_EX   = 92
                LIGHTYELLOW_EX  = 93
                LIGHTBLUE_EX    = 94
                LIGHTMAGENTA_EX = 95
                LIGHTCYAN_EX    = 96
                LIGHTWHITE_EX   = 97
"""

def tab(level=0, file=False, offset=2, inc=2):
    """Generate file explorer style prefix string."""
    if level:
        tab_string = (offset + level*inc) * ' '
        if file:
            return tab_string + '|-- '
        return tab_string + '+-- '
    return ''


# # noinspection PyAttributeOutsideInit
# class Verbose:
#     """Singleton."""
#     def __new__(cls, state=True):
#         if not hasattr(cls, 'instance'):
#             cls.instance = super(Verbose, cls).__new__(cls)
#             cls.active = state
#         return cls.instance
#     def reset(self, active=True):
#         """."""
#         self.active = active
#     def state(self):
#         """."""
#         return self.active
#     def switchstate(self):
#         """."""
#         self.active = not self.active
#     def print(self, string, color='RESET', level=None, file=False, end='\n'):
#         """Check Verbose state and print if active."""
#         if self.active:
#             string = f'{tab(level, file)}{string}'
#             print(f'{getattr(Fore, color.upper())}{string}', end=end)


# ------------test------------
if __name__ == '__main__':

    test = folderstructure(currentdir(), recursive=True)

    def iter_nested_dict(dico):
        for key, value in dico.items():
            if isinstance(value, dict):
                list_items.append((key, 'folder'))
                iter_nested_dict(value)
            else:
                list_items.append((key, 'file'))

    list_items = []
    iter_nested_dict(test)
