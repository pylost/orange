# -*- coding: utf-8 -*-
""""""

from datetime import datetime as dt
from pathlib import Path
import numpy as np

from . import units as u
from .generic import MetrologyData, Surface

label_x = 'mirror_coord'
label_y = 'slopes'


class SlopesSurface(MetrologyData, Surface):
    """Imagine Optics slopes data"""

    def __init__(self):
        super().__init__()
        self.units = {'coords': u.mm, 'values': u.urad, 'angle': u.urad, 'length': u.mm, 'radius': u.m, 'pixel': u.mm}

        self.raw_signal = None

    # def __repr__(self):
    #     return 'repr'

    def __str__(self):
        return 'HASO Slopes 2D'

    # ----overriding----
    def readfile(self, path, source=None, **kwargs):
        # data = None
        if isinstance(path, str) and '\n' in path:
            data = path  # data is passed as argument
            if source is None:
                source = 'buffer'
        else:
            try:
                with open(path) as slpfile:
                    path = Path(path)
                    self.path = path
                    source = path.name
                    data = slpfile.readlines()
            except Exception as e:
                print('Error reading HASO file (No such file).', e)
                return None

        self.source = source
        if data is None:
            return None

        self._build_header(self.header, data)

        raw_slopes = self._search_key(self.header, 'raw_slopes')
        slopes = self._search_key(raw_slopes, 'slopes')
        step = self._search_key(slopes, 'step')
        pixel_res = self._search_key(step, 'x') * 1e-3

        x_slopes = np.multiply(self._search_key(slopes, 'x_slopes')['buffer'].T, -500.0)
        y_slopes = np.multiply(self._search_key(slopes, 'y_slopes')['buffer'].T, -500.0)

        shape = x_slopes.shape
        x = np.linspace(0, shape[0] * pixel_res, num=shape[0], endpoint=False, dtype=np.float64)
        y = np.linspace(0, shape[1] * pixel_res, num=shape[1], endpoint=False, dtype=np.float64)
        super(MetrologyData, self).__init__([x, y], x_slopes, self.units, self.source, **kwargs)
        self.center_coordinates()

        self.x_slopes = Surface([x, y], x_slopes, self.units, self.source, **kwargs)
        self.x_slopes.center_coordinates()
        self.y_slopes = Surface([x, y], y_slopes, self.units, self.source, **kwargs)
        self.y_slopes.center_coordinates()
        self.values = self.x_slopes.values
        return self

    def _build_header(self, dico, lines):
        total = len(lines)

        for _ in range(total):

            if not lines:
                return dico

            line = lines.pop(0).strip()

            if line.startswith('<?'): # skip xml version
                continue

            elif line.startswith('<item '):  # create items dico
                item = {}
                for param in line[:-1].split()[1:]:
                    key, val = param.split('=')
                    item[key] = 'true' in val.lower()
                dtype = str
                for _ in range(total - 1):
                    line = lines.pop(0).strip()
                    if line.startswith('</item'):
                        break
                    if line.startswith('</'):
                        return dico
                    name = line.split('<')[1].split('>')[0]
                    value = line.split('>')[1].split('<')[0]
                    if '<name>' in line:
                        dico[value] = item
                        continue
                    if '<type>' in line:
                        if 'boolean' in value:
                            dtype = bool
                        elif 'integer' in value:
                            dtype = int
                        elif 'real' in value:
                            dtype = float
                        continue
                    item[name] = dtype(value)

            elif line.startswith('</'): # return subdico
                return dico

            elif line.endswith('/>'): # skip line
                continue

            elif line.startswith('<'): # new subdico or value
                parent = line.split('<')[1].split('>')[0]

                if '<buffer>' in line: # arrays
                    arr = []
                    dtype = np.uint32
                    for _ in range(total - 1):
                        line = lines.pop(0).strip()
                        if '</buffer>' in line:
                            break
                        if '.' in line:
                            dtype = np.float64
                        arr.append(np.fromstring(line, sep='\t'))
                    dico[parent] = np.array(arr, dtype=dtype)
                    continue

                if '</' in line: # value
                    value = line.split('>')[1].split('<')[0]
                    dico[parent] = int(value) if 'crc' in parent else SlopesSurface._val_interp(value)
                    continue

                # parse subdico
                children = {}
                dico[parent] = self._build_header(children, lines)

    @staticmethod
    def _val_interp(string:str):
        """Return variable type from input string"""
        if len(string) < 1:  # empty string
            return ''
        if string.lower() in ('false', 'true'):
            return 'true' in string.lower()
        array = string.split()
        try:
            dtype = 'float64'
            if len(array) == 1 and np.all(np.char.isdigit(string.rsplit('-')[-1])):
                dtype = 'int32'
            array = np.array(array, dtype=dtype)
        except ValueError:
            return string
        except OverflowError:
            return int(string)
        if len(array) > 1:
            return array
        return array[0]

    @staticmethod
    def _search_key(parent:dict, entry:str):
        def inception(dico, search:str):
            for key, val in dico.items():
                if key == search:
                    return dico[key]
                if isinstance(val, dict):
                    found = inception(dico[key], search)
                    if found:
                        return found
            return False
        return inception(parent, entry)

    # ----properties----
    @property
    def datetime(self):
        datetime_str = self._search_key(self.header, 'acquisition_date')
        try:
            date, time = datetime_str.split('T')
            return dt.strptime(f'{date} {time}', '%Y-%m-%d %H:%M:%S.%f')
        except Exception:
            return dt(1970, 1, 1, 0, 0,0)
