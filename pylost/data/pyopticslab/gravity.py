# -*- coding: utf-8 -*-
""""""

import numpy as np
from .generic import Profile, Surface

materials = { # name: (rho, young's modulus)
    'custom': (0, 0),
    'ULE':(2205, 6.80e10),
    'Glidcop':(8900, 1.30e11),
    'SiO2':(2510, 7.00e10),
    'SiC_CVD':(3210, 4.66e11),
    'Si-Ansys':(2330, 1.66e11),
    'Si-100':(2330, 1.30e11),
    'Si-110':(2330, 1.68e11),
    'Si-111':(2330, 1.88e11),
    'Zerodur':(2530, 9.10e10),
    'Beryllium':(1850, 3.03e11),
    'Pyrex':(2230, 6.55e10),
    'Graphite':(2225, 3.30e10),
    }

G = 9.81

def model(data:[Surface, Profile], length, thickness, distance, material='Silicon', **custom_coefs):
    """Length, thickness, distance in mm."""
    sgrav = Profile(data.x, data.z, data.u)
    sgrav.units_to_SI()
    sgrav.values = calc_gravity_slopes(sgrav.x, length, thickness, distance, material, **custom_coefs)
    data.level_data()
    if data.is_heights:
        sgrav = sgrav.integral()
        sgrav.mean_removal()
    sgrav.change_units(data.units)
    if data.is_2D:
        sgrav = Surface(coords=data.coords, values=np.tile(sgrav.values, (data.shape[1], 1)).T,
                        units=data.units, source='gravity model')
    sgrav.copy_attributes(data)
    sgrav.source = 'Gravity analytical model'
    return sgrav

def calc_gravity_slopes(x, length, thickness, distance, material='Si-100', **custom_coefs):
    if material not in materials:
        material = 'custom'
    p, E = materials[material]
    if material == 'custom':
        p = custom_coefs.get('rho', 2330)
        E = custom_coefs.get('young', 1.30e11)
    L = length * 1e-3
    t = thickness * 1e-3
    d = distance* 1e-3
    x -= np.nanmean(x)
    S = ((2* G * p * L**3) / (E * t**2))
    C = 1.5
    return np.where(abs(x)<(d/2),
                    S * ((x/L)**3 + C*(0.5 - d/L)*x/L),
                    S * (abs((x/L)**3) - C*(x/L)**2 + (C/2)*abs(x)/L - (C/4)*(d/L)**2) * np.sign(x)
                    )
