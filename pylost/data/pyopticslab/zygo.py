# -*- coding: utf-8 -*-
""""""

import os
import datetime as dt
import numpy as np
from pathlib import Path

import h5py

from uuid import uuid4

from . import units as u
from .generic import MetrologyData, Surface


class MxData(MetrologyData, Surface):
    """MX data class."""

    def __init__(self):
        super().__init__()

        self.datetime = None

        self.intensity = None
        self.phase = None

        self.units = {'coords': u.m, 'values': u.m, 'angle': u.urad, 'length': u.mm, 'radius': u.km, 'pixel': u.mm}

        self.attrs_path = None
        self.attributes = None
        # self.measurement_surface = None
        # self.measurement_intensity = None
        # self.lateral_resolutions = None

    class Struct:
        def __init__(self, file_obj):
            self.metadata = self._metadata(file_obj)
            self.attributes = self._measurement_attributes(file_obj)
            self.surface, self.surface_attrs = self._surface(file_obj)
            # self.intensity, self.intensity_attrs = self._intensity(file_obj)

        @staticmethod
        def _metadata(datx_struct):
            source = datx_struct['MetaData']['Source'].astype(str)
            link = datx_struct['MetaData']['Link'].astype(str)
            dest = datx_struct['MetaData']['Destination'].astype(str)
            meta = {}
            for s, l, d in zip(source, link, dest):
                if meta.get(s, None) is None:
                    meta[s] = {}
                path = d.split('/')
                if len(path) > 1:
                    d = path[1:]
                meta[s][l] = d
            return meta

        @staticmethod
        def _find_data(parent, path_parts):
            child = parent[path_parts[0]]
            if len(path_parts) < 2:
                return child
            return MxData.Struct._find_data(child, path_parts[1:])

        @property
        def timestamp(self):
            stamp = self.attributes['Data']['Time Stamp']['time']
            return dt.datetime.utcfromtimestamp(stamp)

        @property
        def _measurement_guid(self):
            return self.metadata['Root']['Measurement']

        @property
        def measurement_attributes_path(self):
            return self.metadata[self._measurement_guid]['Attributes']

        def _measurement_attributes(self, datx_struct):
            attr_path = self.metadata[self._measurement_guid]['Attributes']
            attrs = self._find_data(datx_struct, attr_path).attrs
            return self._measurement_attributes_to_dict(attrs)

        @property
        def _surface_guid(self):
            return self.metadata[self._measurement_guid].get('Surface', None)

        def _surface(self, datx_struct):
            data_path = self.metadata[self._surface_guid]['Path']
            dataset = self._find_data(datx_struct, data_path)
            if dataset.compression == 'szip':
                raise Exception('File uses szip compression, resave with a earlier version of MX (>=5.25.0.1).')
            data = np.zeros(dataset.shape)
            dataset.read_direct(data)
            return data, self._data_attributes_to_dict(dataset.attrs)

        @property
        def _intensity_guid(self):
            return self.metadata[self._measurement_guid].get('Intensity', None)

        def _intensity(self, datx_struct):
            data_path = self.metadata[self._intensity_guid]['Path']
            dataset = self._find_data(datx_struct, data_path)
            data = np.zeros(dataset.shape)
            dataset.read_direct(data)
            return data, self._data_attributes_to_dict(dataset.attrs)

        @staticmethod
        def _measurement_attributes_to_dict(attrs):
            dico = {}
            bag_list = [p.strip().split('\n')[0].split('.')[0] for p in attrs['Property Bag List']]
            for k in bag_list:
                key_name = k.split(' ')[0]
                dico[key_name] = {}
            for key, val in attrs.items():
                attr_path = key.split('.')
                if attr_path[0] in bag_list:
                    key_name = attr_path[0].split(' ')[0]
                    attr_name = attr_path[-1]  # .replace(' ', '_')
                    parent = dico[key_name]
                    if isinstance(val[0], np.void):
                        parent[attr_name] = MxData.Struct._void_to_dict(val[0])
                    else:
                        parent[attr_name] = val[0]
            return dico

        @staticmethod
        def _data_attributes_to_dict(attrs):
            dico = {}
            for key, val in attrs.items():
                if isinstance(val[0], np.void):
                    dico[key] = MxData.Struct._void_to_dict(val[0])
                else:
                    dico[key] = val[0]
            return dico

        @staticmethod
        def _void_to_dict(void):
            data = void.tolist()
            dtype = np.array(void.dtype.descr, dtype=object)  # can raise warning if not 'dtype=object'
            return dict(zip(dtype[:, 0], data))

        @staticmethod
        def convert_unit(value, unit_string):
            conv = {'Meters': (1, u.m), 'CentiMeters': (10, u.mm), 'MilliMeters': (1, u.mm), 'MicroMeters': (1, u.um),
                    'NanoMeters': (1, u.nm), 'Angstroms': (1, u.A), 'Feet': (304.8, u.mm), 'Inches': (25.4, u.mm),
                    'Mils': (25.4, u.um), 'MicroInches': (25.4, u.nm), 'NanoInches': (25.4, u.pm),
                    'Pixels': (1, u.pix), 'Waves': (1, u.wav), 'Fringes': (1, u.fr), 'FringeRadians': (1, u.frad),}
            new_unit = conv[unit_string]
            return value * new_unit[0], new_unit[1]

        @property
        def lateral_res(self):
            unit = self.attributes['Data']['Lateral Resolution:Unit']
            value = self.attributes['Data']['Lateral Resolution:Value']
            value, unit = self.convert_unit(value, unit)
            return u.m(value, unit)

        @property
        def wavelength(self):
            unit = self.attributes['Data'].get('Wavelength:Unit', None)
            if unit is None:
                return 1
            value = self.attributes['Data'].get('Wavelength:Value', 1)
            value, unit = self.convert_unit(value, unit)
            return u.m(value, unit)

        @property
        def scale_factor(self):
            return self.surface_attrs.get('Interferometric Scale Factor', 1)

        @property
        def obliquity_factor(self):
            return self.surface_attrs['Obliquity Factor']

        # ----overriding----
    def readfile(self, path, source=None):
        self.path = path
        with h5py.File(path, 'r') as datx:
            data = MxData.Struct(datx)

        self.attrs_path = data.measurement_attributes_path
        self.attributes = data.attributes
        self.datetime = data.timestamp
        if isinstance(path, str):
            path = Path(path)
        path = path
        self.source = path.name
        self.header['lateral_res'], self.units['coords'] = self.Struct.convert_unit(
            data.lateral_res, data.attributes['Data']['Lateral Resolution:Unit'])
        self.header['wavelength_in'] = data.wavelength
        # compatibilty
        self.header['ac_org_x'] = 0
        self.header['ac_org_y'] = 0
        self.header['ac_width'] = int(data.attributes['Data']['Camera Width:Value'])
        self.header['ac_height'] = int(data.attributes['Data']['Camera Height:Value'])
        # self.header['ac_n_buckets'] = int(data.attributes['Data']['ac_n_buckets'])
        # self.header['ac_range'] = int(data.attributes['Data']['ac_range'])
        # self.header['ac_n_bytes'] = int(data.attributes['Data']['ac_n_bytes'])
        self.header['cn_width'] = int(data.surface_attrs['Coordinates']['Width'])
        self.header['cn_height'] = int(data.surface_attrs['Coordinates']['Height'])
        self.header['cn_org_x'] = int(data.surface_attrs['Coordinates']['ColumnStart'])
        self.header['cn_org_y'] = int(data.surface_attrs['Coordinates']['RowStart'])

        no_data = data.surface_attrs['No Data']
        rawdata = np.full((self.header['ac_width'], self.header['ac_height']), np.nan)
        idx = [self.header['cn_org_x'],
               self.header['cn_org_y'],
               self.header['cn_org_x'] + self.header['cn_width'],
               self.header['cn_org_y'] + self.header['cn_height']]
        rawdata[idx[0]:idx[2], idx[1]:idx[3]] = np.where(data.surface < no_data, data.surface, np.nan).T

        if data.surface_attrs['Unit'] == 'Fringes':
            Wav = data.wavelength
            Sca = data.scale_factor
            Obl = data.surface_attrs['Obliquity Factor']
            rawdata = rawdata * Wav * Sca * Obl
        else:
            rawdata, self.units['values'] = self.Struct.convert_unit(rawdata, data.surface_attrs['Unit'])
        rawdata -= np.nanmean(rawdata)
        rawdata = np.fliplr(rawdata)  # TODO check
        # by using fliplr, it is required to flip origin y as well to keep consistency:
        self.header['cn_org_y'] = int(self.header['ac_height'] - self.header['cn_org_y'] - self.header['cn_height'])

        shape = rawdata.shape
        x = np.linspace(0, shape[0] * self.header['lateral_res'],
                        num=shape[0], endpoint=False, dtype=np.float64)
        y = np.linspace(0, shape[1] * self.header['lateral_res'],
                        num=shape[1], endpoint=False, dtype=np.float64)
        super(MetrologyData, self).__init__([x, y], rawdata, self.units, self.source)
        return self

        # metaX = self._axis_converter(attributes['phase']['X Converter'][0])
        # metaY = self._axis_converter(attributes['phase']['Y Converter'][0])
        # self.lateral_resolutions = (metaX['Pixels'], metaY['Pixels'])
        # # 'Meters' in metaX
        # x = np.linspace(0, shape[0] * metaX['Pixels'], num=shape[0], endpoint=False, dtype=np.float64)
        # y = np.linspace(0, shape[1] * metaY['Pixels'], num=shape[1], endpoint=False, dtype=np.float64)

    @staticmethod
    def _axis_converter(conv):
        header = []
        data = []
        for val in conv:
            if not isinstance(val, np.ndarray):
                try:
                    header.append(val.decode())
                except Exception as e:
                    print(f'_axis_converter', e)
            else:
                data = val
        dico = {}
        for key, val in zip(header, data):
            dico[key] = val
        return dico

    # ----export----
    @staticmethod
    def writeDatxFile(path:str, data:np.ndarray, pixel_size, units:dict = None, comment:str = '', **custom_attrs):
        """
        data heights array and pixel_size in meters,
        """

        if units is not None:
            data = u.m(data, units['values'])
            pixel_size = u.m(pixel_size, units['coords'])

        data = np.flipud(data.T)
        frame_size = data.shape
        timestamp =int(dt.datetime.utcnow().timestamp()) + 7434058

        str_dtype = h5py.string_dtype(encoding='ascii')

        with h5py.File(path, 'w') as f:
            # entry = f.create_group(Path(path).name)
            measurement_uuid = '{' + str(uuid4()).upper() + '}'
            attributes_uuid = '{' + str(uuid4()).upper() + '}'
            surface_uuid = '{' + str(uuid4()).upper() + '}'
            source = ('Root', 'Root', measurement_uuid, measurement_uuid,
                      measurement_uuid, measurement_uuid, measurement_uuid,
                      surface_uuid, surface_uuid, surface_uuid)
            link = ('Attributes', 'Measurement', 'Attributes', 'Surface',
                    'Group Number', 'Element Type', 'Name',
                    'Path', 'Name', 'MpxDataType')
            destination = ('/Attributes/System', measurement_uuid, f'/Attributes/{attributes_uuid}', surface_uuid,
                           '1', 'Measurement', 'Measurement',
                           f'/Data/Surface/{surface_uuid}', 'Surface', 'DoubleMatrix2D')
            meta_dt = np.dtype([('Source', str_dtype), ('Link', str_dtype), ('Destination', str_dtype)])
            metadata = f.create_dataset('MetaData', shape=(len(destination),), dtype=meta_dt, chunks=True)
            for i in range(metadata.shape[0]):
                metadata[i] = source[i], link[i], destination[i]

            # basic attributes
            comment = comment if comment else 'exported from python.'
            datx_attributes = [
                ('Data Context.Data Attributes.Camera Height:Unit', np.array(['Pixels'], dtype=str_dtype)),
                ('Data Context.Data Attributes.Camera Height:Value', np.array([frame_size[1]])),
                ('Data Context.Data Attributes.Camera Width:Unit', np.array(['Pixels'], dtype=str_dtype)),
                ('Data Context.Data Attributes.Camera Width:Value', np.array([frame_size[0]])),
                ('Data Context.Data Attributes.Comment', np.array([comment], dtype=str_dtype)),
                ('Data Context.Data Attributes.Resolution:Unit', np.array(['Meters'], dtype=str_dtype)),
                ('Data Context.Data Attributes.Resolution:Value', np.array([pixel_size[0]])),
                ('Data Context.Data Attributes.Time Stamp', np.array([(timestamp, 0, 120, 0)],
                                                                     dtype=[('time', '<i8'), ('millitm', '<u2'),
                                                                            ('timezone', '<i2'),  ('dstflag', '<i2')])),
                ('Data Context.Lateral Resolution:Unit', np.array(['Meters'], dtype=str_dtype)),
                ('Data Context.Lateral Resolution:Value', np.array([pixel_size[0]])),
                ('Data Context.Window', np.array([(0, 0, frame_size[0], frame_size[1])],
                                                 dtype=[('RowStart', '<i4'), ('ColumnStart', '<i4'),
                                                        ('Width', '<i4'), ('Height', '<i4')])),
                ('Group Number', [1]),
                ('Property Bag List', np.array(['Data Context.Data Attributes'], dtype=str_dtype)),
                ('Surface Data Context.Lateral Resolution:Unit', np.array(['Meters'], dtype=str_dtype)),
                ('Surface Data Context.Lateral Resolution:Value', np.array([pixel_size[1]])),
                ('Surface Data Context.Window', np.array([(0, 0, frame_size[1], frame_size[0])],
                                                         dtype=[('RowStart', '<i4'), ('ColumnStart', '<i4'),
                                                                ('Width', '<i4'), ('Height', '<i4')])),
            ]

            data_attrs = [
                ('Coordinates', np.array([(0, 0, frame_size[1], frame_size[0])],
                                         dtype=[('RowStart', '<i4'), ('ColumnStart', '<i4'),
                                                ('Width', '<i4'), ('Height', '<i4')])),
                ('Group Number', np.array([1])),
                ('No Data', np.array([1.79769313e+308])),
                ('Unit', np.array(['Meters'], dtype=str_dtype)),
                ('X Converter', np.array([('LateralCat', 'Pixels', 'LinearCat', 'Meters',
                                           np.array([0, pixel_size[0], 0, 0]))],
                                         dtype={'names': ['Category', 'BaseUnit', 'SubCategory', 'SubUnit', 'Parameters'],
                                                'formats': [str_dtype, str_dtype, str_dtype, str_dtype,
                                                            h5py.vlen_dtype(np.dtype('f4'))],
                                                'offsets': [0, 8, 16, 24, 32],
                                                'itemsize': 48})
                 ),
                ('Y Converter', np.array([('LateralCat', 'Pixels', 'LinearCat', 'Meters',
                                           np.array([0, pixel_size[1], 0, 0]))],
                                         dtype={
                                             'names': ['Category', 'BaseUnit', 'SubCategory', 'SubUnit', 'Parameters'],
                                             'formats': [str_dtype, str_dtype, str_dtype, str_dtype,
                                                         h5py.vlen_dtype(np.dtype('f4'))],
                                             'offsets': [0, 8, 16, 24, 32],
                                             'itemsize': 48})
                 ),
                ('Z Converter', np.array([('HeightCat', 'Meters', np.array([0, 1, 1, 1]))],
                                         dtype={
                                             'names': ['Category', 'BaseUnit', 'Parameters'],
                                             'formats': [str_dtype, str_dtype, h5py.vlen_dtype(np.dtype('f4'))],
                                             'offsets': [0, 8, 16],
                                             'itemsize': 32})
                 ),
            ]
            attributes = f.create_group('Attributes')
            attributes.attrs.create('File Layout Version', [0])
            attributes.create_group('System')
            attrs = attributes.create_group(str(attributes_uuid))
            for name, val in datx_attributes:
                attrs.attrs.create(name, val)
            for name, val in MxData._parse_custom_attrs(custom_attrs):
                attrs.attrs.create(name, val)
            data_group = f.create_group('Data')
            surface = data_group.create_group('Surface')
            dataset = surface.create_dataset(surface_uuid, data=np.where(np.isnan(data), 1.79769313e+308, data))
            for name, val in data_attrs:
                dataset.attrs.create(name, val)

    @staticmethod
    def _parse_custom_attrs(custom_attrs:dict):
        """
        additional attributes as dict {'section_name': {'key_name':value or dict}}
        example:
            custom_attrs = {
                'Stitching': {'Instrument':'AT+',
                              'Final Length':{'Value':283.512, 'Unit':'mm'},
                              'Overlap':{'Value':80.35, 'Unit':'pct'},
                              'Constant Step':{'Value':3.929, 'Unit':'mm'},
                              'Current Position':{'Value':-125.465, 'Unit':'mm'}},
                'Temperatures': {'Probe1':20.5134, 'Probe2':21.6645, 'Probe3':20.4665, 'Unit':'celsius'},
                'AC_measurement': {'Rx':1.2354, 'Ry':0.3146, 'Unit':'urad'},
                'Motors': {'Axmo':{'Value':864521, 'Unit':'step'},
                           'rot_huber':{'Value':99.465, 'Unit':'urad'}},
            }
        """
        str_dtype = h5py.string_dtype(encoding='ascii')
        attrs = []
        for section, items in custom_attrs.items():
            attributes_path = str(section) + '.'
            for key, value in items.items():
                key_path = attributes_path + str(key)
                if isinstance(value, dict):
                    for name, val in value.items():
                        if isinstance(val, str):
                            attrs.append((key_path + ':' + str(name), np.array([val], dtype=str_dtype)))
                        else:
                            attrs.append((key_path + ':' + str(name), np.array([val])))
                elif isinstance(value, str):
                    attrs.append((key_path, np.array([value], dtype=str_dtype)))
                else:
                    attrs.append((key_path, np.array([value])))
        return attrs

    @staticmethod
    def add_custom_attributes(filepath, custom_attrs:dict):
        obj = MxData()
        obj = obj.read(filepath)
        attributes_path = obj.attrs_path
    # def write_x_position(self, position):
        with h5py.File(filepath, 'r+') as hf:
            attrs_group = hf
            for key in attributes_path:
                attrs_group = attrs_group[key]
            for section in custom_attrs.keys():
                custom_bag = np.append(attrs_group.attrs['Property Bag List'], str(section))
                attrs_group.attrs['Property Bag List'] = custom_bag
            for name, val in MxData._parse_custom_attrs(custom_attrs):
                attrs_group.attrs.create(name, val)


class MetroProData(MetrologyData, Surface):
    """MetroPro 9 data class."""

    def __init__(self):
        super().__init__()

        self.header_format = None
        self.header_size = None
        self.note = None

        self.datetime = None

        # self.intensity = None
        # self.phase = None

        self.units = {
            'coords': u.m, 'values': u.m, 'angle': u.urad,
            'length': u.mm, 'radius': u.km, 'pixel': u.mm,
        }

    def __str__(self):
        return 'Zygo surface map'

    # ----overriding----
    def readfile(self, path, source=None):
        self.path = path
        with open(path, 'rb') as zygo_file:

            self.datetime = dt.datetime.fromtimestamp(os.path.getmtime(path))

            if isinstance(path, str):
                path = Path(path)
            path = path
            self.source = path.name

            # Header
            magic_number = np.fromfile(zygo_file, dtype='>u4', count=1)[0]
            header_dict = header_from_txt()[magic_number]
            for key, value in header_dict.items():
                zygo_file.seek(value['pos'])
                dtype = value['dtype']
                if dtype is str:
                    size = int(value.get('size', '1'))
                    try:
                        self.header[key] = zygo_file.read(size).decode().strip('\x00')
                    except UnicodeDecodeError:
                        zygo_file.seek(value['pos'])
                        self.header[key] = zygo_file.read(size).decode('latin1').strip('\x00')
                else:
                    self.header[key] = np.fromfile(zygo_file, dtype=value['dtype'], count=1,)[0]
                    if 'f4' in value['dtype']:
                        self.header[key] = np.float64(self.header[key])

            zygo_file.seek(self.header['header_size'])
            rows = int(self.header['ac_height'])
            cols = int(self.header['ac_width'])
            size = rows * cols * self.header['ac_n_buckets']
            # self.intensity = np.resize(np.fromfile(zygo_file, dtype='>u2', count=size), (rows, cols))
            # self.intensity = np.where(self.intensity < self.header['ac_range'] + 1, self.intensity, np.nan).T
            zygo_file.seek(2*size, 1)

            rows = int(self.header['cn_width'])
            cols = int(self.header['cn_height'])
            size = rows * cols
            # self.phase = np.resize(np.fromfile(zygo_file, dtype='>i4', count=size), (cols, rows))
            # self.phase = np.where(self.phase < 2147483640, self.phase, np.nan).T
            rawdata = np.resize(np.fromfile(zygo_file, dtype='>i4', count=size), (cols, rows))
            rawdata = np.where(rawdata < 2147483640, rawdata, np.nan).T

            Wav = self.header['wavelength_in']
            Sca = self.header['intf_scale_factor']
            Obl = self.header['obliquity_factor']
            if self.header['phase_res'] == 0:
                Res = 4096
            elif self.header['phase_res'] == 1:
                Res = 32768
            elif self.header['phase_res'] == 2:
                Res = 131072
            # rawdata = self.phase * Wav * Sca * Obl / Res
            rawdata = rawdata * Wav * Sca * Obl / Res

            # full camera array
            # shape = (self.header['ac_width'], self.header['ac_height'])
            shape = (self.header['camera_width'], self.header['camera_height'])
            if shape != (rows, cols) and np.sum(shape) != 0:
                idx = [self.header['cn_org_x'],
                       self.header['cn_org_y'],
                       self.header['cn_org_x'] + self.header['cn_width'],
                       self.header['cn_org_y'] + self.header['cn_height']]
                try:
                    full = np.full(shape, np.nan)
                    full[idx[0]:idx[2], idx[1]:idx[3]] = rawdata
                    self.slice = idx
                    # self.offset = [idx[2] - idx[0], -idx[1]]  # TODO: check
                    rawdata = full - np.nanmean(rawdata)
                except Exception:
                    rawdata -= np.nanmean(rawdata)
            else:
                rawdata -= np.nanmean(rawdata)
            rawdata = np.fliplr(rawdata)
            shape = rawdata.shape
            x = np.linspace(0, shape[0] * self.header['lateral_res'],
                            num=shape[0], endpoint=False, dtype=np.float64)
            y = np.linspace(0, shape[1] * self.header['lateral_res'],
                            num=shape[1], endpoint=False, dtype=np.float64)
            super(MetrologyData, self).__init__([x, y], rawdata, self.units, self.source)
        return self

    def writefile(self, path):
        import struct
        with open(path, 'wb') as zygo_file:
            header_dict = header_from_txt()[2283471729]  # last format version
            zygo_file.write(bytearray(4096))

            self.header['lateral_res'] = self._pixel_res[0]  # already in meter
            self.header['pixel_width'] = self._pixel_res[0]  # already in meter
            self.header['pixel_height'] = self._pixel_res[1]  # already in meter

            # if self.intensity is None:
            if True:
                self.header['ac_org_x'] = 0
                self.header['ac_org_y'] = 0
                # self.header['ac_width'] = 0
                # self.header['ac_height'] = 0
                self.header['ac_n_buckets'] = 0
                self.header['ac_range'] = 0
                self.header['ac_n_bytes'] = 0

            # if self.phase is None:
            if True:
                self.header['cn_org_x'] = 0
                self.header['cn_org_y'] = 0
                # self.header['cn_width'] = 0
                # self.header['cn_height'] = 0
                # self.header['cn_n_bytes'] = 0
                # self.header['phase_res'] = 0

            for key, value in header_dict.items():
                # print(key, value)
                zygo_file.seek(value['pos'])
                if key not in self.header:
                    if 'magic_number' in key:
                        val = struct.pack('>i', -2011495567)
                    elif 'header_format' in key:
                        val = struct.pack('>h', 3)
                    elif 'header_size' in key:
                        val = struct.pack('>i', 4096)
                    else:
                        continue
                else:
                    if isinstance(self.header[key], str):
                        val = bytearray(self.header[key], 'utf-8')
                    elif value['fmt'] == 'c':
                        val = bytearray(str(self.header[key]), 'utf-8')
                    else:
                        val = struct.pack(value['fmt'], self.header[key])
                zygo_file.write(val)
            zygo_file.seek(1490)
            zygo_file.write(bytearray(2606))

            # if self.intensity is not None:
            #     intensity = np.where(np.isnan(self.intensity), 65535, self.intensity).T
            #     intensity = intensity.astype('>u2').ravel().tobytes()
            #     zygo_file.seek(self.header['header_size'])
            #     zygo_file.write(intensity)
            #
            # if self.phase is not None:
            #     phase = np.where(np.isnan(self.phase), 2147483641, self.phase).T
            #     phase = phase.astype('>i4').ravel().tobytes()
            #     zygo_file.write(phase)
            self.change_values_unit(u.m)
            phase = self.values
            W = np.float64(self.header['wavelength_in'])  # in meter
            S = self.header['intf_scale_factor']
            obliquity = self.header['obliquity_factor']
            R = [4096, 32768, 131072]
            phase_res = self.header['phase_res']
            phase = phase * R[phase_res] / (W * S * obliquity)
            phase = np.where(np.isnan(phase), 2147483641, phase).T
            phase = phase.astype('>i4').ravel().tobytes()
            zygo_file.write(phase)
        return self

    # ----export----
    @staticmethod
    def writeMetroprofile(path:str, data:np.ndarray, pixel_size, units:dict = None, frame_size=None, phase_res=1):
        """
        data heights array and pixel_size in meters,
        frame_size as tuple (width, height), data.shape by default
        phase_res: present in the Metropro file format v3, default 1 -> 32768
        """
        new_data = MetroProData()

        if units is not None:
            data = u.m(data, units['values'])
            pixel_size = u.m(pixel_size, units['coords'])

        new_data.header['header_size'] = 4096
        # new_data.header['swinfo.date'] = dt.datetime.now().strftime('%a %b %d %H:%M:%S %Y')
        new_data.header['time_stamp'] = np.int32(dt.datetime.now().timestamp())
        new_data.header['comment'] = 'file created from imported data'
        new_data.header['source'] = 1
        new_data.header['lateral_res'] = pixel_size[0]
        new_data.header['pixel_width'] = pixel_size[0]
        new_data.header['pixel_height'] = pixel_size[1]
        new_data._pixel_res = pixel_size

        data = np.fliplr(data)

        new_data.detector_size = data.shape
        width, height = new_data.detector_size
        if width > 32767 or height > 32767:
            raise OverflowError('size must be < 32768 pixels')
        new_data.header['cn_width'] = width
        new_data.header['cn_height'] = height
        if frame_size is not None:
            if width > frame_size[0] or height > frame_size[1]:
                print('Frame size not consistent with the data size, input ignored!')
            else:
                width, height = frame_size
                width = np.int32(width)
                height = np.int32(height)
        new_data.header['ac_width'] = width
        new_data.header['ac_height'] = height
        new_data.header['cn_n_bytes'] = width * height * 4  # BUG: pylost gives int16 --> overflow can happens
        new_data.header['camera_width'] = width
        new_data.header['camera_height'] = height

        new_data.header['wavelength_in'] = np.float64(6.327999813038332e-07)  # must be in meter
        new_data.header['intf_scale_factor'] = 0.5
        new_data.header['obliquity_factor'] = 1.0
        new_data.header['phase_res'] = phase_res

        new_data.header_dict_to_class()
        x = np.linspace(0, new_data.detector_size[0] * new_data.header['pixel_width'],
                        num=new_data.detector_size[0], endpoint=False, dtype=np.float64)
        y = np.linspace(0, new_data.detector_size[1] * new_data.header['pixel_height'],
                        num=new_data.detector_size[1], endpoint=False, dtype=np.float64)
        new_data.coords = [x, y]
        new_data.values = data
        new_data.writefile(path)
        print(f'  data saved in MetroPro format ({path}).')
        return new_data

    # ----properties----
    # @property
    # def datetime(self):
    #     return dt.datetime.strptime(self.header['swinfo.date'], '%a %b %d %H:%M:%S %Y')
    # @property
    # def shape(self):
    #     if self.values is None:
    #         return self.detector_size
    #     return self.values.shape
    @property
    def title(self):
        return self.header['comment']

    @property
    def mode(self):
        return 'PSI'

    @property
    def objective(self):
        return self.header['obj_name']

    @property
    def magnification(self):
        return self.header.get('gpi_enc_zoom_mag', 'no encoded zoom')
        # mag = self.header['Magnification']
        # return f'{mag:.1f} X'

    @property
    def fov(self):
        return self.header['obj_name']
        # default = '1 X'
        # return self.header.get('FOVLabel', default)

    @property
    def stage_x(self):
        return self.header['coords.x_pos']

    @property
    def stage_y(self):
        return self.header['coords.y_pos']

    @property
    def average(self):
        return self.header['phase_avg_cnt']

    @property
    def wavelength(self):
        return self.header['wavelength_in']  # m

    # @property
    # def histogram(self):
    #     mask = self.masking()
    #     return np.histogram(self.values[mask], bins=512, density=False)
    #     # plt.plot(histo[1][:-1], histo[0])


def header_from_txt():
    path_header = str(Path(Path(__file__).parent, 'metropro_headers.txt'))
    with open(path_header) as header_file:
        dico = {}
        fmt = None
        for li, line in enumerate(header_file.readlines()):
            line = line.strip()
            li = line.split()
            if 'magic_number' in line:
                fmt = int(li[4], 16)
                dico[fmt] = {}
            if fmt is None or len(li) < 5:
                continue
            try:
                size = int(li[2])
                if 'I' in li[0]:
                    dtype = 'i' + li[2]
                    if size == 1:
                        struct = 'b'
                    elif size == 2:
                        struct = 'h'
                    elif size == 4:
                        struct = 'i'
                elif 'F' in li[0]:
                    dtype = 'f' + li[2]
                    if size == 2:
                        struct = 'e'
                    elif size == 4:
                        struct = 'f'
                    elif size == 8:
                        struct = 'd'
                elif 'S' in li[0]:
                    dtype = str
                    struct = li[2] + 's'
                elif 'C' in li[0]:
                    dtype = 'B'
                    struct = 'B'
                else:
                    continue
                if 'B' in li[0]:
                    dtype = '>' + dtype
                    struct = '>' + struct
                elif 'L' in li[0]:
                    dtype = '<' + dtype
                    struct = '<' + struct
                dico[fmt][li[3]] = {
                    'dtype': dtype,
                    'fmt': struct,
                    'pos': int(li[1].split('-')[0]) - 1,
                    'size': li[2],
                    'default': li[4],
                }
            except ValueError:
                pass
        return dico
