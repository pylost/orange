# -*- coding: utf-8 -*-
""""""

import warnings
from copy import deepcopy
from time import perf_counter_ns
from datetime import datetime as dt
import numpy as np
from numpy.polynomial.polynomial import polyfit, polyval
from scipy.interpolate import interpn
from scipy.ndimage import rotate
from scipy.fft import fftshift, fft2, ifftshift, ifft2#, fftfreq
from scipy.linalg import lstsq

from matplotlib import cm
from matplotlib.colors import LinearSegmentedColormap as lsc

from . import units as u

from functools import wraps

def commenting(method):
    @wraps(method)
    def wrapper(self, *args, **kwargs):
        # start_time = time.time()
        result, comment = method(self, *args, **kwargs)
        self.add_comment_entry(comment)
        # end_time = time.time()
        # print(f"{method.__name__} => {(end_time-start_time)*1000:.3f} ms")
        return result
    return wrapper

# from numba import jit
# @jit(nopython=True, nogil=True, cache=True)
def lstsq_jit(a, b):
    return lstsq(a, b)#, rcond=None)


# noinspection PyAttributeOutsideInit
class _ProtoData:
    def __init__(self, coords:[list, np.array], values:np.array, units:dict, source=None, **kwargs):

        if isinstance(coords, list):
            for i, c in enumerate(coords):
                coords[i] = np.float64(c)
        self.coords = coords.copy()
        self.values = np.array(values, dtype='float64')
        self.units = deepcopy(units)
        self.source = source

        self.is_oversampled = False
        self.mean_step = 0.0

        self.comment = ''

        self.mask = None

        self.initial = None
        if kwargs.get('include_initial', False):
            self.initial = self.duplicate(update_roc=False)

        self.ellipse = None
        self.p = None
        self.q = None
        self.theta = None
        self.fit = None
        self.terms = None
        self.ellipse_errors = False

        self.fit_terms = None
        self.fit_coefs = None

        self.piston = None
        self.tilt_x = None
        self.tilt_y = None
        self.rot_xy = None
        self.majcyl = None
        self.mincyl = None
        self.rc_x = None
        self.rc_y = None
        self._roc_x = None
        self._roc_y = None

        self._centre_offset = None

        self.is_fft = False
        self.fft_cpx = None

    def __len__(self):
        return len(self.values)

    # ----properties----
    @property
    def gravity(self):
        return self._gravity

    @gravity.setter
    def gravity(self, gravity_on):
        self._gravity = gravity_on

    @property
    def reference(self):
        return self._reference

    @reference.setter
    def reference(self, reference_on):
        self._reference = reference_on

    @property
    def kind(self):
        unit_str = str(self.values_unit)
        if 'rad' in unit_str.lower():
            return 'slopes'
        if 'm-1' in unit_str.lower():
            return 'curvature'
        return 'heights'

    @property
    def is_heights(self):
        return self.kind == 'heights'

    @property
    def is_slopes(self):
        return self.kind == 'slopes'

    @property
    def is_1D(self):
        if self.values.ndim == 1:
            return True
        return False

    @property
    def is_2D(self):
        if self.values.ndim == 2:
            return True
        return False

    @property
    def has_slopes(self):
        return False

    @property
    def x(self):
        if self.is_1D:
            return self.coords
        return self.coords[0]

    @property
    def x_unit(self):
        return self.coords_unit

    @property
    def y(self):
        if self.is_1D:
            return None
        return self.coords[1]

    @property
    def y_unit(self):
        return self.coords_unit

    @property
    def z(self):
        return self.values

    @property
    def z_unit(self):
        return self.values_unit

    @property
    def shape(self):
        if self.values is None:
            return None
        return self.values.shape

    @property
    def u(self):
        return self.units

    @property
    def coords_unit(self):
        return self.units['coords']

    @property
    def default_coords_unit(self):
        if self.initial is not None:
            return self.initial.coords_unit
        return self.coords_unit

    @property
    def values_unit(self):
        return self.units['values']

    @property
    def radius_unit(self):
        return self.units['radius']

    @property
    def default_values_unit(self):
        if self.initial is not None:
            return self.initial.values_unit
        return self.values_unit

    @property
    def hasnan(self):
        return np.any(~self.valid_mask)

    @property
    def valid_mask(self):
        return np.isfinite(self.values)

    @property
    def radius(self):
        if self.is_2D and self.is_slopes:
            x_slopes, y_slopes = getattr(self, 'x_slopes', None), getattr(self, 'y_slopes', None)
            if x_slopes and y_slopes:
                self._roc_x = x_slopes.radius
                self._roc_y = None if y_slopes.radius_sag is None \
                    else self.radius_unit(y_slopes.radius_sag, y_slopes.radius_unit)

        if self._roc_x is not None:
            return self._roc_x

        if str(self.coords_unit.SI_unit) not in ('m',):
            return None

        if str(self.values_unit.SI_unit) not in ('m', 'rad'):
            return None

        factor_radius = self.coords_unit(1, self.coords_unit.SI_unit)

        if self.is_2D:
            x, y = np.meshgrid(self.x, self.y, indexing='ij')
            if self.is_heights:
                A, B, C, D, E, F = Surface._fit2D(x, y, self.values, fit='conic', verbose=False)
                theta = np.arctan(B / (A - C)) / 2
                Arot = (A * np.cos(theta) * np.cos(theta)
                        + B * np.cos(theta) * np.sin(theta)
                        + C * np.sin(theta) * np.sin(theta))
                roc = self.radius_unit(1 / (2 * Arot) * factor_radius, u.m)
                if np.isfinite(roc):
                    self._roc_x, self.units['radius'] = self.radius_unit.auto(roc)
                else:
                    self._roc_x = roc
                theta += np.pi / 2
                Arot = (A * np.cos(theta) * np.cos(theta)
                        + B * np.cos(theta) * np.sin(theta)
                        + C * np.sin(theta) * np.sin(theta))
                self._roc_y = self.radius_unit(1 / (2 * Arot) * factor_radius, u.m)
                return self._roc_x
            else:
                A, B, C = Surface._fit2D(x, y, self.values, fit='plane', verbose=False)
                roc = self.radius_unit(1 / A * factor_radius, u.m)
                if np.isfinite(roc):
                    self._roc_x, self.units['radius'] = self.radius_unit.auto(roc)
                else:
                    self._roc_x = roc
                self._roc_y = self.radius_unit(1 / B * factor_radius, u.m)
                return self._roc_x

        # profile
        order = 1 if self.is_slopes else 2
        coords = self.coords[self.valid_mask]
        values = self.values[self.valid_mask]
        if len(coords) > 0 and len(values) > 0:
            pol = polyfit(coords, values, deg=order)
            roc = self.radius_unit(1 / (order * pol[-1]) * factor_radius, u.m)
            if np.isfinite(roc):
                self._roc_x, self.units['radius'] = self.radius_unit.auto(roc)
            else:
                self._roc_x = roc
        else:
            self._roc_x = np.nan
        return self._roc_x

    @property
    def radius_sag(self):
        if self.is_1D:
            return None

        x_slopes, y_slopes = getattr(self, 'x_slopes', False), getattr(self, 'y_slopes', False)
        if x_slopes and y_slopes:
            self._roc_x = x_slopes.radius
            self._roc_y = None if y_slopes.radius_sag is None \
                else self.radius_unit(y_slopes.radius_sag, y_slopes.radius_unit)

        if self._roc_y is not None:
            return self._roc_y

        if str(self.coords_unit.SI_unit) not in ('m',):
            return None

        if str(self.values_unit.SI_unit) not in('m', 'rad'):
            return None

        factor_radius = self.coords_unit(1, self.coords_unit.SI_unit)
        order = 1 if self.is_slopes else 2
        values = np.nanmean(self.values, axis=0)
        mask = np.isfinite(values)
        coords = self.coords[1][mask]
        values = values[mask]
        pol = polyfit(coords, values, deg=order)
        self._roc_y = self.radius_unit(1 / (order * pol[-1]) * factor_radius, u.m)
        return self._roc_y

    # ----common functions----
    # noinspection PyUnresolvedReferences
    def add_comment_entry(self, entry:str):
        if not self.comment:
            self.comment = entry
            return
        self.comment = self.comment + ' - ' + entry

    def apply(self):
        if self.initial is not None:
            self.initial.coords = self.coords.copy()
            self.initial.values = self.values.copy()
            self.initial.units = deepcopy(self.units)
        return self

    def duplicate(self, update_roc=True):
        cls = Surface if self.is_2D else Profile
        new = cls(self.coords, self.values, deepcopy(self.units), self.source)
        skip = ['coords', 'values', 'units', '_roc_x', '_roc_y']
        class_copy = ['x_slopes', 'y_slopes']
        for attr_name, value in self.__dict__.items():
            if attr_name in skip:
                continue
            if attr_name in class_copy and value is not None:
                value = value.duplicate(update_roc)
            setattr(new, attr_name, value)
        new._roc_x, new._roc_y = (None, None) if update_roc else (self.radius, self.radius_sag)
        return new

    def duplicate_units(self):
        return deepcopy(self.units)

    def copy_attributes(self, obj, attrs=None, update_roc=True, source=None):
        if attrs is None:
            attrs = ('is_oversampled', 'mean_step', '_gravity', '_reference')
        for attr_name in attrs:
            try:
                setattr(self, attr_name, getattr(obj, attr_name))
            except AttributeError:
                """"""
        if source is not None:
            self.source = source
        self._roc_x = None if update_roc else obj.radius
        self._roc_y = None if update_roc else obj.radius_sag

    def reload(self):
        if self.initial is not None:
            if self.is_1D:
                self.coords = np.copy(self.initial.coords)
            else:
                for i, coords in enumerate(self.initial.coords):
                    self.coords[i] = np.copy(coords)
            self.values = np.copy(self.initial.values)
            self.units = deepcopy(self.initial.units)

    def automasking(self, threshold=None, apply=False):
        if threshold is None:
            threshold = 6.0 * self.rms
        self.mask = np.fabs(self.values - self.median) < threshold
        newval = np.where(self.mask, self.values, np.nan)
        if np.sum(~np.isnan(newval)) == 0:
            return self.values
        if apply:
            self.values = newval
        return newval

    @staticmethod
    def _central_value(data):
        values = np.atleast_2d(data)
        coords = (np.linspace(0, values.shape[0], num=values.shape[0], endpoint=False, dtype=np.float64),
                  np.linspace(0, values.shape[1], num=values.shape[1], endpoint=False, dtype=np.float64))
        central = np.nanmean(coords[0]), np.nanmean(coords[1])
        return float(interpn(coords, values, central, method='linear'))

    def level_data(self, **kwargs):
        """Level the central value (along the X-axis) to zero and return the new values.
        Perform a small interpolation if case of even number of points.
        """
        self.values -= _ProtoData._central_value(self.values)
        if kwargs.get('auto_units', False):
            self.auto_units()
        return self.values

    # ----units functions----
    def auto_units(self, limit=True):
        self.auto_coords_unit(limit)
        self.auto_values_unit(limit)
        self.auto_radius_unit()

    def auto_coords_unit(self, limit=True):
        if str(self.coords_unit) in ('unitless', 'pix'):
            return
        pv = np.nanmax(self.coords) - np.nanmin(self.coords)
        _, new_unit = self.coords_unit.auto(pv / 2)
        # factor = new_unit.SI_unit(1, new_unit)
        if limit and str(new_unit) == 'm':
            new_unit = u.mm
        self.change_coords_unit(new_unit)

    def auto_values_unit(self, limit=True):
        if str(self.values_unit) in ('unitless', 'pix'):
            return
        _, new_unit = self.values_unit.auto(self.rms)
        factor = new_unit.SI_unit(1, new_unit)
        if limit and self.is_slopes and factor < 1e6:
            new_unit = u.urad
        if limit and self.is_heights and factor < 1e9:
            new_unit = u.nm
        self.change_values_unit(new_unit)

    def auto_radius_unit(self):
        if str(self.radius_unit) in ('unitless', 'pix'):
            return
        _, new_unit = self.radius_unit.auto(self.radius)
        self.change_radius_unit(new_unit)

    def reset_units(self):
        self.reset_coords_unit()
        self.reset_values_unit()

    def reset_coords_unit(self):
        if self.initial is not None:
            self.change_coords_unit(self.initial.coords_unit)

    def reset_values_unit(self):
        if self.initial is not None:
            self.change_values_unit(self.initial.values_unit)

    def change_defaults_units(self, new_units):
        if self.initial is None:
            return
        if isinstance(new_units, dict):
            self.initial.units = new_units
            return
        self.initial.coords_unit = new_units[0]
        self.initial.values_unit = new_units[1]

    def change_units(self, new_units):
        if isinstance(new_units, dict):
            self.change_coords_unit(new_units['coords'])
            self.change_values_unit(new_units['values'])
            self.change_radius_unit(new_units['radius'])
            return
        for unit, func in zip(new_units, ('change_coords_unit', 'change_values_unit', 'change_radius_unit')):
            getattr(self, func)(unit)

    def change_coords_unit(self, new_unit):
        if str(new_unit) == str(self.coords_unit):
            return
        self.coords = new_unit(self.coords, self.coords_unit)
        self.units['coords'] = new_unit

    def change_values_unit(self, new_unit):
        if str(new_unit) == str(self.values_unit):
            return
        self.values = new_unit(self.values, self.values_unit)
        self.units['values'] = new_unit

    def change_radius_unit(self, new_unit):
        if str(new_unit) == str(self.radius_unit):
            return
        if self.radius is not None:
            self._roc_x = new_unit(self.radius, self.radius_unit)
        if self.radius_sag is not None:
            self._roc_y = new_unit(self.radius_sag, self.radius_unit)
        self.units['radius'] = new_unit

    def units_to_SI(self):
        self.coords_unit_to_SI()
        self.values_unit_to_SI()
        self.radius_unit_to_SI()

    def coords_unit_to_SI(self):
        self.change_coords_unit(self.coords_unit.SI_unit)

    def values_unit_to_SI(self):
        self.change_values_unit(self.values_unit.SI_unit)

    def radius_unit_to_SI(self):
        self.change_radius_unit(self.radius_unit.SI_unit)

    # ----ellipse----
    def set_ellipse(self, p, q, theta, reload=True):
        """p and q in meter, theta in mrad"""
        from .ellipse import Ellipse
        data = self
        if reload and self.initial is not None:
            data = self.initial
        self.ellipse = Ellipse(data, p, q, theta)
        return self.ellipse

    def ellipse_removal(self, p, q, theta, optimize=True, optimization=('q', 'theta'), offset=False, rotation=False):
        """p and q in meter, theta in mrad"""

        if not self.ellipse:
            self.set_ellipse(p, q, theta)

        if optimize:
            self.ellipse.optimize(optimization, offset=offset, rotation=rotation)

        self.coords = self.ellipse.residues.coords
        self.values = self.ellipse.residues.values
        self.units = deepcopy(self.ellipse.residues.units)

        if self.is_slopes:
            self.change_values_unit(u.urad)

        self.p = self.ellipse.p
        self.q = self.ellipse.q
        self.theta = self.ellipse.theta
        self.fit = 'ellipse'
        self.terms = 'Ellipse'

    # ----analysis----
    @commenting
    def gravity_correction(self, length, thickness, distance, material='Si-100', orientation='faceup', **custom_coefs):
        """Length, thickness, distance in mm."""
        from . import gravity
        p, E = gravity.materials[material]
        if material == 'custom':
            p = custom_coefs.get('rho', 2330)
            E = custom_coefs.get('young', 1.30e11)
        grav = gravity.model(self, length, thickness, distance, material, **custom_coefs)
        grav.source = 'Gravity model'
        if 'facedown' in orientation.lower():
            grav.values = -grav.values
        delta = grav.mean - self.mean
        self.values += grav.values - delta
        self.gravity = True
        comment = f'Gravity: {material} ({float(p):.1f}, {E:.2e}), L={float(length):.1f} , T={float(thickness):.1f} , d={float(distance):.1f}'
        return grav, comment

    def center_coordinates(self):
        if self.is_1D:
            self.coords = self.coords - np.nanmean(self.coords)
        else:
            for i, coords in enumerate(self.coords):
                self.coords[i] = coords - np.nanmean(coords)
        return self.coords, self.values

    def flip(self, axis='x'):
        ax = -1 + int('x' in axis.lower()) + 2 * int('y' in axis.lower())
        if ax >= 0:
            if self.is_1D:
                self.values = np.flip(self.values)
            else:
                ax = None if ax == 2 else ax
                self.values = np.flip(self.values, ax)
            if 'slope' in self.kind.lower():
                self.values = np.negative(self.values)
        return self.coords, self.values

    def min_removal(self):
        """Substract the minimum of the values."""
        self.values -= self.min
        return self.values

    def mean_removal(self, **kwargs):
        """Substract the average of the values."""
        self.values -= self.mean
        if kwargs.get('auto_units', False):
            self.auto_units()
        return self.values

    @staticmethod
    def _closest_idx(coordinate, coords, pixel_size):
        diff = (coordinate - coords[0]) / pixel_size
        virtual = round(np.abs(diff) + 1e-12 * np.sign(diff)) * np.sign(diff) # float->int rounding, x.5 give always upper integer
        return int(virtual)

    @property
    def central_pixel(self):
        if isinstance(self, Profile):
            return (len(self.coords) - 1) / 2, 0
        return (len(self.coords[0]) - 1) / 2, (len(self.coords[1]) - 1) / 2

    @property
    def max(self):
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='All-NaN slice encountered')
            return np.nanmax(self.values)

    @property
    def min(self):
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='All-NaN slice encountered')
            return np.nanmin(self.values)

    @property
    def mean(self):
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='Mean of empty slice')
            return np.nanmean(self.values)

    @property
    def pv(self):
        return self.max - self.min

    @property
    def rms(self):
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='Degrees of freedom <= 0 for slice.')
            return np.nanstd(self.values)

    @property
    def median(self):
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='All-NaN slice encountered')
            return np.nanmedian(self.values)


class Profile(_ProtoData):
    def __init__(self, coords:[list, tuple], values:[list, tuple, np.array], units:dict, source=None):
        super().__init__(coords, values, units, source)

    def __call__(self):
        return self.coords, self.values

    def __getitem__(self, item):
        return self.coords[item], self.values[item]

    # ----maths----
    def bin_data(self, bins, stat_method='nanmean', copy=False):
        if bins == 1:
            return self
        res = self.duplicate() if copy else self
        size = len(res.values)
        aug_size = bins * (size // bins + 1)
        new_size = aug_size // bins
        idx = (aug_size - size) // 2
        new_pixel_size = res.length / new_size
        res.coords = np.multiply(np.arange(new_size), new_pixel_size)
        res.center_coordinates()
        values = np.full((aug_size,), np.nan)
        values[idx:idx + len(res)] = res.values
        res.values = getattr(np, stat_method)(values.reshape((new_size, bins,)), -1)
        return res

    def get_bspline_coeffs(self, k=3, bc_type=None):
        from scipy.interpolate import make_interp_spline
        return make_interp_spline(self.coords[self.valid_mask], self.values[self.valid_mask], k=k, bc_type=bc_type)

    def derivative(self, method='cubicbspline', copy=True, create_initial=False):
        """methods: gradient, cubicbspline, """
        res = self.duplicate() if copy else self
        factor = res.z_unit.SI_unit(1, res.z_unit) / res.coords_unit.SI_unit(1, res.coords_unit)
        if method == 'gradient':
            res.coords = res.coords - 0.5 * res.pixel_size
            res.values = np.gradient(res.values, res.coords)
        elif method == 'cubicbspline':
            spline = res.get_bspline_coeffs(k=3)
            res.values = np.multiply(spline.derivative()(res.coords), factor)
        else:
            return None
        if self.is_heights:
            res.units['values'] = res.values_unit.si_angle
        elif self.is_slopes:
            res.units['values'] = res.values_unit.si_curvature
        res.auto_units()
        if create_initial:
            res.initial = res.duplicate()
        return res

    def integral(self, method='cubicbspline', copy=True, create_initial=False, interpolate=True):
        """ methods: trapezoidal, simpson, romb, cubicbspline, """
        res = self.duplicate() if copy else self
        res.level_data()
        factor = res.z_unit.SI_unit(1, res.z_unit) * res.coords_unit.SI_unit(1, res.coords_unit)

        n = 0
        old_coords = None
        if interpolate:
            # oversampling and add n points at each side
            n = 1
            old_coords = res.coords
            res.oversampling(factor=8, extra_points=True, method='cubicbspline', k=2, copy=False)

        if method == 'cubicbspline':
            k = 2
            spline = res.get_bspline_coeffs(k=k, bc_type=None)
            integr = [spline.integrate(spline.t[k + i - n], spline.t[k + i + 1 - n]) for i in range(len(res.values))]
        else:
            # TODO: some issues
            from scipy.integrate import simpson, romb
            k = 3
            spline = res.get_bspline_coeffs(k=k)
            interpolate = spline(spline.t)
            if method == 'trapezoidal':
                integr = [np.trapz(interpolate[k + i - 1:k + i + 1], spline.t[k + i - 1:k + i + 1])
                          for i in range(len(res.values))]
            elif method == 'romberg':
                step = abs(res.pixel_size)
                integr = [romb(interpolate[k + i - 1:k + i + 1], step) for i in range(len(res.values))]
            else:
                integr = [simpson(interpolate[k + i - 1:k + i + 1], x=spline.t[k + i - 1:k + i + 1])
                          for i in range(len(res.values))]
        res.values = np.cumsum(integr)

        if interpolate:
            # revert oversampling
            res.interpolation(old_coords, method='interp', copy=False)

        res.values = np.multiply(res.values, factor)
        if self.is_slopes:
            res.units['values'] = res.values_unit.si_length
        res.auto_units()
        if create_initial:
            res.initial = res.duplicate()
        return res

    # noinspection PyUnresolvedReferences
    def interpolation(self, new_coords, method='interp', k=2, copy=False):
        """method: cubicbspline, interp"""
        res = self.duplicate() if copy else self
        new_sampling = np.nanmean(np.diff(new_coords))

        if method == 'cubicbspline':
            # bin data by averaging if new sampling too far TODO: check binning for all ?
            bins = int(new_sampling // res.pixel_size)
            if bins > 1:
                res.bin_data(bins)

            spline = res.get_bspline_coeffs(k=k)
            res.values = spline(new_coords)
            res.coords = new_coords
        elif method == 'interp':
            try:
                from pylost.utils.psd import spline_interpolate
            except ImportError:
                from .psd import spline_interpolate
            # method = 'nearest' # 'cubic', 'slinear'
            new_val = np.interp(new_coords, res.coords, res.values)
            res.values = spline_interpolate(new_val)
            res.coords = new_coords
        else:
            return None
        return res

    def oversampling(self, factor=8, even=True, extra_points=False, method='cubicbspline', k=2, copy=False): # 'interp', 'cubicbspline'
        n = 1 if extra_points else 0
        sampling = self.pixel_size
        factor = factor + 1 if even and factor % 2 == 1 else factor
        new_sampling = np.divide(sampling, factor)
        num_samples = int(self.length // new_sampling + 2 * n * factor)
        new_coords = np.linspace(np.nanmin(self.coords) - n * sampling, np.nanmax(self.coords) + n * sampling,
                                 num=num_samples, endpoint=True, dtype=np.float64)
        self.interpolation(new_coords, method=method, k=k, copy=copy)

    @staticmethod
    def theoretical(x, shape, p=None, q=None, theta=None, roc=None):
        """kind: 'elliptical', 'parabolic', 'spherical'    (SI units)"""
        x -= np.nanmean(x)
        if 'spherical' not in shape.lower():
            from .ellipse import Ellipse
            if 'parabolic' in shape.lower():
                p = 1e5
            slp = Ellipse.from_parameters(x, 'slope', p, q, theta)
            hgt = Ellipse.from_parameters(x, 'height', p, q, theta)
        elif 'spherical' in shape.lower():
            slp = x / roc
            slp -= np.nanmean(slp)
            hgt = 2 * x ** 2 / roc
            hgt -= np.nanmin(hgt)
        else:
            return None
        return slp, hgt

    # ----analysis----
    @property
    def length(self):
        if self.is_oversampled:
            return (len(self.values) - 1) * self.pixel_size
        return len(self.values) * self.pixel_size

    @property
    def pixel_size(self):
        return np.nanmean(np.diff(self.coords))

    @property
    def meshgrid(self):
        # """compatibility with Surface methods"""
        return self.x, None

    def closest_idx(self, coordinate, axis=0):
        if axis > 0:
            return 0
        return self._closest_idx(coordinate, self.coords, self.pixel_size)

    def change_pixel_size(self, new_pixel_size, center_coordinates=True):
        if self.is_oversampled:
            factor = new_pixel_size / self.pixel_size
            self.mean_step *= factor
        shape_x = self.z.shape[0]
        self.coords = np.linspace(0, shape_x * new_pixel_size, num=shape_x, endpoint=False, dtype=np.float64)
        self._roc_x = None
        self._roc_y = None
        if center_coordinates:
            self.center_coordinates()

    def reverse(self):
        """slopes: Flip the X coordinates and negate the values."""
        self.coords = self.coords[::-1]
        self.values = self.values[::-1]
        if 'slope' in self.kind.lower():
            self.values = np.negative(self.values)
        return self.coords, self.values

    def offsetting(self, offset=0.0):
        # left = self.values[0] + np.diff(self.values[:3]).mean() * np.sign(self.coords[0])
        # right = self.values[-1] + np.diff(self.values[-3:]).mean() * np.sign(self.coords[-1])
        self.values = np.interp(self.coords + offset, self.coords, self.values,
                                # left=left, right=right,
                                )
        return self.values

    def subarray_pixel(self, xrangearray=None, **kwargs):
        """profile: Cut the data to the x index given by [Xmin, Xmax]."""
        if xrangearray is None:
            xrangearray = (0, self.values.shape[0])
        slc = slice(max(0, xrangearray[0]), xrangearray[1])
        coords = self.coords[slc]
        values = self.values[slc]
        if kwargs.get('apply', False):
            self.coords = coords
            self.values = values
        if kwargs.get('center_coordinates', True):
            self.center_coordinates()
            self._centre_offset = self.pixel_size * (xrangearray[1] - xrangearray[0]) / 2
        return coords, values

    def polynomial_removal(self, order=1):
        """Remove a polynomial fit to the values.
        Return the polynomial fit
        """
        if np.all(np.isnan(self.values)):
            return self.values
        mask = ~np.isnan(self.values)
        domain = np.linspace(0, 1, num=len(self.values))
        poly = polyfit(domain[mask], self.values[mask], order)
        self.values -= polyval(domain, poly)
        return polyval(domain, poly)

    @staticmethod
    def writeESRFfileformat(path, coords, values, units:dict, slp:bool = True, header=None):
        if slp:
            from .ltp import LTPdata
            profile = LTPdata()
            if len(values) % 2 == 0: # len is even
                # interpolation to get odd size centered
                method, aug = 'slinear', 4
                xi = np.linspace(min(coords), max(coords), num=len(coords) * aug + 1, endpoint=True, dtype=np.float64)
                val = interpn((coords,), values, xi, method=method, bounds_error=False)
                coords = xi
                values = val
        else:
            from .misc import HeightProfile
            profile = HeightProfile()
        profile.coords = coords
        profile.values = values
        profile.units = units
        profile.header = {'legacy_header':
            {
                'comment': 'exported from pylost' if header is None else header['comment'],
                'date_time': dt.strftime(dt.now(), '%d/%m/%Y @ %H:%M:%S') if header is None else header['date_time'],
                'reference': 'ref substracted' if header is None else header['reference'],
                'gravity': 'No gravity' if header is None else header['gravity'],
                'focal_length': 810.0 if header is None else header['focal_length'],
            }
        }
        return profile.writefile(str(path))

    def profile_to_surface(self, width=1, update_roc=True):
        # x = self.coords
        size = len(self.values)
        x = np.linspace(0, size * self.pixel_size, num=size, endpoint=False, dtype=np.float64)
        x -= np.nanmean(x)
        pixel_size = self.pixel_size
        size_y = max(1, int(np.round(width / u.mm(pixel_size, self.coords_unit))))
        # if size_y % 2 == 0:
        #     size_y += 1
        y = np.linspace(0, size_y * pixel_size, num=size_y, endpoint=False, dtype=np.float64)
        y -= np.nanmean(y)
        data = np.tile(self.values, (len(y), 1)).T
        surf = Surface([x, y], data, self.units, source=self.source)
        surf.copy_attributes(self, update_roc=update_roc)
        surf.from_profile = True
        return surf

    def plot(self, block=True, **kwargs):
        """Matplotlib 1D plot with some additional functions.

            Parameters :
                matplotlib.pyplot.plot arguments
                https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html
        """
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(1)  # , figsize=(10,5))
        # fig.set_tight_layout(True)
        ax.plot(*self(), linewidth=1, **kwargs)
        plt.grid()
        plt.show(block=block)


class Surface(_ProtoData):
    def __init__(self, coords:list, values:[list, tuple, np.array], units:dict, source=None, **kwargs):
        super().__init__(coords, values, units, source)
        self._pixel_res = np.asfarray([np.nanmean(np.diff(coords[0])), np.nanmean(np.diff(coords[1]))])
        try:
            self._pixel_res = u.m(self._pixel_res, units['coords'])
        except KeyError:
            self._pixel_res = (1, 1)

        self.from_profile = False

        self.cmap = None
        self.cmap_prms = None

        self.detector_size = values.shape

        self.x_slopes = None
        self.y_slopes = None

    def __call__(self):
        return self.values, self.coords[0], self.coords[1]

    def __getitem__(self, item):
        return self.values[item]

    # ----overriding units functions----
    def auto_coords_unit(self, limit=True):
        pv = (np.nanmax(self.x) - np.nanmin(self.x),
              np.nanmax(self.y) - np.nanmin(self.y))
        _, new_unit = self.x_unit.auto(max(pv))
        # factor = new_unit.SI_unit(1, new_unit)
        if limit and str(new_unit) == 'm':
            new_unit = u.mm
        self.change_coords_unit(new_unit)

    def change_coords_unit(self, new_unit):
        if str(new_unit) == str(self.coords_unit):
            return
        self.coords[0] = new_unit(self.coords[0], self.coords_unit)
        self.coords[1] = new_unit(self.coords[1], self.coords_unit)
        self.units['coords'] = new_unit
        if self.is_slopes and self.has_slopes:
            self.x_slopes.coords[0] = new_unit(self.x_slopes.coords[0], self.x_slopes.coords_unit)
            self.x_slopes.coords[1] = new_unit(self.x_slopes.coords[1], self.x_slopes.coords_unit)
            self.x_slopes.units['coords'] = new_unit
            self.y_slopes.coords[0] = new_unit(self.y_slopes.coords[0], self.y_slopes.coords_unit)
            self.y_slopes.coords[1] = new_unit(self.y_slopes.coords[1], self.y_slopes.coords_unit)
            self.y_slopes.units['coords'] = new_unit

    def change_values_unit(self, new_unit):
        if str(new_unit) == str(self.values_unit):
            return
        self.values = new_unit(self.values, self.values_unit)
        self.units['values'] = new_unit
        if self.is_slopes and self.has_slopes:
            self.x_slopes.values = new_unit(self.x_slopes.values, self.x_slopes.values_unit)
            self.x_slopes.units['values'] = new_unit
            self.y_slopes.values = new_unit(self.y_slopes.values, self.y_slopes.values_unit)
            self.y_slopes.units['values'] = new_unit

    # ----properties----
    @property
    def length(self)->[list, float]:
        lengths = [np.nanmax(self.coords[0]) - np.nanmin(self.coords[0]),
                   np.nanmax(self.coords[1]) - np.nanmin(self.coords[1])]
        lengths = [lengths[axis] + self.pixel_size[axis] for axis in (0, 1)]
        return lengths

    @property
    def pixel_size(self):
        try:
            return self.coords_unit(self._pixel_res, u.m)
        except KeyError:
            return [1, 1]

    @property
    def meshgrid(self):
        return np.meshgrid(self.x, self.y, indexing='ij')

    @property
    def valid_size(self):
        return np.sum(self.valid_mask)

    @property
    def invalid_pts(self):
        return np.sum(~self.valid_mask)

    @property
    def invalid_pct(self):
        return 100 * self.invalid_pts / self.values.size

    @property
    def has_slopes(self):
        return self.x_slopes is not None and self.y_slopes is not None

    # ----manipulation----
    def remove_invalid_lines(self):
        invalid_row = np.all(np.isnan(self.values), axis=0)
        invalid_col = np.all(np.isnan(self.values), axis=1)
        first, last = 0, len(invalid_col) - 1
        for c in range(len(invalid_col) // 2):
            first = c
            if not invalid_col[c]:
                break
        for c in range(len(invalid_col) // 2):
            last = c
            if not invalid_col[-c - 1]:
                break
        xrangearray = [first, last + len(invalid_col)]

        first, last = 0, len(invalid_row) - 1
        for c in range(len(invalid_row) // 2):
            first = c
            if not invalid_row[c]:
                break
        for c in range(len(invalid_row) // 2):
            last = c
            if not invalid_row[-c - 1]:
                break
        yrangearray = [first, last + len(invalid_row)]
        self.subarray_pixel(xrangearray, yrangearray)

    def bin_data(self, bins, stat_method='nanmean', copy=False):
        if bins[0] * bins[1] == 1:
            return self
        method = getattr(np, stat_method)
        res = self.duplicate() if copy else self
        length = res.length
        aug_shape, new_shape, idx = [0, 0], [0, 0], [0, 0]
        for axis in (0, 1):
            aug_shape[axis] = bins[axis] * (len(res.coords[axis]) // bins[axis] + 1)
            new_shape[axis] = aug_shape[axis] // bins[axis]
            idx[axis] = (aug_shape[axis] - res.shape[axis]) // 2
            res.coords[axis] = np.arange(new_shape[axis])
        binning_shape = (new_shape[0], bins[0], new_shape[1], bins[1])
        values = np.full(aug_shape, np.nan)
        values[idx[0]:idx[0] + res.shape[0], idx[1]:idx[1] + res.shape[1]] = res.values

        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='Mean of empty slice')
            res.values = method(method(values.reshape(binning_shape), -1), 1)

        new_pixel_size = np.divide(length, new_shape)
        res.change_pixel_size(new_pixel_size[0], new_pixel_size[0])
        return res

    def closest_idx(self, coordinate, axis=0):
        return self._closest_idx(coordinate, self.coords[axis], self.pixel_size[axis])

    def change_pixel_size(self, *new_pixel_size, center_coordinates=True):
        if self.is_oversampled:
            factor = np.divide(new_pixel_size, self.pixel_size)
            self.mean_step = np.divide(self.mean_step, factor)
        size_x, size_y = new_pixel_size
        shape_x, shape_y = self.values.shape
        self.coords[0] = np.linspace(0, shape_x * size_x, num=shape_x, endpoint=False, dtype=np.float64)
        self.coords[1] = np.linspace(0, shape_y * size_y, num=shape_y, endpoint=False, dtype=np.float64)
        self._pixel_res = u.m(new_pixel_size, self.coords_unit)
        self._roc_x = None
        self._roc_y = None
        if center_coordinates:
            self.center_coordinates()

    def rotate(self, angle=None):
        """Rotation angle in degrees clockwise."""
        if angle is None:
            return
        angle = angle % 360
        if np.fabs(angle) > 180:
            angle -= 360
        if 89.999 < np.fabs(angle) < 90.001:
            if angle > 0:
                self.values = np.flipud(self.values).T
            else:
                self.values = np.fliplr(self.values).T
        elif 179.999 < np.fabs(angle) < 180.001:
            self.values = np.flipud(np.fliplr(self.values))
        else:
            self.values = rotate(self.values, angle=np.negative(angle), # clockwise rotation
                                 reshape=True, cval=np.nan,
                                 axes=(1, 0), prefilter=False)
        size = self.values.shape
        self.coords[0] = np.linspace(0, self.pixel_size[0] * size[0], size[0], endpoint=False, dtype=np.float64)
        self.coords[1] = np.linspace(0, self.pixel_size[1] * size[1], size[1], endpoint=False, dtype=np.float64)
        self._pixel_res = u.m(self.pixel_size, self.coords_unit)
        self.center_coordinates()

    def rotation_auto(self):
        print('finding best rotation angle minimizing cylinder...')
        from scipy.optimize import fminbound
        data = self.duplicate()
        data.initial = data.duplicate()  # TODO: binning for faster calculation ?

        def _rotate(deg):
            data.reload()
            data.rotate(deg)
            data.cylinder_removal(reload=False, auto_units=False, verbose=False)
            _, res = data.derivative()
            print(deg, res.rms * 1e6)
            return res.rms * 1e6
            # print(deg, data.pv)
            # return data.pv

        toc = perf_counter_ns()
        popt = [np.nan,]
        for i in range(0, 1):
            # popt = data.cylinder_removal(reload=False, auto_units=False, verbose=False)
            popt = fminbound(_rotate, -1.0, 1.0, xtol=1e-4, full_output=True, maxfun=100, disp=0)
        tic = perf_counter_ns()
        print(f'best rotation angle based on conic fit: {popt[0]:.3f}deg ({(tic - toc) * 1e-6:.0f}ms)')
        self.rotate(popt[0])

    def dropnan(self):
        mask = ~np.all(np.isnan(self.values), axis=0)
        self.coords[1] = self.coords[1][mask]
        self.values = self.values[:, mask]
        mask = ~np.all(np.isnan(self.values), axis=1)
        self.coords[0] = self.coords[0][mask]
        self.values = self.values[mask, :]

    # ----analysis----
    def subarray_pixel(self, xrangearray=None, yrangearray=None, apply=True, center_coordinates=True):
        if xrangearray is None:
            xrangearray = (0, self.values.shape[0])
        if yrangearray is None:
            yrangearray = (0, self.values.shape[1])
        slc_x = slice(max(0, xrangearray[0]), xrangearray[1])
        slc_y = slice(max(0, yrangearray[0]), yrangearray[1])
        coords_x = self.coords[0][slc_x]
        coords_y = self.coords[1][slc_y]
        values = self.values[slc_x, slc_y]
        if center_coordinates:
            with warnings.catch_warnings():
                warnings.filterwarnings(action='ignore', message='Mean of empty slice')
                coords_x -= np.nanmean(coords_x)
                coords_y -= np.nanmean(coords_y)
        if apply:
            self.coords[0] = coords_x
            self.coords[1] = coords_y
            self.values = values
            if self.has_slopes:
                self.x_slopes.values = self.x_slopes.values[slc_x, slc_y]
                self.x_slopes.coords = self.coords.copy()
                self.y_slopes.values = self.y_slopes.values[slc_x, slc_y]
                self.y_slopes.coords = self.coords.copy()
        return (coords_x, coords_y), values

    def extract_profile(self, position=0, width=1, length=None, from_center=True, axis='x'):
        ax = 0
        if 'y' in axis.lower():
            ax = 1
        if length is None:
            length = self.length[ax]
        centre = [0, position]
        size = [length, width]
        if ax == 1:
            centre = centre[::-1]
            size = size[::-1]
        if not from_center:
            centre[~ax] -= np.nanmean(self.coords[~ax])

        half = np.divide(size, 2)
        start = np.add(centre, np.negative(half))
        stop = np.add(centre, half)
        start_pix = [self.closest_idx(val, axis=axis) for axis, val in enumerate(start)]
        stop_pix = [self.closest_idx(val, axis=axis) for axis, val in enumerate(stop)]
        xrangearray = [start_pix[0], stop_pix[0]]
        yrangearray = [start_pix[1], stop_pix[1]]

        coords, values = self.subarray_pixel(xrangearray, yrangearray, apply=False)

        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='Mean of empty slice')
            extract = Profile(coords[ax], np.nanmean(values, axis=~ax), self.units)
        extract.mean_removal()

        # reverse roc
        _roc_x = self.reverse_radius(extract)
        extract.copy_attributes(self, update_roc=False)
        extract._roc_x = _roc_x
        extract._roc_y = None
        return extract

    def reverse_radius(self, profile):
        if self.from_profile:
            return self.radius
        if self.ellipse_errors:
            return profile.radius
        if self._roc_x is None:
            _ = self.radius
            return profile.radius

        order = 1 if profile.is_slopes else 2
        mask = profile.valid_mask
        coords_SI = self.coords_unit.SI_unit(profile.coords[mask], profile.coords_unit)
        values_SI = self.values_unit.SI_unit(profile.values[mask], profile.values_unit)
        if self.ellipse:
            from .ellipse import Ellipse
            values_SI = values_SI + Ellipse.from_parameters(coords_SI, self.kind, self.p, self.q, self.theta)
        try:
            pol = polyfit(coords_SI, values_SI, deg=order)
        except Exception:
            return np.nan
        roc_SI = 1 / (order * pol[-1])
        roc = self.radius_unit(roc_SI, u.m)
        if abs(roc / self._roc_x) < 2:
            # errors not removed
            return roc
        reverted = values_SI + np.power(coords_SI, order) / u.m(order * self._roc_x, self.radius_unit)
        try:
            pol = polyfit(coords_SI, reverted, deg=order)
        except Exception:
            return np.nan
        return self.radius_unit(1 / (order * pol[-1]), u.m)

    # ----maths----
    def derivative(self, origin='lower-left', copy=True, **kwargs):
        """ origin: combination of 'lower' 'upper' 'right' 'left'  (default: 'lower-left') """
        res_x = self.duplicate() if copy else self
        res_y = self.duplicate()
        factor_x = res_x.z_unit.SI_unit(1, res_x.z_unit) / res_x.coords_unit.SI_unit(1, res_x.coords_unit)
        factor_y = res_y.z_unit.SI_unit(1, res_y.z_unit) / res_y.coords_unit.SI_unit(1, res_y.coords_unit)

        # if method == 'gradient':
        y = res_y.y
        for i, line in enumerate(res_x.values.T):
            x = res_x.x
            if 'right' in origin.lower():
                x = x[::-1]
            res_x.values.T[i] = np.multiply(np.gradient(line, x), factor_x)
            y = res_y.y
            if 'upper' in origin.lower():
                y = y[::-1]
        for i, line in enumerate(res_y.values):
            res_y.values[i] = np.multiply(np.gradient(line, y), factor_y)

        if 'height' in self.kind.lower():
            res_x.units['values'] = res_x.values_unit.si_angle
            res_y.units['values'] = res_y.values_unit.si_angle
        elif 'slope' in self.kind.lower():
            res_x.units['values'] = res_x.values_unit.si_curvature
            res_y.units['values'] = res_y.values_unit.si_curvature
        res_x.auto_units()
        res_y.auto_units()
        if kwargs.get('create_initial', False):
            res_x.initial = res_x.duplicate()
            res_y.initial = res_y.duplicate()
        return res_x, res_y

    @staticmethod
    def _plane2D(coef, x, y, z):
        A, B, C = coef
        return z - (A * x + B * y + C)

    @staticmethod
    def _conic2D(coef, x, y, xx, xy, yy, z):
        A, B, C, D, E, F = coef
        return z - (A * xx + B * xy + C * yy + D * x + E * y + F)

    @staticmethod
    def _sphere2D(coef, x, y, xx, yy, z):
        A, B, C, D = coef
        return z - (A * (xx + yy) + B * x + C * y + D)

    @staticmethod
    def _fit2D(x, y, z, fit='plane', verbose=True):
        tic = perf_counter_ns()
        x = x.ravel()
        y = y.ravel()
        z = z.ravel()
        mask = np.isnan(z)
        one = np.ones((len(x),), dtype=np.float64)
        M = np.array([x, y, one]).T
        if fit == 'conic':
            M = np.c_[x * x, x * y, y * y, M]
        elif fit == 'sphere':
            M = np.c_[(x * x + y * y), M]
        if M is not None:
            # res = lstsq(M[~mask], z[~mask], rcond=None)  # numpy linalg
            res = lstsq_jit(M[~mask], z[~mask])  # scipy linalg
            toc = perf_counter_ns()
            if verbose:
                print(f'applying {fit} least square fit to data ({(toc - tic) * 1e-6:.0f}ms)')
            return res[0]
        return None

    def plane_removal(self, reload=True, auto_units=True, verbose=True):
        if reload:
            self.reload()
        x, y = self.meshgrid
        pla = Surface._fit2D(x, y, self.values, fit='plane', verbose=verbose)
        self.values = Surface._plane2D(pla, x, y, self.values)
        self.tilt_x, self.tilt_y, self.piston = pla
        factor_angle = np.power(self.coords_unit(1, self.coords_unit.SI_unit), 2)
        self.tilt_x = self.units['angle'](self.tilt_x * factor_angle, u.rad)
        self.tilt_y = self.units['angle'](self.tilt_y * factor_angle, u.rad)
        self.rot_xy = None
        self.majcyl = None
        self.mincyl = None
        self.fit = 'plane'
        self.terms = 'Tilt'
        if auto_units:
            self.auto_units()
        # return piston, tilt_x, tilt_y

    def cylinder_removal(self, reload=True, auto_units=True, verbose=True):
        if reload:
            self.reload()
        x, y = self.meshgrid
        cyl = Surface._fit2D(x, y, self.values, fit='conic', verbose=verbose)
        self.values = Surface._conic2D(cyl, x, y, x * x, x * y, y * y, self.values)
        A, B, C, D, E, F = cyl
        theta = np.arctan(B / (A - C)) / 2
        if np.fabs(A) > np.fabs(C):
            if theta > 0:
                theta -= np.pi / 2
            elif theta < 0:
                theta += np.pi / 2
        self.rot_xy = u.deg(theta, u.rad)
        factor_radius = self.coords_unit(1, self.coords_unit.SI_unit)
        Arot = (A * np.cos(theta) * np.cos(theta)
                + B * np.cos(theta) * np.sin(theta)
                + C * np.sin(theta) * np.sin(theta))
        self.majcyl = self.radius_unit(1 / (2 * Arot) * factor_radius, u.m)
        theta += np.pi / 2
        Arot = (A * np.cos(theta) * np.cos(theta)
                + B * np.cos(theta) * np.sin(theta)
                + C * np.sin(theta) * np.sin(theta))
        self.mincyl = self.radius_unit(1 / (2 * Arot) * factor_radius, u.m)
        factor_angle = np.power(factor_radius, -2)
        self.tilt_x = self.units['angle'](D * factor_angle, u.rad)
        self.tilt_y = self.units['angle'](E * factor_angle, u.rad)
        self.piston = F
        self.fit = 'conic'
        self.terms = 'Cylinder & Tilt'
        if auto_units:
            self.auto_units()
        # return piston, tilt_x, tilt_y, rot_xy, majcyl, mincyl

    def sphere_removal(self, reload=True, auto_units=True, verbose=True):
        if reload:
            self.reload()
        x, y = self.meshgrid
        sph = Surface._fit2D(x, y, self.values, fit='sphere', verbose=verbose)
        A, B, C, D = sph
        factor_radius = self.coords_unit(1, self.coords_unit.SI_unit)
        # self._roc = self.radius_unit(1 / (2 * A) * factor_radius, u.m)
        factor_angle = np.power(factor_radius, -2)
        self.tilt_x = self.units['angle'](B * factor_angle, u.rad)
        self.tilt_y = self.units['angle'](C * factor_angle, u.rad)
        self.piston = D
        self.values = Surface._sphere2D(sph, x, y, x * x, y * y, self.values)
        self.rot_xy = None
        self.majcyl = None
        self.mincyl = None
        self.fit = 'sphere'
        self.terms = 'Sphere & Tilt'
        if auto_units:
            self.auto_units()
        # return piston, tilt_x, tilt_y, curv

    # ----export----
    @staticmethod
    def writeMetroprofile(path, data:np.ndarray, pixel_size, units:dict = None, frame_size=None, phase_res=1):
        """
        data heights array and pixel_size in meters,
        frame_size as tuple (width, height), data.shape by default
        phase_res: present in the Metropro file format v3, default 1 -> 32768
        """
        from .zygo import MetroProData
        return MetroProData.writeMetroprofile(str(path), data, pixel_size, units, frame_size, phase_res)

    @staticmethod
    def writeVeecofile(path, data:np.ndarray, pixel_size, title='File exported'):
        """
        data heights array in nanometer and pixel_size in millimeter,
        """
        from .veeco import OpdData
        return OpdData.writefile(str(path), data, pixel_size, title=title)

    # ----roughness----
    @property
    def ra(self):
        return np.nansum(np.fabs(self.z)) / self.valid_size

    @property
    def rq(self):
        return self.rms

    @property
    def rp(self):
        return self.max

    @property
    def rv(self):
        return self.min

    @property
    def rt(self):
        return self.pv

    @property
    def rsk(self):
        return np.nansum(np.power(self.z, 3)) / (self.valid_size * np.power(self.rq, 3))

    @property
    def rku(self):
        return np.nansum(np.power(self.z, 4)) / (self.valid_size * np.power(self.rq, 4))

    @property
    def rz(self):
        return self.filt_rz(self.z)

    @staticmethod
    def filt_rz(surfacedata, num_pts=10, window=None):
        data = surfacedata.copy()
        if window is None:
            window = [-5, 5]
        shape = data.shape
        if len(shape) == 2:
            data = data.ravel()
        if len(shape) > 2:
            print('rz calc: invalid data')
            return np.nan
        window = window + np.array([0, 1])
        r, c = np.meshgrid(np.arange(*window), np.arange(*window), indexing='ij')
        hi = []
        lo = []
        for i in range(0, num_pts):
            iloc = np.nanargmax(data)
            hi.append(data[iloc])
            row, col = np.unravel_index(iloc, shape)
            locs = np.ravel_multi_index(
                ((row + r).ravel(), (col + c).ravel()),
                shape, mode='clip')
            data[locs] = np.nan
            iloc = np.nanargmin(data)
            lo.append(data[iloc])
            row, col = np.unravel_index(iloc, shape)
            locs = np.ravel_multi_index(
                ((row + r).ravel(), (col + c).ravel()),
                shape, mode='clip')
            data[locs] = np.nan
        rz = (np.nansum(hi) - np.nansum(lo)) / num_pts
        # del data
        return rz

    def calc_fft2d(self, shift=True, plot=False, return_cls=True, **kwargs):
        val_si = self.values_unit.SI_unit(self.values, self.values_unit)
        fourier = fft2(val_si, workers=-1)
        if shift:
            fourier = fftshift(fourier)
        self.fft_cpx = fourier
        if plot or return_cls:
            units = {'coords': u.pix, 'values': u.unitless, 'angle': u.rad,
                     'length': u.m, 'radius': u.m, 'pixel': u.unitless}
            x = range(fourier.shape[0])
            y = range(fourier.shape[1])
            tmp = Surface([x, y], np.abs(fourier), units=units, source=self.source + ' - FFT 2D')
            tmp.mean_removal()
            tmp.is_fft = True
            tmp.fft_cpx = fourier
            if plot:
                title = kwargs.pop('title', self.source + ' - FFT 2D')
                tmp.plot(title, **kwargs)
            if return_cls:
                return tmp
        return fourier

    @staticmethod
    def reconstruct_from_PSD(fourier, shifted=True, plot=False, return_cls=True, units=None, **kwargs):
        if shifted:
            fourier = ifftshift(fourier)
        reconstructed = ifft2(fourier).real
        if plot or return_cls:
            if units is None:
                units = {'coords': u.m, 'values': u.m, 'angle': u.rad, 'length': u.m, 'radius': u.m, 'pixel': u.m}
            else:
                units = deepcopy(units)
            x = range(reconstructed.shape[0])
            y = range(reconstructed.shape[1])
            tmp = Surface([x, y], reconstructed, units=units, source='reconstructed from FFT 2D')
            tmp.mean_removal()
            tmp.fft_cpx = fourier
            if plot:
                title = kwargs.pop('title', 'reconstructed from FFT 2D')
                tmp.plot(title, **kwargs)
            if return_cls:
                return tmp
        return reconstructed

    # ----z scale----
    def calc_colormap(self, **params):
        colorscale = Surface.ColorScale(self, **params)
        self.cmap = colorscale.cmap
        self.cmap_prms = colorscale.cmap_prms
        # print(colorscale)
        return self.cmap, self.cmap_prms

    # noinspection PyUnusedLocal
    class ColorScale:
        """
        base: 'ra', 'rq', 'pv', 'fixed'
        params: list
            if stats based:  'zfac' as stretching factor (<0 to force default)
            if manual scale: 'zmin, zmax, z_lo, z_hi'
        """

        def __init__(self, surface, cmap='turbo', **params):
            self.lut = cm.get_cmap(cmap)(np.linspace(0, 1, 256))
            self.surface = surface  # any class with mandatory methods (ra, rq, rv, rp, median, automasking)
            self.cmap_prms = {'cmap': cmap, 'base': params.get('base', 'ra')}
            self.cmap, new_params = self._update(**params)
            self.cmap_prms.update(new_params)

        def __str__(self):
            # return str(self.cmap_prms)
            string = 'ColorScale:'
            for key, value in self.cmap_prms.items():
                fmt = '0.3f'
                if isinstance(value, str):
                    fmt = 's'
                string = string + f' {key}={value:{fmt}}'
            return string

        def _update(self, base='ra', **params):
            if params.get('zfac', -1) < 0.001:  # force default
                params.pop('zfac', 2.0)
            zfac, zmin, zmax, z_lo, z_hi = getattr(self, f'_{base}_based')(**params)
            new_params = dict(zip(['zfac', 'zmin', 'zmax', 'z_lo', 'z_hi'],
                                  [zfac, min([zmin, z_lo]), max([zmax, z_hi]), max([zmin, z_lo]), min([zmax, z_hi])]))
            if np.isnan(new_params['zmin']) or np.isnan(new_params['zmax']):
                new_params['zmin'] = -0.1
                new_params['zmax'] = 0.1
                new_params['z_lo'] = -0.05
                new_params['z_hi'] = 0.05
                return cm.get_cmap(self.cmap_prms['cmap']), new_params
            return self._calc_cmap(self.lut, **new_params), new_params

        def _ra_based(self, zfac=2.0, **kwargs):
            ra = self.surface.ra
            rq = self.surface.rq
            if kwargs.get('automasking', True):
                data = self.surface.automasking(3.0 * rq)
            else:
                data = self.surface.values
            median = np.nanmedian(data)
            z_lo = -zfac * ra + median
            z_hi = zfac * ra + median
            return zfac, np.nanmin(data), np.nanmax(data), z_lo, z_hi

        def _rq_based(self, zfac=3.0, **kwargs):
            rq = self.surface.rq
            if kwargs.get('automasking', True):
                data = self.surface.automasking(6.0 * rq)
            else:
                data = self.surface.values
            median = np.nanmedian(data)
            z_lo = -zfac * rq + median
            z_hi = zfac * rq + median
            return zfac, np.nanmin(data), np.nanmax(data), z_lo, z_hi

        def _pv_based(self, zfac=12.0, **kwargs):
            rq = self.surface.rq
            rv = self.surface.rv
            rp = self.surface.rp
            median = self.surface.median
            ratio = zfac * rq / (rp - rv)
            z_lo = -ratio * rq + median
            z_hi = ratio * rq + median
            return zfac, rv, rp, z_lo, z_hi

        def _fixed_based(self, zfac=3.0, **kwargs):
            if len(kwargs) == 4:
                return [zfac, kwargs['zmin'], kwargs['zmax'], kwargs['z_lo'], kwargs['z_hi']]
            zmin = np.negative(zfac)
            zmax = zfac
            median = self.surface.median
            z_lo = 0.40 * zmin + median
            z_hi = np.negative(z_lo)
            return zfac, zmin, zmax, z_lo, z_hi

        @staticmethod
        def _calc_cmap(cmap, zmin, zmax, z_lo, z_hi, **kwargs):
            num = 240
            z = (np.linspace(zmin, zmax, num=num, endpoint=True))
            z_lo1 = np.linspace(zmin, z_lo, num=15, endpoint=False)
            z_lo2 = np.linspace(z_lo, 0.0, num=105, endpoint=False)
            z_lo0 = np.append(z_lo1, z_lo2)
            z_hi1 = np.linspace(0.0, z_hi, num=85, endpoint=False)
            z_hi2 = np.linspace(z_hi, zmax, num=35, endpoint=True)
            z_hi0 = np.append(z_hi1, z_hi2)
            new_z = np.append(z_lo0, z_hi0)
            new_z = np.interp(z, new_z, z)
            idx = 0
            blue = cmap[idx:idx + num, 0]
            green = cmap[idx:idx + num, 1]
            red = cmap[idx:idx + num, 2]
            new_cmap = np.array([np.interp(new_z, z, blue),
                                 np.interp(new_z, z, green),
                                 np.interp(new_z, z, red)]).T
            return lsc.from_list('veeco', new_cmap, num)

    def plot(self, title=None, block=True, reverse='', **kwargs):
        """Matplotlib 2D plot with some additional functions.

            Parameters :
                'title' : string

                'reverse' : None,'x', 'y' or 'xy'  (default None).

                colormap parameters to set the colormap (see ColorScale class):
                    'base': 'ra', 'rq', 'pv', 'fixed'
                    'params': list
                        if stats based:  'zfac' as stretching factor
                        if manual scale: 'zmin, zmax, z_lo, z_hi'

                matplotlib.axes.Axes.imshow arguments
                https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.imshow.html
        """
        colorprms = {}
        for arg in ('base', 'cmap', 'zfac', 'zmin', 'zmax', 'z_lo', 'z_hi'):
            if arg in kwargs:
                colorprms[arg] = kwargs.pop(arg, None)
        self.calc_colormap(**colorprms)

        import matplotlib.pyplot as plt
        plt.matshow(
            self.z.T,
            origin=kwargs.get('origin', 'lower'),
            cmap=self.cmap,
            vmin=self.cmap_prms['zmin'], vmax=self.cmap_prms['zmax'],
            extent=[self.x.min(), self.x.max(),
                    self.y.min(), self.y.max()],
            **kwargs,
        )
        if title is not None:
            plt.title(title)
        plt.colorbar()
        if 'x' in reverse.lower():
            plt.gca().invert_xaxis()
        if 'y' in reverse.lower():
            plt.gca().invert_yaxis()
        plt.show(block=block)


# ------------generic data class------------
class Header:
    # def __repr__(self):
    #     return self
    def __str__(self):
        return 'Header block'


def _iter_header_dict(dico, parent):
    for key, value in dico.items():
        if isinstance(value, dict):
            child = Header()
            setattr(parent, key, child)
            _iter_header_dict(value, child)
        else:
            setattr(parent, key, value)


class MetrologyData:
    """Common data class."""
    def __init__(self):
        self.path = None
        self.source = None
        self.header = dict()
        self.initial = None
        self._gravity = False
        self._reference = False
        self.slice = [0, 0, 0, 0]  # offset x1, offset y1, offset x2, offset y2
        self.offset = [0, 0]  # offset x, offset y (correction to apply in the common frame)
        self.analysis_steps = list()
        self.legacy_header = None

        self.coords = None
        self.values = None

        # mask = None
        self.units = { # as example, dict  must be present
            'coords': None, 'values': None, 'angle': None, 'length': None, 'radius': None, 'pixel': None}

        self.detector_size = None

    @classmethod
    def read(cls, *args, verbose=False, **kwargs):
        self = cls().readfile(*args, **kwargs)
        if self is None:
            raise Exception('Error reading data.')
            # warnings.warn('Error reading data (No such file).')
            # return None
        if verbose:
            print(f'  file \'{self.source}\' opened.')
        return self

    # ----methods to be overrided----
    def readfile(self, path, source=None):
        error_msg = f'{self!r}: \'readfile\' function must be overrided.'
        print(error_msg)
        return self

    # ----common methods----
    def header_dict_to_class(self):
        _iter_header_dict(self.header, self)

    # ----analysis----
    def add_analysis_step(self, funcname, *params):
        step = (funcname, params)
        self.analysis_steps.append(step)

    def analyze(self):
        for funcname, params in self.analysis_steps:
            print(f'      {self.source}: applying {funcname}')
            getattr(self, funcname)(*params)
