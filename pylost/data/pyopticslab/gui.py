# -*- coding: utf-8 -*-
""""""

import sys
from pathlib import Path
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QWidget


class DialogBoxSimple(QWidget):
    """Subclass for PyQT5 in-build dialog box staticlass.

        callable functions:
            filedialog
            folderdialog
    """
    def __init__(self, app=None):
        if app is None:
            self.app = QApplication(sys.argv)
        # noinspection PyArgumentList
        super().__init__()

    def _show(self):
        self.show()
        sys.exit(self.app.exec_())

    # ---------public  functions---------
    def filedialog(self,
                   title='Open generic selection dialog with PyQt5',
                   geometry=(10, 10, 800, 600),
                   startpath=None,
                   filters=None):
        """Call the QFileDialog.getOpenFolderName function.

            Folder and files are always displayed, filters can be applied.

            Return a 'Path' object of a file (pathlib).

            Parameters :
                'title' : string.

                'geometry' : tuple of int (left, top, width, height)
                                  (don't seem to work properly)

                'startpath' : 'Path' object from pathlib.
                              'None' object sets the working folder as starting folder.
                              (default None)

                'filters' : tuple ('desc', 'ext') or a list of tuples.
                            'None' object means no filters   ('All Files','*'))
                            (default None)
        """
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filters = _filters_to_string(filters)
        if startpath is None:
            startpath = Path.cwd()
        self._init_ui(title, geometry)
        return self._openfiledialog(title, startpath, options, filters)

    def folderdialog(self,
                     title='Open generic selection dialog with PyQt5',
                     geometry=(10, 10, 800, 600),
                     startpath=None,
                     showfiles=False):
        """Call the QFileDialog.getExistingDirectory function.

            Folder and files can be displayed, no filters.

            Return a 'Path' object of a folder (pathlib).

            Parameters :
                'title' : string.

                'geometry' : tuple of int (left, top, width, height)
                                  (don't seem to work properly)

                'startpath' : 'Path' object from pathlib.
                              'None' object sets the working folder as starting folder.
                              (default None)

                'showfiles' : boolean   (default False)
        """
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        if not showfiles:
            options |= QFileDialog.ShowDirsOnly
        if startpath is None:
            startpath = Path.cwd()
        self._init_ui(title, geometry)
        return self._openfolderdialog(title, startpath, options)

    # --------protected functions--------
    def _init_ui(self, title, geometry):
        """Basic geometry.""" # XXX: don't work with dialog static method
        self.setWindowTitle(title)
        self.setGeometry(*geometry)

    def _openfiledialog(self, title, startpath, options, filters):
        """QFileDialog.getOpenFileName call.

            Return a 'Path' object from pathlib.
        """
        filename, _ = QFileDialog.getOpenFileName(
            self,
            caption=title,
            directory=str(startpath),
            filter=filters,
            options=options
            )
        if filename:
            return Path(rf'{filename}')
        # if file is None:
        # print('Canceled.')
        return None

    def _openfolderdialog(self, title, startpath, options):
        """QFileDialog.getExistingDirectory call.

            Return a 'Path' object from pathlib.
        """
        foldername = QFileDialog.getExistingDirectory(
            self,
            caption=title,
            directory=str(startpath),
            options=options
            )
        if foldername:
            return Path(foldername)
        print('Canceled.')
        return None

def _filters_to_string(filters=None):
    """Build the string to feed the getOpenFileName filter option.
    filters must be a tuple ('desc', 'ext') or a list of tuples.
    """
    filters_string = ''
    if filters is not None:
        if isinstance(filters, tuple):
            filters = [filters]
        for desc, ext in filters:
            filters_string = filters_string + f'{desc} (*.{ext});;'
    return filters_string + 'All Files (*.*)'
