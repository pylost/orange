# -*- coding: utf-8 -*-
""""""

import os
import datetime as dt
import numpy as np
from pathlib import Path

from . import units as u
from .generic import MetrologyData, Surface
from . import vk4extract as vk4extract


class KeyenceData(MetrologyData, Surface):
    """Keyence VK4 data class."""

    def __init__(self):
        super().__init__()

        self.header_format = None
        self.header_size = None
        self.note = None

        self.datetime = None

        self.intensity = None
        self.phase = None

        self.units = {'coords': u.um, 'values': u.um, 'angle': u.urad, 'length': u.mm, 'radius': u.km, 'pixel': u.um}

    def __str__(self):
        return 'Keyence surface map'

    # ----overriding----
    def readfile(self, path, source=None):
        with open(path, 'rb') as input_file:
            self.datetime = dt.datetime.fromtimestamp(os.path.getmtime(path))

            if isinstance(path, str):
                path = Path(path)
            path = path
            self.source = path.name

            print(f'opening \'{self.source}\'...')

            offsets = vk4extract.extract_offsets(input_file)
            self.header['offsets'] = offsets
            meas_cond = vk4extract.extract_measurement_conditions(offsets, input_file)
            self.header['meas_cond'] = meas_cond
            height_arr = vk4extract.extract_img_data(offsets, 'height', input_file)
            # int_arr = vk4extract.extract_img_data(offsets, 'light', input_file)

            h, w = height_arr['height'], height_arr['width']
            spixX, spixY, spixZ = meas_cond['x_length_per_pixel'], meas_cond['y_length_per_pixel'], meas_cond[
                'z_length_per_digit']
            rawdata = np.resize(height_arr['data'], (h, w)).T * spixZ * 1e-6
            # intensity = np.resize(int_arr['data'], (h, w)).T
            # fVx, fVy = w * spixX * 1e-6, h * spixY * 1e-6
            # fieldView = [-0.5 * fVx, 0.5 * fVx, -0.5 * fVy, 0.5 * fVy]

            self.header['lateral_res'] = np.float64(spixX * 1e-6)
            shape = rawdata.shape
            x = np.linspace(0, shape[0] * self.header['lateral_res'],
                            num=shape[0], endpoint=False, dtype=np.float64)
            y = np.linspace(0, shape[1] * self.header['lateral_res'],
                            num=shape[1], endpoint=False, dtype=np.float64)
            super(MetrologyData, self).__init__([x, y], rawdata, self.units, self.source)
        return self
