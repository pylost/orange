# -*- coding: utf-8 -*-
""""""

import datetime
import numpy as np
from pathlib import Path

from . import units as u
from .generic import MetrologyData, Surface

BPF = 1e+38
BLCK_SIZE = 24


class OpdData(MetrologyData, Surface):
    """Vision32 data class."""

    def __init__(self):
        super().__init__()

        self.header_size = None
        self.note = None

        self.units = {'coords': u.mm, 'values': u.nm, 'angle': u.urad, 'length': u.mm, 'radius': u.km, 'pixel': u.mm}

    def __str__(self):
        return 'Veeco 2D surface map'

    # ----overriding----
    def readfile(self, path, source=None, **kwargs):
        with open(path, 'rb') as opd_file:

            if isinstance(path, str):
                path = Path(path)
            path = path
            self.source = path.name

            block_ppos = 0
            directory = self.opd_block(opd_file, block_ppos)
            if 'Directory' not in directory.name:
                print('Unable to read Directory entry, aborting...')
                return self
            self.header_size = directory.len
            num_block_max = int(self.header_size / (BLCK_SIZE + 2))

            blocks = []
            for b in range(1, num_block_max + 1):
                block_ppos += BLCK_SIZE
                block = self.opd_block(opd_file, block_ppos)
                if block.signature != 0:
                    blocks.append(block)

            opd_file.seek(self.header_size + 2)
            for block in blocks:
                try:
                    block.decode_value(opd_file)
                    if block.name in ('RAW_DATA', 'Raw', 'OPD'):
                        rawdata = np.where(block.value < BPF, block.value, np.nan)
                    else:
                        self.header[block.name] = block.value
                except Exception:
                    continue

            # stage positions: inches to mm
            if 'StageX' in self.header:
                self.header['StageX'] = self.header['StageX'] * 25.4
            if 'StageY' in self.header:
                self.header['StageY'] = self.header['StageY'] * 25.4

            self.header['Pixel_size'] = np.float64(self.header['Pixel_size'])
            self.header['Wavelength'] = np.float64(self.header['Wavelength'])

            # nanometers
            rawdata = rawdata * self.header['Wavelength']
            rawdata -= np.nanmean(rawdata)
            shape = rawdata.shape
            # if shape[1] / shape[0] != 0.75:  # remove nan lines/columns in Vision32 stitching (TODO need to be reproduced)
            #     rawdata = rawdata[~(np.isnan(rawdata).all(axis=1)), :]
            #     rawdata = rawdata[:, ~(np.isnan(rawdata).all(axis=0))]
            #     shape = rawdata.shape
            x = np.linspace(0, shape[0] * self.header['Pixel_size'],
                            num=shape[0], endpoint=False, dtype=np.float64)
            y = np.linspace(0, shape[1] * self.header['Pixel_size'],
                            num=shape[1], endpoint=False, dtype=np.float64)
            super(MetrologyData, self).__init__([x, y], rawdata, self.units, self.source, **kwargs)
        return self

    @staticmethod
    def writefile(path, data, pixel_size, stages=(0.0, 0.0), mode='PSI', wavelength=620.3375854492188, title='File exported'): # SI
        import struct

        date_time = datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')
        date, time = date_time.split(' ')

        header_default = {'MeasMode': (mode, 1, 10), 'StageX': (stages[0], 16, 4), 'StageY': (stages[1], 8, 4), 'Aspect': (1.0, 8, 4),
                          'Wavelength': (wavelength, 1, 4), 'Pixel_size': (pixel_size, 8, 4),
                          'Magnification': (100.08377075195312, 8, 4), 'Wedge': (0.5, 16, 4), 'Note': ('', 16, 60),
                          'Title': (title, 1, 20), 'Time': (time, 1, 8), 'Date': (date, 16, 10),
                          'Pupil': (100.0, 16, 4), 'Use_XYR_cent': (0, 8, 2), 'Use_XYR_spac': (1, 16, 2),
                          'Pupil_diam': (0.0, 8, 4), 'XYR_x_spac': (pixel_size, 8, 4), 'Terms_String': ('Tilt ', 8, 40),
                          'Data_Restore': ('No', 8, 10), 'Data_Invert': ('No', 8, 10), 'Filt_Type': ('None', 8, 40),
                          'Vol_opt_String': ('Normal', 8, 40), 'UseApodization': (0, 8, 2), 'MaskingString': (' ', 8, 40)}
        header_size = 6000

        directory_block = b'\x01\x00Directory\x00\x00\x00\x00\x00\x00\x00\x01\x00p\x17\x00\x00'
        data_block = b'\xff\xffRAW_DATA\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00' \
                     + struct.pack('i', int(6 + data.size * 4))

        values = b''
        blocks = directory_block + data_block
        for name, (value, signature, data_len) in header_default.items():
            data_type = 0
            fmt = None
            if isinstance(value, str):
                data_type = 5
                value = value.encode()
            elif isinstance(value, int):
                data_type = 6 if data_len == 2 else 12
                fmt = 'h' if data_len == 2 else 'i' # np.long
            elif isinstance(value, float):
                data_type = 7
                fmt = '<e' if data_len == 2 else '<f' # np.single

            # values
            bval = value if isinstance(value, bytes) else struct.pack(fmt, value)
            values = values + bval + bytearray(data_len - len(bval))

            # block
            signature = struct.pack('<h', signature)  # [0:2], short
            name = name.encode() + bytearray(16 - len(name))  # [2:18]
            data_type = struct.pack('<h', data_type)  # [18:20], short
            data_len = struct.pack('i', data_len)  # [20:24], int32
            header_block = signature + name + data_type + data_len
            if len(header_block) != BLCK_SIZE:
                return
            blocks = blocks + header_block

        blocks = blocks + bytearray(header_size - len(blocks))

        # data
        bdata = bytearray(2) + struct.pack('H', data.shape[0]) + struct.pack('H', data.shape[1]) + struct.pack('H', 4)
        data32 = np.divide(data, wavelength)
        data32[np.isnan(data)] = 1e31 + np.finfo('f').max / 2
        bdata = bdata + data32.astype('<f').ravel().tobytes()

        with open(path, 'wb') as opd_file:
            opd_file.write(blocks)
            opd_file.write(bdata)
            opd_file.write(values)

        return

    # ----properties----
    @property
    def datetime(self):
        try:
            date = self.header['Date']
            time = self.header['Time']
        except KeyError:
            date, time = '01/01/1970', '00:00:00'
        return datetime.datetime.strptime(f'{date} {time}', '%m/%d/%Y %H:%M:%S')

    @property
    def title(self):
        return self.header.get('Title', '')

    @property
    def mode(self):
        return self.header.get('MeasMode', '')

    @property
    def objective(self):
        return self.header.get('ObjectiveLabel', self.magnification)

    @property
    def magnification(self):
        mag = self.header.get('Magnification', 0)
        return f'{mag:.1f} X'

    @property
    def fov(self):
        default = '1 X'
        return self.header.get('FOVLabel', default)

    @property
    def fov_num(self):
        fov = float(self.fov.split(' ')[0])
        part, frac = divmod(fov, 1)
        is_integer = abs(round(frac, 15)) < 1e-16
        if is_integer:
            return int(fov)
        return fov

    @property
    def phase_average(self):
        return self.header.get('Averages', None)

    @property
    def stage_x(self):
        try:
            return self.header['StageX']  # mm
        except KeyError:
            return 0.0

    @property
    def stage_y(self):
        try:
            return self.header['StageY']  # mm
        except KeyError:
            return 0.0

    @property
    def average(self):
        return self.header['Averages']

    @property
    def wavelength(self):
        return self.header['Wavelength']  # nm

    # @property
    # def histogram(self):
    #     mask = self.masking()
    #     return np.histogram(self.values[mask], bins=512, density=False)
    #     # plt.plot(histo[1][:-1], histo[0])

    class opd_block:
        def __init__(self, file, ppos):
            file.seek(ppos)
            block = file.read(BLCK_SIZE)# + 2)
            self.signature = np.frombuffer(block[0:2], dtype=np.short)[0]
            if self.signature == 0:
                return
            try:
                self.name = block[2:18].decode().rstrip('\x00')
            except UnicodeDecodeError:
                self.name = block[2:18].decode('latin1').rstrip('\x00')
            self.type = np.frombuffer(block[18:20], dtype=np.short)[0]
            self.len = np.frombuffer(block[20:24], dtype=np.int32)[0]
            # self.attr = np.frombuffer(block[24:26], dtype=np.ushort)[0]
            self.value = None

        def decode_value(self, file):
            buf = file.read(self.len)
            if self.type == 3:  # array of data
                rows, cols, elsize = np.frombuffer(buf[0:6], dtype=np.ushort)[0:3]
                dtype = np.single
                if elsize == 2:
                    dtype = np.short
                self.value = np.resize(np.frombuffer(buf[6:], dtype=dtype),
                                       (rows, cols))
            elif self.type == 5:  # string
                self.value = buf.decode().strip('\x00')
            elif self.type == 6:  # short
                self.value = np.frombuffer(buf, dtype=np.short)[0]
            elif self.type == 7:  # float
                if len(buf) == 4:
                    self.value = np.float64(np.frombuffer(buf, dtype=np.single)[0])
                if len(buf) == 2:
                    self.value = np.float64(np.frombuffer(buf, dtype=np.half)[0])
            elif self.type == 8:  # double
                self.value = np.float64(np.frombuffer(buf, dtype=np.double)[0])
            elif self.type == 12:  # long
                self.value = np.frombuffer(buf, dtype=np.int32)[0]
