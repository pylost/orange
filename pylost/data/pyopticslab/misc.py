# -*- coding: utf-8 -*-
""""""

from datetime import datetime as dt
from pathlib import Path
import numpy as np

from . import units as u
from .generic import MetrologyData, Profile

label_x = 'mirror_coord'
label_y = 'heights'


class HeightProfile(MetrologyData, Profile):
    """HGT profile class."""
    # method = 'laser measuring system'
    # instrument = "Long Trace Profiler"

    def __init__(self):
        super().__init__()
        self.units = {'coords': u.mm, 'values': u.um, 'angle': u.urad, 'length': u.mm, 'radius': u.m, 'pixel': u.mm}

        self.raw_signal = None

    # def __repr__(self):
    #     return 'repr'

    def __str__(self):
        return 'Height 1D profile'

    # ----overriding----
    def readfile(self, path, source=None, oversampling=True, force_even=False):
        # data = None
        if isinstance(path, str) and '\n' in path:
            data = path  # data is passed as argument
            if source is None:
                source = 'buffer'
        else:
            try:
                with open(path) as hgtfile:
                    path = Path(path)
                    self.path = path
                    source = path.name
                    data = hgtfile.read()
            except Exception as e:
                print('Error reading height profile (No such file).', e)
                return None
            # pass
        self.source = source
        if data is None:
            return None
        self._parse_hgt(data, oversampling, force_even)
        return self

    def writefile(self, path):
        if isinstance(path, str):
            path = Path(path)
        path = Path(path.parent, path.stem + '.hgt')
        buf = self._build_hgt()
        with open(path, 'w') as hgt_file:
            hgt_file.write(buf)
        return buf

    # ----properties----
    @property
    def datetime(self):
        date_time = self.header['legacy_header']['date_time']
        return dt.strptime(date_time, '%d/%m/%Y @ %H:%M:%S')

    # ----write hgt file----
    def _build_hgt(self):
        saved = self.duplicate()
        saved.change_units([u.mm, u.um, u.m])
        header = self.header['legacy_header']
        buf = header.get('comment', '') + '\n'
        buf = buf + header.get('date_time', '') + '\n'
        buf = buf + header.get('reference', 'No ref/') + '/'
        buf = buf + header.get('gravity', '') + '\n'
        focal = header.get('focal_length', '0')
        buf = buf + f'{focal:.6f}' + '\n'
        buf = buf + f'{saved.pixel_size:.10f}' + '\n'
        buf = buf + f'{len(saved.values)}' + '\n'
        for i, val in enumerate(saved.values):
            buf = buf + f'{saved.coords[i]:.10f}\t{val:.10f}' + '\n'
        return buf

    # ----read hgt file----
    def _parse_hgt(self, buf, oversampling=False, force_even=False):
        self.header['legacy_header'] = {}
        data_array = []
        header = ['comment', 'date_time', 'reference/gravity',
                  'focal_length', 'step', 'nb_acquisitions']
        for num, line in enumerate(buf.splitlines()):
            value = self._val_interp(line)
            if not line:
                continue
            if num < len(header):
                if num < 3:
                    value = line.strip()
                if num == 2:
                    twice = line.split('/')
                    self.header['legacy_header']['reference'] = twice[0]
                    try:
                        self.header['legacy_header']['gravity'] = twice[1]
                    except IndexError:
                        self._raisevaluerror(3)
                else:
                    self.header['legacy_header'][header[num]] = value
                continue
            if len(value) > 1:
                try:
                    data_array.append(value)
                except ValueError:
                    self._raisevaluerror(3)
        data_header = [label_x, label_y]
        self._finalize(data_array, data_header, oversampling, force_even, errorcodes=(3, 4, 5))

    def _finalize(self, data_array, data_header, oversampling, force_even, errorcodes=None):
        if errorcodes is None:
            errorcodes = 2
        if isinstance(errorcodes, int):
            errorcodes = (errorcodes, errorcodes, errorcodes)
        format_error, value_error, data_error = errorcodes
        try:
            data_header[0] = label_x
            data_header[1] = label_y
            num_acq = self.header['legacy_header']['nb_acquisitions']
        except KeyError:
            num_acq = -1
            self._raisevaluerror(format_error)
        if num_acq == 0:
            self._raisevaluerror(value_error)
        if num_acq == len(data_array) + 1:
            # bug in firsts static slp2 files
            if (data_array[-1] + data_array[-2])[0] < 1e-3:
                data_array = data_array[:-1]
            self._raisevaluerror(data_error)
        self.header['legacy_header']['nb_acquisitions'] = len(data_array)
        data_array = np.asfarray(data_array).T
        num_entries = data_array.shape[0]
        self.header['loaded_data'] = {}
        for i, key in enumerate(data_header):
            if i > num_entries - 1:
                break
            self.header['loaded_data'][key] = data_array[i]
        self.header_dict_to_class()
        super(MetrologyData, self).__init__(data_array[0], data_array[1], self.units, self.source)

        self.gravity = self.header['legacy_header']['gravity'] == 'gravity correction'
        self.reference = self.header['legacy_header']['reference'] == 'ref substracted'

        self.mean_step = np.nanmean(np.diff(data_array[0]))

        if not oversampling and not force_even:
            return self

        method, k = 'cubicbspline', 2
        factor = 1 if force_even and not oversampling else max(1, int(round(1500 / len(self.values))))  # max(1, int(self.mean_step // 0.06))
        if factor > 1:
            self.is_oversampled = True
        self.oversampling(factor, method=method, k=k) # always even shape

        if oversampling and self.is_oversampled:
            return self

        original_size = len(data_array[0])
        method = 'interp'
        if force_even and len(data_array[0]) % 2 == 1:
            original_size -= 1
            method = 'cubicbspline'
        coords = np.linspace(0, original_size * self.mean_step, num=original_size, endpoint=False)
        coords -= np.nanmean(coords)
        self.interpolation(coords, method=method, k=k, copy=False)
        return self

    @staticmethod
    def _val_interp(string):
        """Return variable type from input string"""
        string = string.strip()
        if len(string) < 1:  # empty string
            return ''
        array = string.split()
        try:
            dtype = 'float64'
            if len(array) == 1 and np.all(np.char.isdigit(string.rsplit('-')[-1])):
                dtype = 'int'
            array = np.array(array, dtype=dtype)
        except ValueError:
            return string
        if len(array) > 1:
            return array
        return array[0]

    @staticmethod
    def _raisevaluerror(code, string=''):
        msg = ['Not the right extension!',
               f'source {string} has not the right extension!',
               'Data not valid!',
               'Wrong slp format',
               'Wrong slp format or error on slp data!',
               'Error on slp data!',
               'Wrong slp2 format',
               'Error on slp2 data!'
               ]
        raise ValueError(msg[code])
