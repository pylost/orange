# coding=utf-8
"""
accepted unit string in "set_coords" and "set_values":
    - heights
        'm', 'km', 'cm', 'mm', 'um', 'nm', 'A', 'pm', 'inch'
    - slopes
        'rad', 'mrad', 'urad', 'nrad'
"""

import numpy as np

from pylost.data.readers import PylostReader


class ZeissASCII(PylostReader):
    EXTENSIONS = ('.asc', '.ASC')
    DESCRIPTION = 'Zeiss ASCII file reader'
    PRIORITY = 2
    DATATYPE = 'heights'
    NDIM = '2D'

    @staticmethod
    def read(filepath):
        try:
            item = np.loadtxt(filepath, comments='!').T
            x, y, z = item[0], item[1], item[2]
            num_cols = len(y[y==y[0]])
            num_rows = int(len(y) / num_cols)
            z = np.reshape(z, (num_rows, num_cols))
            z = np.where(np.fabs(z) > 1e-15, z, np.nan).T
            item = PylostReader._data_to_dict([x[:num_cols], y[0::num_cols]], z, 'mm', 'mm')
            return item, None, None
        except Exception as e:
            raise Exception(e)
