# coding=utf-8
"""
accepted unit string in "set_coords" and "set_values":
    - heights
        'm', 'km', 'cm', 'mm', 'um', 'nm', 'A', 'pm', 'inch'
    - slopes
        'rad', 'mrad', 'urad', 'nrad'
"""

import numpy as np

import h5py

from pylost.data.readers import Path, PylostReader, Surface, Profile, u
from pylost.user.settings import INSTRUMENTS


class HDF5_base:
    @staticmethod
    def group_to_dict(group:h5py.Group):
        dico = {}
        for key, value in list(group.attrs.items()) + list(group.items()):
            if not key:
                continue
            if isinstance(value, h5py.Group):
                dico[key] = HDF5_base.group_to_dict(value)
                continue
            elif isinstance(value, h5py.Dataset):
                dico[key] = {}
                for attr_name, attr in value.attrs.items():
                    dico[key][attr_name] = attr
                dico[key]['value'] = np.array(value, dtype=value.dtype)
                continue
            else:
                dico[key] = value
        return dico

    @staticmethod
    def dict_to_group(group:h5py.Group, dico:dict):
        for key, value in dico.items():
            if not key:
                continue
            if isinstance(value, dict):
                HDF5_base.dict_to_group(group.create_group(key), value)
                continue
            if isinstance(value, np.ndarray):
                group.create_dataset(key, data=value)
            else:
                group.attrs[key] = value
        return group

    @staticmethod
    def target_from_path(dico:dict, link:str):
        obj = dico
        for depth in link.split('/'):
            if len(depth) == 0:
                continue
            obj = obj.get(depth, None)
            if obj is None:
                break
        return obj

    @staticmethod
    def is_nexus(dico:dict):
        nexus = False
        for key, value in dico.items():
            if not key:
                continue
            if isinstance(value, dict):
                nexus = HDF5_base.is_nexus(value)
                continue
            if 'NXentry' in value:
                return True
        return nexus


class NexusFormat(PylostReader):
    EXTENSIONS = ('.nxs', '.NXS', '.h5', '.H5', '.hdf5', '.HDF5')
    DESCRIPTION = 'Nexus file reader'
    PRIORITY = 0
    DATATYPE = 'any'
    NDIM = '2D'

    # noinspection PyTypeChecker
    def loadfile(self, filepath):
        try:
            with h5py.File(filepath, 'r') as f:
                data = HDF5_base.group_to_dict(f)
        except Exception as e:
            raise Exception(e)
        # if 'nexusformat_version' in list(data.keys()):
        if HDF5_base.is_nexus(data):
            return NexusFormat.parse_nexus(self, data, filepath)
        raise(TypeError('Not a Nexus data format'))

    @staticmethod
    def parse_nexus(reader, data, filepath):
        from pylost.data import StitchingDataset
        entry = data.get('default', None)
        if entry is None:
            return None
        tgt = HDF5_base.target_from_path(data, entry)
        if tgt is None:
            return None
        instrument = None
        for key, value in tgt.items():
            if isinstance(value, dict):
                if value.get('NX_class', '') == 'NXinstrument':
                    for k, v in value.items():
                        if isinstance(v, dict):
                            if v.get('NX_class', '') == 'NXdetector':
                                instrument = k
        link = tgt.get('default', None)
        if link is None:
            return None
        tgt = HDF5_base.target_from_path(tgt, link)
        if tgt is None:
            return None
        datatype = tgt.get('signal', None)
        if datatype is None:
            return None
        if datatype in ('height', 'slope'):
            reader.DATATYPE = datatype
        if instrument in list(INSTRUMENTS.keys()):
            reader.DATATYPE = INSTRUMENTS[instrument]
        val = HDF5_base.target_from_path(tgt, datatype)
        values = val.get('value', None)
        if values is None:
            return None
        shape = values.shape
        value_unit = NexusFormat.convert_unit(val.get('unit', 'unitless'))
        axes = tgt.get('axes', ('x', 'y'))
        if values.ndim == len(axes):
            values = np.reshape(values, (1,) + values.shape)
        coord_x = tgt.get(axes[-2], None)
        if coord_x is None:
            coord_x = {'value':np.linspace(0, shape[-1] - 1, shape[-1]), 'unit': 'mm'}
        x = coord_x['value'] - np.nanmean(coord_x['value'])
        if np.argmin(x) > np.argmax(x):
            x = np.negative(x)
            if values.ndim == 2:
                values = np.asfarray([sub[::-1] for sub in values])
        elif values.ndim == 3:
            values = np.asfarray([np.flipud(sub) for sub in values])
        x_unit = NexusFormat.convert_unit(coord_x.get('unit', 'mm'))
        coord_y = tgt.get(axes[-1], None)
        if coord_y is None:
            coord_y = {'value':np.linspace(0, shape[-2] - 1, shape[-2]), 'unit': 'mm'}
        y = coord_y['value'] - np.nanmean(coord_y['value'])
        if np.argmin(y) > np.argmax(y):
            y = np.negative(y)
        elif values.ndim == 3:
            values = np.asfarray([np.fliplr(sub) for sub in values])
        coords = x
        reader.NDIM = '1D'
        cls = Profile
        if len(axes) == 2:
            reader.NDIM = '2D'
            cls = Surface
            coords = [x, y]
        title = str(Path(filepath).name)
        units = {
            'coords': getattr(u, x_unit), 'values': getattr(u, value_unit), 'angle': u.urad,
            'length': u.mm, 'radius': u.km, 'pixel': u.mm
        }

        subapertures = [cls(coords, array.T, units, source=title) for array in values]
        return StitchingDataset(subapertures, title=title, extra=None, folder=Path(filepath).parent)

    @staticmethod
    def convert_unit(unit_str):
        dico = {'unitless':'unitless', 'pix':'pix', 'meter':'m', 'radian':'rad', 'mm':'mm'} # not extensive
        return dico.get(unit_str, None)


class PylostFormat(PylostReader):  # TODO: transform to MX compatible format
    EXTENSIONS = ('.plst', '.PLST')
    DESCRIPTION = 'Pylost file reader'
    PRIORITY = 0
    DATATYPE = 'heights'
    NDIM = '2D'

    def loadfile(self, filepath):
        from pylost.data import StitchingDataset
        try:
            with h5py.File(filepath, 'r') as f:
                # version = f.attrs.get('version', -1)
                attrs = f['dataset'].attrs
                title = attrs.get('title', 'loaded file')
                subapertures = f['dataset'].get('subapertures', [])

                extras = None
                if subapertures:
                    subapertures, extras = list(zip(*[PylostFormat.dataset_to_generic_data(scan)
                                                      for scan in subapertures.values()]))
                    subapertures = list(subapertures)
                    if len(subapertures) == 1:
                        subapertures[0].source = title

                dataset = StitchingDataset(subapertures, title=title, extra=extras, folder=Path(filepath).parent)
                dataset.history = tuple(attrs.get('history', ('',)))
                dataset.data_offsets = attrs.get('data_offsets', [0, 0])

                stitched_data = f['dataset'].get('stitched_data', None)
                if stitched_data is not None:
                    stitched_data, _ = PylostFormat.dataset_to_generic_data(stitched_data)
                    stitched_data.source = title
                    if not subapertures:
                        dataset.scans = [stitched_data]
                    else:
                        dataset.stitched = stitched_data

                reference_data = f['dataset'].get('reference_data', None)
                if reference_data is not None:
                    reference_data, _ = PylostFormat.dataset_to_generic_data(reference_data)
                    if not subapertures and stitched_data is None:
                        dataset.scans = [reference_data]
                    else:
                        dataset.reference = reference_data

                motors_data = f['dataset'].get('motors_data', None)
                if motors_data is not None:
                    motors_x = motors_data.get('motors_x', None)
                    motors_y = motors_data.get('motors_y', None)
                    dataset.set_motors(np.asfarray([motors_x, motors_y]).T, motors_data.attrs.get('unit', 'mm'))

                if not subapertures and stitched_data is None and reference_data is None:
                    scan, extras = PylostFormat.dataset_to_generic_data(f['dataset'])
                    dataset.scans = [scan]
                    dataset.extras = extras

            return dataset

        except Exception as e:
            raise Exception(e)

    @staticmethod
    def generic_data_to_group(group:h5py.Group, data_name:str, data:[Surface, Profile]):
        dataset = group.create_dataset(data_name, data=data.values)
        dataset.attrs['unit'] = str(data.values_unit)
        dataset.attrs['lateral_resolution'] = data.pixel_size
        dataset.attrs['lateral_unit'] = str(data.coords_unit)
        dataset.attrs['source'] = str(data.source)
        return dataset

    @staticmethod
    def dataset_to_generic_data(data:[h5py.Group, h5py.Dataset]):
        if data is None:
            return
        extras = None
        if isinstance(data, h5py.Group):
            for value in data.values():
                if isinstance(value, h5py.Dataset):
                    data = value
                if isinstance(value, h5py.Group):
                    extras = value
            if extras is not None:
                extras = HDF5_base.group_to_dict(extras)
        units = {'coords': None, 'values': None, 'angle': u.urad, 'length': u.mm, 'radius': u.km, 'pixel': u.mm}
        cls = Surface if data.ndim > 1 else Profile
        attrs = data.attrs
        unit = attrs.get('unit', None)
        if unit is None:
            return
        units['values'] = getattr(u, unit)
        lateral_unit = attrs.get('lateral_unit', None)
        if lateral_unit is None:
            return
        units['coords'] = getattr(u, lateral_unit)
        lateral_resolution = attrs.get('lateral_resolution', None)
        if lateral_resolution is None:
            return
        if cls == Surface:
            coords = [np.arange(size) * res for size, res in zip(data.shape, lateral_resolution)]
        else:
            coords = np.arange(data.shape[0]) * lateral_resolution
        source = attrs.get('source', None)
        data = np.array(data, dtype=data.dtype)
        obj = cls(coords, data, units, source)
        obj._gravity = attrs.get('gravity', False)
        obj._reference = attrs.get('reference', False)
        return obj, extras

    @staticmethod
    def export(filepath, dataset, **kwargs):
        try:
            include_subapertures = kwargs.get('include_subapertures', True)
            with h5py.File(filepath, 'w') as f:
                f.attrs['version'] = 0
                f.attrs['dataset'] = 'dataset'

                entry = f.create_group('dataset')
                entry.attrs['title'] = filepath.name
                entry.attrs['history'] = dataset.history

                if len(dataset) == 1:
                    PylostFormat.generic_data_to_group(entry, dataset.scans[0].source, dataset.scans[0])
                    if isinstance(dataset.extras, dict):
                        group_extra = entry.create_group('extras')
                        HDF5_base.dict_to_group(group_extra, dataset.extras)
                    return

                if dataset.is_stitched:
                    PylostFormat.generic_data_to_group(entry, 'stitched_data', dataset.stitched)

                if dataset.has_reference:
                    PylostFormat.generic_data_to_group(entry, 'reference_data', dataset.reference)

                if include_subapertures:
                    group = entry.create_group('subapertures')
                    extras = dataset.extras
                    sz = int(np.log10(len(dataset)))
                    for i, subaperture in enumerate(zip(dataset.scans, extras)):
                        scan, extra = subaperture
                        group_scan = group.create_group(f'{i:0{sz}}')
                        PylostFormat.generic_data_to_group(group_scan, scan.source, scan)
                        group_extra = group_scan.create_group('extras')
                        if isinstance(extra, dict):
                            HDF5_base.dict_to_group(group_extra, extra)
                    if dataset.has_motors:
                        group = entry.create_group('motors_data')
                        motors_x, motors_y = dataset.motors_to_list()
                        group.create_dataset('motors_x', data=motors_x)
                        group.create_dataset('motors_y', data=motors_y)
                        group.attrs['unit'] = str(dataset.units['pixel'])

        except Exception as e:
            raise Exception(e)
