# coding=utf-8
"""
pylost stitching algorithms module
"""

from functools import wraps
from time import perf_counter_ns

from pylost.data import StitchingDataset, deepcopy
from pylost.utils.image_processing import optimize_XY

from orangewidget import gui


def time_perf(method=None):
    def decorate(self, *arg, **kw):
        @wraps(method)
        def wrapper(*args, **kwargs):
            if self.verbose:
                print(f' - {method.__name__}:')
            start_time = perf_counter_ns()
            result = method(self, *args, **kwargs)
            end_time = perf_counter_ns()
            if self.verbose:
                print(f'      --> {(end_time-start_time)*1e-6:.3f} ms')
            return result
        return wrapper(*arg, **kw)
    return decorate


class StitchingAlgorithm(gui.OWComponent):
    name = 'no method'
    saved_params = []
    controls_list = []

    def __init__(self, verbose=None, sigCancel=None, sigStatus=None, sigProgress=None, *args, **kwargs):
        super(StitchingAlgorithm, self).__init__()
        self.patches = []
        self.num_patches = None
        self.pixel_size = None
        self.patches_shape = []
        self.patches_units = None
        self._pixel_shifts = ()
        self.pixel_shifts = ()
        self.return_overlapped = kwargs.pop('return_overlapped', False)
        self.overlapped = None
        self.args = args
        self.kwargs = kwargs
        self.verbose = verbose
        self.sigCancel = sigCancel
        self.sigStatus = sigStatus
        self.sigProgress = sigProgress

        # @time_perf(line='Stitching')
    def _stitch(self):
        """"""
        start = perf_counter_ns()
        if self.verbose:
            print(f'Starting {self.name}')

        try:
            stitched_pixel_size = None
            self.pixel_shifts = self._pixel_shifts
            if self.patches[0].is_2D:
                if self.kwargs.get('optimize_xy', False):
                    self.sigStatus.emit('Optimizing XY...')
                    _, self.pixel_shifts, stitched_pixel_size = optimize_XY(self.patches, self.pixel_shifts,
                                                                            self.pixel_size, self.sigProgress, 5)
            if self.patches[0].is_heights:
                result = self.stitch(*self.args, **self.kwargs)
                result['slopes'] = False
            else:
                x_slopes = [scan.x_slopes for scan in self.patches]
                y_slopes = [scan.y_slopes for scan in self.patches]

                self.patches = y_slopes
                result = self.stitch(*self.args, **self.kwargs)
                y_slopes = result.pop('stitched', None)
                y_ref = result.pop('reference', None)

                self.patches = x_slopes
                result = self.stitch(*self.args, **self.kwargs)
                result['x_slopes'] = result.get('stitched', None).copy()
                result['y_slopes'] = y_slopes

                if result.get('reference', None) is not None:
                    # noinspection PyUnresolvedReferences
                    result['x_ref'] = result['reference'].copy()
                    result['y_ref'] = y_ref

                result['slopes'] = True

            if stitched_pixel_size is not None:
                result['stitched_sampling'] = stitched_pixel_size

        except Exception as e:
            raise Exception(e)

        stop = perf_counter_ns()
        runtime = stop - start
        if self.verbose:
            print(f'{self.name} runtime: {runtime * 1e-6:.3f}ms')
        result['runtime'] = runtime

        return result

    def enable_parameters(self, flag:bool):
        """ if controls list is not empty, enable or disable the qt objects in that list """
        for control in self.controls_list:
            try:
                getattr(self, control).setEnabled(flag)
            except TypeError:
                """ no 'setEnabled' method in control """

    def set_data(self, dataset:StitchingDataset, motors:[tuple, list], *args, **kwargs):
        """motors as [[x_pos], [y_pos]]"""
        self.patches = dataset.copypatches()
        self.pixel_size = dataset.pixel_size
        self.num_patches = len(dataset)
        self.patches_shape = self.patches[0].shape
        self.patches_units = deepcopy(self.patches[0].units)
        self._pixel_shifts = dataset.motors_to_pixel_shift(motors)
        self.pixel_shifts = self._pixel_shifts

    def get_overlapped_regions(self):
        return self.overlapped

    def stitch(self, *args, **kwargs):
        """ to be overrided """
        return {'stitched':None, 'reference':None, 'caption':''}

    def clear(self):
        for attr_name, value in self.__dict__.items():
            setattr(self, attr_name, None)

    def set_widget(self, options_area, params, *args, **kwargs):
        """ to be overrided """

    @staticmethod
    def clear_widget(options_area):
        for w in options_area.children():
            if isinstance(w, gui.QWidget):
                options_area.layout().removeWidget(w)
                w.deleteLater()

    def get_params(self):
        params = {'name':self.name}
        for param in self.saved_params:
            params[param] = getattr(self, param, None)
        return params
