# coding=utf-8
"""
global optimize algorithm

to be put in the user plugin folder
        windows: C:/users/[username]/.pylost/plugins/stitching_algorithms
        linux:   ~/.pylost/plugins/stitching_algorithms

and declared in the '__init__.py' file:
    from .test import TestStitching

Adding a new algorithm to pylost consist in few elements to be set:
    - the class must be derived from the 'StitchingAlgorithm' object to inherit its methods
    - methods 'set_widget' and 'stitch' must be present for setting the display and the processing
    - may include some parameters that could be saved in the worflow
    - 'set_data' can also be overrided to set algorithm specific attributes

"""

import numpy as np
from scipy.sparse import coo_matrix
from scipy.optimize import lsq_linear

# pylost imports, 'StitchingAlgorithm' is mandatory
from pylost.stitching import time_perf, StitchingAlgorithm, gui
from pylost.orange.gui import Spacer, FloatLineEdit, QSizePolicy
from pylost.utils.methods import mean_removal, level_data,_plane_fit2D, _plane_res2D, _conic_res2D
# , _conic_fit2D, _central_value
from pylost.utils.multithreading import Executor


class GlobalOptimize(StitchingAlgorithm):
    """ each algorithm must be a 'StitchingAlgorithm' object """

    #   !!MANDATORY!!
    name = 'Global Optimisation'

    # list of parameters name wich will be saved in the ows file
    saved_params = ['min_overlap', 'optim_tolerance', 'reference_extraction', 'reference_order',
                    'ref_tolerance', 'remove_reference', 'reverse_order']
    # default value
    min_overlap = 0.4
    optim_tolerance = 1e-10
    reference_extraction = False
    reference_order = 1
    ref_tolerance = 1e-01
    remove_reference = True
    reverse_order = False

    # list of controls that can be managed by the widget
    controls_list = ['sp_min_overlap', 'fl_opt_tol', 'cb_ref_extract', 'fl_ref_tol', 'cb_ref_remove']

    def __init__(self, verbose=None, *args, **kwargs):
        #   !!MANDATORY!!
        super().__init__(verbose=verbose, *args, **kwargs)

    def enable_parameters(self, flag:bool):
        super().enable_parameters(flag)
        self._enable_ref_params()

    def _enable_ref_params(self):
        flag = self.cb_ref_extract.isChecked()
        for control in ['fl_ref_tol', 'cb_ref_remove']:
            getattr(self, control).setEnabled(flag)

    def _optim_tolerance_changed(self):
        # qt signal called this method when fields have been changed
        self.optim_tolerance = self.fl_opt_tol.value()

    def _ref_tolerance_changed(self):
        # qt signal called this method when fields have been changed
        self.ref_tolerance = self.fl_ref_tol.value()

    # noinspection PyUnresolvedReferences,PyAttributeOutsideInit
    def set_widget(self, options_area, params, *args, **kwargs):
        """   !!MANDATORY!!
            This method is called when selecting the algorithm.
            It sets the display in the dedicated widget on the main right panel.
            Use qt5 ('options_area' is the widget object).
        """
        options_area.setMinimumSize(200, 100)

        tooltip = 'Size of the minimal overlapped region to look after (ratio of subapertures size).\n'\
                  'Lowering this value will increase the amount of data used in the optimization.'
        self.sp_min_overlap = gui.doubleSpin(options_area, self, 'min_overlap', label='Overlap ratio min:',
                              minv=0.00, maxv=0.99, step=0.01, tooltip=tooltip)
        self.sp_min_overlap.setValue(params.get('min_overlap', 0.4))

        hbox = gui.hBox(options_area, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        hbox.layout().addWidget(gui.QLabel('Optimization tolerance:'))
        self.fl_opt_tol = FloatLineEdit(None, params.get('optim_tolerance', 1e-10), fmt='.0e', maxwidth=61,
                                        callback=self._optim_tolerance_changed)
        self.fl_opt_tol.setToolTip('Global optimization tolerance parameter (scipy.optimize.lsq_linear, defaut 1e-10)')
        hbox.layout().addWidget(self.fl_opt_tol)

        options_area.layout().addWidget(Spacer(parent=options_area))

        tooltip = 'LSQ residuals between subapertures will be subtracted from every patches before stitching.'
        self.cb_ref_extract = gui.checkBox(options_area, self, 'reference_extraction', 'Extract reference',
                                           tooltip=tooltip, callback=self._enable_ref_params)
        hbox = gui.hBox(options_area, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        hbox.layout().addWidget(gui.QLabel('Reference extraction tolerance:'))
        self.fl_ref_tol = FloatLineEdit(None, params.get('ref_tolerance', 1e-1), fmt='.0e', maxwidth=61,
                                        callback=self._ref_tolerance_changed)
        self.fl_ref_tol.setToolTip('Reference extraction tolerance parameter (scipy.optimize.lsq_linear, defaut 1e-1)')
        hbox.layout().addWidget(self.fl_ref_tol)

        options_area.layout().addWidget(Spacer(parent=options_area))

        tooltip = 'Extracted rference will be removed before joining subapertures.'
        self.cb_ref_remove = gui.checkBox(options_area, self, 'remove_reference', 'Remove reference', tooltip=tooltip)
        self.cb_ref_remove.setChecked(params.get('remove_reference', True))

        # reference_order = params.get('reference_order', 1)
        # gui.spin(options_area, self, 'reference_order', label='Reference order:', minv=1, maxv=2).setValue(reference_order)

        self.cb_ref_extract.setChecked(params.get('reference_extraction', False))
        # initialize
        self._enable_ref_params()
        self._optim_tolerance_changed()
        self._ref_tolerance_changed()

        tooltip = 'Stitch in reverse order.'
        self.cb_reverse = gui.checkBox(options_area, self, 'reverse_order', 'Reverse direction', tooltip=tooltip)
        self.cb_reverse.setChecked(params.get('reverse_order', False))

    def set_data(self, dataset, motors, *args, **kwargs):
        """     can be overrided
            method called just before starting the stitching
            defaults attributes are set here
        """
        return super().set_data(dataset, motors, *args, **kwargs)

    class MultithreadedFunctions:
        """ list of the multithreaded methods on one element of the iterable to be called in an executor """

        @staticmethod
        def preprocessing(data): # TODO: only heights ?
            # data.center_coordinates() # TODO: is it needed ?
            func = level_data
            if data.is_heights:
                func = mean_removal
                func(data, center_coordinates=True, auto_units=False)

        @staticmethod
        def diff_and_fit_2D(overlapped:tuple, pixel_size:[tuple, list], reference=False, order=1, slopes=False):
            # correction pitch x or roll y
            diff = overlapped[0] - overlapped[1]
            offset = overlapped[2]
            x, y = np.mgrid[0:diff.shape[0], 0:diff.shape[1]]
            x = (x + offset[0]) * pixel_size[0]
            y = (y + offset[1]) * pixel_size[1]
            if slopes:
                # fit_params = _plane_fit2D(x, y, diff) if order == 2 else [_central_value(diff), 0.0, 0.0]
                fit_params = [0.0, 0.0, np.nanmean(diff)]
                diff = np.subtract(diff, fit_params[2])
                return fit_params, diff
            else:
                # fit_params = _conic_fit2D(x, y, diff) if order == 2 else _plane_fit2D(x, y, diff)
                fit_params = _plane_fit2D(x, y, diff)
                if reference:
                    if order == 2:
                        return fit_params, _conic_res2D(fit_params, x, y, x * x, x * y, y * y, diff)
                    else:
                        return fit_params, _plane_res2D(fit_params, x, y, diff)
                return fit_params, diff

    def stitch(self, *args, **kwargs):
        """   !!MANDATORY!!
            This is the entry point of the algorithm, called when stitching starts

            must returns a dictionnary with at least the 'stitched' entry containing a 2D array
            'reference' is also a 2D array
            'caption' can be used to return some infos to display below the widget. '\n' can be used to split into lines
        """
        # cumulative steps in pixel
        shift_x, shift_y = self.pixel_shifts
        if self.reverse_order:
            self.patches = self.patches[::-1]
            shift_x = shift_x[::-1]
            shift_y = shift_y[::-1]
        pix_shift = (shift_x, shift_y)

        # get overlap regions and get initial correctors
        result, coo_data, patch_slices = self.get_overlaps_2D(pix_shift, self.pixel_size, min_overlap=self.min_overlap,
                                                              slopes=self.patches[0].is_slopes)
        fit_params, residuals = list(zip(*result))

        # correctors optimization (linear least square)
        correctors = self.optimize_correctors(coo_data, fit_params)

        # reference extraction (full / linear least square)
        reference = None
        if self.reference_extraction:
            reference = self.extract_reference(residuals, patch_slices)

        # apply corrections and join patches by averaging at each pixels
        if self.remove_reference:
            stitched = self.correct_and_join(pix_shift, correctors, self.pixel_size, reference)
        else:
            stitched = self.correct_and_join(pix_shift, correctors, self.pixel_size, None)

        caption = f'     Minimum overlap: {self.min_overlap:.2f}  (tol: {self.optim_tolerance:.0e})'
        if self.reference_extraction:
            caption = caption + f'\n     Reference extracted  (tol: {self.ref_tolerance:.0e})'
            if self.remove_reference:
                caption = caption + f'\n     Reference removed.'

        return {'stitched':stitched, 'reference':reference, 'caption':caption, 'correctors':correctors, 'patch_slices':patch_slices}

    @time_perf
    def correct_and_join(self, shifts:tuple, correctors:(list, tuple, np.array), pixel_size, reference=None):
        """"""
        self.sigStatus.emit('Patchworking...')
        progress_inc = 40 / self.num_patches
        if reference is not None:
            progress_inc = progress_inc / 2.5

        shifts = shifts[0] - min(shifts[0]), shifts[1] - min(shifts[1])
        positions = list(zip(*shifts))
        full_mirror = np.full((max(np.abs(shifts[0])) + self.patches_shape[0], max(np.abs(shifts[1])) + self.patches_shape[1]), np.nan)
        weights = np.full_like(full_mirror, np.nan)
        origins = (int((full_mirror.shape[0] - 0.5) / -2), int((full_mirror.shape[1] - 0.5) / -2))
        # origins = (full_mirror.shape[0] - 0.5) // -2, (full_mirror.shape[1] - 0.5) // -2
        for patch, corrector, position in zip(self.patches, correctors, positions):
            x, y = np.mgrid[0:patch.shape[0], 0:patch.shape[1]]
            x = (x + position[0] + origins[0]) * pixel_size[0]
            y = (y + position[1] + origins[1]) * pixel_size[1]
            if self.reference_order == 2:
                A, B, C, D, E, F = corrector
                # correction = A * x*x + B * x*y + C * y*y + D * x + E * y + F
                correction = D * x + E * y + F
            else:
                A, B, C = corrector
                correction = A * x + B * y + C
            corrected = patch.values + correction
            if reference is not None:
                corrected = np.sum((corrected, np.negative(reference)), axis=0)
            slc = slice(position[0], position[0] + corrected.shape[0]), \
                slice(position[1], position[1] + corrected.shape[1])
            full_mirror[slc] = np.nansum(np.dstack((full_mirror[slc], corrected)), 2)
            weights[slc] = np.nansum(np.dstack((weights[slc], np.where(np.isnan(corrected), 0, 1))), 2)
            if self.sigProgress is not None:
                self.sigProgress.emit(progress_inc)

        # self.sigStatus.emit('Finalizing...')
        mask = (~np.isnan(full_mirror)) & (weights != 0)
        stitched = np.divide(full_mirror, weights, where=mask)
        return np.where(mask, stitched, np.nan)

    @time_perf
    def extract_reference(self, residuals:[tuple, list], patch_slices:list):
        self.sigStatus.emit('Reference: building sparse matrix...')
        progress_inc = (2.5 / len(residuals), 67)

        # build the arrays beforehand
        sz = 0
        for resid in residuals:
            sz += np.count_nonzero(~np.isnan(resid))
        rows = np.empty((sz, 2), dtype=np.int32)  # counts overlaps x valid pixels in each ovrlap
        cols = np.empty((sz, 2), dtype=np.int32)  # counts number of terms to fit in the reference
        data = np.empty((sz, 2), dtype=np.int8)  # valid pixels   # int8 for memory savings?
        resid_vector = np.empty((sz,), dtype=np.float64)  # valid pixels
        x, y = np.mgrid[0:self.patches_shape[0], 0:self.patches_shape[1]]
        index_mat = np.arange(x.size).reshape(x.shape)
        # build sparse ref_coo_matrix
        col = 0
        for s, resid in enumerate(residuals):
            mask = ~np.isnan(resid)
            osz = resid[mask].size
            slc_o = patch_slices[2 * s]
            slc_s = patch_slices[2 * s + 1]
            rows[col:col + osz, :] = np.arange(col, col + osz).reshape(-1, 1)
            cols[col:col + osz, 0] = index_mat[slc_o][mask]
            cols[col:col + osz, 1] = index_mat[slc_s][mask]
            data[col:col + osz, :] = np.array([-1, 1]).reshape(1, -1)
            resid_vector[col:col + osz] = resid[mask]
            col = col + osz
            if self.sigProgress is not None:
                self.sigProgress.emit(progress_inc[0])
        ref_coo = coo_matrix((data.ravel(), (rows.ravel(), cols.ravel())), (col, x.size))

        self.sigStatus.emit('Reference: solving LSQ problem...\n!! DO NOT ABORT !!')
        # tolerance reduced to gain some speed & less artifacts
        res = lsq_linear(ref_coo, resid_vector, tol=self.ref_tolerance)
        # noinspection PyUnresolvedReferences
        reference = res.x
        # reference = lsq_linear_pythran(ref_coo, resid_vector, tol=1e-1)  # pythranized
        if self.sigProgress is not None:
            self.sigProgress.emit(progress_inc[1])

        return reference.reshape(self.patches_shape)

    @time_perf
    def optimize_correctors(self, coo_data:dict, fit_params:list):
        """"""
        self.sigStatus.emit('Optimizing correctors...')
        fits = list(zip(*fit_params))
        num_terms = len(fits)

        # build sparse coo_matrix from root coo_matrix
        rows = []
        cols = []
        data = []
        for f in range(num_terms):
            r = np.array(coo_data['rows']) + len(coo_data['rows']) / 2 * f
            rows.extend(r.astype(int))
            c = np.array(coo_data['cols']) + self.num_patches * f
            cols.extend(c.astype(int))
            data.extend(coo_data['data'])
        for f in range(num_terms):
            rows.extend([rows[-1] + 1, ] * self.num_patches)
            cols.extend(np.arange(self.num_patches) + (self.num_patches * f))
            data.extend([1, ] * self.num_patches)
        A = coo_matrix((np.array(data, dtype=np.int8), (rows, cols)))  # int8 for memory savings?

        # build the target vector
        E = ()
        for terms in fits:
            E = E + terms
        E = E + (0,) * num_terms
        # E = fits[0] + fits[1] + fits[2] + (0,) * num_terms

        res = lsq_linear(A, E, tol=self.optim_tolerance)  # method == 'least_squares_linear'
        # noinspection PyUnresolvedReferences
        correctors = res.x
        # correctors = lsq_linear_pythran(A, E)  # pythranized

        return correctors.reshape((num_terms, self.num_patches)).T

    @time_perf
    def get_overlaps_2D(self, shifts:tuple, pixel_size, min_overlap:float = 0, slopes=False):
        """"""
        progress_inc = 5 / self.num_patches
        if self.reference_extraction:
            progress_inc = progress_inc / 4

        full_shape = (max(shifts[0]) + self.patches_shape[0], max(shifts[1]) + self.patches_shape[1])
        origins = (int((full_shape[0] - 0.5) / -2), int((full_shape[1] - 0.5) / -2))

        # preprocessing
        self.sigStatus.emit('Preprocessing...')
        Executor(self.MultithreadedFunctions.preprocessing,
                 self.patches,
                 verbose=self.verbose,
                 cancel_signal=self.sigCancel,
                 progress_signal=self.sigProgress,
                 progressbar_inc=progress_inc
                 ).start()

        # position of each patch in the final array
        slices = []
        for p, patch in enumerate(self.patches):
            p = p - self.num_patches
            cx = slice(shifts[0][p], shifts[0][p] + self.patches[p].shape[0])
            cy = slice(shifts[1][p], shifts[1][p] + self.patches[p].shape[1])
            slices.append((cx, cy))

        min_overlap_num = min_overlap * self.patches_shape[0] * self.patches_shape[1]

        # find overlapped patches for each patch & root sparse coo_matrix rows, cols, data
        overlapped = []
        coo_slices = []
        rows = []
        cols = []
        data = []
        for s, slc in enumerate(slices):
            sx, sy = slc
            # the positions of the other patch must be inside the starting patch to have an overlap
            min_x = sx.start + int((sx.stop - sx.start) * (1 - min_overlap))
            min_y = sy.start + int((sy.stop - sy.start) * (1 - min_overlap))
            # check & slice every patches
            for o, other in enumerate(slices):
                if o <= s:
                    continue
                ox, oy = other
                if ox.start < min_x <= ox.stop and oy.start < min_y <= oy.stop:
                    overlap_size = np.abs(sx.stop - ox.start) * np.abs(sy.stop - oy.start)
                    if overlap_size > min_overlap_num:
                        step_x = ox.start - sx.start
                        step_y = oy.start - sy.start
                        slc_sx = slice(max(0, step_x), min(self.patches_shape[0], self.patches_shape[0] + step_x))
                        slc_sy = slice(max(0, step_y), min(self.patches_shape[1], self.patches_shape[1] + step_y))
                        slc_ox = slice(max(0, -step_x), min(self.patches_shape[0], self.patches_shape[0] - step_x))
                        slc_oy = slice(max(0, -step_y), min(self.patches_shape[1], self.patches_shape[1] - step_y))
                        start = self.patches[s][slc_sx, slc_sy]
                        other = self.patches[o][slc_ox, slc_oy]
                        #  excluding this other patch if too much nan
                        if start.size - np.isnan(start + other).sum() < min_overlap_num:
                            continue
                        # start positions in full mirror pixel centred coordinates
                        start_pixels = (sx.start + slc_sx.start + origins[0], sy.start + slc_sy.start + origins[1])
                        overlapped.append((start, other, start_pixels))
                        rows.extend([int(len(rows) / 2),] * 2)
                        cols.extend([s, o])
                        data.extend([-1, 1])
                        coo_slices.extend(((slc_ox, slc_oy), (slc_sx, slc_sy)))
                        # coo_slices.extend((slices[s], slices[o]))
        if not data:
            min_overlap_pct = (abs(np.median(np.diff(shifts[0]))) / self.patches_shape[0] +
                               abs(np.median(np.diff(shifts[1]))) / self.patches_shape[1])
            # min_overlap_pct = min_overlap_pct * 0.8
            raise ZeroDivisionError('no overlapped region found. '
                                    f'Minimum overlap ratio may be too high, try {min_overlap_pct:.2f}')

        if overlapped and self.return_overlapped:
            self.overlapped = []
            for start, other, _ in overlapped:
                for patch in (start, other):
                    self.overlapped.append(patch)

        progress_inc = 22 * progress_inc * self.num_patches / len(data)

        # pixel_size = self.patches[0].pixel_size
        self.sigStatus.emit('Finding initial correctors...')
        result = Executor(self.MultithreadedFunctions.diff_and_fit_2D,
                          overlapped,
                          pixel_size=pixel_size,
                          reference=self.reference_extraction,
                          order=self.reference_order,
                          slopes=slopes,
                          verbose=self.verbose,
                          cancel_signal=self.sigCancel,
                          progress_signal=self.sigProgress,
                          progressbar_inc=progress_inc).start()

        return result, {'rows':rows, 'cols':cols, 'data':data}, coo_slices
