# coding=utf-8

import numpy as np

from pylost.data.readers import PylostReader

header = [
    # [('fileformat', str)],
    [('SoftwareType', int), ('MajorVers', int), ('MinorVers', int), ('BugVers', int), ('SoftwareDate', str)],
    [('IntensOriginX', int), ('IntensOriginY', int), ('IntensWidth', int), ('IntensHeight', int), ('NBuckets', int),
     ('IntensRange', int)],
    [('PhaseOriginX', int), ('PhaseOriginY', int), ('PhaseWidth', int), ('PhaseHeight', int)],
    [('Comment', str)],
    [('PartSerNum', str)],
    [('PartNum', str)],
    [('Source', int), ('IntfScaleFactor', float), ('WavelengthIn', float), ('NumericAperture', float),
     ('ObliquityFactor', float), ('Magnification', float), ('CameraRes', float), ('TimeStamp', int)],
    [('CameraWidth', int), ('CameraHeight', int), ('SystemType', int), ('SystemBoard', int),
     ('SystemSerial', int), ('InstrumentId', int), ('ObjectiveName', str)],
    [('AcquireMode', int), ('IntensAvgs', int), ('PZTCal', int), ('PZTGain', int), ('PZTGainTolerance', int),
     ('AGC', int), ('TargetRange', float), ('LightLevel', float), ('MinMod', int), ('MinModPts', int)],
    [('PhaseRes', int), ('PhaseAvgs', int), ('MinimumAreaSize', int), ('DisconAction', int), ('DisconFilter', float),
     ('ConnectionOrder', int), ('RemoveTiltBias', int), ('DataSign', str), ('CodeVType', int)],
    [('SubtractSysErr', int), ('SysErrFile', str)],
    [('RefractiveIndex', float), ('PartThickness', float)],
    [('ZoomDesc', str)]
]

class XYZ_Reader(PylostReader):
    EXTENSIONS = ('.xyz', '.XYZ')
    DESCRIPTION = 'Zygo XYZ Data File reader'
    PRIORITY = 3
    DATATYPE = 'heights'
    NDIM = '2D'

    @staticmethod
    def _parser(filepath):
        def converter(element):
            return 'nan' if element == b'No' else element
        header_dict = {}
        with open(filepath) as file:
            lines = file.readlines()
        header_dict['fileformat'] = lines.pop(0)
        li = 0
        for line, header_split in zip(lines, header):
            li += 1
            line_split = line.strip().split()
            if '#' in line_split[0] and len(line_split[0]) == 1:
                break
            for value, param in zip(line_split, header_split):
                name, dtype = param
                if '"' in value:
                    value = value.strip('"')
                header_dict[name] = dtype(value)
        return np.loadtxt(lines[li:-2], usecols=(2,), converters=converter), header_dict

    @staticmethod
    def read(filepath):
        try:
            z, infos = XYZ_Reader._parser(filepath)
            # phase data
            # Wav = infos['WavelengthIn']
            # Sca = infos['IntfScaleFactor']
            # Obl = infos['ObliquityFactor']
            # if infos['PhaseRes'] == 0:
            #     Res = 4096
            # elif infos['PhaseRes'] == 1:
            #     Res = 32768
            # elif infos['PhaseRes'] == 2:
            #     Res = 131072
            # z = z * Wav * Sca * Obl / Res
            # full camera array
            fullshape = (infos['CameraHeight'], infos['CameraWidth'])
            shape = (infos['PhaseHeight'], infos['PhaseWidth'])
            z = np.reshape(z, shape)
            if shape != fullshape:
                idx = [infos['PhaseOriginY'],
                       infos['PhaseOriginX'],
                       infos['PhaseOriginY'] + infos['PhaseHeight'],
                       infos['PhaseOriginX'] + infos['PhaseWidth']]
                full = np.full(fullshape, np.nan)
                full[idx[0]:idx[2], idx[1]:idx[3]] = z
                z = full.T - np.nanmean(z)
            else:
                z = z.T - np.nanmean(z)
            z = np.fliplr(z)
            pixel_res = infos['CameraRes']
            x = np.arange(shape[1]) * pixel_res
            y = np.arange(shape[0]) * pixel_res

            # x_pos = item.header.get('StageX', None)
            # y_pos = item.header.get('StageY', None)
            # motors = PylostReader._motor_to_dict(x_pos, y_pos, 'mm')
            motors = None
            # noinspection PyTypeChecker
            item = PylostReader._data_to_dict([x, y], z, 'm', 'um')
            extra = None
            return item, motors, extra
        except Exception as e:
            raise Exception(e)
