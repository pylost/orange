# coding=utf-8
"""
user pylost data readers module
"""

# ########## import customized readers here ########## #
from ._test import XYZ_Reader  # <-- import simple example


"""
A file reader plugin must follow the examples below.

Can return a single measurement file or a list of measurements (full dataset from hdf5 filem for ex.)

accepted unit strings:
----------------------
    - heights
        'm', 'km', 'cm', 'mm', 'um', 'nm', 'A', 'pm', 'inch'
    - slopes
        'rad', 'mrad', 'urad', 'nrad'
----------------------

examples:
---------
####################### BASICS #######################
    from pylost.data.readers import PylostReader
    from pylost.data.pyopticslab.veeco import OpdData

    class VeecoReader(PylostReader):
        EXTENSIONS = ('.opd',)
        DESCRIPTION = 'Veeco OPD file reader'
        PRIORITY = 2
        DATATYPE = 'heights'
        NDIM = '2D'

        @staticmethod
        def read(filepath):
            try:
                item = OpdData().read(filepath)
                x_pos = item.header.get('StageX', None)
                y_pos = item.header.get('StageY', None)
                motors = PylostReader._motor_to_dict(x_pos, y_pos, 'mm')
                item = PylostReader._data_to_dict(item.coords, item.values, 'mm', 'nm')
                extra = None
                return item, motors, extra
            except Exception as e:
                raise Exception(e)
######################################################


###################### ADVANCED ######################
    from pylost.data.readers import PylostReader
    from my_module import datareader

    class MyReader(PylostReader):
        EXTENSIONS = ('.any', '.other', )  # custom extensions is better (see PRIORITY attribute)
        DESCRIPTION = 'My file reader'  # it will appear in the open dialog list
        PRIORITY = 99  # given an extension shared by other readers, which priority should be given to this one.
        DATATYPE = 'heights'  # or 'slopes'
        NDIM = '2D'  # or '1D'

        # ----- !!! DO NOT SET "__init__" method !!! -----
        # but method "read" must be implemented as staticmethod
        @staticmethod
        def read(filepath):
            # example of method for a single measurement
            def _single_measurement(single):
                # single should include informations about the coordinates and the values, better with motors positions
                x_coords, y_coords, values, encoders, extra = single
                # for 1D: must be an numpy.ndarray/list/tuple of the x coordinates
                if values.ndim == 1:
                    coords = x_coords
                    # for 2D: must be a list/tuple of the 2 axis (numpy.ndarray/list/tuple)
                elif values.ndim == 2:
                    coords = list((x_coords, y_coords))
                else:
                    raise ValueError(f'{MyReader.__class__}: invalid data dimension.')
                # must be called prior to return
                item = PylostReader._data_to_dict(coords=coords,         # 1D array or list/tuple of 1D arrays
                                                 values=values,         # array/list/tuple
                                                 coords_unit_str='mm',  # see accepted unit strings
                                                 values_unit_str='nm'   # see accepted unit strings
                                                 )
                # positions in the 2 dimensions X/Y
                motor_positions = None
                if encoder is not None:
                    x_pos, y_pos = encoders  # y_pos not used in 1D
                    # must be called prior to return
                    motor_positions = PylostReader._motor_to_dict(x_pos=float(x_pos),  # float
                                                                 y_pos=float(y_pos),  # float
                                                                 unit_str='mm'        # see accepted unit strings
                                                                 )
                # return the objects
                return item, motor_positions, extra

            # example of method for a multiple measurement dataset
            def _multiple_measurements(multiple):
                items = [_single_measurement(single) for single in multiple]
                # split the 2 lists of dictionaries before return
                item_list, motor_positions_list, extras = list(zip(*items))
                return item_list, motor_positions_list, extras

            try:
                dataset = datareader(filepath, *args, **kwargs)
                # example to discriminate different types of data
                if len(dataset) == 1:
                    return _single_measurement(dataset)
                elif len(dataset) > 1:
                    return _multiple_measurements(dataset)
                else:
                    raise ValueError(f'{MyReader.__class__} has returned an empty object.')
            # any exception will be catched
            except Exception as e:
                raise Exception(e)
######################################################

"""
