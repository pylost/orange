# coding=utf-8
"""
custom import test algorithm

to be put in the user plugin folder
        windows: C:/users/[username]/.pylost/plugins/stitching_algorithms
        linux:   ~/.pylost/plugins/stitching_algorithms

and declared in the '__init__.py' file:
    from ._test import TestStitching


Adding a new algorithm to pylost consist in few elements to be set:
    - the class must be derived from the 'StitchingAlgorithm' object to inherit its methods
    - methods 'set_widget' and 'stitch' must be present for setting the display and the processing
    - may include some parameters that could be saved in the worflow
    - 'set_data' can also be overrided to set algorithm specific attributes

"""

# pylost imports, 'StitchingAlgorithm' is mandatory
from pylost.stitching import StitchingAlgorithm, gui


class TestStitching(StitchingAlgorithm):
    """ each algorithm must be a 'StitchingAlgorithm' object """

    #   !!MANDATORY!!
    name = 'test plugin import'

    # list of parameters name wich will be saved in the ows file
    saved_params = ['parameter']
    parameter= 0.0

    def __init__(self, verbose=None, *args, **kwargs):
        #   !!MANDATORY!!
        super().__init__(verbose=verbose, *args, **kwargs)

    # noinspection PyUnresolvedReferences,PyAttributeOutsideInit
    def set_widget(self, options_area, params, *args, **kwargs):
        """   !!MANDATORY!!
            This method is called when selecting the algorithm.
            It sets the display in the dedicated widget on the main right panel.
            Use qt5 ('options_area' is the widget object).

            *.ui file can be used.

        """
        options_area.setMinimumSize(200, 100)

        gui.doubleSpin(options_area, self, 'parameter', label='parameter:',
                       minv=0.0, maxv=1.0, step=0.01).setValue(params.get('parameter', 0.0))

    def set_data(self, dataset, motors, *args, **kwargs):
        """     can be overrided
            method called just before starting the stitching
            defaults attributes are set here
        """
        super().set_data(dataset, motors, *args, **kwargs)

    def stitch(self, *args, **kwargs):
        """   !!MANDATORY!!
            This is the entry point of the algorithm, stitching starts here

            must returns a dictionnary with at least the 'stitched' entry containing a 2D array
            'reference' is also a 2D array
            'caption' can be used to return some infos to display below the widget. '\n' can be used to split into lines

            external code with different language could be called from here.

        """
        return {'stitched':None, 'reference':None, 'caption':'no infos\navailable'}
