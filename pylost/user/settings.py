# coding=utf-8

import sys
import shutil
from pathlib import Path


# ######## DEFAULT SETTINGS ########
VERBOSE = True
AUTOCALC = True
AUTOCLOSE = True
MAX_RECENT_FILE = 25
AUTOSTITCH = False
INFONICON = True
DIALOG_MULTIPLOT = True
TIMEOUT = 60
# ##################################


def default_user_path(rootfolder=None):
    if rootfolder is None:
        rootfolder = Path.home()
    user_path = Path(rootfolder, '.pylost')
    if not user_path.exists():
        user_path.mkdir()
    return user_path


USERDIR = default_user_path()


def check_user_settings(user_path=None):
    verbose = False
    if user_path is None:
        user_path = USERDIR
    defaults = []
    for name, value in globals().copy().items():
        if not name.startswith('_') and isinstance(value, (int, bool, str)):
            defaults.append((name, value))
    user_settings = Path(user_path, 'settings.ini')
    mode = 'r+'
    if not user_settings.exists():
        mode = 'a+'
    file = open(user_settings, mode)
    try:
        for line in file.readlines():
            for d, default in enumerate(defaults):
                name, value = default
                if '=' in line and line.startswith(name):
                    defaults.pop(d)
                    val = line.split('=')[1].strip().split(' ')[0]
                    cls = str
                    if isinstance(value, int):
                        cls = int
                    if isinstance(value, bool):
                        val = 1 if 'true' == val.lower() else 0
                        cls = bool
                    globals()[name] = cls(val)
                    if 'verbose' in name.lower():
                        verbose = cls(val)
                    break
        for default in defaults:
            file.write(f'{default[0]} = {default[1]}\n')
    except Exception as exception:
        print(f'error when importing user settings: \'{exception}\'')
    finally:
        file.close()
    return verbose


PATH_HISTORY = Path(USERDIR, 'recent_paths.txt')

def get_user_history():
    mode = 'r'
    if not PATH_HISTORY.exists():
        mode = 'w'
    with open(PATH_HISTORY, mode) as file:
        if mode == 'w':
            return []
        history = [line.strip().split(',') for line in file.readlines()]
    return history


def save_user_history(updated):
    with open(PATH_HISTORY, 'w') as file:
        try:
            for num, entry in enumerate(updated):
                # if num > MAX_RECENT_FILE:
                #     break
                line = f'{entry[0]},' + ','.join([item.strip() for item in entry[1:][0]]) + '\n'
                file.write(line)
        except Exception as exception:
            print(f'error when writing history: \'{exception}\'')

def check_user_history():
    print('    Loading history: ', end='')
    history = get_user_history()
    updated = []
    for load_array in history:
        load_type = load_array[0]
        first_file = load_array[1].strip()
        try:
            if Path(first_file).exists():
                updated.append((int(load_type), load_array[1:]))
        except PermissionError:
            """"""
    save_user_history(updated)
    print(f'{len(updated)} valid entries loaded ({len(history) - len(updated)} removed).')
    return updated


PATH_REGEX = Path(USERDIR, 'sequence_style.ini')
SEQUENCE_STYLES = {
    'MSI DLS': r'ROI\d+',
    'Fizeau run': r'[_-]p\d+$',
    'MSI run': r'[_-]\d+$',
    'Last parenthesis': r'\(\d+\)$',
    'Last position': r'\d+$',
}

def save_user_regex(updated):
    with open(PATH_REGEX, 'w') as file:
        file.write('# pattern name, regex in raw format\n')
        try:
            for key, val in updated.items():
                file.write(f'{key}, {val}\n')
        except Exception as exception:
            print(f'error when writing history: \'{exception}\'')

def check_user_regex():
    print('    Loading sequence style: ', end='')
    if not PATH_REGEX.exists():
        with open(PATH_REGEX, 'w') as file:
            try:
                file.write('# pattern name, regex in raw format\n')
                file.writelines([f'{key}, {val}\n' for key, val in SEQUENCE_STYLES.items()])
            except Exception as exception:
                print(f'error when writing sequence styles: \'{exception}\'')
    else:
        with open(PATH_REGEX, 'r') as file:
            lines = [line.strip() for line in file.readlines()]
        for line in lines:
            if line.startswith('#'):
                continue
            try:
                style_name, regex_str = line.split(',')
            except ValueError:
                continue
            SEQUENCE_STYLES[style_name.strip()] = rf'{regex_str.strip()}'
        print(f'{len(SEQUENCE_STYLES)} sequence styles loaded.')
    return SEQUENCE_STYLES


MATERIALS = { # name: (rho, young's modulus)
    'custom': (0, 0),
    'ULE':(2205, 6.80e10),
    'Glidcop':(8900, 1.30e11),
    'SiO2':(2510, 7.00e10),
    'SiC_CVD':(3210, 4.66e11),
    'Si-Ansys':(2330, 1.66e11),
    'Si-100':(2330, 1.30e11),
    'Si-110':(2330, 1.68e11),
    'Si-111':(2330, 1.88e11),
    'Zerodur':(2530, 9.10e10),
    'Beryllium':(1850, 3.03e11),
    'Pyrex':(2230, 6.55e10),
    'Graphite':(2225, 3.30e10),
    }

def _add_test_import(filepath, import_str):
    with open(filepath) as file:
        lines = file.readlines()
    if import_str in lines:
        return
    last_import_line = len(lines)
    comment = False
    for li, line in enumerate(lines):
        if line.strip().startswith('"""'):
            comment = not comment
            continue
        if line.strip().startswith('#') or comment:
            continue
        if 'import' in line and not comment:
            last_import_line = li
    lines.insert(last_import_line, import_str)
    if VERBOSE:
        print(f'_test.py import added to {filepath}')
    with open(filepath, 'w') as file:
        file.writelines(lines)
    return

def check_user_materials(user_path=None):
    if user_path is None:
        user_path = USERDIR
    user_settings = Path(user_path, 'materials.ini')

    if not user_settings.exists():
        with open(user_settings, 'w+') as file:
            try:
                file.writelines('# physical properties used in the gravity model\n')
                file.writelines('# material name, density in kg/m3, Young\'s modulus\n')
                for name, values in MATERIALS.items():
                    file.writelines(f'{name}, {values[0]}, {values[1]}\n')
            except Exception as exception:
                print(f'error when creating user materials: \'{exception}\'')
        return MATERIALS

    with open(user_settings, 'r+') as file:
        for line in file.readlines():
            try:
                if line.startswith('#'):
                    continue
                user_material, user_rho, user_young = line.strip().split(',')
                if user_material not in list(MATERIALS.keys()):
                    MATERIALS[user_material] = (float(user_rho), float(user_young))
            except Exception:
                continue
    return MATERIALS


INSTRUMENTS = {'marana11': 'height',}

def check_user_instruments(user_path=None):
    if user_path is None:
        user_path = USERDIR
    user_instruments = Path(user_path, 'NXinstruments.ini')

    if not user_instruments.exists():
        with open(user_instruments, 'w+') as file:
            try:
                for instrument, datatype in INSTRUMENTS.items():
                    file.writelines(f'{instrument}, {datatype}\n')
            except Exception as exception:
                print(f'error when creating user NXinstruments: \'{exception}\'')
        return INSTRUMENTS

    with open(user_instruments, 'r+') as file:
        try:
            for line in file.readlines():
                if line.startswith('#'):
                    continue
                instrument, datatype = line.strip().split(',')
                if instrument not in list(INSTRUMENTS.keys()):
                    INSTRUMENTS[instrument] = datatype
        except Exception as exception:
            print(f'error when importing user NXinstruments: \'{exception}\'')
    return INSTRUMENTS


def check_user_folder(user_path=None):
    if user_path is None:
        user_path = USERDIR
    root = Path(globals()['__file__']).parent
    root = Path(root, 'plugins')

    plugins_path = Path(user_path, 'plugins')
    if not plugins_path.exists():
        if VERBOSE:
            print(f'user folder created at the location {root}')
        plugins_path.mkdir()
    sys.path.append(str(plugins_path))

    readers_path = Path(plugins_path, 'data_readers')
    if not readers_path.exists():
        readers_path.mkdir()
    tgt_init = Path(readers_path, '__init__.py')
    if not tgt_init.exists():
        shutil.copy(Path(root, readers_path.stem, '__init__.py'), tgt_init)
    else:
        _add_test_import(tgt_init, 'from ._test import XYZ_Reader  # <-- import simple example\n')
    tgt_test = Path(readers_path, '_test.py')
    if not tgt_test.exists():
        shutil.copy(Path(root, readers_path.stem, '_test.py'), tgt_test)

    algorithms_path = Path(plugins_path, 'stitching_algorithms')
    if not algorithms_path.exists():
        algorithms_path.mkdir()
    tgt_init = Path(algorithms_path, '__init__.py')
    if not tgt_init.exists():
        shutil.copy(Path(root, algorithms_path.stem, '__init__.py'), tgt_init)
    else:
        _add_test_import(tgt_init, 'from ._test import TestStitching\n')
    tgt_test = Path(algorithms_path, '_test.py')
    if not tgt_test.exists():
        shutil.copy(Path(root, algorithms_path.stem, '_test.py'), tgt_test)

    globals()['READERS'] = readers_path
    globals()['ALGORITHMS'] = algorithms_path

    custom_widgets_path = Path(plugins_path, 'custom_widgets')
    if not custom_widgets_path.exists():
        custom_widgets_path.mkdir()
    tgt_init = Path(custom_widgets_path, '__init__.py')
    if not tgt_init.exists():
        buf = "# coding=utf-8\n"
        with open(tgt_init, 'w') as init:
            init.write(buf)
            print('    ' + 'writing __init__.py in ' + str(custom_widgets_path) + '\n')


READERS = None
ALGORITHMS = None
