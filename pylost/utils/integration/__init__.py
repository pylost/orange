# coding=utf-8
"""
2D integration algorithms
"""

# import numpy as np
# from scipy.interpolate import interp2d
# from pylost.utils.integration.frankot_chellappa import frankot_chellappa
# from pylost.utils.integration.sylvester import g2s
#
# def integrate_slopes_XY_2D(sx, sy, x=None, y=None, pix_sz=[1, 1], interpolate_nans=True, method='frankot_chellappa'):
#     """
#     Integrate slopes_x, slopes_y images to heights with 2D integration.
#
#     :param sx: Slopes x nd array
#     :type sx: np.ndarray / MetrologyData
#     :param sy: Slopes y nd array
#     :type sy: np.ndarray / MetrologyData
#     :param x: X positions
#     :type x: np.ndarray
#     :param y: Y positions
#     :type y: np.ndarray
#     :param pix_sz: Pixel size
#     :type pix_sz: list[Quantity]
#     :param interpolate_nans: Interpolate nans
#     :type interpolate_nans: bool
#     :param method: Integration method in ['frankot_chellappa', 'sylvester']
#     :type method: str
#     :return: Heights
#     :rtype: np.ndarray / Quantity
#     """
#     z = np.full_like((sx.value if isinstance(sx, Quantity) else sx), np.nan)
#     msk = ~np.isnan(sx) & ~np.isnan(sy)
#     if not msk.any():
#         return z
#     v = np.transpose(np.nonzero(msk))
#     mn = np.min(v, axis=0)
#     mx = np.max(v, axis=0)
#     m = (slice(mn[0], mx[0] + 1), slice(mn[1], mx[1] + 1))  # np.ix_(msk.any(axis=1),msk.any(axis=0))
#     if isinstance(pix_sz[0], Quantity) and isinstance(pix_sz[1], Quantity):
#         pix_unit = [x.unit for x in pix_sz]
#         pix_sz = [x.to(pix_unit[0]).value for x in pix_sz]
#     sx_m = sx.value[m] if isinstance(sx, Quantity) else sx[m]
#     sy_m = sy.value[m] if isinstance(sy, Quantity) else sy[m]
#     if (not np.any(sx_m)) or (not np.any(sy_m)):
#         return z
#
#     (ny, nx) = sx.shape
#     if x is None:
#         x = np.linspace(0, nx - 1, num=nx) * pix_sz[0]
#     if y is None:
#         y = np.linspace(0, ny - 1, num=ny) * pix_sz[1]
#     x_m = x[m[1]]
#     y_m = y[m[0]]
#     if interpolate_nans:
#         xx, yy = np.meshgrid(x_m, y_m)
#         sx_nans = np.isnan(sx_m)
#         sy_nans = np.isnan(sy_m)
#         if sx_nans.any():
#             f = interp2d(xx[~sx_nans], yy[~sx_nans], sx_m[~sx_nans])
#             sx_m = f(x_m, y_m)
#         if sy_nans.any():
#             f = interp2d(xx[~sy_nans], yy[~sy_nans], sy_m[~sy_nans])
#             sy_m = f(x_m, y_m)
#
#     if method == 'sylvester':
#         z[m] = g2s(x_m, y_m, sx_m, sy_m)
#     elif method == 'frankot_chellappa':
#         z[m] = - 0.5 * (pix_sz[0] + pix_sz[1]) * frankot_chellappa(sx_m, sy_m, reflec_pad=True)
#
#     if isinstance(sx, Quantity):
#         z = (z * pix_unit[0] * sx.unit).to('nm', equivalencies=u.dimensionless_angles())
#     return z
