# coding=utf-8
"""
methods to be called in through multithreaded.Worker or multithreaded.WorkerThread

To be used in the 'Executor', each front-end method should return the processed data.

"""

import numpy as np
from scipy.linalg import lstsq
from scipy.interpolate import interpn, griddata, RBFInterpolator
from scipy.ndimage import map_coordinates
from pylost.utils.integration.frankot_chellappa import frankot_chellappa
from pylost.utils.integration.sylvester import g2s

from silx.gui.colors import Colormap, registerLUT

from pylost.data.pyopticslab.generic import Surface, Profile, u
from pylost.utils.psd import spline_interpolate
# from zernike import RZern#, FitZern

from scipy.fft import fftfreq, rfft, irfft, rfft2, irfft2
from scipy.ndimage import gaussian_filter, gaussian_filter1d, median_filter
from scipy.interpolate import make_interp_spline

# from pylost import timeit

cartesians = ('piston', 'plane', 'tiltX', 'tiltY', 'sphere', 'cylinder', 'cylX', 'cylY', 'twist')

_central_value = Surface._central_value

# def zernike(data:Surface, n=6):
#     cart = RZern(n)
#     # data.change_values_unit(u.m)
#     xv, yv = data.meshgrid
#     xv = xv / np.max(xv)
#     yv = yv / np.max(yv)
#     cart.make_cart_grid(xv, yv)
#     c1 = cart.fit_cart_grid(data.values)
#     phi = cart.eval_grid(c1[0], matrix=True)
#     data.values = data.values - phi
#     return data, c1


def bin_data(data:[Surface, Profile], bins, stat_method):
    return data.bin_data(bins, stat_method)


def _median_filter(data:[Surface, Profile], filter_size):
    if np.any(np.isnan(data.values)):
        data.values = spline_interpolate(data.values)
    sampling = data.coords_unit.SI_unit(data.pixel_size, data.coords_unit)
    if data.is_2D:
        sampling = sampling[0]
    filter_size = int(0.001 * filter_size / sampling)
    data.values = median_filter(data.values, size=filter_size)
    return data


def _gaussian_filter(data:[Surface, Profile], sigma):
    if np.any(np.isnan(data.values)):
        data.values = spline_interpolate(data.values)
    if data.is_2D:
        func = gaussian_filter
    else:
        func = gaussian_filter1d
    data.values = func(data.values, sigma=sigma)
    return data


def _fft_filtering(data:[Surface, Profile], low_freq, high_freq):
    if not np.isfinite(low_freq) or not np.isfinite(high_freq):
        return data

    if np.any(np.isnan(data.values)):
        data.values = spline_interpolate(data.values)

    sampling = data.pixel_size
    invalid = np.isnan(data.values)
    if data.is_2D:
        im_fft = rfft2(data.values)
        col, row = im_fft.shape
        freq_x = np.abs(fftfreq(col, sampling[0]).real)
        mask_x = np.logical_and(freq_x >= low_freq, freq_x <= high_freq)
        mask_x[0] = mask_x[1]
        mask_x = np.tile(mask_x, (row, 1)).T
        freq_y = np.abs(fftfreq(row, sampling[1]).real)
        mask_y = np.logical_and(freq_y >= low_freq, freq_y <= high_freq)
        mask_y[0] = mask_y[1]
        mask_y = np.tile(mask_y, (col, 1))
        mask = np.logical_and(mask_x, mask_y)
        data.values = irfft2(np.where(mask, im_fft, 0), s=invalid.shape).real
    else:
        im_fft = rfft(data.values)
        freq_x = np.abs(fftfreq(len(im_fft), sampling).real)
        mask_x = np.logical_and(freq_x > low_freq, freq_x < high_freq)
        mask_x[0] = mask_x[1]
        data.values = irfft(np.where(mask_x, im_fft, 0), n=len(invalid.len)).real
    data.values[invalid] = np.nan
    return data


def _moving_average(data:[Surface, Profile], win_x, win_y):
    from scipy.signal import convolve, convolve2d
    if data.is_2D:
        data.values = convolve2d(data.values, np.ones((win_x, win_y)), 'same') / (win_x * win_y)
    else:
        data.values = convolve(data.values, np.ones(win_x), 'same') / win_x
    return data


def filtering(data:[Surface, Profile], func:str, **kwargs):
    func = globals().get('_' + func)
    if func is None:
        return
    data = func(data, **kwargs)
    if data.is_1D:
        if len(data.values) != len(data.coords):
            data.coords = data.coords[1:]
    else:
        if data.values.shape[0] != len(data.coords[0]):
            data.coords[0] = data.coords[0][1:]
        if data.values.shape[1] != len(data.coords[1]):
            data.coords[1] = data.coords[1][1:]
    return data


def rotate(data:[Surface, Profile], rotation_angle:float):
    data.rotate(rotation_angle)
    return data


def spline_interpolation(data:[Surface, Profile], print_warning=False):
    data.values = spline_interpolate(data.values, print_warning=print_warning)  # TODO: keep superflat method ?
    return data


def moving_average(data:[Surface, Profile], window_size):
    if data.is_1D:
        data.values = np.asfarray([np.nanmean(data.values[idx:idx + window_size]) for idx in range(len(data))])


def interpolate(data:[Surface, Profile], func='scipy_griddata', method='cubic', **kwargs):
    """ func: 'scipy_griddata', 'scipy_interp', 'bsplines' """
    # TODO: check method (linear?)
    new_sampling = kwargs.get('new_sampling', None)
    force_length = kwargs.get('force_length', None)

    if data.is_1D:
        data.center_coordinates()
        length = force_length if force_length is not None else data.length
        size = int(length // new_sampling)
        new_coords = np.linspace(0, length, num=size, endpoint=False)
        new_coords -= np.nanmean(new_coords)
        method = 'interp' if data.is_heights else 'cubicbspline'
        return data.interpolation(new_coords, method=method, k=2, copy=False)

    # 2D data   TODO: to be moved to generic.py
    print_warning = kwargs.get('print_warning', False)
    input_shape = data.shape
    data = spline_interpolation(data, False)
    pixel_size = data.pixel_size

    # binning before interpolating
    bins = [int(res[0] // res[1]) for res in zip(new_sampling, pixel_size)]
    if bins[0] * bins[1] > 1:
        bin_data(data, bins, 'nanmean')

    num_samples = np.round(np.divide(data.length, new_sampling), 0).astype(int)
    xi = np.linspace(0, num_samples[0] * new_sampling[0], num=num_samples[0], endpoint=False, dtype=np.float64)
    xi -= np.nanmean(xi)
    yi = np.linspace(0, num_samples[1] * new_sampling[1], num=num_samples[1], endpoint=False, dtype=np.float64)
    yi -= np.nanmean(yi)
    if func == 'map_coordinates':
        # https://docs.scipy.org/doc/scipy/tutorial/interpolate/ND_regular_grid.html#uniformly-spaced-data
        x_coords = np.arange(num_samples[0]) * data.shape[0] / num_samples[0]
        y_coords = np.arange(num_samples[1]) * data.shape[1] / num_samples[1]
        meshgrid = np.meshgrid(x_coords, y_coords, indexing='ij')
        new = map_coordinates(data.values, np.array(list(zip(meshgrid[0].ravel(), meshgrid[1].ravel()))).T, mode='mirror')
    else:
        meshgrid = np.meshgrid(xi, yi, indexing='ij')
        new_coords = np.array(list(zip(np.ravel(meshgrid[0]), np.ravel(meshgrid[1]))))
        if func == 'scipy_griddata':
            mesh = data.meshgrid
            coords = np.array(list(zip(np.ravel(mesh[0]), np.ravel(mesh[1]))))
            new = griddata(coords, data.values.ravel(), new_coords, method=method)
        elif func == 'scipy_RBFInterpolator':
            mesh = data.meshgrid
            coords = np.asfarray((mesh[0].ravel(), mesh[1].ravel()))
            new_coords = np.asfarray((meshgrid[0].ravel(), meshgrid[1].ravel()))
            new = RBFInterpolator(coords, data.values)(new_coords)
        else:
            new = interpn(data.coords, data.values, new_coords, method=method, bounds_error=False)

    new = new.reshape((len(xi), len(yi)))

    if force_length is not None:
        xnum = np.round(np.divide(force_length, new_sampling), 0).astype(int)
        xnew = np.linspace(0, xnum[0] * new_sampling[0], num=xnum[0], endpoint=False, dtype=np.float64)
        xnew -= np.nanmean(xnew)
        tmp = []
        empty = [np.nan] * xnum[0]
        for row in new.T:
            mask = np.isfinite(row)
            if np.all(~mask):
                tmp.append(empty)
                continue
            spline = make_interp_spline(xi[mask], row[mask], k=3, bc_type='natural', check_finite=False)(xnew)
            tmp.append(spline)
        data.coords = [xnew, yi]
        data.values = np.asfarray(tmp).T
    else:
        data.coords = [xi, yi]
        data.values = new
    data._pixel_res = u.m(new_sampling, data.coords_unit)

    if print_warning:
        print("input shape", input_shape)
        print("output shape", data.shape)

    return data


def integrate(data:[Surface, Profile], method_1D='cubicbspline', method_2D='Frankot-Chellappa'):
    if data.is_2D:
        if not data.is_slopes:
            return
        data.x_slopes.level_data()
        data.y_slopes.level_data()
        factor = data.z_unit.SI_unit(1, data.z_unit) * data.coords_unit.SI_unit(1, data.coords_unit)

        # no interpolation
        # data.values = np.multiply(g2s(data.y, data.x, data.y_slopes, data.x_slopes, N=13), factor)

        # interpolation
        new_sampling = np.divide(data.pixel_size, 4)
        num_samples = np.round(np.divide(data.length, new_sampling), 0).astype(int)
        xi = np.linspace(min(data.coords[0]), max(data.coords[0]), num=num_samples[0], endpoint=False, dtype=np.float64)
        yi = np.linspace(min(data.coords[1]), max(data.coords[1]), num=num_samples[1], endpoint=False, dtype=np.float64)
        meshgrid = np.meshgrid(xi, yi, indexing='ij')
        new_coords = np.array(list(zip(np.ravel(meshgrid[0]), np.ravel(meshgrid[1]))))
        func = 'interpn' # 'griddata'
        method = 'nearest' # 'cubic', 'slinear'
        meshgrid = data.meshgrid
        coords = np.array(list(zip(np.ravel(meshgrid[0]), np.ravel(meshgrid[1]))))

        if func == 'griddata':
            method = 'nearest'
            x_slopes = griddata(coords, data.x_slopes.values.ravel(), new_coords, method=method)
            y_slopes = griddata(coords, data.y_slopes.values.ravel(), new_coords, method=method)
        else:
            x_slopes = interpn(data.coords, data.x_slopes.values, new_coords, method=method,
                               bounds_error=False).reshape((len(xi), len(yi)))
            y_slopes = interpn(data.coords, data.y_slopes.values, new_coords, method=method,
                               bounds_error=False).reshape((len(xi), len(yi)))

        x_slopes = spline_interpolate(x_slopes.reshape((len(xi), len(yi))))
        y_slopes = spline_interpolate(y_slopes.reshape((len(xi), len(yi))))

        if 'Grad2Surf' in method_2D:
            values = np.multiply(g2s(yi, xi, y_slopes, x_slopes, N=13), factor)
        else:
            values = np.multiply(frankot_chellappa(y_slopes, x_slopes), factor * -new_sampling[0])

        if func == 'griddata':
            data.values = griddata(new_coords, values.ravel(), coords, method=method).reshape(data.shape)
        else:
            values = spline_interpolate(values)
            old_coords = np.array(list(zip(np.ravel(meshgrid[0]), np.ravel(meshgrid[1]))))
            data.values = interpn([xi, yi], values, old_coords, method='cubic',
                                  bounds_error=False).reshape(data.shape)

        data.units['values'] = data.values_unit.si_length
        data.auto_units()
        data.x_slopes, data.y_slopes = None, None
        return data

    # 1D data
    data.integral(method_1D, copy=False, create_initial=False)
    return data


def gravity_correction(data:[Surface, Profile], length, thickness, distance, rho, young, orientation='faceup'):
    coefs = {'rho':rho, 'young':young}
    model = data.gravity_correction(length, thickness, distance, material='custom', orientation=orientation, **coefs)
    return model, data


def preprocessing(data:[Surface, Profile], **options):
    if options.get('binning', False):
        bins = options.get('bins', (1, 1))
        stat_method = options.get('stat_method', 'nanmean')
        bins = bins[0] if data.is_1D else bins
        return data.bin_data(bins, stat_method, copy=True)
    return data


# noinspection PyUnusedLocal
def ellipse_removal(data:[Surface, Profile], terms:list, optimize=True, optimization=('q', 'theta'), rotation=False, **kwargs):
    # res = preprocessing(data, **kwargs)
    p, q, theta = terms
    # _ = data.radius
    data.ellipse_removal(p, q, theta, optimize=optimize, optimization=optimization, rotation=rotation)
    return data


def _calc_stats(data:[Surface, Profile], terms:dict, coefs:list):
    factor_radius = data.coords_unit(1, data.coords_unit.SI_unit)
    factor_angle = np.power(factor_radius, -2)
    data.piston = coefs[-1]
    if terms.get('tiltY', False):
        data.tilt_y = data.units['angle'](coefs[-2] if data.is_2D else 0 * factor_angle, u.rad)
        if data.is_slopes:
            data.rc_y = data.radius_unit(1 / coefs[-2] * factor_radius if data.is_2D else 0, u.m)
    if terms.get('tiltX', False):
        idx = -3 if data.is_2D and terms.get('tiltY', False) else -2
        data.tilt_x = data.units['angle'](coefs[idx] * factor_angle, u.rad)
        if data.is_slopes:
            data.rc_x = data.radius_unit(1 / coefs[idx] * factor_radius, u.m)
    if terms.get('sphere', False) and data.is_heights:
        idx = -4 if data.is_2D else -3
        data.rc_x = data.radius_unit(1 / (2 * coefs[idx]) * factor_radius, u.m)
    elif terms.get('cylinder', False):
        A, B, C = 0, 0, 0
        siz = len(coefs)
        if terms.get('cylinder', False) and terms.get('cylY', False):
            C = coefs[-siz + 2]
        if terms.get('cylinder', False) and terms.get('cylX', False) and terms.get('cylY', False):
            B = coefs[-siz + 1] if terms.get('cylY', False) else coefs[-siz + 2]
        if terms.get('cylinder', False) and terms.get('cylX', False):
            A = coefs[-siz + 0] if terms.get('cylY', False) else coefs[-siz + 1]
        theta = np.arctan(B / (A - C)) / 2
        if np.fabs(A) > np.fabs(C):
            if theta < 0:
                theta -= np.pi / 2
            elif theta > 0:
                theta += np.pi / 2
        data.rot_xy = u.deg(theta, u.rad)
        Arot = (A * np.cos(theta) * np.cos(theta)
                + B * np.cos(theta) * np.sin(theta)
                + C * np.sin(theta) * np.sin(theta))
        data.majcyl = data.radius_unit(1 / (2 * Arot) * factor_radius, u.m) if abs(Arot) > 1e-25 else 0
        theta += np.pi / 2
        Arot = (A * np.cos(theta) * np.cos(theta)
                + B * np.cos(theta) * np.sin(theta)
                + C * np.sin(theta) * np.sin(theta))
        data.mincyl = data.radius_unit(1 / (2 * Arot) * factor_radius, u.m) if abs(Arot) > 1e-25 else 0
        if data.is_heights:
            data.rc_x = data.radius_unit(1 / (2 * A) * factor_radius, u.m) if abs(A) > 1e-25 else 0
            data.rc_y = data.radius_unit(1 / (2 * C) * factor_radius, u.m) if abs(C) > 1e-25 else 0


# @timeit
def _fit_res2D(z, coefs, coords_mats):
    return z - np.reshape(np.sum(np.multiply(coords_mats, coefs), axis=1), z.shape)


def _cartesian_fit2D(meshgrid_x, meshgrid_y, z, terms:dict):
    meshgrid_x = meshgrid_x.ravel()
    if meshgrid_y is not None:
        meshgrid_y = meshgrid_y.ravel()
    else:
        meshgrid_y = 0
    M = ()
    if terms.get('piston', True):
        M = (np.ones_like(meshgrid_x, dtype=np.float64),)
    if terms.get('plane', True) and (terms.get('tiltY', True) or terms.get('cylY', False)):
        M = (meshgrid_y,) + M
    if terms.get('plane', True) and (terms.get('tiltX', True) or terms.get('cylX', False)):
        M = (meshgrid_x,) + M
    if terms.get('sphere', False):
        M = (np.add(np.power(meshgrid_x, 2), np.power(meshgrid_y, 2)),) + M
    if terms.get('cylinder', False) and terms.get('cylY', False):
        M = (np.power(meshgrid_y, 2),) + M
    if terms.get('cylinder', False) and terms.get('cylX', False) and terms.get('cylY', False):
        M = (np.multiply(meshgrid_x, meshgrid_y),) + M
    if terms.get('cylinder', False) and terms.get('cylX', False):
        M = (np.power(meshgrid_x, 2),) + M
    M = np.column_stack(M)
    z = z.ravel()
    mask = np.isnan(z)
    return lstsq(M[~mask], z[~mask])[0], M


def cartesian_removal(data:[Surface, Profile], terms:dict, center_coordinates=True, **kwargs):
    if center_coordinates:
        data.center_coordinates()
    res = preprocessing(data, **kwargs)
    if res.is_1D:
        terms['tiltX'] = terms['plane']
        terms['tiltY'] = False
        terms['cylinder'] = False
        terms['cylX'] = False
        terms['cylY'] = False
        terms['twist'] = False
    coefs, coords_mat = _cartesian_fit2D(*res.meshgrid, res.values, terms)
    data.values = _fit_res2D(data.values, coefs, coords_mat)
    data.fit = 'cartesian'
    data.fit_terms = terms
    data.fit_coefs = coefs
    set_auto_units(data)
    _calc_stats(data, terms, coefs)
    return data


# @timeit
# def _get_coords_mat_meshgrid(meshgrid_x, meshgrid_y, deg_max, deg_min, axis='both'):
#     meshgrid_x = meshgrid_x.ravel() if 'x' in axis or 'both' in axis else 1
#     meshgrid_y = meshgrid_y.ravel() if 'y' in axis or 'both' in axis else 1
#     M = np.zeros((meshgrid_x.size, (deg_max - deg_min + 1)**2))
#     ij = [[i, j] for i in range(deg_min, deg_max + 1) for j in range(deg_min, deg_max + 1)]
#     for k, (i, j) in enumerate(ij):
#         M[:, -k] = np.multiply(np.power(meshgrid_x, i), np.power(meshgrid_y, j))
#     return M

# @timeit
def _get_coords_mat_1d(x, deg_max, deg_min, axis='x'):
    num_coeffs = (deg_max - deg_min + 1)**2 if 'both' in axis else deg_max - deg_min + 1
    M = np.zeros((len(x), num_coeffs))
    for k, deg in enumerate(range(deg_min, deg_max + 1)):
        M[:, k] = np.power(x, deg)
    return M


# @timeit
def _get_coords_mat_2d(x, y, deg_max, deg_min, axis='both'):
    num_coeffs = (deg_max - deg_min + 1)**2 if 'both' in axis else deg_max - deg_min + 1
    M = np.zeros((len(x), len(y), num_coeffs))

    orders = range(max(deg_min, 1), deg_max + 1)
    xx = [np.power(x, i) for i in orders] if 'x' in axis or 'both' in axis else None
    yy = [np.power(y, j) for j in orders] if 'y' in axis or 'both' in axis else None

    if 'both' in axis:
        ij = [[i, j] for i in range(deg_min, deg_max + 1) for j in range(deg_min, deg_max + 1)]
    else:
        ij = [[b if 'x' in axis else None, b if 'y' in axis else None] for b in range(deg_min, deg_max + 1)]
    for k, (i, j) in enumerate(ij):
        if i == 0 or i is None:
            if j == 0 or j is None:
                res = 1
            else:
                res = np.tile(yy[orders.index(j)], (len(x), 1))
        else:
            if j == 0 or j is None:
                res = np.tile(xx[orders.index(i)], (len(y), 1)).T
            else:
                res = np.multiply(np.tile(xx[orders.index(i)], (len(y), 1)).T, np.tile(yy[orders.index(j)], (len(x), 1)))
        M[:, :, k] = res
    return np.reshape(M, (len(x)*len(y), -1))


# @timeit
def _multipoly_fit(coords_mat, z):
    z = z.ravel()
    mask = np.isnan(z)
    coefs, _, _, _ = np.linalg.lstsq(coords_mat[~mask], z[~mask], rcond=None)
    return coefs


def poly_cartesians_removal(data:[Surface, Profile], terms:dict, center_coordinates=True, **kwargs):
    if center_coordinates:
        data.center_coordinates()
    # data.units_to_SI()
    res = preprocessing(data, **kwargs)
    if res.is_1D:
        axis = 'x'
    else:
        bw = terms.get('x_axis', True) + 2 * terms.get('y_axis', True)
        axis = ('no axis', 'x', 'y', 'both')[bw]
    deg_max = terms.get('deg_max', 2)
    deg_min = terms.get('deg_min', 0)
    if res.is_2D:
        coords_mat = _get_coords_mat_2d(*res.coords, deg_max, deg_min, axis)
    else:
        coords_mat = _get_coords_mat_1d(res.x, deg_max, deg_min)
    coefs = _multipoly_fit(coords_mat, res.values)
    data.values = _fit_res2D(data.values, coefs, coords_mat)
    data.fit = 'poly_cart'
    data.fit_terms = terms
    data.fit_coefs = coefs
    x_axis = axis in ('x', 'both')
    y_axis = axis in ('y', 'both')
    terms = {'piston': deg_min == 0,
             'plane': deg_min < 2 and deg_max > 0, 'tiltX': x_axis, 'tiltY': y_axis,
             'cylinder': deg_min < 3 and deg_max > 1 if res.is_2D else False, 'cylX': x_axis, 'cylY': y_axis,
             'sphere': deg_min < 3 and deg_max > 1 if res.is_1D else False, 'twist': False}
    _calc_stats(data, terms, coefs)
    set_auto_units(data)
    return data


def _plane_fit2D(x, y, z):
    z = z.ravel()
    one = np.ones((len(z),), dtype=np.float64)
    x = x.ravel()
    y = y.ravel()
    M = np.array([x, y, one]).T
    mask = np.isnan(z)
    return lstsq(M[~mask], z[~mask])[0]


def _plane_res2D(coef, x, y, z):
    A, B, C = coef
    return z - (A * x + B * y + C)


def _sphere_fit2D(x, y, z):
    z = z.ravel()
    one = np.ones_like(z, dtype=np.float64)
    x = x.ravel()
    y = y.ravel()
    M = np.array([x, y, one]).T
    M = np.c_[(x * x + y * y), M]
    mask = np.isnan(z)
    return lstsq(M[~mask], z[~mask])[0]


def _conic_fit2D(x, y, z):
    z = z.ravel()
    one = np.ones_like(z, dtype=np.float64)
    x = x.ravel()
    y = y.ravel()
    M = np.array([x, y, one]).T
    M = np.c_[x * x, x * y, y * y, M]
    mask = np.isnan(z)
    return lstsq(M[~mask], z[~mask])[0]


def _conic_res2D(coef, x, y, xx, xy, yy, z):
    A, B, C, D, E, F = coef
    return z - (A * xx + B * xy + C * yy + D * x + E * y + F)


def mean_removal(data:[Surface, Profile], center_coordinates=True, auto_units=True):
    """"""
    if center_coordinates:
        data.center_coordinates()
    data.mean_removal()
    if auto_units:
        set_auto_units(data)
    return data


def level_data(data:[Surface, Profile], center_coordinates=True, auto_units=True):
    """"""
    if center_coordinates:
        data.center_coordinates()
    data.level_data(auto_units=auto_units)
    return data


def plane_removal(data:[Surface, Profile], center_coordinates=True, auto_units=True):
    """"""
    if center_coordinates:
        data.center_coordinates()
    x, y = data.meshgrid
    pla = _plane_fit2D(x, y, data.values)
    data.values = _plane_res2D(pla, x, y, data.values)
    if auto_units:
        set_auto_units(data)
    return data


def cylinder_removal(data:[Surface, Profile], center_coordinates=True, auto_units=True):
    """"""
    if center_coordinates:
        data.center_coordinates()
    x, y = data.meshgrid
    cyl = _conic_fit2D(x, y, data.values)
    data.values = _conic_res2D(cyl, x, y, x * x, x * y, y * y, data.values)
    if auto_units:
        set_auto_units(data)
    return data


def set_auto_units(data:[Surface, Profile]):
    data.change_coords_unit(u.mm)
    data.auto_values_unit()
    data.auto_radius_unit()
    return data


def minimal_processing(data:[Surface, Profile], center_coordinates=True, auto_units=True):
    if data.is_slopes:
        mean_removal(data, center_coordinates=center_coordinates, auto_units=auto_units)
        return
    level_data(data, center_coordinates=center_coordinates, auto_units=auto_units)
    _, _ = data.radius, data.radius_sag


def convert_colormap(data, **params):
    """
    params: dict
        base: 'ra', 'rq', 'pv', 'fixed'
        if stats based:  'zfac' as stretching factor (<0 to force default)
        if manual scale: 'zmin, zmax, z_lo, z_hi'
    """
    class Dummy:
        def __init__(self, values, kind='heights'):
            self.values = np.array(values[~np.isnan(values)]).ravel()
            self.kind = kind

        def calc_colormap(self, **cmap_params):
            colorscale = Surface.ColorScale(self, **cmap_params)
            return colorscale.cmap, colorscale.cmap_prms

        def automasking(self, threshold=None):
            if threshold is None:
                threshold = 3.0 * self.rq
            return np.where(np.fabs(self.values - self.median) < threshold, self.values, np.nan)

        @property
        def valid_size(self):
            return len(self.values)

        @property
        def ra(self):
            return np.sum(np.fabs(self.values)) / len(self.values)

        @property
        def rq(self):
            return np.std(self.values)

        @property
        def rp(self):
            return np.max(self.values)

        @property
        def rv(self):
            return np.min(self.values)

        @property
        def median(self):
            return np.median(self.values)

    if not isinstance(data, Surface):
        datakind = params.pop('kind', 'heights')
        data = Dummy(data, datakind)

    cmap_name = 'veeco'
    name = params.pop('cmap', 'turbo')
    base = params.pop('base', 'ra')
    default = {'ra': 2.0, 'rq': 3.0, 'pv': 12.0, 'fixed': 2.0}
    factor = params.pop('zfac', default[base])
    if params.pop('slopes', False):
        cmap_name = 'slopes'
        name = 'Greys_r'
        factor = 4.7  # good view for slopes
    lut, params = data.calc_colormap(cmap=name, base=base, zfac=factor, **params)
    registerLUT(name=cmap_name, colors=lut([n / 255 for n in range(0, 256)]))
    del data
    return Colormap(name=cmap_name, vmin=params['zmin'], vmax=params['zmax']), params
