# coding=utf-8
"""
https://www.mathworks.com/matlabcentral/fileexchange/60817-surface-generator-artificial-randomly-rough-surfaces
"""

import numpy as np
from scipy.fft import ifftshift, ifft2


def art_surf(m, n, sampling, sigma, H, S=-2.2, qr=0, qr_exp=0.65, qr_x=0, qr_y=0, qc=0, qc_exp=1.25):
    m = m - 1 if m % 2 == 1 else m
    n = n - 1 if n % 2 == 1 else n

    two_pi = 2 * np.pi
    two_pi_pow2 = np.power(two_pi, 2)

    # Wavevectors (note that q = (2*pi) / lambda; where lambda is wavelength
    qx = np.linspace(-np.pi, np.pi, m, endpoint=False) / sampling
    qy = np.linspace(-np.pi, np.pi, n, endpoint=False) / sampling
    qx[m//2] = 1e-15
    qy[n//2] = 1e-15
    qxx, qyy = np.meshgrid(np.power(qx, 2), np.power(qy, 2), indexing='ij')
    rho = np.sqrt(qxx + qyy)

    # 2D matrix of Cq values
    fractal_dim = float(S * (H + 1))
    Cq = np.power(rho, fractal_dim)

    # roll-off
    if qr > 0:
        if abs(qr_exp) > 0:
            Cq = np.where(rho < two_pi / qr, np.power(Cq, qr_exp), Cq)

    # if abs(qr_exp) > 0:
    #     if qr_x > 0:
    #         x0 = Cq[:, n//2]
    #         Cq[:, n//2] = np.where(np.abs(qx) < two_pi / qr_x, np.power(x0, qr_exp), x0)
    #     if qr_y > 0:
    #         y0 = Cq[m//2, :]
    #         Cq[m//2, :] = np.where(np.abs(qy) < two_pi / qr_y, np.power(y0, qr_exp), y0)

        else:
            roll_off = np.power(two_pi / qr, fractal_dim)
            Cq = np.where(rho < two_pi / qr, roll_off, Cq)

    # cut-off
    if qc > 0:
        qc = two_pi / qc
        # cut_off = np.power(qc, fractal_dim)
        Cq = np.where(rho > qc, np.power(Cq, qc_exp), Cq)

    # applying rms
    Cq[m//2, n//2] = 0 # remove mean
    length = m * sampling
    width = n * sampling
    rms_f2d = np.sqrt(np.sum(Cq) * (two_pi_pow2 / (length * width)))
    alfa = sigma / rms_f2d
    Cq = Cq * np.power(alfa, 2)

    # PSD to fft
    Bq = np.sqrt(Cq / (np.power(sampling, 2) / (n * m * two_pi_pow2)))

    # conjugate symmetry to magnitude
    Bq[1, 0] = 0
    Bq[m//2, 0] = 0
    Bq[m//2, n//2] = 0
    Bq[0, n//2] = 0
    Bq[1:m//2, 1:] = np.flip(Bq[m//2+1:, 1:], (0, 1))
    Bq[1:m//2, 0] = np.flip(Bq[m//2+1:, 0])
    Bq[1:, 1:n//2] = np.flip(Bq[1:, n//2+1:], (0, 1))
    Bq[0, 1:n//2] = np.flip(Bq[0, n//2+1:])

    # random phase between -pi and pi
    phi = two_pi * np.random.random((m, n)) - np.pi

    # conjugate symmetry to phase
    phi[1, 0] = 0
    phi[m//2, 0] = 0
    phi[m//2, n//2] = 0
    phi[0, n//2] = 0
    phi[1:m//2, 1:] = -np.flip(phi[m//2+1:, 1:], (0, 1))
    phi[1:m//2, 0] = -np.flip(phi[m//2+1:, 0])
    phi[1:, 1:n//2] = -np.flip(phi[1:, n//2+1:], (1, 0))
    phi[0, 1:n//2] = -np.flip(phi[0, n//2+1:])

    # Generate topography very randomly ;) --> force pseudo-random initialization ?
    a = Bq * np.cos(phi) * np.random.random((m, n)) * 8 * np.random.random((m, n)) * np.random.random((m, n))
    a[a == 0] = 1e-15

    b = Bq * np.sin(phi) * np.random.random((m, n)) * 8 * np.random.random((m, n)) * np.random.random((m, n))
    b[b == 0] = 1e-15

    Hm = a + 1j * b # Complex Hm with 'Bq' as abosulte values and 'phi' as phase components

    # phase components
    z = np.abs(ifft2(ifftshift(Hm)))

    return z, Hm
