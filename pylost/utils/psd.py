# coding=utf-8
"""

"""

import scipy
import numpy as np
from scipy.fft import fftfreq, fft, fft2  # TODO switch to rfft
# from .nfft import nfft_adjoint
from scipy.signal import periodogram#, detrend as sig_detrend
from scipy.signal.windows import blackman, hann, hamming, blackmanharris, kaiser, tukey

from pylost.data.pyopticslab.generic import Surface, Profile, u


WINDOWS = ['None', 'Blackman', 'Hanning', 'Hamming', 'Blackman-Harris', 'Kaiser', 'Tukey']


def get_window(window:int, num_points:int, **prms):
    w = np.linspace(0, 1, num_points)
    if window == 1:
        w = blackman(num_points)
    elif window == 2:
        w = hann(num_points)
    elif window == 3:
        w = hamming(num_points)
    elif window == 4:
        w = blackmanharris(num_points)
    elif window == 5:
        w = kaiser(num_points, prms.get('kaiser_beta', 10))
    elif window == 6:
        w = tukey(num_points, prms.get('tukey_alpha', 0.2))
    return w


def windowing(data, window:int, axis=0, **prms):
    """"""
    if window == 0:
        return data, np.full_like(data, 1)
    w = get_window(window, num_points=data.shape[axis], **prms)
    if data.ndim == 1:
        return np.multiply(data, w), w
    return np.multiply(data, np.tile(w, (data.shape[axis - 1], 1)).T), w


def calc_csp(freq, psd, freq_min=0, freq_max=0, flip_integration=False):
    if psd is None:
        return None
    q = np.isfinite(freq)
    if freq_min != 0:
        q = np.multiply(q, freq >= freq_min * 1e3)
    if freq_max != 0:
        q = np.multiply(q, freq <= freq_max * 1e3)
    csp = np.where(q, psd, 0)
    if flip_integration:
        csp = np.flip(csp)
    csp = np.sqrt(np.cumsum(csp) * (freq[1] - freq[0]))
    if flip_integration:
        csp = np.flip(csp)
    return csp


def psd_1d(data:[Surface, Profile], window:int = 0, **prms):
    """"""
    SI_unit = data.values_unit.SI_unit
    T = u.m(data.pixel_size, data.coords_unit)
    N = data.shape[0]
    halfN = N // 2 + 1
    windata, _ = windowing(SI_unit(data.values, data.values_unit), window, **prms)
    windata = fft(windata, axis=0)
    if windata.ndim == 1:
        freq = abs(fftfreq(N, T)[:halfN])
        windata = (2.0 * T / N) * np.abs(windata[:halfN]) ** 2
    else:
        freq = abs(fftfreq(N, T[0])[:halfN])
        windata = np.nanmean((2.0 * T[0] / N) * np.abs(windata[:halfN, :]) ** 2, axis=1)
    if data.is_heights:
        slp = data.derivative()
        if slp.is_2D:
            slp = slp[0]
        slp.units_to_SI()
        slp, _ = windowing(slp.values, window, **prms)
        slp = fft(slp, axis=0)
        if slp.ndim == 1:
            slp = (2.0 * T / N) * np.abs(slp[:halfN]) ** 2
        else:
            slp = np.nanmean((2.0 * T[0] / N) * np.abs(slp[:halfN, :]) ** 2, axis=1)
        return freq[1:], windata[1:], slp[1:]
    else:
        return freq[1:], None, windata[1:]

def psd_2d_linewise(data:Surface, window:int, **prms):
    """"""
    if data.is_1D:
        return psd_1d(data, window, **prms)
    SI_unit = data.values_unit.SI_unit
    T = u.m(data.pixel_size, data.coords_unit)
    Nx, Ny = data.shape
    halfNx = Nx // 2 + 1
    halfNy = Ny // 2 + 1
    wx = get_window(window, Nx, **prms)
    wy = get_window(window, Ny, **prms)
    windata = np.multiply(SI_unit(data.values, data.values_unit), np.tile(wx, (Ny, 1)).T)
    windata = np.multiply(windata, np.tile(wy, (Nx, 1)))
    if np.any(np.isnan(windata)):
        windata = spline_interpolate(windata)
    windata = fft2(windata)
    freq = abs(fftfreq(Nx, T[0])[:halfNx])
    windata = np.nanmean((2.0 * T[0] / (Nx * Ny)) * np.abs(windata[:halfNx, :halfNy]) ** 2, axis=1)
    # return freq[1:], windata[1:], None
    if data.is_heights:
        slp = data.derivative()[0]
        slp.units_to_SI()
        slp = np.multiply(slp.values, np.tile(wx, (Ny, 1)).T)
        slp = np.multiply(slp, np.tile(wy, (Nx, 1)))
        if np.any(np.isnan(slp)):
            slp = spline_interpolate(slp)
        slp = fft2(slp)
        slp = np.nanmean((2.0 * T[0] / (Nx * Ny)) * np.abs(slp[:halfNx, :halfNy]) ** 2, axis=1)
        return freq[1:], windata[1:], slp[1:]
    else:
        return freq[1:], None, windata[1:]


# def psd_2d_areal(data:Surface, window:int, **prms):
#     """"""


def psd_superflat(data:Surface, window:int, method='scipy', **prms):
    """ https://gitlab.synchrotron-soleil.fr/OPTIQUE/leaps/superflat_scripts """
    #   @author François Polack, Uwe Flechsig
    #   @copyright 2022, Superflat-PCP project  <mailto:superflat-pcp@synchrotron-soleil.fr>
    #   @version 0.1.0

    SI_unit = data.values_unit.SI_unit
    if isinstance(data, Profile):
        xf = 1.0 / u.m(data.pixel_size, data.coords_unit)  # sampling rate in m
        # if data.is_slopes:
        #     data = data.integral()
    else:
        xf = 1.0 / u.m(data.pixel_size[0], data.coords_unit)  # sampling rate in m
    # compute from heights
    if method == 'scipy':
        windata, _ = windowing(SI_unit(data.values, data.values_unit), window, **prms)
        # windata = SI_unit(data.values.copy(), data.values_unit) * get_window(window, data.shape[0], **prms)
        if not np.all(data.valid_mask):
            # print("\nWARNING --  ROI contains invalid values  -- WARNING\n            Results might be inaccurate")
            print("\n --  ROI contains invalid values  -- Using spline interpolation\n")
            # slope PSD frequency array
            sf, h2psd = periodogram(spline_interpolate(windata), xf, window='boxcar', return_onesided=True, axis=0)
        else:
            # one sided psd with tukey (0.2) taper along fast axis
            sf, h2psd = periodogram(windata, xf, window='boxcar', return_onesided=True, axis=0)
    # elif method == 'NFFT':
    #     sf, h2psd = holey_periodogram(data.values, xf, window=window, return_onesided=True)
    else:
        print("invalid method. Cannot compute the PSD")
        return
    # print("FToutput type", h2psd.dtype)

    if data.is_1D:
        h1psd = h2psd
    else:
        h1psd = np.mean(h2psd, axis=1)  # average over slow varying axis

    if data.is_slopes:
        return sf[1:], None, h1psd[1:]
    else:
        s1psd = h1psd * (2 * np.pi * sf) ** 2  # height to slope in fourier space
        return sf[1:], h1psd[1:], s1psd[1:]

    # compute from slopes
    # (sf, s2psd) = scipy.signal.periodogram(self.dzdx, wf, window=('tuckey',0.2),
    # return_onesided=True)  # one sided psd with tukey (0.2) taper
    # s1psd = np.mean(s2psd, axis=0)    # average over l
    # self.sf = sf

    # uncomment to reverse cumulation direction
    #  s1csd = np.cumsum(s1psd[::-1])[::-1] * (self.sf[1] - self.sf[0])
    # s1csd = np.cumsum(s1psd) * (sf[1] - sf[0])
    # srms = np.sqrt(s1csd)
    # print("Total cumulated rms =", srms[srms.shape[0] - 1] * 1e0, "µrad")


# # def holey_periodogram(data, fs=1.0, window='boxcar', FFTlength=None, detrend='constant', return_onesided=True,
# #                       scaling='density', axis=-1):
# def holey_periodogram(data, window=6, fs=1.0, FFTlength=None, detrend='constant', return_onesided=True,
#                       scaling='density', axis=-1, **prms):
#
#     windata = data * get_window(window, data.shape[1], **prms)
#     xbase = np.linspace(-0.5, 0.5, num=data.shape[1], endpoint=False)
#     # print(xbase)
#     if FFTlength:
#         N = 2 * (FFTlength // 2)
#     else:
#         N = 2 * (data.shape[1] // 2)
#     N2 = N // 2
#     F = np.ma.empty(dtype=np.cdouble, shape=(data.shape[0], N))
#     for row in range(data.shape[0]):
#         segment = np.ma.array(sig_detrend(windata[row, :], type=detrend), mask=windata[row, :].mask)
#         num_invalid = segment[segment.mask].size
#         if num_invalid:
#             print(row, ":", num_invalid, "invalid data points")
#             if num_invalid > 0.2 * data.shape[1]:
#                 print("   too many datapoints, line will be skipped from computation")
#                 F[row, :] = np.ma.masked_all(shape=(N,))
#                 continue
#         x = xbase[~ segment.mask]
#         y = segment[~ segment.mask]
#         # F[row,:]=np.ma.zeros(shape=(N,))
#         F[row, :] = nfft_adjoint(x, y, N, sigma=20, tol=1E-8, m=None, kernel='bspline')
#         # if num_invalid :
#         # print (x.shape,y.shape)
#
#     if scaling == 'density':
#         if return_onesided:
#             psd = np.square(np.abs(F[:, N2:])) * 2 / (N * fs)
#             psd[:, 0] = np.square(np.abs(F[:, N2])) / (N * fs)
#             return np.linspace(0, 0.5 * fs, num=N2, endpoint=False), psd
#         else:
#             return np.linspace((-0.5 + 1 / N) * fs, 0.5 * fs, num=N2, endpoint=True), np.square(np.abs(F)) / (N * fs)
#     elif scaling == 'spectrum':
#         # Not shure this is what scipy.signal calls spectrum
#         if return_onesided:
#             return np.linspace(0, 0.5 * fs, num=N2, endpoint=False), np.square(np.abs(F[:, N2:]) / N)
#         else:
#             return np.linspace((-0.5 + 1 / N) * fs, 0.5 * fs, num=N2, endpoint=True), np.square(np.abs(F) / N)

def spline_interpolate(data, print_warning=False):
    #   @author François Polack, Uwe Flechsig
    #   @copyright 2022, Superflat-PCP project  <mailto:superflat-pcp@synchrotron-soleil.fr>
    #   @version 0.1.0
    is_1d = data.ndim == 1
    if is_1d == 1:
        data = np.atleast_2d(data)
    else:
        data = np.transpose(data)
    xbase = np.arange(0., data.shape[1], dtype=float)
    outrow = 0
    invalids = 0
    outdata = np.ma.empty(dtype=float, shape=data.shape)
    for inrow in range(data.shape[0]):
        segment = np.ma.array(data[inrow, :], mask=np.isnan(data[inrow, :]))
        if segment.mask[0]:
            segment[0] = 0
            segment.mask[0] = False
        if segment.mask[-1]:
            segment[-1] = 0
            segment.mask[-1] = False

        num_invalid = segment[segment.mask].size
        if num_invalid:
            if print_warning:
                print(inrow, ":", num_invalid, "invalid data points")
            if num_invalid > 0.2 * data.shape[1]:
                if print_warning:
                    print("   too many datapoints, line will be skipped from computation")
                invalids += data.shape[1]
                continue

            invalids += num_invalid

            x = xbase[~ segment.mask]
            y = segment[~ segment.mask]
            # defining 1st derivative null at ends (periodic is not working)
            spline3 = scipy.interpolate.make_interp_spline(x, y, bc_type=([(1, 0.0)], [(1, 0.0)])) # 'clamped'
            outdata[outrow, :] = spline3(xbase, extrapolate='periodic')
        else:
            outdata[outrow, :] = data[inrow, :]
        outrow += 1
    np.ma.resize(outdata, (outrow,))
    if print_warning:
        print("percent of invalid data=", 100 * invalids / (data.shape[0] * data.shape[1]))
        print("input shape", data.shape)
        print("output shape", outdata.shape)
    if is_1d == 1:
        return np.ma.getdata(outdata[0, :]).copy()
    return np.transpose(np.ma.getdata(outdata)).copy()
