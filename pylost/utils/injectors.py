# coding=utf-8

import numpy as np
from pathlib import Path
from collections import OrderedDict
from silx.gui import qt

from pylost.user.settings import get_user_history

class Injector:
    """ used fo applying modified methods requiring 'self' """
    def __init__(self, obj):
        self.obj = obj

    def _updateMinMax(self):
        """ silx.gui.plot.ColorBar.ColorScaleBar """
        if self.obj.minVal is None:
            text, tooltip = '', ''
        else:
            if self.obj.minVal == 0 or 0 <= np.log10(abs(self.obj.minVal)) < 7:
                text = '%.2f' % self.obj.minVal
            else:
                text = '%.2e' % self.obj.minVal
            tooltip = repr(self.obj.minVal)

        self.obj._minLabel.setText(text)
        self.obj._minLabel.setToolTip(tooltip)

        if self.obj.maxVal is None:
            text, tooltip = '', ''
        else:
            if self.obj.maxVal == 0 or 0 <= np.log10(abs(self.obj.maxVal)) < 7:
                text = '%.2f' % self.obj.maxVal
            else:
                text = '%.2e' % self.obj.maxVal
            tooltip = repr(self.obj.maxVal)

        self.obj._maxLabel.setText(text)
        self.obj._maxLabel.setToolTip(tooltip)

    def getValueFromRelativePosition(self, value):
        """Return the value in the colorMap from a relative position in the
        ColorScaleBar (y)

        :param value: float value in [0, 1]
        :return: the value in [colormap['vmin'], colormap['vmax']]
        """
        colormap = self.obj.getColormap()
        if colormap is None:
            return

        value = np.clip(value, 0., 1.)
        normalizer = colormap._getNormalizer()
        normMin, normMax = normalizer.apply([self.obj.vmin, self.obj.vmax], self.obj.vmin, self.obj.vmax)

        norm = normalizer.revert(normMin + (normMax - normMin) * value, self.obj.vmin, self.obj.vmax)

        return f'{norm:.2f}'

    # "C:\ProgramData\miniconda3\envs\pylost\Lib\site-packages\silx\gui\plot\actions\io.py"
    # noinspection PyUnusedLocal, PyUnresolvedReferences
    def _actionTriggered(self, checked=False):
        """Handle save action."""
        # Set-up filters
        filters = OrderedDict()

        # Add image filters if there is an active image
        if self.obj.plot.getActiveImage() is not None:
            filters.update(self.obj._filters['image'].items())

        # Add curve filters if there is a curve to save
        if (self.obj.plot.getActiveCurve() is not None or
                len(self.obj.plot.getAllCurves(withhidden=False)) == 1):
            filters.update(self.obj._filters['curve'].items())
        if len(self.obj.plot.getAllCurves(withhidden=False)) >= 1:
            filters.update(self.obj._filters['curves'].items())

        # Add scatter filters if there is a scatter
        # todo: CSV
        if self.obj.plot.getScatter() is not None:
            filters.update(self.obj._filters['scatter'].items())

        filters.update(self.obj._filters['all'].items())

        # Create and run File dialog
        dialog = qt.QFileDialog(self.obj.plot)
        dialog.setOption(qt.QFileDialog.DontUseNativeDialog)
        dialog.setWindowTitle("Output File Selection")
        dialog.setModal(1)
        dialog.setNameFilters(list(filters.keys()))

        # add last directory target
        history = get_user_history()
        dialog.setDirectory(str(Path(history[0][-1]).parent))

        dialog.setFileMode(qt.QFileDialog.AnyFile)
        dialog.setAcceptMode(qt.QFileDialog.AcceptSave)

        def onFilterSelection(filt_):
            # disable overwrite confirmation for NXdata types,
            # because we append the data to existing files
            if filt_ in self.obj._appendFilters:
                dialog.setOption(qt.QFileDialog.DontConfirmOverwrite)
            else:
                dialog.setOption(qt.QFileDialog.DontConfirmOverwrite, False)

        dialog.filterSelected.connect(onFilterSelection)

        if not dialog.exec():
            return False

        nameFilter = dialog.selectedNameFilter()
        filename = dialog.selectedFiles()[0]
        dialog.close()

        if '(' in nameFilter and ')' == nameFilter.strip()[-1]:
            # Check for correct file extension
            # Extract file extensions as .something
            extensions = [ext[ext.find('.'):] for ext in
                          nameFilter[nameFilter.find('(') + 1:-1].split()]
            for ext in extensions:
                if (len(filename) > len(ext) and
                        filename[-len(ext):].lower() == ext.lower()):
                    break
            else:  # filename has no extension supported in nameFilter, add one
                if len(extensions) >= 1:
                    filename += extensions[0]

        # Handle save
        func = filters.get(nameFilter, None)
        if func is not None:
            return func(self.obj.plot, filename, nameFilter)
        else:
            # _logger.error('Unsupported file filter: %s', nameFilter)
            return False
