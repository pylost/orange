# coding=utf-8
"""
"""
import numpy as np
from sympy import lambdify, parse_expr, symbols

from pylost.data.pyopticslab.generic import Surface, u
from pylost.data.pyopticslab.ellipse import Ellipse
from .rand_surf_PSD import art_surf


def lambdify_equation(eqn):
    if not eqn:
        return None
    try:
        x, y = symbols('x y')
        equation = parse_expr(eqn)  # , transformations='all')
        return lambdify([x, y], equation)
    except Exception:
        ...

def randomize():
    """"""
    # length = np.round(10 ** np.random.uniform(1, 3), 4)
    # width = np.round(np.random.uniform(5, 50), 4)
    # res_x = res_y = np.round(10 ** np.random.uniform(-2, 0), 4)
    # radius = np.round(10 ** np.random.uniform(-2, 0), 4)
    # radius_tan = np.round(10 ** np.random.uniform(-2, 0), 4)
    # radius_sag = np.round(10 ** np.random.uniform(-2, 0), 4)
    # pv_err = np.round(10 ** np.random.uniform(-2, 0), 4)
    # rms_err = np.round(10 ** np.random.uniform(-2, 0), 4)
    #
    # sub_length = np.round(np.min([length / 2, 150.0]), 4)
    # sub_width = np.round(width, 4)
    # step_x = np.round(sub_length * np.random.uniform(0.05, 0.4), 4)
    # step_y = 0.0


# def

def generate_surface(**mirror_prms):
    units = {'coords': u.mm, 'values': u.nm, 'angle': u.urad, 'length': u.mm, 'radius': u.m, 'pixel': u.um, }
    px = int(round(1e3 * mirror_prms['size_x'] / mirror_prms['res_x'], 0))
    py = int(round(1e3 * mirror_prms['size_y'] / mirror_prms['res_y'], 0))
    x = np.arange(px) * mirror_prms['res_x'] * 1e-3 # mm
    y = np.arange(py) * mirror_prms['res_y'] * 1e-3 # mm
    surf = Surface(coords=[x, y], values=np.zeros((px, py)), units=units, source='Simulated surface')
    surf.center_coordinates()
    surf.units_to_SI()
    meshgrid = surf.meshgrid

    fft = None

    if mirror_prms['errors'] == 1:
        # fe = np.random.random((px, py))
        # surf.values += fe * mirror_prms['pv_errors'] * 1e-9 / (np.max(fe) - np.min(fe))
        z, fft_out = art_surf(px, py, surf.pixel_size[0], mirror_prms['rms_errors'] * 2e-9,
                              mirror_prms['hurst_exp'], mirror_prms['psd_slope'],
                              mirror_prms['roll_off'] * 1e-3, mirror_prms['roll_off_exp'],
                              mirror_prms['low_x'] * 1e-3, mirror_prms['low_y'] * 1e-3,
                              mirror_prms['cut_off'] * 1e-3, mirror_prms['cut_off_exp'], )
        surf.values = z
        if fft_out is not None:
            fft = surf.duplicate()
            fft.values = np.log(np.abs(fft_out))
    elif mirror_prms['errors'] == 2: # equation
        func = lambdify_equation(mirror_prms['equ_errors'])
        if func is None:
            return None
        surf.values = surf.values_unit(func(*np.multiply(meshgrid, 1e3)), units['values'])
        if mirror_prms['noise_errors'] > 0:
            surf.values += np.random.normal(scale=np.sqrt(np.max(surf.values) * 1e-9 * mirror_prms['noise_errors']),
                                            size=surf.shape)

    if mirror_prms['shape'] == 1:
        ctan = 1 / mirror_prms['roc_x'] if np.abs(mirror_prms['roc_x']) > 0 else 0.0
        csag = 1 / mirror_prms['roc_y'] if np.abs(mirror_prms['roc_y']) > 0 else 0.0
        z = 0.5 * ctan * np.power(meshgrid[0], 2) + 0.5 * csag * np.power(meshgrid[1], 2)

    elif mirror_prms['shape'] == 2: # ellipse
        theoretical = Ellipse.from_parameters(surf.x, 'height', mirror_prms['p'], mirror_prms['q'],
                                              mirror_prms['theta'] * 1e-3)
        z = np.tile(theoretical, (surf.shape[1], 1)).T

    elif mirror_prms['shape'] == 3: # sphere
        R = mirror_prms['roc']  # in meters
        r = np.sqrt(np.power(meshgrid[0], 2) + np.power(meshgrid[1], 2))
        z = R - np.sqrt(np.power(R, 2) - np.power(r, 2))  # if r<<R, h = r**2/2R

    elif mirror_prms['shape'] == 4: # equation
        func = lambdify_equation(mirror_prms['equ_shape'])
        if func is None:
            return None
        z = func(*meshgrid)

    else:
        z = 1

    surf.values += z

    surf.mean_removal()
    surf.auto_units()
    return surf, fft


def generate_subapertures(**sub_ref_prms):
    """"""
    return None, None
