# coding=utf-8
"""
pylost multithreading module

https://rednafi.github.io/digressions/python/2020/04/21/python-concurrent-futures.html
https://www.pythonguis.com/tutorials/multithreading-pyqt-applications-qthreadpool/
"""

import sys
import traceback
import concurrent.futures as pool
from AnyQt.QtCore import QObject, QRunnable, pyqtSignal, pyqtSlot, QThread
from time import sleep, perf_counter_ns

# import multiprocessing
# MAX_WORKERS = 2 * multiprocessing.cpu_count() + 1


class Signals(QObject):
    past = pyqtSignal(float)
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(float)
    interrupt = pyqtSignal(str)
    finished = pyqtSignal()


class WorkerThread(QThread):
    def __init__(self, parent, fn, *args, **kwargs):
        QThread.__init__(self, parent)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = Signals()

    def cancel(self):
        self.signals.interrupt.emit('aborted')
        self.terminate()
        self.wait()
        self.signals.finished.emit()
        self.deleteLater()

    def run(self):
        try:
            result = self.fn(*self.args, **self.kwargs)
            self.signals.result.emit(result)
        except Exception as e:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, e))
        self.signals.finished.emit()
        self.deleteLater()


class Executor:
    def __init__(self, fn, data,
                 verbose=False,
                 cancel_signal=None,
                 use_progressbar=True,
                 progress_signal=None,
                 progressbar_inc=None,
                 *args, **kwargs):
        self.fn = fn
        self.data = data
        self.verbose = verbose
        self.signals = Signals()
        self.finished = False
        self.result = []
        self.tic = 0
        self.toc = 0
        self.worker = Worker(fn, data, *args, **kwargs)
        # signals
        self.worker.signals.result.connect(self._handle_results)
        self.worker.signals.finished.connect(self._thread_complete)
        if cancel_signal is not None:
            self.signals.interrupt = cancel_signal
        self.signals.interrupt.connect(self.worker.cancel)
        if use_progressbar and progress_signal is not None:
            inc = progressbar_inc
            if progressbar_inc is None:
                inc = 100 / len(data)
            self.worker.set_increment(inc)
            self.worker.signals.progress = progress_signal

    def _wait_for(self, timeout=2):
        runningtime = 0
        tempo = 0.01
        while not self.finished:
            sleep(tempo)
            runningtime += tempo
            if runningtime > timeout:
                if self.verbose:
                    print('executor timeout')
                break

    def start(self, timeout=2):
        self.toc = perf_counter_ns()
        self.worker.run()
        self._wait_for(timeout)
        del self.worker
        return self.result

    def _handle_results(self, processed):
        self.tic = perf_counter_ns()
        if self.verbose:
            print(f'   ({self.fn.__name__}: {len(processed)} elements processed in {(self.tic-self.toc) * 1e-6:.3f}ms)')
        self.result = processed

    def _thread_complete(self):
        self.finished = True


class Worker(QRunnable):

    def __init__(self, fn, data, timeout=60, *args, **kwargs):
        # data must be iterable : [list, tuple, zip] or other class with __iter__ method
        super(Worker, self).__init__()
        self.setAutoDelete(True)
        self.fn = fn
        self.data = data
        self.canceled = False
        self.signals = Signals()
        self.inc = kwargs.pop('increment', 100 / len(data))
        self.timeout = timeout
        self.verbose = kwargs.pop('verbose', False)
        self.args = args
        self.kwargs = kwargs

    def set_increment(self, inc):
        self.inc = inc

    def cancel(self):
        self.canceled = True

    @pyqtSlot(name='multithreaded')
    def run(self):
        results = []
        try:
            if self.verbose:
                print('starting executor...', self.fn)
            futures_list = []
            # with pool.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
            with pool.ThreadPoolExecutor() as executor:
                for item in self.data:
                    futures = executor.submit(self.fn, item, *self.args, **self.kwargs)
                    if self.canceled:
                        executor.shutdown(wait=False, cancel_futures=True)
                        cancelled = futures.cancelled()
                        msg = 'futures failed to cancel'
                        if cancelled:
                            msg = 'futures cancelled'
                        raise UserWarning(msg)
                    futures_list.append(futures)
                for future in futures_list:
                    if self.canceled:
                        executor.shutdown(wait=False, cancel_futures=True)
                        cancelled = futures.cancelled()
                        msg = 'task aborted, futures failed to cancel'
                        if cancelled:
                            msg = 'task aborted, futures cancelled'
                        raise UserWarning(msg)
                    try:
                        result = future.result(timeout=self.timeout)
                        results.append(result)
                        self.signals.progress.emit(self.inc)
                    except Exception as e:
                        results.append(None)
                        raise e
                self.signals.past.emit(100)
        except UserWarning as e:
            self.signals.interrupt.emit(str(e))
        except Exception:
            # print('error during gathering results', self.fn, e)
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(results)
        finally:
            self.signals.finished.emit()
