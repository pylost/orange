# coding=utf-8
"""
scikit-image
Stéfan van der Walt, Johannes L. Schönberger, Juan Nunez-Iglesias, François Boulogne, Joshua D. Warner, Neil Yager,
Emmanuelle Gouillart, Tony Yu and the scikit-image contributors.
scikit-image: Image processing in Python. PeerJ 2:e453 (2014) https://doi.org/10.7717/peerj.453

https://scikit-image.org/docs/stable/auto_examples/registration/plot_register_translation.html

https://www.fabriziomusacchio.com/blog/2023-01-02-image_registration/
"""

import numpy as np

from scipy.signal import correlate

from pylost.utils.methods import _multipoly_fit, _fit_res2D, _get_coords_mat_2d
from pylost.utils.multithreading import Executor

scikit_present = True
try:
    # noinspection PyUnresolvedReferences
    from skimage.registration import phase_cross_correlation
except ImportError:
    scikit_present = False
    print('scikit-image not installed')


def temp(patches):
    both = []
    x = np.linspace(0, 1, num=patches[0].shape[0])
    y = np.linspace(0, 1, num=patches[1].shape[1])
    for data in patches:
        # data = np.array(spline_interpolate(data, print_warning=False))
        coords_mat = _get_coords_mat_2d(x, y, 2, 0)
        coefs = _multipoly_fit(coords_mat, data)
        both.append(_fit_res2D(data, coefs, coords_mat))
    res = []
    first, second = both[0][1:-1, 1:-1], both[1][1:-1, 1:-1]
    first = np.where(np.isfinite(first), first, 0)
    second = np.where(np.isfinite(second), second, 0)
    patches_shape = first.shape
    tests = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 0), (0, 1), (1, -1), (1, 0), (1, 1)]
    # test = np.multiply(tests, 2)
    for shifts in tests:
        sx = slice(-shifts[0], -shifts[0] + first.shape[0])
        sy = slice(-shifts[1], -shifts[1] + first.shape[1])
        ox = slice(shifts[0], shifts[0] + second.shape[0])
        oy = slice(shifts[1], shifts[1] + second.shape[1])
        step_x = ox.start - sx.start
        step_y = oy.start - sy.start
        slc_sx = slice(max(0, step_x), min(patches_shape[0], patches_shape[0] + step_x))
        slc_sy = slice(max(0, step_y), min(patches_shape[1], patches_shape[1] + step_y))
        slc_ox = slice(max(0, -step_x), min(patches_shape[0], patches_shape[0] - step_x))
        slc_oy = slice(max(0, -step_y), min(patches_shape[1], patches_shape[1] - step_y))
        res.append(correlate(first[slc_sx, slc_sy], second[slc_ox, slc_oy], mode='valid', method='auto'))
    print(np.argmin(res), np.argmax(res))
    return tests[np.argmax(res)]


def cross_correlations(patches, overlap_ratio=0.2):
    both = []
    x = np.linspace(0, 1, num=patches[0].shape[0])
    y = np.linspace(0, 1, num=patches[1].shape[1])
    for data in patches:
        # data = np.array(spline_interpolate(data, print_warning=False))
        coords_mat = _get_coords_mat_2d(x, y, 2, 0)
        coefs = _multipoly_fit(coords_mat, data)
        both.append(_fit_res2D(data, coefs, coords_mat))
    corrections, _, _ = phase_cross_correlation(*both,
                                                reference_mask=~np.isnan(patches[0]),
                                                moving_mask=~np.isnan(patches[1]),
                                                upsample_factor=1, overlap_ratio=overlap_ratio)
    return corrections


def optimize_XY(patches, shifts:tuple, pixel_size=None, progress_signal=None, progress_full=100):
    if not scikit_present:
        return None, shifts, pixel_size

    num_patches = len(patches)
    patches_shape = patches[0].shape

    progress_inc = progress_full / num_patches

    # position of each patch in the final array
    slices = []
    for p, patch in enumerate(patches):
        p = p - num_patches
        cx = slice(shifts[0][p], shifts[0][p] + patches[p].shape[0])
        cy = slice(shifts[1][p], shifts[1][p] + patches[p].shape[1])
        slices.append((cx, cy))

    # slice and regroup two consecutives patches
    binomials = []
    for idx in range(num_patches - 1):
        sx, sy = slices[idx]
        ox, oy = slices[idx + 1]
        step_x = ox.start - sx.start
        step_y = oy.start - sy.start
        slc_sx = slice(max(0, step_x), min(patches_shape[0], patches_shape[0] + step_x))
        slc_sy = slice(max(0, step_y), min(patches_shape[1], patches_shape[1] + step_y))
        slc_ox = slice(max(0, -step_x), min(patches_shape[0], patches_shape[0] - step_x))
        slc_oy = slice(max(0, -step_y), min(patches_shape[1], patches_shape[1] - step_y))
        binomials.append((patches[idx][slc_sx, slc_sy], patches[idx + 1][slc_ox, slc_oy]))

    # corrections = [temp(binomial) for binomial in binomials]
    corrections = Executor(cross_correlations, binomials, overlap_ratio=0.8,
                           progress_signal=progress_signal, progressbar_inc=progress_inc).start()

    full_shape_origin = (max(shifts[0]) + patches_shape[0], max(shifts[1]) + patches_shape[1])
    cumulated = np.cumsum([(0, 0)] + corrections, axis=0)
    new_pix_shift = (shifts[0] + cumulated[:, 0].astype(int), shifts[1] + cumulated[:, 1].astype(int))
    full_shape_corrected = (max(new_pix_shift[0]) + patches_shape[0], max(new_pix_shift[1]) + patches_shape[1])

    if pixel_size is not None:
        pixel_size = np.divide(full_shape_origin, full_shape_corrected) * pixel_size

    return corrections, new_pix_shift, pixel_size#, [pixel_size[0], pixel_size[0]]
