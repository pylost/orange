# coding=utf-8
"""
silx DataViewer customization

"""
from time import sleep
from silx.gui import qt

from .views import PylostPlot1D, PylostPlot2D, PylostMulti2D
from .legendwidget import PylostLegendDockWidget
from pylost.data import StitchingDataset

from pylost.orange.gui import QtVertical

import sys
import traceback
from pylost.utils.multithreading import QThread, pyqtSignal

class PylostSilxPlotViewer(qt.QMainWindow):

    sigFrameChanged = qt.Signal(int)

    # noinspection PyArgumentList
    def __init__(self, parent=None, mask=True, custom_toolbar=True, verbose=False):
        self.ow = parent
        self.verbose = verbose
        self._plot = None
        self._profileswidget = None
        self._data = None
        self._cmap = None
        self._offsets = [0, 0]
        self._preprocessing = None
        self._browser = None
        self._dockWidgets = []
        self._legendsDockWidget = None
        self._show_custom_toolbar = custom_toolbar
        self._show_mask = mask

        qt.QMainWindow.__init__(self, parent)
        # QThread.__init__(self, parent)
        if parent is not None:
            # behave as a widget
            self.setWindowFlags(qt.Qt.Widget)
        else:
            self.setWindowTitle('PylostSilxViewer')
            raise Exception('PylostSilxPlotViewer must have an Orange Widget as parent')

    def _updateFrameNumber(self, index):
        self.updateData(index)

    @property
    def mask_widget(self):
        if self._plot is not None:
            return self._plot.getMaskToolsDockWidget().widget()

    # noinspection PyArgumentList,PyUnresolvedReferences,PyAttributeOutsideInit
    def createWidgetFromData(self, surface:bool, length:int):
        # noinspection PyArgumentList
        plot = qt.QWidget(self)
        if surface:
            self._profileswidget = PylostPlot1D(parent=plot,
                                                mask=False,
                                                show_stats=False,
                                                verbose=self.verbose)
            self._plot = PylostPlot2D(parent=plot,
                                      mask=self._show_mask,
                                      cmap=self._cmap,
                                      custom_toolbar=self._show_custom_toolbar,
                                      show_stats=False,
                                      roi=False,
                                      profileswidget=self._profileswidget,
                                      verbose=self.verbose)
        else:
            self._plot = PylostPlot1D(parent=plot,
                                      mask=self._show_mask,
                                      show_stats=False,
                                      show_fit=False,
                                      verbose=self.verbose)

        # connect silx view widget to orange widget
        self.mask_widget.sigMaskSent.connect(self.ow.send_mask_params)
        self.mask_widget.sigInputsChanged.connect(self.ow.set_mask_attr)

        from silx.gui.widgets.FrameBrowser import HorizontalSliderWithBrowser
        self._browser_label = qt.QLabel("Stack index:")
        self._browser = HorizontalSliderWithBrowser(plot)
        self._browser.setRange(0, 0)
        self._browser.valueChanged[int].connect(self._updateFrameNumber)
        self._browser.setEnabled(False)
        self._browser._slider.setTickPosition(3)
        self._browser._slider.setTickInterval(1)

        splitter = qt.QSplitter(QtVertical)
        layout = qt.QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._plot, 0, 0, 1, 3)
        layout.addWidget(self._browser_label, 1, 0)
        layout.addWidget(self._browser, 1, 1)
        plot.setLayout(layout)
        splitter.addWidget(plot)

        if surface:
            splitter.addWidget(self._profileswidget)
            self._profileswidget.hide()

        if length < 2:
            self._browser.hide()
            self._browser_label.hide()

        self._legendsDockWidget = self.getLegendsDockWidget()
        self._legendsDockWidget.sigLegendRenamed.connect(self._plot.update_statistics)

        # TODO: add data info (PV, rms, etc.)

        # central_widget.setLayout(splitter)
        self.setCentralWidget(splitter)

    def show_mask_widget(self):
        if self._plot is not None:
            return self._plot.getMaskToolsDockWidget().show()

    # noinspection PyUnresolvedReferences
    def getLegendsDockWidget(self):
        """DockWidget with Legend panel"""
        if self._legendsDockWidget is None:
            self._legendsDockWidget = PylostLegendDockWidget(plot=self._plot)
            self._legendsDockWidget.hide()
            self._legendsDockWidget.toggleViewAction().triggered.connect(
                self._handleDockWidgetViewActionTriggered)
            self._legendsDockWidget.visibilityChanged.connect(
                self._handleFirstDockWidgetShow)
        return self._legendsDockWidget

    def addTabbedDockWidget(self, dock_widget):
        """Add a dock widget as a new tab if there are already dock widgets
        in the plot. When the first tab is added, the area is chosen
        depending on the plot geometry:
        if the window is much wider than it is high, the right dock area
        is used, else the bottom dock area is used.

        :param dock_widget: Instance of :class:`QDockWidget` to be added.
        """
        if dock_widget not in self._dockWidgets:
            self._dockWidgets.append(dock_widget)
        if len(self._dockWidgets) == 1:
            # The first created dock widget must be added to a Widget area
            width = self.ow.width()
            height = self.ow.height()
            if width > (1.25 * height):
                area = qt.Qt.RightDockWidgetArea
            else:
                area = qt.Qt.BottomDockWidgetArea
            self.addDockWidget(area, dock_widget)
        else:
            # Other dock widgets are added as tabs to the same widget area
            self.tabifyDockWidget(self._dockWidgets[0],
                                  dock_widget)

    def removeDockWidget(self, dockwidget):
        """Removes the *dockwidget* from the main window layout and hides it.

        Note that the *dockwidget* is *not* deleted.

        :param QDockWidget dockwidget:
        """
        if dockwidget in self._dockWidgets:
            self._dockWidgets.remove(dockwidget)
        super(PylostSilxPlotViewer, self).removeDockWidget(dockwidget)

    def _handleDockWidgetViewActionTriggered(self, checked):
        if checked:
            action = self.sender()
            if action is None:
                return
            dockWidget = action.parent()
            if dockWidget is None:
                return
            dockWidget.show()  # Show needed here for raise to have an effect
            dockWidget.raise_()

    def _handleFirstDockWidgetShow(self, visible):
        """Handle QDockWidget.visibilityChanged

        It calls :meth:`addTabbedDockWidget` for the `sender` widget.
        This allows to call `addTabbedDockWidget` lazily.

        It disconnect itself from the signal once done.

        :param bool visible:
        """
        if visible:
            dockWidget = self.sender()
            dockWidget.visibilityChanged.disconnect(
                self._handleFirstDockWidgetShow)
            self.addTabbedDockWidget(dockWidget)

    def clear(self):
        if self._plot is not None:
            self._plot.clear()
            self._plot.hide()
            self._legendsDockWidget.hide()
            self._plot = None
            self._legendsDockWidget = None
        # noinspection PyArgumentList
        self.setCentralWidget(qt.QWidget(self))

    def currentSurface(self):
        if isinstance(self._data, list):
            return self._data[self._browser.value()]
        else:
            return self._data

    def updateData(self, idx=0, fit_window=True):
        data = self._data[idx] if isinstance(self._data, list) else self._data
        if data.is_1D:
            self._plot.updateProfile(data, name='0.1', legend=data.source, fit_window=fit_window, replace=True)
        elif data.is_2D:
            self._plot.updateImage(data, fit_window=fit_window)
        else:
            raise ValueError('not a valid data')

    def setData(self, dataset:StitchingDataset, **kwargs):
        if dataset is None:
            self.clear()
            return
        self._offsets = dataset.data_offsets
        self._data = dataset.stitched if dataset.is_stitched else dataset.scans
        self.createWidgetFromData(dataset.is_2D, 1 if dataset.is_stitched else len(dataset))
        if self._plot is None:
            return
        self._plot.clear()
        if isinstance(self._data, list):
            self._browser.setRange(0, len(dataset) - 1)
            self._browser.setEnabled(len(dataset) > 1)
        self.updateData()


class PylostSilxComparisonViewer(PylostSilxPlotViewer):

    sigUpdateCaption = qt.Signal(str)

    def __init__(self, parent=None, mask=True, verbose=False, no_caption=False):
        super().__init__(parent=parent, mask=mask, verbose=verbose)
        self._profileswidget = None
        self._show_mask = mask
        self.noise_data = False
        if not no_caption:
            self.sigUpdateCaption.connect(self.ow._update_caption)

    # noinspection PyArgumentList,PyUnresolvedReferences,PyAttributeOutsideInit
    def createWidgetFromData(self, surface:bool, length:int, noise_data=False):
        # noinspection PyArgumentList
        central_widget = qt.QWidget(self)
        self._profileswidget = PylostPlot1D(parent=central_widget, mask=False, verbose=self.verbose)
        self._plot = PylostMulti2D(parent=central_widget,
                                   mask=self._show_mask,
                                   cmap=self._cmap,
                                   profileswidget=self._profileswidget,
                                   noise_data=noise_data,
                                   verbose=self.verbose)

        splitter = qt.QSplitter(QtVertical)
        layout = qt.QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._plot, 0, 0, 1, 3)
        central_widget.setLayout(layout)
        splitter.addWidget(central_widget)

        splitter.addWidget(self._profileswidget)
        # self._profileswidget.hide()

        self._legendsDockWidget = self.getLegendsDockWidget()
        self._legendsDockWidget.sigLegendRenamed.connect(self._plot.update_statistics)
        self._legendsDockWidget.show()

        # TODO: add data info (PV, rms, etc.)

        self.setCentralWidget(splitter)

    def remove_link(self, link, invalidated):
        if self._plot is not None:
            self._plot.removeLink(link, invalidated)
            if self._plot.isempty:
                self.clear()

    def setData(self, dataset:StitchingDataset, link=0, **kwargs):
        if dataset is None:
            self.clear()
            return

        self.noise_data = dataset.noise_data

        self._offsets = dataset.data_offsets
        self._data = [dataset.stitched] if dataset.is_stitched else dataset.scans
        if self._plot is None:
            self.createWidgetFromData(dataset.is_2D, 1 if dataset.is_stitched else len(dataset), dataset.noise_data)
        else:
            if self._plot.isempty:
                self.clear()
                self.sigUpdateCaption.emit('Cleared.')
                return

        if kwargs.get('threading', True):
            worker = MultiProcessThread(self.ow, self._plot, self._data, link)
            worker.error.connect(self.ow._handle_errors)
            worker.progress.connect(self._update_progress)
            worker.finished.connect(self._update_display)
            worker.run()
        else:
            for i, data in enumerate(self._data):
                if data.is_1D:
                    data = data.profile_to_surface(width=2, update_roc=False)
                self._plot.updateImage(data, link=link, subelement=i, legend=data.source)
            self._plot.updateDisplay()

    def _update_progress(self, value):
        self.sigUpdateCaption.emit(f'MultiView: processing image {value + 1}')

    def _update_display(self):
        # self.sigUpdateCaption.emit('MultiView: Processing display')
        self._plot.updateDisplay()
        self.sigUpdateCaption.emit('Noise Result' if self.noise_data else 'Multiview complete')
        self._plot.resetZoom()

class MultiProcessThread(QThread):
    error = pyqtSignal(tuple)
    progress = pyqtSignal(int)
    finished = pyqtSignal()

    def __init__(self, parent, plot, data, link):
        QThread.__init__(self, parent)
        self.plot = plot
        self.data = data
        self.link = link

    def update_plot(self):
        for i, data in enumerate(self.data):
            self.progress.emit(i)
            sleep(0.05)
            if data.is_1D:
                data = data.profile_to_surface(width=2, update_roc=False)
            self.plot.updateImage(data, link=self.link, subelement=i, legend=data.source, multiple=True)

    def run(self):
        try:
            self.update_plot()
            # self.signals.result.emit(result)
        except Exception as e:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.error.emit((exctype, value, e))
        self.finished.emit()
        self.deleteLater()
