# coding=utf-8

import re
from pathlib import Path

from Orange.widgets.gui import Qt, QtWidgets, QtCore

from AnyQt import uic
from AnyQt.QtCore import pyqtSignal

from silx.gui.qt import QLineEdit, QLocale, QDoubleValidator

from pylost import SEQUENCE_STYLES, save_user_regex

qt = Qt
QSize = QtCore.QSize
QLabel = QtWidgets.QLabel
QStyle = QtWidgets.QStyle
QWidget = QtWidgets.QWidget
QCheckBox = QtWidgets.QCheckBox
QComboBox = QtWidgets.QComboBox
QScrollArea = QtWidgets.QScrollArea
QSplitter = QtWidgets.QSplitter
QHBoxLayout = QtWidgets.QHBoxLayout
QVBoxLayout = QtWidgets.QVBoxLayout
QGridLayout = QtWidgets.QGridLayout
QSizePolicy = QtWidgets.QSizePolicy
QFileDialog = QtWidgets.QFileDialog
QtHorizontal = Qt.Horizontal
QtVertical = Qt.Vertical
# QTableView = QtWidgets.QTableView
QHeaderView = QtWidgets.QHeaderView
QTableWidget = QtWidgets.QTableWidget
QTableWidgetItem = QtWidgets.QTableWidgetItem
QMessageBox = QtWidgets.QMessageBox
QTreeWidget = QtWidgets.QTreeWidget
QTreeWidgetItem = QtWidgets.QTreeWidgetItem
QPlainTextEdit = QtWidgets.QPlainTextEdit
QModelIndex = QtCore.QModelIndex

class Spacer(QWidget):
    def __init__(self, *args, **kwargs):
        super(Spacer, self).__init__(*args, **kwargs)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)


# noinspection PyUnresolvedReferences
class FloatLineEdit(QLineEdit):
    """Field to edit a float value without arbitrary rounding

    :param parent: See :class:`QLineEdit`
    :param float value: The value to set the QLineEdit to.
    """
    def __init__(self, parent=None, value=None, fmt:str = '.4f', auto_dtype=True, maxwidth:int = None, callback=None):
        self.stored_value = 0
        self.format_string = fmt
        self.auto_dtype = auto_dtype
        self.callback = None

        QLineEdit.__init__(self, parent)

        validator = QDoubleValidator(self)
        # validator.setLocale(QLocale(QLocale.Spanish, QLocale.Spain))
        validator.setLocale(QLocale().system())
        self.setValidator(validator)

        self.comma = ',' in self.validator().locale().decimalPoint()

        self.setAlignment(Qt.AlignRight)

        if value is not None:
            self.setValue(value)

        self.callback = callback

        if maxwidth is not None:
            self.setMaximumSize(maxwidth, 23)

        # noinspection PyUnresolvedReferences
        self.editingFinished.connect(self.to_value)

    def update_format(self, fmt:str):
        self.format_string = fmt

    def to_value(self):
        """Set the QLineEdit current value as a float."""
        text = self.text()
        text = text.replace('.', ',') if self.comma else text.replace(',', '.')
        value, validated = self.validator().locale().toDouble(text)
        if validated:
            self.setValue(value)

    def value(self):
        """Return the stored value"""
        # text = self.text()
        # value, validated = self.validator().locale().toDouble(text)
        # if not validated:
        #     self.setValue(value)
        # # return value
        return self.stored_value

    def setValue(self, value, block_signal=False):
        """Set the current value to the stored value (choose wether int or float)

        :param float value: The value to set the QLineEdit to.
        :param bool block_signal: inhibit callback.
        """
        # super(FloatLineEdit, self).setValue(value)
        value = round(value, 15)
        part, frac = divmod(value, 1)
        is_integer = abs(round(frac, 15)) < 1e-16
        if self.auto_dtype and is_integer:
            self.stored_value = int(value)
            text = str(self.stored_value)
        else:
            self.stored_value = float(value)
            text = f'{self.stored_value:{self.format_string}}'

        text = text.replace('.', ',') if self.comma else text.replace(',', '.')
        self.setText(text)

        if block_signal:
            return

        if self.callback is not None and not self.signalsBlocked():
            try:
                self.callback()
            except Exception:
                ...


class Regex_Dialog(QtWidgets.QDialog):

    sigClose = pyqtSignal(int)
    ui_path = Path(Path(__file__).parent, 'regex_dialog.ui')

    # noinspection PyArgumentList
    def __init__(self, last=0, filename='', parent=None):
        super(Regex_Dialog, self).__init__(parent)
        uic.loadUi(self.ui_path, self)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowModality(QtCore.Qt.NonModal)
        self.set_ui(last, filename)

    def closeEvent(self, *args, **kwargs):
        self.sigClose.emit(self.combo.currentIndex())
        super().closeEvent(*args, **kwargs)

    # noinspection PyUnresolvedReferences
    def _validate(self):
        regex = self.prefix.text() + r'\d+' + self.suffix.text()
        test = self.test_filename.text()
        match = re.search(regex.lower(), test.lower())
        result = 'no match'
        if match:
            m = re.search(r'\d+', str(match.group()))
            if m:
                start = match.start() + m.start()
                num = test[start:start + len(m.group())]
                result = f'match measurement index as {int(num)}\n' \
                         f'sequence enumeration start at char {start + 1}'
        self.result.setText(result)

    def _update_regex(self):
        regex = SEQUENCE_STYLES[self.combo.currentText()]
        prefix, suffix = regex.split(r'\d+')
        self.prefix.setText(prefix)
        self.suffix.setText(suffix)

    def _save(self):
        new_entry = self.savename.text()
        if not new_entry:
            return
        regex = self.prefix.text() + r'\d+' + self.suffix.text()
        keys = list(SEQUENCE_STYLES.keys())
        values = list(SEQUENCE_STYLES.values())
        priority = self.priority.value() - 1
        if priority > len(keys):
            priority = len(keys) - 2
        keys.insert(priority, new_entry)
        values.insert(priority, rf'{regex}')
        SEQUENCE_STYLES.clear()
        SEQUENCE_STYLES.update(dict(zip(keys, values)))
        save_user_regex(SEQUENCE_STYLES)
        self.combo.blockSignals(True)
        self.combo.clear()
        self.combo.addItems(SEQUENCE_STYLES.keys())
        self.combo.blockSignals(False)
        self.combo.setCurrentIndex(priority)

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def set_ui(self, last, filename):
        self.combo.clear()
        self.combo.addItems(SEQUENCE_STYLES.keys())
        self.combo.currentIndexChanged.connect(self._update_regex)
        self.combo.setCurrentIndex(last)
        self._update_regex()
        self.test_filename.setText(filename)
        self.validate.clicked.connect(self._validate)
        self.result.setText('\n')
        self.saveButton.clicked.connect(self._save)
