# coding=utf-8
"""
silx dataviews customization

"""

import numpy as np
from pathlib import Path

from silx._version import version_info as silx_version
from silx import config
from silx.gui.qt import Signal, QToolTip, QApplication, QWidget, QHBoxLayout, QPalette, QColor, QSizePolicy
from silx.gui.plot import PlotWindow, Plot2D
from silx.gui.plot.items import ImageData, MaskImageData, ItemChangedType
from silx.gui.plot.tools.profile.rois import ProfileImageHorizontalLineROI
from silx.utils.weakref import WeakMethodProxy
from silx.gui.plot.ColorBar import ColorBarWidget
from silx.gui.plot.backends.BackendMatplotlib import BackendMatplotlibQt

from .maskwidget import PylostMaskToolsDockWidget
from .toolbar import PylostProfileToolBar, PylostCustomToolbar, PylostColorToolbar, PylostProfileRangeToolbar
from .statswidget import StatsWindow
from pylost.data import StitchingDataset
from pylost.data.pyopticslab.generic import Surface, Profile, u
from pylost.utils.methods import convert_colormap
from pylost.utils.injectors import Injector
from pylost.orange.gui import QHBoxLayout

DEFAULT_PLOT_CURVE_COLORS = [#'#000000',  # black
                             '#0000ff',  # blue
                             '#ff0000',  # red
                             '#00ff00',  # green
                             '#ff66ff',  # pink
                             '#ffff00',  # yellow
                             '#a52a2a',  # brown
                             # '#00ffff',  # cyan
                             '#ff00ff',  # magenta
                             '#ff9900',  # orange
                             '#6600ff',  # violet
                             '#a0a0a4',  # grey
                             '#000080',  # darkBlue
                             '#800000',  # darkRed
                             '#008000',  # darkGreen
                             '#008080',  # darkCyan
                             '#800080',  # darkMagenta
                             '#808000',  # darkYellow
                             '#660000']  # darkBrown
"""Default list of colors for plot widget displaying curves."""


class PylostPlot1D(PlotWindow):
    """Custom view based on PlotWindow, instance of ProfileWindow to be used with ROI manager"""

    sigClose = Signal()
    """seen by silx as a ProfileWindow"""

    def __init__(self, parent, backend=None, verbose=False, **kwargs):
        # Custom attributes
        self.initialized = False
        self.verbose = verbose
        self._viewer = parent.parent()
        self.parent_plot = None
        self.roi = None
        self.silx_roi_active = False
        self.last_roi = None
        self._units = None
        mask = kwargs.get('mask', True)
        roi = kwargs.get('roi', True)
        show_stats = kwargs.get('show_stats', True)
        show_fit = kwargs.get('show_fit', True)

        self.noise_data = False

        self.apply_fit = True

        self._ctrl_pressed = False

        # manager.ProfileWindow.__init__(self, parent=parent, backend=backend)
        super(PylostPlot1D, self).__init__(parent=parent, backend=backend,
                                           resetzoom=True, autoScale=True,
                                           logScale=True, grid=True,
                                           curveStyle=True, colormap=False,
                                           aspectRatio=True, yInverted=False,
                                           copy=True, save=True, print_=True,
                                           control=True, position=True,
                                           roi=roi, mask=mask, fit=True)

        self.setAxesMargins(0.047, 0.1, 0.07, 0.1) # TODO: dynamic ?

        if parent is None:
            self.setWindowTitle('PylostPlot1D')

        self.getXAxis().setLabel('X')
        self.getYAxis().setLabel('Y')
        self.setGraphGrid()
        action = self.getFitAction()
        action.setXRangeUpdatedOnZoom(True)
        action.setFittedItemUpdatedFromActiveCurve(True)

        self._customToolbar = PylostCustomToolbar(parent, self, 'Pylost Actions',
                                                  show_profiles=False,
                                                  apply_fit=True,
                                                  verbose=verbose)
        self.addToolBar(self._customToolbar)

        self._customToolbar.show_profiles_widget.setVisible(False)
        self._customToolbar.show_stats_widget.setVisible(show_stats)

        self._customToolbar.plane_fit_widget.setVisible(show_fit)

        self._statswindow = None

        self.range = PylostProfileRangeToolbar(plot=self)
        self.addToolBar(self.range)

    def __call__(self, plot2d):
        """called from profile manager createProfileWindow in __displayResult"""
        self.parent_plot = plot2d
        try:
            self.setstatswindow(self.parent_plot.getstatswindow())
            self.getstatswindow().sigShowEvent.connect(self._statswindowShowEvent)
        except (AttributeError, RuntimeError):
            self.setstatswindow()
            """not a multi2D viewer, skip signal"""
        return self

    def _statswindowShowEvent(self, shown):
        self._customToolbar.show_stats_widget._setChecked(shown)

    def setstatswindow(self, statswindow=None):
        if statswindow is not None:
            if self._statswindow is not None:
                self._statswindow.deleteLater()
            self._statswindow = statswindow
        else:
            self._statswindow = StatsWindow()

    def getprofileswidget(self):
        return self

    def getstatswindow(self):
        return self._statswindow

    def getstatstable(self):
        return self._statswindow.stats

    def showEvent(self, event):
        super().showEvent(event)
        if self.initialized:
            if self.parent_plot is not None:
                self._customToolbar.show_stats_widget.switch()
            # self.resetZoom()
        self.initialized = True

    def hideEvent(self, event):
        super().hideEvent(event)
        if self.parent_plot is not None:
            self._customToolbar.show_stats_widget.switch()
        self.initialized = False

    def closeEvent(self, **kwargs):
        self.getstatswindow().deleteLater()

    def onMouseMove(self, xPixel, yPixel):
        """Handle mouse move event."""
        super().onMouseMove(xPixel, yPixel)
        if self._maskToolsDockWidget:
            self._maskToolsDockWidget.widget().mouse_moved()

    def onMousePress(self, xPixel, yPixel, btn):
        super().onMousePress(xPixel, yPixel, btn)

    def getLegendsDockWidget(self):
        return self._viewer._legendsDockWidget

    def prepareWidget(self, roi):
        """ if called from PylostMulti2D and toolbar editor not customized """

    # TODO: need to adapt for each roi (horizontal line, vertical line, diagonal, etc.)

    def init_roi(self, pos=0):
        if self.roi is None:
            # link with plot2d
            # self(parent)
            roi = ProfileImageHorizontalLineROI()
            # roi.removeItem(roi._DefaultImageProfileRoiMixIn__area)
            roi.setPosition(pos)
            roi.setProfileLineWidth(round(1 / self.parent_plot.getActiveImage().getScale()[1]))
            self.setRoiProfile(roi, True)
        self.setProfile(update_all=True)

    def roi_active(self, state=True):
        """ silx roi initiated """
        self.silx_roi_active = state
        if not state:
            # self.setRoiProfile(self.last_roi)
            if self.parent_plot is None:
                return
            start = [0, 0]
            if self.last_roi is not None:
                start, _, _ = self.last_roi._getRoiInfo()
            self.init_roi(pos=start[1])

    def setRoiProfile(self, roi, pylost=False):
        """ seen as a ProfileWindow """
        self.roi = roi
        if roi is None:
            return
        if pylost or not isinstance(self.parent_plot, PylostMulti2D):
            try:
                self.roi.sigRegionChanged.disconnect(self._region_changed)
            except TypeError:
                """"""
                # print('setRoiProfile', e)
        else:
            self.roi.sigRegionChanged.connect(self._region_changed)
            silx_marker = self.parent_plot._getMarker()
            silx_marker.setText('')
            self._region_changed()

    def _region_changed(self):
        print('_region_changed')
        start, _, _ = self.roi._getRoiInfo()
        self.parent_plot.set_profiles(start) #  ProfileImageHorizontalLineROI

    def roi_infos(self, image):
        self.last_roi = self.roi
        start, stop, mode = self.roi._getRoiInfo()
        _, centre_y = self.parent_plot.get_image_centre(image)
        _, offset_y = 0.0, image.marker.offset
        start = start[0], centre_y + offset_y
        stop = stop[0], centre_y + offset_y
        return start, stop, mode

    def set_roi_position(self, pos):
        if self.roi is None:
            self.init_roi()
        self.roi.setPosition(pos)

    def create_profile(self, image):
        lineWidth = int(max(1, round(self.parent_plot.get_profile_width() / image.getScale()[1])))
        obj = image.surface.extract_profile(position=image.marker.offset, width=lineWidth * image.getScale()[1])
        obj.coords = np.add(obj.coords, image.latshift)
        if self.apply_fit:
            obj.polynomial_removal()
            func = 'level_data' if obj.is_slopes else 'mean_removal'
            getattr(obj, func)()
        return obj

    def get_name(self, legend):
        for curve in self.getAllCurves(withhidden=True):
            if curve.legend == legend:
                return curve.getName()

    @staticmethod
    def get_curve_id(curve):
        return float(PylostMulti2D.get_id_string(curve.link, curve.subelement))

    def get_curve_index(self, curve):
        return self.getOrderedCurves().index(curve)

    def getOrderedCurves(self, withhidden=True):
        curves = self.getAllCurves(withhidden=withhidden)
        curve_ids = [self.get_curve_id(curve) for curve in curves]
        return [curves[curve_id] for curve_id in np.argsort(curve_ids)]

    def get_visible_curves(self, ordered=False):
        if ordered:
            curves = self.getOrderedCurves(withhidden=False)
        else:
            curves = self.getAllCurves(withhidden=False)
        visibles = []
        for curve in curves:
            if curve.isVisible():
                visibles.append(curve)
        return visibles

    def setActiveCurve(self, legend):
        # if self.parent_plot is not None:
        #     return self.parent_plot.setActiveImage(legend)
        return super().setActiveCurve(legend)

    def set_active_curve(self, legend):
        return super().setActiveCurve(legend)

    def update_legends(self):
        self.getLegendsDockWidget().updateLegends()

    # noinspection PyUnusedLocal
    def setProfile(self, *args, **kwargs):
        """seen as a ProfileWindow"""
        # method called 2 times when using silx ROI
        if self.parent_plot is None or self.roi is None:
            return

        update_all = kwargs.get('update_all', False)
        update_one = kwargs.get('update_one', False)

        silx_roi = kwargs.get('silx_roi', False)

        if silx_roi:
            return

        x_shifts = []
        y_shifts = []
        image = None
        coord_unit = None
        for image in self.parent_plot.getAllImages():
            if not isinstance(image, ImageData) or isinstance(image, MaskImageData):
                continue
            if image.surface is None:
                continue
            coord_unit = str(image.surface.coords_unit)
            if update_all or update_one == image or True:
                self.updateProfile(data=self.create_profile(image),
                                   name=image.getName(),
                                   legend=image.legend,
                                   link=image.link,
                                   subelement=image.subelement,
                                   infos=image.getInfo(),
                                   fit_window=True,
                                   hide=not image.isVisible())
            x_shifts.append(image.latshift)
            y_shifts.append(image.marker.offset)

        width = self.parent_plot.get_profile_width()# * self.parent_plot.getActiveImage().getScale()[1]
        if silx_roi:
            title = 'Silx profile' if image is None else self.getCurve(image.getName()).profile.source
        else:
            title = f'width:{width:.2f}{coord_unit} - vertical pos:' + \
                    ', '.join(['%.2f' + coord_unit] * len(y_shifts)) % tuple(y_shifts) + \
                    ' - offsets:' + ', '.join(['%.2f' + coord_unit] * len(x_shifts)) % tuple(x_shifts)

        self.setGraphTitle(title)

        self.update_legends()

        self.update_statistics()

    # noinspection PyUnresolvedReferences
    def updateProfile(self, data:Profile, name, legend, link=0, subelement=0, infos=None,
                      fit_window=False, hide=False, replace=False):
        # if self.verbose:
        #     print('set profile data', data.source)

        self.addCurve(x=data.x, y=data.z,
                      legend=name,
                      info=data.source if infos is None else infos,
                      replace=replace,
                      selectable=True,
                      resetzoom=False,
                      xlabel=str(data.x_unit), ylabel=str(data.z_unit),
                      yaxis='left',
                      linewidth=1, linestyle='-',
                      color=None,  # symbol='.',
                      copy=False)

        self.update_range(fit_window)

        curve = self.getCurve(name)
        setattr(curve, 'profile', data)
        setattr(curve, 'link', link)
        setattr(curve, 'subelement', subelement)
        setattr(curve, 'index', self.get_curve_index(curve))
        setattr(curve, 'legend', legend)

        curve.setColor(DEFAULT_PLOT_CURVE_COLORS[curve.index % len(DEFAULT_PLOT_CURVE_COLORS)])

        self.hideCurve(legend, hide)
        self.setGraphXLabel(str(data.x_unit))
        self.setGraphYLabel(str(data.z_unit))
        # title = f'{data.kind} in {data.values_unit}'
        self.setGraphTitle(legend)
        # self.setActiveCurve(data.source)

        self._units = data.units

        save_action = self.getSaveAction()
        nameFilter = 'ESRF HGT format (*.hgt)'
        if data.is_slopes:
            nameFilter = 'ESRF LTP format (*.slp)'
        save_action.setFileFilter(dataKind='curve', nameFilter=nameFilter,
                                  func=self._saveCurve_as_esrf_profile, index=0)
        save_action.setFileFilter(dataKind='curves', nameFilter=nameFilter,
                                  func=self._saveCurves_as_esrf_profile, index=0)
        save_action.triggered.disconnect(save_action._actionTriggered)
        injector = Injector(save_action)
        save_action._actionTriggered = injector._actionTriggered
        save_action.triggered[bool].connect(save_action._actionTriggered)

    def update_range(self, fit_window=True):
        if self.range.is_fixed():
            self.setGraphYLimits(*self.range.get_elements())
            return
        elif fit_window:
            self.resetZoom()

    def set_noise_full_stats(self):
        full = [image.surface.z - image.surface.mean for image in self.parent_plot.get_visible_images(ordered=True)]
        self.getstatstable().add_noise_full_stats(np.nanstd(full), np.nanmax(full) - np.nanmin(full),
                                                  unit=self._units['values'])

    def update_statistics(self):
        if self.getstatswindow() is None:
            self.setstatswindow()
        self.getstatstable().removeAllRows()
        if self.noise_data:
            self.set_noise_full_stats()
        for index, curve in enumerate(self.get_visible_curves(ordered=True)):
            image = self.parent_plot.getImage(curve.getName())
            if self.noise_data:
                index += 1
            self.getstatstable().fill_row(index, curve, image, self.parent_plot.get_profile_width())
        self.getstatstable().resizeColumns()

    def removeCurve(self, legend):
        curve = self.getCurve(legend)
        self.getstatstable().removeRow(curve.index)
        super().removeCurve(legend)

    @staticmethod
    def _saveCurve_as_esrf_profile(plot, filepath, nameFilter):
        curve = plot.getActiveCurve()
        if curve is None:
            plot._saveCurves_as_esrf_profile(plot, filepath, nameFilter)
        x, y, _, _ = curve.getData()
        Profile.writeESRFfileformat(filepath, x, y, plot._units, 'slp' in nameFilter)

    @staticmethod
    def _saveCurves_as_esrf_profile(plot, filepath, nameFilter):
        curves = plot.getAllCurves()
        if not curves:
            return
        filepath = Path(filepath)
        parent = Path(filepath.parent, filepath.stem)
        if len(curves) > 1:
            if not parent.exists():
                parent.mkdir()
        for curve in curves:
            is_slp = 'slp' in nameFilter
            extension = '.slp' if is_slp else '.hgt'
            x, y, _, _ = curve.getData()
            if len(curves) > 1:
                split = curve.legend.split(': ')[-1][::-1].rpartition('.')
                filename = split[2][::-1] + extension
                filepath = Path(parent.parent, filename) if len(curves) == 1 else Path(parent, filename)
            Profile.writeESRFfileformat(filepath, x, y, plot._units, is_slp)

    # customizing
    def _customControlButtonMenu(self):
        super()._customControlButtonMenu()
        controlMenu = self.controlButton.menu()
        controlMenu.removeAction(self.getConsoleAction())  # introduces some fatal crashes

    # noinspection PyUnresolvedReferences
    def getMaskToolsDockWidget(self):
        """DockWidget with image mask panel (lazy-loaded)."""
        if self._maskToolsDockWidget is None:
            self._maskToolsDockWidget = PylostMaskToolsDockWidget(viewer=self._viewer, plot=self,
                                                                  name='Mask', profile=True)
            self._maskToolsDockWidget.hide()
            self._maskToolsDockWidget.toggleViewAction().triggered.connect(
                self._handleDockWidgetViewActionTriggered)
            self._maskToolsDockWidget.visibilityChanged.connect(
                self._handleFirstDockWidgetShow)
        return self._maskToolsDockWidget

    def graphCallback(self, ddict=None):
        """This callback is going to receive all the events from the plot.

        Those events will consist on a dictionary and among the dictionary
        keys the key 'event' is mandatory to describe the type of event.
        This default implementation only handles setting the active curve.
        """

        if ddict is None:
            ddict = {}
        # _logger.debug("Received dict keys = %s", str(ddict.keys()))
        # _logger.debug(str(ddict))
        if ddict['event'] in ["legendClicked", "curveClicked"]:
            if ddict['button'] == "left":
                self.setActiveCurve(ddict['label'])
                # noinspection PyArgumentList
                QToolTip.showText(self.cursor().pos(), self.getCurve(ddict['label']).legend)
        elif ddict['event'] == 'mouseClicked' and ddict['button'] == 'left':
            self.setActiveCurve(None)

    def _spacebar_hit(self):
        is_checked = self._customToolbar.show_stats_widget.isChecked()
        action = 'hide' if is_checked else 'show'
        getattr(self.getstatswindow(), action)()

    def keyPressEvent(self, event):
        if event.key() in (32,):
            self._spacebar_hit()
        elif self._ctrl_pressed:
            if event.key() == 67:
                self.figure_to_clipboard()
        elif event.key() == 16777249:
            self._ctrl_pressed = True
        else:
            super(PlotWindow, self).keyPressEvent(event)

    def keyReleaseEvent(self, event):
        if event.key() == 16777249 and self._ctrl_pressed:
            self._ctrl_pressed = False
        else:
            super(PlotWindow, self).keyPressEvent(event)

    # noinspection PyTypeChecker,PyArgumentList
    def figure_to_clipboard(self):
        if 'Matplotlib' not in str(self.getBackend()):
            return
        child = None
        for child in self.children():
            if 'PyQt5.QtWidgets.QWidget' in str(child):
                break
        if child is not None:
            plot = None
            for grandchild in child.children():
                if 'BackendMatplotlibQt' in str(grandchild):
                    plot = BackendMatplotlibQt(plot=self)
                    for name, attribute in grandchild.__dict__.items():
                        setattr(plot, name, attribute)
                    break
            if plot is not None:
                hbox = QWidget()
                hboxlayout = QHBoxLayout()
                hboxlayout.setContentsMargins(0, 0, 0, 0)
                hboxlayout.setSpacing(0)
                hboxlayout.addStretch(0)
                hboxlayout.addWidget(plot)
                hbox.setLayout(hboxlayout)
                QApplication.clipboard().setImage(hbox.grab().toImage())
                # layout.addWidget()
                print('Profile figure saved to clipboard!')
                del plot


class PylostPlot2D(Plot2D):
    """Custom plot based on Plot2d, single plot"""
    def __init__(self, parent=None, backend=None, cmap=None, profileswidget=None, verbose=False, **kwargs):
        # Custom attributes
        self._ctrl_pressed = False
        self.initialized = False
        self.colormap = cmap
        self.cmap_prms = None
        self.verbose = verbose
        self._viewer = parent.parent()
        self._profileswidget = profileswidget
        self._profilewidth = 1 # mm
        self._units = None
        mask = kwargs.get('mask', True)
        roi = kwargs.get('roi', True)
        custom_toolbar = kwargs.get('custom_toolbar', True)
        show_profiles = kwargs.get('show_profiles', False)
        show_markers = kwargs.get('show_markers', False)
        show_stats = kwargs.get('show_stats', True)
        self.process_noise = False
        # List of information to display at the bottom of the plot  TODO: customize
        posInfo = [
            ('X', lambda x, y: x),
            ('Y', lambda x, y: y),
            ('Data', WeakMethodProxy(self._getImageValue)),
            ('Dims', WeakMethodProxy(self._getImageDims)),
        ]
        # change some default behaviours
        super(Plot2D, self).__init__(parent=parent, backend=backend,
                                     resetzoom=True, autoScale=True,
                                     logScale=False, grid=True,
                                     curveStyle=False, colormap=True,
                                     aspectRatio=True, yInverted=True,
                                     copy=True, save=True, print_=False,
                                     control=True, position=posInfo,
                                     roi=roi, mask=mask, fit=True)

        self.setAxesMargins(0.05, 0.1, 0.01, 0.1)

        if parent is None:
            self.setWindowTitle('PylostPlot2D')
        self.getXAxis().setLabel('Columns')
        self.getYAxis().setLabel('Rows')

        if config.DEFAULT_PLOT_IMAGE_Y_AXIS_ORIENTATION == 'downward':
            self.getYAxis().setInverted(True)

        # must be created before calling _profileswidget
        self._statswindow = StatsWindow()
        self._statswindow.sigShowEvent.connect(self._statswindowShowEvent)

        self._customToolbar = PylostCustomToolbar(parent, self, 'Pylost Actions',
                                                  show_profiles=show_profiles,
                                                  verbose=verbose)
        self.addToolBar(self._customToolbar)

        self._customToolbar.show_stats_widget.setVisible(show_stats and custom_toolbar)
        self._customToolbar.show_profiles_widget.setVisible(custom_toolbar)
        self._customToolbar.plane_fit_widget.setVisible(False)

        self.getprofileswidget()(self)
        self.profile = PylostProfileToolBar(plot=self,
                                            profilewidget=self.getprofileswidget(),
                                            show_markers=show_markers,
                                            verbose=self.verbose)
        self.addToolBar(self.profile)
        if not roi:
            self.profile.hide()

        self.colors = PylostColorToolbar(plot=self)
        self.addToolBar(self.colors)

        # inject modified methods to have less displayed digits
        colorscale = self.getColorBarWidget().getColorScaleBar()
        injector = Injector(colorscale)
        colorscale._updateMinMax = injector._updateMinMax
        injector = Injector(colorscale.colorScale)
        colorscale.colorScale.getValueFromRelativePosition = injector.getValueFromRelativePosition

        self.colorbarAction.setVisible(True)
        self.getColorBarWidget().setVisible(True)

        # # Put colorbar action after colormap action
        # actions = self.toolBar().actions()
        # for action in actions:
        #     if action is self.getColormapAction():
        #         break

        if kwargs.get('connect', True):
            self.sigActiveImageChanged.connect(self.__activeImageChanged)

        self.getIntensityHistogramAction().setVisible(True)
        self.setKeepDataAspectRatio(True)
        self.getXAxis().setLabel('X')
        self.getYAxis().setLabel('Y')
        maskToolsWidget = self.getMaskToolsDockWidget().widget()
        maskToolsWidget.setItemMaskUpdated(True)

    # def notify(self, event, **kwargs):
    #     """ listen to events """
    #     super().notify(event, **kwargs)

    def _init(self):
        self.initialized = True
        # self.resetZoom()

    def _statswindowShowEvent(self, shown):
        self._customToolbar.show_stats_widget._setChecked(shown)

    def showEvent(self, event):
        super().showEvent(event)
        if not self.initialized:
            self._init()
        self._customToolbar.show_stats_widget.switch()

    def hideEvent(self, event):
        super().hideEvent(event)
        self._customToolbar.show_stats_widget.switch()
        self.initialized = False

    def closeEvent(self, **kwargs):
        self.getstatswindow().deleteLater()

    def onMousePress(self, xPixel, yPixel, btn):
        super().onMousePress(xPixel, yPixel, btn)

    def getprofileswidget(self):
        return self._profileswidget

    def getstatswindow(self):
        return self._statswindow

    def getstatstable(self):
        return self._statswindow.stats

    def getLegendsDockWidget(self):
        return self._viewer._legendsDockWidget

    def getOrderedCurves(self):
        return self.getprofileswidget().getOrderedCurves()

    def getActiveCurve(self, just_legend=False):
        return self.getprofileswidget().getActiveCurve(just_legend)

    def get_name(self, legend):
        for curve in self.getprofileswidget().getAllCurves(withhidden=True):
            if curve.legend == legend:
                return curve.getName()

    @staticmethod
    def get_image_centre(image):
        x_min, x_max, y_min, y_max = image.getBounds()
        return x_min + (x_max - x_min) / 2, y_min + (y_max - y_min) / 2

    def get_profile_width(self):
        return self.getProfileToolbar().getWidth()

    def add_marker(self, image, slopes):
        color = 'black' if slopes else 'grey'
        marker = self._getMarker(self.addYMarker(self.get_image_centre(image)[1], legend=image.getName(), color=color))
        setattr(marker, 'offset', 0.0)
        setattr(image, 'marker', marker)
        visible = self.profile.show_line_marks.isChecked() and not image.surface.from_profile
        marker.setVisible(visible)

    def addImage(self, data:Surface, add_marker=True, multi=False, **kwargs):
        """ overload to bypass the data copy (silx don't pass the copy argument)"""

        kwargs['resetzoom'] = False
        image = super().addImage(np.array([[1]]), **kwargs)
        name = kwargs.get('legend', '')

        if silx_version.major < 2:
            name = image
            image = self.getImage(name)

        previousShape = image._data.shape
        image._data = data.values.T
        image._valueDataChanged()
        image._boundsChanged()
        image._updated(ItemChangedType.DATA)
        if (image.getMaskData(copy=False) is not None and
                previousShape != image._data.shape):
            # Data shape changed, so mask shape changes.
            # Send event, mask is lazily updated in getMaskData
            image._updated(ItemChangedType.MASK)

        # add useful attributes to image
        setattr(image, 'surface', data)
        setattr(image, 'link', 0)
        setattr(image, 'subelement', 0)
        setattr(image, 'index', '0')
        setattr(image, 'legend', name)
        setattr(image, 'latshift', 0)

        if add_marker:
            self.add_marker(image, data.is_slopes)

        # DEBUG
        nocopy = np.shares_memory(image.getData(copy=False), image.surface.values)
        if not nocopy:
            print('Plot2D WARNING: data memory NOT SHARED !')

        if silx_version.major < 2:
            return name
        return image

    def updateImage(self, data:Surface, infos=None, fit_window=False):
        # if self.verbose:
        #     print('set surface data', data.source)
        self._units = data.units

        if silx_version.major < 2:
            maskToolsWidget = self.getMaskToolsDockWidget().widget()
            mask = self.getImage(maskToolsWidget._maskName)
            if mask is not None:
                self.removeItem(mask)
        else:
            mask = None
            for image in self.getAllImages()[::-1]:
                if isinstance(image, MaskImageData):
                    if mask is None:
                        mask = image
                    self.removeItem(image)

        # self.getprofileswidget().init_roi(self, 0)

        self.update_colormap(data, update=False)

        legend = data.source
        self.addImage(data=data, legend=legend, info=legend if infos is None else infos,
                      replace=True,
                      colormap=self.colormap,
                      xlabel=str(data.x_unit), ylabel=str(data.y_unit),
                      origin=(min(data.x), min(data.y)),
                      scale=list(data.pixel_size),
                      resetzoom=fit_window,
                      copy=False,
                      )

        if mask is not None:
            self.addItem(mask)

        self.setGraphXLabel(str(data.x_unit))
        self.setGraphYLabel(str(data.y_unit))

        title = f'{data.source} - ({data.kind} in {data.z_unit})'
        self.setGraphTitle(title)

        # self.profile.getProfileOptionToolAction().setUnits(data.pixel_size[0], data.coords_unit)
        # self.profile.getProfileWidthAction().setUnits(data.pixel_size[0], data.coords_unit)

        if fit_window:
            self.resetZoom()

        self.getProfileToolbar().getProfileWidthAction().update_unit(data.coords_unit)

        self._add_saving_filters(data.is_heights)

        self.getColorBarWidget().setLegend(str(data.z_unit))

        active = self.getActiveImage()
        self.set_profile(active)
        self.update_marker(active)
        active.marker.setVisible(False)

    def update_colormap(self, data=None, update=True):
        if data is None:
            image = self.getActiveImage()
            if image is None:
                return
            data = image.surface
        params = {'automasking':True, 'slopes':data.is_slopes, 'kind':data.kind}
        if self.colors.is_fixed():
            params = dict(zip(('base', 'zfac', 'zmin', 'z_lo', 'z_hi', 'zmax'),
                              ('fixed', 2) + tuple(self.colors.get_elements())))
            params['slopes'] = data.is_slopes
        self.colormap, self.cmap_prms = convert_colormap(data, **params)
        self.colors.fill_elements(**self.cmap_prms)
        if update:
            [image.setColormap(self.colormap) for image in self.getAllImages()]

    def update_statistics(self):
        self.getprofileswidget().update_statistics()

    def setActiveImage(self, image):
        if image is None:
            return
        legend = image
        if not isinstance(image, str):
            legend = image.getName()
        self.getprofileswidget().set_active_curve(legend)
        return super().setActiveImage(legend)

    def set_profile(self, image):
        curve = self.getprofileswidget().getCurve(image.getName())
        if curve is not None:
            curve.setVisible(image.isVisible())
        if not image.isVisible():
            return
        x_min, x_max, y_min, y_max = image.getBounds()
        centre_y = y_min + (y_max - y_min) / 2
        new_y = centre_y + image.marker.offset
        new_y = max((y_min, new_y))
        new_y = min((new_y, y_max))
        image.marker.offset = new_y - centre_y
        self.getprofileswidget().set_roi_position(new_y)
        self.getprofileswidget().setProfile(update_one=image)

    def shift_profile(self, image, direction:str):
        factor = 1
        shift = {'up': (0, factor), 'down': (0, -factor), 'left': (-factor, 0), 'right': (factor, 0)}
        if direction in ('up', 'down'):
            image.marker.offset += shift[direction][1] * image.getScale()[1]
            self.set_profile(image)
            self.update_marker(image)

    def update_marker(self, image, reset=False):
        if image.surface is None:
            image.marker.setVisible(False)
            return
        if reset:
            image.marker.offset = 0.0
        image.marker.setPosition(0, self.get_image_centre(image)[1] + image.marker.offset)
        image.marker.setText(f'{image.marker.offset:.2f}')

    def reset_marker(self, image):
        self.update_marker(image, reset=True)

    # noinspection PyUnresolvedReferences
    def _add_saving_filters(self, heights=True):
        if heights:
            save_action = self.getSaveAction()
            save_action.setFileFilter(dataKind='image', nameFilter='Zygo dat format (*.dat)',
                                      func=self._saveImages_as_zygo_dat, index=0)
            save_action.setFileFilter(dataKind='image', nameFilter='Optical Path Difference format (*.opd)',
                                      func=self._saveImages_as_opd, index=1)
            save_action.triggered.disconnect(save_action._actionTriggered)
            injector = Injector(save_action)
            save_action._actionTriggered = injector._actionTriggered
            save_action.triggered[bool].connect(save_action._actionTriggered)

    # noinspection PyUnusedLocal
    @staticmethod
    def _saveImages_as(plot, filepath, nameFilter, fmt):
        """ assuming height data """
        images = plot.getAllImages()
        if not images:
            return
        filepath = Path(filepath)
        parent = Path(filepath.parent, filepath.stem)
        if len(images) > 1:
            if not parent.exists():
                parent.mkdir()
        for image in images:
            if len(images) == 1:
                filepath = Path(filepath)
            else:
                split = image.legend.split(': ')[-1][::-1].rpartition('.')
                filename = split[2][::-1] + fmt
                filepath = Path(parent, filename)
            if fmt == '.dat':
                Surface.writeMetroprofile(filepath, image.getValueData().T, image.getScale(), plot._units)
            elif fmt == '.opd':

                Surface.writeVeecofile(path=filepath,
                                       data=u.nm(image.getValueData().T, plot._units['values']),
                                       pixel_size=u.mm(image.getScale()[0], plot._units['coords']),
                                       title='Created from Pylost')

    # noinspection PyUnusedLocal
    @staticmethod
    def _saveImages_as_zygo_dat(plot, filepath, nameFilter):
        PylostPlot2D._saveImages_as(plot, filepath, nameFilter, '.dat')

    # noinspection PyUnusedLocal
    @staticmethod
    def _saveImages_as_opd(plot, filepath, nameFilter):
        PylostPlot2D._saveImages_as(plot, filepath, nameFilter, '.opd')

    # customizing
    def _customControlButtonMenu(self):
        super()._customControlButtonMenu()
        controlMenu = self.controlButton.menu()
        controlMenu.removeAction(self.getConsoleAction())  # introduces some fatal crashes

    # noinspection PyUnresolvedReferences
    def getMaskToolsDockWidget(self):
        """DockWidget with image mask panel (lazy-loaded)."""
        if self._maskToolsDockWidget is None:
            self._maskToolsDockWidget = PylostMaskToolsDockWidget(viewer=self._viewer, plot=self, name='Mask')
            self._maskToolsDockWidget.hide()
            self._maskToolsDockWidget.toggleViewAction().triggered.connect(
                self._handleDockWidgetViewActionTriggered)
            self._maskToolsDockWidget.visibilityChanged.connect(
                self._handleFirstDockWidgetShow)
        return self._maskToolsDockWidget

    # name mangling
    def __activeImageChanged(self, previous, legend):
        try:
            self._Plot2D__activeImageChanged(previous, legend)
        except TypeError:
            """"""
        # print('__activeImageChanged', self.getDataRange())
        if previous is None:
            self.getMaskToolsDockWidget().widget().resetSelectionMask()

    def _spacebar_hit(self):
        pass

    def keyPressEvent(self, event):
        if event.key() in (32,):
            self._spacebar_hit()
        elif self._ctrl_pressed:
            if event.key() == 67:
                self.figure_to_clipboard()
        elif event.key() == 16777249:
            self._ctrl_pressed = True
        else:
            super(Plot2D, self).keyPressEvent(event)

    def keyReleaseEvent(self, event):
        if event.key() == 16777249 and self._ctrl_pressed:
            self._ctrl_pressed = False
        else:
            super(Plot2D, self).keyPressEvent(event)

    # noinspection PyTypeChecker,PyArgumentList
    def figure_to_clipboard(self):
        if 'Matplotlib' not in str(self.getBackend()):
            return
        child = None
        for child in self.children():
            if 'QtWidgets.QWidget' in str(child):
                break
        if child is not None:
            plot = None
            colorbar = None
            infobar = None
            for grandchild in child.children():
                if 'BackendMatplotlibQt' in str(grandchild):
                    plot = BackendMatplotlibQt(plot=self)
                    for name, attribute in grandchild.__dict__.items():
                        setattr(plot, name, attribute)
                elif 'ColorBarWidget' in str(grandchild):
                    colorbar = ColorBarWidget(plot=self, legend=grandchild.getLegend())
                    colorscale = colorbar.getColorScaleBar()
                    injector = Injector(colorscale)
                    colorscale._updateMinMax = injector._updateMinMax
                    palette = colorbar.palette()
                    palette.setColor(QPalette.Window, QColor('white'))
                    colorbar.setPalette(palette)
                    colorbar.setAutoFillBackground(True)
                elif '.QtWidgets.QWidget' in str(grandchild):
                    infobar = grandchild
                    break
            if plot is not None and colorbar is not None and infobar is not None:
                hbox = QWidget()
                hbox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
                hbox.setMinimumWidth(child.frameGeometry().width())
                hbox.setMinimumHeight(child.frameGeometry().height() - infobar.frameGeometry().height())
                hboxlayout = QHBoxLayout()
                hboxlayout.setContentsMargins(0, 0, 0, 0)
                hboxlayout.setSpacing(0)
                hboxlayout.addStretch(0)
                hboxlayout.addWidget(plot)
                hboxlayout.addWidget(colorbar)
                hbox.setLayout(hboxlayout)
                QApplication.clipboard().setImage(hbox.grab().toImage())
                print('2D figure saved to clipboard!')
                del plot
                del colorbar

    def calc_noise(self):
        """ only multi2D """

class PylostMulti2D(PylostPlot2D):

    # sigActiveImageMoved = Signal()

    def __init__(self, parent=None, profileswidget=None, noise_data=False, verbose=False, **kwargs):
        super().__init__(parent, backend=None, profileswidget=profileswidget,
                         show_profiles=True,
                         # show_markers=not noise_data,
                         verbose=verbose,
                         connect=False,
                         **kwargs)
        self.noise_data = noise_data
        self._idx = 0

        # self.sigActiveImageChanged.disconnect(self.__activeImageChanged)
        self.sigActiveImageChanged.connect(self.__activeImageChanged)

        # self.setPanWithArrowKeys(True)
        self._shift_pressed = False
        self.sigSetKeepDataAspectRatio.connect(self.shift_images)

        self.getprofileswidget().sigVisibilityChanged.connect(self.switch_line_markers)

        self._add_saving_filters(True)

    def __len__(self):
        """can be test as list"""
        return len(self.getAllImages())

    def _init(self):
        # called when opened
        try:
            # self.update_markers_pos(reset=True)
            self._format_colorscale_label()
        except AttributeError:
            """ not fully initialized """
        super()._init()

    def _format_colorscale_label(self):
        self.getColorBarWidget().setLegend(str(self._units['values']))
        text = self.getColorBarWidget().getColorScaleBar()._minLabel.text()
        self.getColorBarWidget().getColorScaleBar()._minLabel.setText(f'{float(text):.2f}')
        text = self.getColorBarWidget().getColorScaleBar()._maxLabel.text()
        self.getColorBarWidget().getColorScaleBar()._maxLabel.setText(f'{float(text):.2f}')

    # def showEvent(self, event):
    #     super().showEvent(event)

    def _spacebar_hit(self):
        is_checked = self._customToolbar.show_stats_widget.isChecked()
        action = 'hide' if is_checked else 'show'
        getattr(self.getstatswindow(), action)()
        return

    def keyPressEvent(self, event):
        # print(self._ctrl_pressed, event.key())
        key = event.key()
        if key in (32,):
            self._spacebar_hit()
            return
        if key == 16777249: # control key (master key)
            self._ctrl_pressed = True
        elif key == 16777248: # shift keys (additional function)
            self._shift_pressed = True
        # specific functions for managing profiles
        if self._ctrl_pressed:
            if event.key() == 67:
                self.figure_to_clipboard()
            elif key in self._ARROWS_TO_PAN_DIRECTION:
                active = self.getActiveImage()
                if key in (16777234, 16777236):
                    self.lateral_shift(active, self._ARROWS_TO_PAN_DIRECTION[key])
                    return
                func = 'shift_profiles' if self._shift_pressed else 'shift_profile'
                getattr(self, func)(active, self._ARROWS_TO_PAN_DIRECTION[key])
            return
        elif key in self._ARROWS_TO_PAN_DIRECTION and not self._ctrl_pressed:
            super(PylostMulti2D, self).keyPressEvent(event)

    def keyReleaseEvent(self, event):
        if event.key() == 16777249 and self._ctrl_pressed: # control keys
            self._ctrl_pressed = False
        if event.key() == 16777248: # shift keys
            self._shift_pressed = False
        elif event.key() in self._ARROWS_TO_PAN_DIRECTION and not self._ctrl_pressed:
            super(PylostMulti2D, self).keyReleaseEvent(event)

    def onMousePress(self, xPixel, yPixel, btn):
        if btn in ['left']:
            dataPos = self.pixelToData(xPixel, yPixel, check=True)
            self.set_active(dataPos)
            if self._ctrl_pressed:
                active = self.getActiveImage()
                _, centre_y = self.get_image_centre(active)
                active.marker.offset = dataPos[1] - centre_y
                func = 'set_profiles' if self._shift_pressed else 'set_profile'
                getattr(self, func)(active)
                return
        super().onMousePress(xPixel, yPixel, btn)

    def set_active(self, dataPos:tuple):
        if dataPos is None:
            return
        for image in self.getAllImages():
            legend = image.getName()
            _, _, y_min, y_max = image.getBounds()
            if y_min < dataPos[1] < y_max:
                if legend == self.getActiveImage(just_legend=True):
                    break
                if self.verbose:
                    print('image ' + legend + ' set as active')
                self.setActiveImage(legend)
                break
        self.setGraphXLabel(self._units['coords'])
        self.setGraphYLabel(self._units['coords'])

    def update_profiles(self):
        for image in self.getAllImages():
            self.set_profile(image)

    def set_profile(self, image):
        super().set_profile(image)
        self.update_marker(image)

    def set_profiles(self, active):
        offset = active.marker.offset
        for image in self.getAllImages():
            image.marker.offset = offset
            self.set_profile(image)
            self.update_marker(image)

    def lateral_shift(self, image, direction, reset=False):
        factor = 10 if self._shift_pressed else 1
        shift = {'left': (-factor, 0), 'right': (factor, 0)}
        origin_x, origin_y = image.getOrigin()
        if reset:
            image.latshift = 0
        else:
            image.latshift += shift[direction][0] * image.getScale()[0]
        image.setOrigin((min(image.surface.x) + image.latshift, origin_y))
        self.set_profile(image)

    def shift_profiles(self, active, direction:str):
        if direction in ('left', 'right'):
            self.lateral_shift(active, direction)
            return
        offset = active.marker.offset
        for image in self.getAllImages():
            image.marker.offset = offset
            self.shift_profile(image, direction)

    def update_markers(self, reset=False):
        for image in self.getAllImages():
            self.update_marker(image, reset)

    def reset_markers(self):
        for image in self.getAllImages():
            image.latshift = 0
            self.reset_marker(image)
            self.lateral_shift(image, 'left', reset=True)
            # self.set_profile(image)

    def switch_line_markers(self, flag):
        if flag and self.profile.show_line_marks.isChecked():
            self.show_line_markers()
        else:
            self.hide_line_markers()

    def show_line_markers(self):
        for image in self.getAllImages():
            image.marker.setVisible(image.isVisible() and not image.surface.from_profile)

    def hide_line_markers(self):
        for image in self.getAllImages():
            image.marker.setVisible(False)

    @property
    def isempty(self):
        return np.all([image.surface is None for image in self.getAllImages()])

    def get_id(self, link, subelement):
        for image in self.getAllImages():
            if float(self.get_id_string(link, subelement)) == self.get_image_id(image):
                # same link updated
                return image.getName()
        # new link
        self._idx += 1
        return str(self._idx)

    @staticmethod
    def get_id_string(link, subelement):
        return f'{link}.{subelement + 1:05d}'

    def get_image_id(self, image):
        return float(self.get_id_string(image.link, image.subelement))

    def get_image_index(self, image):
        return self.getOrderedImages().index(image)

    def getOrderedImages(self):
        images = self.getAllImages()
        image_ids = [self.get_image_id(image) for image in images]
        return [images[image_id] for image_id in np.argsort(image_ids)]

    def get_visible_images(self, ordered=False):
        if ordered:
            images = self.getOrderedImages()
        else:
            images = self.getAllImages()
        visibles = []
        for image in images:
            if image.isVisible():
                visibles.append(image)
        return visibles

    def update_index(self, image):
        image.index = self.get_image_index(image)
        num_image = len([image for image in self.getAllImages()])
        fmt = f'0{int(np.log10(num_image)) + 1}'
        image.legend = f'{image.index + 1:{fmt}}: ' + image.legend.split(': ')[-1]

    def update_indexes(self):
        for image in self.getAllImages():
            self.update_index(image)

    def hideImage(self, name, flag=True):
        image = self.getImage(name)
        if image is None:
            return
        isVisible = not flag
        if isVisible != image.isVisible():
            image.setVisible(isVisible)
            image.marker.setVisible(self.profile.show_line_marks.isChecked() and
                                    isVisible and not image.surface.from_profile)
            curve = self.getprofileswidget().getCurve(name)
            if curve is not None:
                curve.setVisible(image.isVisible())
        self.updateDisplay(skip=image.getName())

    def removeImage(self, name):
        image = self.getImage(name)
        if image is None:
            return None

        self.removeMarker(image.marker.getName())
        super().removeImage(name)
        self.getprofileswidget().removeCurve(name)

        if self.getActiveImage() is None:
            images = self.getAllImages()
            if not images:
                self.clear()
                self.getprofileswidget().clearCurves()
                return
            else:
                self.setActiveImage(images[0].getName())

    def removeLink(self, link, invalidated):
        for image in self.getOrderedImages():
            if image.link == link:
                self.removeImage(image.getName())
            elif image.link > link and not invalidated:
                image.link -= 1
                self.update_index(image)
        self.updateDisplay()

    def _addimage(self, data:Surface, link:int, subelement:int, legend=None):
        # called when new image is to be added

        name = self.get_id(link, subelement)

        image = self.addImage(data=data, legend=name, info='infos',
                              xlabel=data.x_unit, ylabel=data.y_unit,
                              origin=[min(data.x), min(data.y)],
                              scale=list(data.pixel_size),
                              copy=False,
                              add_marker=False,
                              multi=True
                              )

        # update attributes to image
        if silx_version.major < 2:
            image = self.getImage(name)
        self.add_marker(image, data.is_slopes)
        image.surface = data
        image.link = link
        image.subelement = subelement
        image.legend = legend
        # image.index = self.get_image_index(image)
        self.update_indexes()

    def updateImage(self, data:Surface, link=0, subelement=0, legend=None, fit_window=True, multiple=False):

        if data is None:
            """ link became None, remove data """
            self.removeLink(link, invalidated=True)

        if self.noise_data:
            self.getprofileswidget().noise_data = True

        if multiple and subelement == 0:
            # self.removeLink(link, True)
            for image in self.getAllImages():
                if image.link == link and image.subelement > subelement:
                    name = image.getName()
                    self.removeMarker(image.marker.getName())
                    super().removeImage(name)
                    self.getprofileswidget().removeCurve(name)

        # check all images are the same type
        for image in self.getAllImages():
            if image.surface is None:
                continue
            if image.surface.is_slopes is not data.is_slopes:
                self._viewer.ow.Error.type_mismatch()
                return

        self._addimage(data, link, subelement, legend)

        # for image in self.getAllImages():
        #     if image.link > link:
        #         # insertion
        #         self.updateDisplay()
        #         break

        if fit_window:
            self.resetZoom()

    def updateDisplay(self, skip:str = ''):
        # print('updateDisplay')
        images = self.getAllImages()
        if len(images) == 0:
            self.getprofileswidget().clearCurves()
            return
        self.auto_units()
        self.set_colormap(images, skip)
        self.shift_images()
        self.update_indexes()
        self.update_profiles()

        self.resetZoom()

        if self.noise_data:
            self.hide_line_markers()
            return

        self.calc_noise()

    def auto_units(self, limit=True):
        images = self.getAllImages()
        if len(images) == 0:
            return
        if self._units is None:
            self._units = images[0].surface.units.copy()
        else:
            lengths = [image.surface.coords_unit.SI_unit(image.surface.length[0], image.surface.coords_unit) for image in images]
            # half_pv = [image.surface.values_unit.SI_unit(image.surface.pv / 2, image.surface.values_unit) for image in images]
            _, coords_unit = self._units['coords'].SI_unit.auto(max(lengths))
            self._units['coords'] = u.mm if str(coords_unit) == 'm' else coords_unit

            rms = [image.surface.values_unit.SI_unit(image.surface.rms, image.surface.values_unit) for image in images]
            SI_val = self._units['values'].SI_unit
            _, self._units['values'] = SI_val.auto(max(rms), modulo=2)
            factor = SI_val(1, self._units['values'])
            if limit and images[0].surface.is_slopes and factor < 1e6:
                _, self._units['values'] = SI_val.auto(1e-5)
            if limit and images[0].surface.is_heights and factor < 1e9:
                _, self._units['values'] = SI_val.auto(1e-8)

            roc = [image.surface.radius_unit.SI_unit(image.surface.radius, image.surface.radius_unit) for image in images]
            if None not in roc:
                SI_val = self._units['radius'].SI_unit
                _, self._units['radius'] = SI_val.auto(max(roc), modulo=5)

            for image in images:
                image.surface.change_units(self._units)
                image.setData(image.surface.values.T)

        self.getProfileToolbar().getProfileWidthAction().update_unit(self._units['coords'])
        self.getColorBarWidget().setLegend(str(self._units['values']))

    def update_colormap(self, data=None, update=True):
        super().update_colormap(data, update) if self.colors.is_fixed() else self.set_colormap()

    def set_colormap(self, images=None, skip:str = '', use_data=False):
        # print('set_colormap')
        if images is None:
            images = self.getAllImages()

        if len(images) == 0:
            return

        slopes = images[0].surface.is_slopes

        if self.colors.is_fixed():
            self.update_colormap()
            return

        if use_data:
            temp = []
            for image in images:
                if image.getName() == skip:
                    continue
                temp = np.add(temp, image.getData(copy=False))
            if len(temp) == 0:
                self.clear()
                self.getprofileswidget().clearCurves()
                return
            self.colormap, self.cmap_prms = convert_colormap(temp / len(images), automasking=True, slopes=slopes)
        else:
            master = np.argmax([image.surface.ra for image in images])
            if master > len(images):
                return
            self.colormap, self.cmap_prms = convert_colormap(images[master].getData(copy=False), automasking=True, slopes=slopes)

        [image.setColormap(self.colormap) for image in images]

    def shift_images(self):
        """ called from updateDisplay or sigSetKeepDataAspectRatio signal """
        images = self.getOrderedImages()[::-1]
        # images = self.get_visible_images(ordered=True)[::-1] # TODO: add option
        if not images:
            return
        lengths = [image.surface.length for image in images]
        widths, heights = zip(*lengths)
        ratios = np.divide(widths, heights)
        spacing = 1.5 * max(heights) * np.log(min(ratios) + 1) / (50 / np.log(min(ratios) + 1))
        cum_hgts = [0] + list(np.cumsum(heights[:-1]))
        for z, image in enumerate(images):
            origin_y = z * spacing + cum_hgts[z]
            image.setOrigin((min(image.surface.x), origin_y))

        self.update_markers()

    def calc_noise(self):
        if not self.process_noise:
            self._viewer.ow.send_noise(None)
            return

        def format_legend(image):
            source = Path(image.surface.source)
            source, suffix = 'noise_' + source.stem, source.suffix
            return source
        images = self.get_visible_images()
        count = len(images)
        if count < 2:
            self._viewer.ow.send_noise(None)
            return
        scales_x = np.array([image.surface.pixel_size[0] for image in images])
        same_res = np.abs(np.sum(scales_x - scales_x[0])) < 1e-5
        shape = np.array([image.surface.shape for image in images])
        same_shape = np.abs(np.sum(shape[:, 0] - shape[0][0]) < 1e-5 and np.abs(np.sum(shape[:, 1] - shape[0][1]))) < 1e-5
        if not same_res or not same_shape:
            self._viewer.ow.send_noise(None)
            return
        try:
            data = [image.surface.z - image.surface.mean for image in images]
            mean = np.nanmean(data, axis=0)
            noises = np.add(data,  np.negative(mean)) / np.sqrt(count)
            noise = StitchingDataset([Surface(image.surface.coords, noise, image.surface.units, format_legend(image))
                                      for image, noise in zip(images, noises)])
            noise.noise_data = True
            self._viewer.ow.send_noise(noise)
        except Exception as e:
            print('error calc_noise: ', e)
            self._viewer.ow.send_noise(None)

    # name mangling
    def __activeImageChanged(self, previous, legend):
        try:
            self._Plot2D__activeImageChanged(previous, legend)
        except TypeError:
            """"""
        # print('__activeImageChanged', self.getDataRange())
        if previous is None:
            self.getMaskToolsDockWidget().widget().resetSelectionMask()
