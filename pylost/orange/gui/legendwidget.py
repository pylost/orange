# coding=utf-8
"""
silx MaskToolsWidget customization

"""

import numpy as np

from silx.gui.qt import Signal, QColor, QMenu
from silx.gui.plot.LegendSelector import LegendsDockWidget, RenameCurveDialog, LegendListContextMenu


# noinspection PySuperArguments
class PylostLegendListContextMenu(LegendListContextMenu):
    """Contextual menu associated to items in a :class:`LegendListView`."""

    sigContextMenu = Signal(object)
    """Signal emitting a dict upon contextual menu actions."""

    # noinspection PyArgumentList
    def __init__(self, model):
        super(QMenu, self).__init__(parent=None)
        self.model = model

        self.addAction('Rename', self.renameItemAction)

        self.addAction('Set Active', self.setActiveAction)
        self.addAction('Map to left', self.mapToLeftAction)
        self.addAction('Map to right', self.mapToRightAction)

        self._pointsAction = self.addAction(
            'Points', self.togglePointsAction)
        self._pointsAction.setCheckable(True)

        self._linesAction = self.addAction('Lines', self.toggleLinesAction)
        self._linesAction.setCheckable(True)

        # self.addAction('Remove curve', self.removeItemAction)


class PylostLegendDockWidget(LegendsDockWidget):

    sigLegendRenamed = Signal()

    def __init__(self, parent=None, plot=None):
        super().__init__(parent, plot)

        self.setContextMenu()

    def setContextMenu(self):
        delegate = self._legendWidget.itemDelegate()
        delegate.contextMenu.sigContextMenu.disconnect(self._legendWidget._contextMenuSlot)
        delegate.contextMenu = PylostLegendListContextMenu(self._legendWidget.model())
        delegate.contextMenu.sigContextMenu.connect(self._legendWidget._contextMenuSlot)

    def _legendSignalHandler(self, ddict):
        name = self.plot.get_name(ddict['legend'])

        if ddict['event'] == "legendClicked":
            if ddict['button'] == "left":
                # self.plot.setActiveImage(name)
                self.plot.getprofileswidget().setActiveCurve(name)

        elif ddict['event'] == "removeCurve":
            # inhibit
            return

        elif ddict['event'] == "renameCurve":
            oldLegends = [curve.legend for curve in self.plot.getprofileswidget().getAllCurves(withhidden=True)]
            item = self.plot.getprofileswidget().getCurve(name)
            try:
                index, legend = item.legend.split(': ')
            except ValueError:
                index = None
                legend = item.legend
            dialog = RenameCurveDialog(self.plot, legend, oldLegends)
            dialog.resize(300, 50)
            ret = dialog.exec()
            if ret:
                updated_legend = dialog.getText()
                if index is not None:
                    updated_legend = index + ': ' + updated_legend
                item.legend = updated_legend
                image = self.plot.getImage(name)
                if image is not None:
                    image.legend = item.legend
                self.updateLegends()
                self.sigLegendRenamed.emit()
                return

        elif ddict['event'] == "setActiveCurve":
            # self.plot.setActiveImage(name)
            self.plot.getprofileswidget().setActiveCurve(name)

        elif ddict['event'] == "checkBoxClicked":
            try:
                self.plot.hideImage(name, not ddict['selected'])
                # self.plot.getprofileswidget().hideCurve(name, not ddict['selected'])
            except AttributeError:
                """ not multiplot """

        elif ddict['event'] in ["mapToRight", "mapToLeft"]:
            curve = self.plot.getprofileswidget().getCurve(name)
            yaxis = 'right' if ddict['event'] == 'mapToRight' else 'left'
            self.plot.getprofileswidget().addCurve(x=curve.getXData(copy=False),
                                                   y=curve.getYData(copy=False),
                                                   legend=curve.getName(),
                                                   info=curve.getInfo(),
                                                   yaxis=yaxis)

        elif ddict['event'] == "togglePoints":
            curve = self.plot.getprofileswidget().getCurve(name)
            # symbol = ddict['symbol'] if ddict['points'] else ''
            symbol = '.' if ddict['points'] else ''
            self.plot.getprofileswidget().addCurve(x=curve.getXData(copy=False),
                                                   y=curve.getYData(copy=False),
                                                   legend=curve.getName(),
                                                   info=curve.getInfo(),
                                                   symbol=symbol)

        elif ddict['event'] == "toggleLine":  # TODO linestyle
            curve = self.plot.getprofileswidget().getCurve(name)
            # linestyle = ddict['linestyle'] if ddict['line'] else ''
            linestyle = '-' if ddict['line'] else ''
            self.plot.getprofileswidget().addCurve(x=curve.getXData(copy=False),
                                                   y=curve.getYData(copy=False),
                                                   legend=curve.getName(),
                                                   info=curve.getInfo(),
                                                   linestyle=linestyle)

        else:
            print("unhandled event %s", str(ddict['event']))
            return

        self.updateLegends()

    def updateLegends(self, *args):
        """Sync the LegendSelector widget displayed info with the plot.
        """
        if self.plot is None:
            return

        legendList = []

        for curve in self.plot.getOrderedCurves():
            # Use active color if curve is active
            active = self.plot.getActiveCurve()
            isActive = curve.legend == active.legend if active is not None else False
            style = curve.getCurrentStyle()
            color = style.getColor()
            if np.array(color, copy=False).ndim != 1:
                # array of colors, use transparent black
                color = 0., 0., 0., 0.

            curveInfo = {
                'color': QColor.fromRgbF(*color),
                'linewidth': style.getLineWidth(),
                'linestyle': style.getLineStyle(),
                'symbol': style.getSymbol(),
                'selected': curve is not None and curve.isVisible(),
                'active': isActive}
            legendList.append((curve.legend, curveInfo))

        self._legendWidget.setLegendList(legendList)
