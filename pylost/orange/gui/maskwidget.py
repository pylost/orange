# coding=utf-8
"""
silx MaskToolsWidget customization

TODO: slow compared to other viewer (transparency?)
"""

import numpy as np
from silx.gui import qt
from silx.gui.plot import items
from pylost.orange.gui import FloatLineEdit

from silx.gui.plot.MaskToolsWidget import MaskToolsDockWidget, MaskToolsWidget


class PylostMaskToolsWidget(MaskToolsWidget):
    """Customization:
            - manual inputs widget added
            - only one level allowed
            - mask color inverted
    """

    sigMaskSent = qt.Signal(dict)
    sigInputsChanged = qt.Signal(dict)
    # sigUnitChanged = qt.Signal(tuple)

    def __init__(self, viewer=None, plot=None, profile=False):
        self._viewer = viewer
        self._viewer_is_1D = profile
        super().__init__(plot=plot)
        self._params = {}
        self._axis = np.array((0, 0))
        self._left = None
        self._right = None
        self.correction = False

    def showEvent(self, event):
        super(PylostMaskToolsWidget, self).showEvent(event)
        self._axis = self._getSurface().coords
        if self._viewer_is_1D:
            self._axis = [self._axis, self._getSurface().values]
        for axis, coords in enumerate(self._axis):
            self._axis[axis] = coords - np.nanmean(coords)
        if self._params:
            self.update_from_dict(self._params)
        else:
            if self._getSurface().is_oversampled and self._getSurface().is_1D:
                self.correction = True
            self._resetInputs()
        if self._viewer_is_1D:
            self.setEnabled(True)
            self._scale = (self._getSurface().pixel_size, self._getSurface().pv / len(self._getSurface().coords))
        else:
            self._activeRectMode()

    def update_from_dict(self, params:dict):
        self._params = params
        if self._viewer_is_1D:
            self._scale = (self._getSurface().pixel_size, self._getSurface().pv / len(self._getSurface().coords))
        else:
            self._scale = tuple(self._getSurface().pixel_size)
        pixel = params['is_pixel']
        inputs = params['pixels'] if pixel else params['values']
        self._setInputs(inputs, pixel, params['is_relative'])
        self._validate()

    def _resetMask(self):
        self._resetInputs()
        self._validate()

    def _resetInputs(self):
        origin = [0, 0]
        if not self.relative.isChecked():
            origin = [self._viewer._offsets[axis] for axis in (0, 1)]
        shape = self._getSurface().shape
        if self._viewer_is_1D:
            shape = [shape[0], 0]
        inputs = [origin, shape]
        if not self.pixel.isChecked():
            inputs = self._PixtoCoords(*inputs)
        self._setInputs(inputs)

    def _getSurface(self):
        """Get the current surface object from PylostSilxPlotViewer"""
        return self._viewer.currentSurface()

    def _plotDrawEvent(self, event):
        if self._drawingMode is None or not len(self._data):
            return
        if event['event'] not in ('drawingProgress', 'drawingFinished'):
            return
        if self._drawingMode in ('rectangle', 'ellipse'):
            # print('drawingProgress', event)
            offsets = [0, 0] if self.relative.isChecked() else self._viewer._offsets
            offsets = offsets if self.pixel.isChecked() else np.multiply(offsets, self._scale)
            sx, sy = self._scale if self.pixel.isChecked() else [1, 1]
            if self._drawingMode == 'rectangle':
                height = abs(event['height']) / sx
                width = abs(event['width']) / sy
                ox = (min(event['xdata'])) / sy
                oy = (min(event['ydata'])) / sx
                centre = [ox + width / 2 + offsets[0], oy + height / 2 + offsets[1]]
            else:
                height = 2 * abs(event['ydata'][1]) / sx
                width = 2 * abs(event['xdata'][1]) / sy
                centre = [event['xdata'][0] / sy + offsets[0], event['ydata'][0] / sx + offsets[1]]
            self._setInputs([centre, [width, height]])
            if event['event'] == 'drawingFinished':
                self._validate()

    def _updatePlotMask(self):
        # 1D plot: draw vertical lines
        if self._viewer_is_1D:
            return

        # 2D plot: overriding: invert mask colors
        mask = self.getSelectionMask(copy=False)
        # print('_updatePlotMask')
        if mask is not None and self._params:
            # invert color if a mask is defined
            invert = mask == 0
            if not np.all(invert):
                mask = np.where(invert, 1, 0)
            # get the mask from the plot
            maskItem = self.plot.getImage(self._maskName)
            mustBeAdded = maskItem is None
            if mustBeAdded:
                maskItem = items.MaskImageData()
                maskItem.setName(self._maskName)
            # update the items
            maskItem.setData(mask, copy=False)
            maskItem.setColormap(self._colormap)
            maskItem.setOrigin(self._origin)
            maskItem.setScale(self._scale)
            maskItem.setZValue(self._z)

            if mustBeAdded:
                self.plot.remove(self._maskName, kind='image')  # replace mask
                self.plot.addItem(maskItem)

        elif self.plot.getImage(self._maskName):
            self.plot.remove(self._maskName, kind='image')

    def _getInputs(self):
        return np.array([[self.centreX.value(), self.centreY.value()],
                         [self.sizeX.value(), self.sizeY.value()]])

    def _setInputs(self, inputs:list, pixel=None, relative=None):
        unit = 'pix'
        fmt = '.4f'
        if self.pixel.isChecked():
            fmt = '.3f'
            inputs = np.asfarray(inputs)
        else:
            unit = str(self._getSurface().coords_unit)
        self.unitXLabel.setText(f'  X ({unit})')
        self.unitYLabel.setText(f'  Y ({unit})')
        self._blockinputssignals(True)
        if pixel is not None:
            self.pixel.setChecked(pixel)
        if relative is not None:
            self.relative.setChecked(relative)
        self.centreX.setValue(inputs[0][0])
        self.centreX.update_format(fmt)
        self.centreY.setValue(inputs[0][1])
        self.centreY.update_format(fmt)
        self.sizeX.setValue(inputs[1][0])
        self.sizeX.update_format(fmt)
        self.sizeY.setValue(inputs[1][1])
        self.sizeY.update_format(fmt)
        self._blockinputssignals(False)

    # noinspection PyTypeChecker
    def _switch_unit(self):
        inputs = self._getInputs()
        values = self._CoordstoPix(*inputs) if self.pixel.isChecked() else self._PixtoCoords(*inputs)
        self._setInputs(values)
        self._validate()

    # noinspection PyTypeChecker
    def _switch_relative(self):
        centre, size = self._getInputs()
        offset = self._viewer._offsets if self.pixel.isChecked() else np.multiply(self._viewer._offsets, self._scale)
        offset = np.negative(offset) if self.relative.isChecked() else offset
        centre = np.add(centre, offset)
        self._setInputs([centre, size])
        self._validate()

    def _CoordstoPix(self, centre, size):
        centre_px = [centre[axis] / self._scale[axis] for axis in (0, 1)]
        size_px = [np.round(size[axis] / self._scale[axis], 0) for axis in (0, 1)]
        if self.correction:
            size_px = [size_px[axis] - axis + 1 for axis in (0, 1)]
        return [centre_px, size_px]

    def _PixtoCoords(self, centre, size):
        centre_unit = [centre[axis] * self._scale[axis] for axis in (0, 1)]
        if self.correction:
            size = [size[axis] + axis - 1 for axis in (0, 1)]
        size_unit = [size[axis] * self._scale[axis] for axis in (0, 1)]
        return [centre_unit, size_unit]

    def _get_rectangle(self, centre, size):
        offset = [0, 0] if self.relative.isChecked() else np.multiply(self._viewer._offsets, self._scale)
        centre = np.add(centre, np.negative(offset))
        half = np.divide(size, 2)
        start = np.add(centre, np.negative(half))
        stop = np.add(centre, half)
        start_pix = [self._getSurface().closest_idx(val, axis=axis) for axis, val in enumerate(start)]
        stop_pix = [self._getSurface().closest_idx(val, axis=axis) for axis, val in enumerate(stop)]
        if self.correction:
            stop_pix = [s + 1 for s in stop_pix]
        size_pix = [stop_pix[0] - start_pix[0], stop_pix[1] - start_pix[1]]
        if self._viewer_is_1D:
            start_pix[1], stop_pix[1], size_pix[1] = 0, 0, 0
        return start_pix, stop_pix, size_pix

    def _validate(self):
        self.resetSelectionMask()
        inputs = self._getInputs().tolist()
        centre, size = inputs

        self.correction = False

        if self.pixel.isChecked():
            if self._getSurface().is_oversampled and self._getSurface().is_1D:
                self.correction = size[0] == len(self._getSurface())
            self._params['is_pixel'] = True
            self._params['unit'] = 'pix'
            centre, size = self._PixtoCoords(centre, size)
        else:
            if self._getSurface().is_oversampled and self._getSurface().is_1D:
                self.correction = True
            self._params['is_pixel'] = False
            self._params['unit'] = str(self._getSurface().coords_unit)

        origin, end, size = self._get_rectangle(centre, size)
        centre = [centre[axis] / self._scale[axis] for axis in (0, 1)]
        centre = [0.0 if abs(c) < 1e-6 else c for c in centre]

        self._params['values'] = self._PixtoCoords(centre, size)
        self._params['pixels'] = [centre, size]
        unit = 'pixels' if self.pixel.isChecked() else 'values'

        self._setInputs(self._params[unit])

        self._params['is_relative'] = self.relative.isChecked()
        self._params['rectangle'] = [origin, size]

        if self._viewer_is_1D:
            x0 = max(0, origin[0])
            left = self._getSurface().coords[x0]
            x1 = min(len(self._getSurface().coords) - 1, end[0])
            right = self._getSurface().coords[x1]
            if self._left is None:
                self._add_markers(left, right)
            else:
                self._marker_position(left, right)
        else:
            self._mask.updateRectangle(level=1,
                                       row=int(origin[1]),
                                       col=int(origin[0]),
                                       height=int(size[1]) - 1,
                                       width=int(size[0]) - 1,
                                       mask=True)
        self.sigInputsChanged.emit(self._params)

    def mouse_moved(self):
        if self._left is not None and self._right is not None:
            if self._left.isBeingDragged() or self._right.isBeingDragged():
                left, right = self._marker_closest_pixel()
                self._marker_position(left, right)
                self._marker_moving()

    def _marker_closest_pixel(self):
        markers_pos = []
        for marker in (self._left, self._right):
            size = len(self._getSurface().coords)
            idx = self._getSurface().closest_idx(marker.getXPosition())
            idx = min(size, max(0, idx))
            if self._getSurface().is_oversampled and idx >= size:
                idx -= 1
            try:
                pos = self._getSurface().coords[idx]
            except IndexError:
                """ should never be reached"""
                pos = self._getSurface().coords[idx - 1]
            markers_pos.append(pos)
        return min(markers_pos), max(markers_pos)

    def _marker_position(self, left, right):
        self._left.setText(f'{left:0.3f}')
        self._left.setPosition(left, 0)
        self._right.setText(f'{right:0.3f}')
        self._right.setPosition(right, 0)

    def _marker_moving(self):
        coord = self._marker_closest_pixel()
        inputs = [[np.mean(coord), 0], [coord[1] - coord[0], 0]]
        inputs = self._CoordstoPix(*inputs) if self.pixel.isChecked() else inputs
        self._setInputs(inputs)

    def _marker_moved(self):
        self._marker_moving()
        self._validate()

    def _add_markers(self, left, right):
        self._viewer._plot.addXMarker(left, legend='left', text=f'l:{left:.3f}',
                                      selectable=False, draggable=True, color=None,
                                      constraint=None, yaxis='left')
        self._viewer._plot.addXMarker(right, legend='right', text=f'r:{right:.3f}',
                                      selectable=False, draggable=True, color=None,
                                      constraint=None, yaxis='left')
        self._left = self._viewer._plot._getMarker('left')
        self._right = self._viewer._plot._getMarker('right')
        self._left.sigDragFinished.connect(self._marker_moved)
        self._right.sigDragFinished.connect(self._marker_moved)

    def _apply(self):
        if self._params:
            self.sigMaskSent.emit(self._params)

    def _blockinputssignals(self, block):
        self.relative.blockSignals(block)
        self.pixel.blockSignals(block)
        self.sizeX.blockSignals(block)
        self.sizeY.blockSignals(block)
        self.centreX.blockSignals(block)
        self.centreY.blockSignals(block)

    def _updateDrawingModeWidgets(self):
        """remove mask/unmask checkbox"""
        # self.maskStateWidget.setVisible(self._drawingMode is not None)
        self.pencilSetting.setVisible(self._drawingMode == 'pencil')

    # noinspection PyArgumentList
    def _initWidgets(self):
        """Create widgets"""
        # print('customizing mask widget')
        layout = qt.QBoxLayout(qt.QBoxLayout.LeftToRight)
        maskGroup = self._initMaskGroupBox()
        layout.addWidget(maskGroup)
        self._levelWidget.hide()  # no different level
        self.transparencySlider.blockSignals(True)
        self.transparencySlider.setValue(6)
        self.transparencySlider.blockSignals(False)
        drawGroup = self._initDrawGroupBox()
        layout.addWidget(drawGroup)
        self.inputGroup = self._initInputGroupBox()
        layout.addWidget(self.inputGroup)
        thresholdGroup = self._initThresholdGroupBox()
        layout.addWidget(thresholdGroup)
        otherGroup = self._initOtherToolsGroupBox()
        layout.addWidget(otherGroup)
        layout.addStretch(1)
        self.setLayout(layout)
        if self._viewer_is_1D:
            maskGroup.hide()
            drawGroup.hide()
            thresholdGroup.hide()
            otherGroup.hide()
            self.relative.setEnabled(False)
            self.centreY.setEnabled(False)
            self.sizeY.setEnabled(False)

    # noinspection PyUnresolvedReferences,PyArgumentList
    def _initInputGroupBox(self):
        """Init manual inputs tools widgets"""

        spacer = qt.QWidget(parent=self)
        spacer.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Preferred)
        mode = qt.QHBoxLayout()
        # mode.setContentsMargins(0, 0, 0, 0)
        self.relativeLabel = qt.QLabel('Relative to data:', self)
        self.relative = qt.QCheckBox()
        self.relative.setToolTip(
            'Choose whether the mask is relative to the valid pixels\n'
            'or centered on the detector array (e.g. Metropro mask).')
        self.relative.stateChanged.connect(self._switch_relative)
        self.pixelLabel = qt.QLabel('Unit in pixel:', self)
        self.pixel = qt.QCheckBox()
        self.pixel.setToolTip('Choose whether inputs are in data unit or in pixel.')
        self.pixel.stateChanged.connect(self._switch_unit)
        mode.addWidget(self.relativeLabel)
        mode.addWidget(self.relative)
        mode.addWidget(self.pixelLabel)
        mode.addWidget(self.pixel)
        mode.addWidget(spacer)

        inputs = qt.QGridLayout()
        inputs.setAlignment(qt.Qt.AlignCenter)
        # inputs.setContentsMargins(0, 0, 0, 0)
        self.centerLabel = qt.QLabel('Center:', self)
        self.sizeLabel = qt.QLabel('Size:', self)
        self.unitXLabel = qt.QLabel('X (pixel)', self)
        # self.unitXLabel.setAlignment(qt.Qt.AlignCenter)
        self.unitYLabel = qt.QLabel('Y (pixel)', self)
        # self.unitXLabel.setAlignment(qt.Qt.AlignCenter)
        inputs.addWidget(self.centerLabel, 1, 0)
        inputs.addWidget(self.sizeLabel, 2, 0)
        inputs.addWidget(self.unitXLabel, 0, 1)
        inputs.addWidget(self.unitYLabel, 0, 2)
        self.centreX = FloatLineEdit(self, value=0, fmt='.4f', maxwidth=61,  callback=self._validate)
        self.centreY = FloatLineEdit(self, value=0, fmt='.4f', maxwidth=61, callback=self._validate)
        self.sizeX = FloatLineEdit(self, value=0, fmt='.4f', maxwidth=61, callback=self._validate)
        self.sizeY = FloatLineEdit(self, value=0, fmt='.4f', maxwidth=61, callback=self._validate)
        inputs.addWidget(self.centreX, 1, 1)
        inputs.addWidget(self.centreY, 1, 2)
        inputs.addWidget(self.sizeX, 2, 1)
        inputs.addWidget(self.sizeY, 2, 2)
        inputs.addWidget(spacer, 2, 3)

        actions = qt.QHBoxLayout()
        self.resetBtn = qt.QPushButton('Reset mask')
        self.resetBtn.clicked.connect(self._resetMask)
        self.applyBtn = qt.QPushButton('Apply mask')
        self.applyBtn.clicked.connect(self._apply)
        actions.addWidget(self.applyBtn)
        actions.addWidget(self.resetBtn)

        layout = qt.QVBoxLayout()
        layout.setAlignment(qt.Qt.AlignCenter)
        layout.setSpacing(11)
        layout.addLayout(mode)
        layout.addLayout(inputs)
        layout.addLayout(actions)
        layout.addStretch(1)

        self.inputsGroup = qt.QGroupBox('Manual inputs')
        self.inputsGroup.setLayout(layout)

        return self.inputsGroup


class PylostMaskToolsDockWidget(MaskToolsDockWidget):
    def __init__(self, viewer=None, plot=None, name='Mask', profile=False):
        widget = PylostMaskToolsWidget(viewer=viewer, plot=plot, profile=profile)
        super(MaskToolsDockWidget, self).__init__(name=name, widget=widget)
