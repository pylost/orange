# coding=utf-8
"""
silx ToolBar customization

"""

import numpy as np
from matplotlib.figure import Figure
# from matplotlib.colors import LogNorm
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT


class PylostPlotCanvas(FigureCanvasQTAgg):
    def __init__(self, parent, ow, figsize=(6.32, 6.32)):
        self.fig = Figure(figsize=figsize, edgecolor='gray', linewidth=0.1, tight_layout=True)
        super().__init__(self.fig)
        self.setParent(parent)
        self.ow = ow
        self.ax = None

    def _add_ax(self):
        self.ax = self.fig.add_subplot(1, 1, 1)

    def _draw(self):
        if self.ax is not None:
            self.ax.legend(fontsize='7', loc='lower left')
        self.draw()
        self.flush_events()
        # plt.pause(0.0001)

    def clear_plots(self):
        self.fig.clf()
        self.ax = None
        self._draw()

    def draw_data(self, *data, clear=True, legend=None, units=None):
        if self.ax is None:
            self._add_ax()
        elif clear:
            self.ax.clear()
        self.ax.plot(*data, label=legend)
        self.ax.grid(color='silver', linestyle='-', linewidth=1, which='both')
        self.ax.set_xlim(min(data[0]), np.nanmax(data[0]))
        self.ax.set_ylim(min(data[1]), np.nanmax(data[1]) * 1.02)
        if units is not None:
            self.ax.set_xlabel(str(units[0]))
            self.ax.set_ylabel(str(units[1]))
        self._draw()


class PSDPlotCanvas(PylostPlotCanvas):
    def __init__(self, parent, ow, figsize=(6.32, 6.32)):
        super().__init__(parent, ow, figsize)
        self.axes = self.fig.subplots(1, 2)

    def clear_plots(self):
        self.fig.clf()
        self.axes = self.fig.subplots(1, 2)
        self._draw()

    def _set_psd(self, freq, psd, units:dict, legend:str):
        unit_freq = str(units['coords']) + '$^-$' + '$^1$'
        unit_psd = str(units['values']) + ' $^2$ / ' + unit_freq
        self.axes[0].loglog(freq, psd, label=legend)
        self.axes[0].set_xlabel('Spatial freq. ' + '(' + unit_freq + ')')
        self.axes[0].set_ylabel(unit_psd)
        self.axes[0].set_title(f'Power Spectral Density', fontdict={'fontsize': 12})
        self.axes[0].grid(color='silver', linestyle='-', linewidth=1, which='both')
        self.axes[0].legend(fontsize='7', loc='upper right')

    def _set_csp(self, freq, csp, units:dict):
        unit_freq = str(units['coords']) + '$^-$' + '$^1$'
        unit_csp = str(units['values'])
        self.axes[1].semilogx(freq, csp)
        self.axes[1].yaxis.tick_right()
        self.axes[1].set_xlabel('Spatial freq. ' + '(' + unit_freq + ')')
        self.axes[1].set_ylabel(unit_csp)
        self.axes[1].set_title(f'Cumulative Spectral Power', fontdict={'fontsize': 12})
        self.axes[1].yaxis.set_label_position('right')
        self.axes[1].grid(color='silver', linestyle='-', linewidth=1, which='both')

    def draw_plots(self, freq, psd, csp, units:dict, legend:str, replace=True):
        if replace:
            for ax in self.axes:
                ax.clear()
        self._set_psd(freq, psd, units, legend)
        self._set_csp(freq, csp, units)
        self._draw()


class PSDPlotToolbar(NavigationToolbar2QT):
    def __init__(self, canvas, parent=None, coordinates=True):
        super().__init__(canvas, parent, coordinates)
