# coding=utf-8
"""
silx ToolBar customization

"""

import weakref

from silx.gui import qt
from silx.gui.plot.PlotToolButtons import ProfileOptionToolButton
from silx.gui.plot.Profile import ProfileToolBar, manager#, _CustomProfileManager
# from silx.gui.plot.tools.profile.manager import ProfileManager
from silx.gui.plot.tools.profile.editors import _DefaultImageProfileRoiEditor, ProfileRoiEditorAction
from silx.gui.plot.actions import PlotAction


class NoiseAction(PlotAction):
    def __init__(self, plot, parent=None, apply_noise=False):
        icon = qt.QIcon()
        text = 'Send Noise'
        tooltip = 'Send noise data to output'
        triggered = self._actionTriggered
        checkable = True
        super(NoiseAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self.setChecked(False)
        self._checked = apply_noise

    # noinspection PyUnusedLocal
    def _actionTriggered(self, checked=True):
        self._checked = not self._checked
        self.plot.process_noise = self._checked
        self.plot.calc_noise()


class PlaneFitAction(PlotAction):
    def __init__(self, plot, parent=None, apply_fit=True):
        icon = qt.QIcon()
        text = 'Linear Fit active' if apply_fit else 'No fit Active'
        tooltip = 'Apply a polynomial fit to each profile'
        triggered = self._actionTriggered
        checkable = True
        super(PlaneFitAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self.setChecked(True)
        self._checked = apply_fit

    # noinspection PyUnusedLocal
    def _actionTriggered(self, checked=True):
        self._checked = not self._checked
        self.plot.getprofileswidget().apply_fit = self._checked
        self.plot.getprofileswidget().setProfile(update_all=True)
        if self._checked:
            self.setIconText('Linear Fit active')
        else:
            self.setIconText('No Fit Active')


class ShowProfileAction(PlotAction):
    def __init__(self, plot, parent=None, show_profiles=True):
        icon = qt.QIcon()
        text = 'Hide Profiles' if show_profiles else 'Show Profiles'
        tooltip = 'Show or Hide Profiles Widget'
        triggered = self._actionTriggered
        checkable = False
        super(ShowProfileAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self._checked = show_profiles

    # noinspection PyUnusedLocal
    def _actionTriggered(self, checked=False):
        self._checked = not self._checked
        if self._checked:
            self.setIconText('Hide Profiles')
            self.plot.getprofileswidget().show()
        else:
            self.setIconText('Show Profiles')
            self.plot.getprofileswidget().hide()
        self.plot.profile.show_line_marks._setChecked(checked)
        # self.plot.switch_line_markers(checked)


class ShowStatsAction(PlotAction):
    def __init__(self, plot, parent=None):
        icon = qt.QIcon()
        text = 'Show Statistics'
        tooltip = 'Show or Hide Profiles Widget'
        triggered = self.switch
        checkable = True
        super(ShowStatsAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self.setChecked(False)

    def switch(self, checked=False):
        if checked:
            self.setIconText('Hide Statistics')
            self.plot.getstatswindow().show()
        else:
            self.setIconText('Show Statistics')
            self.plot.getstatswindow().hide()

    def _setChecked(self, checked):
        self.setChecked(checked)
        if checked:
            self.setIconText('Hide Statistics')
        else:
            self.setIconText('Show Statistics')


class ShowLineMarksAction(PlotAction):
    def __init__(self, plot, show_markers=False, parent=None):
        icon = qt.QIcon()
        text = 'Markers'
        tooltip = 'Show or Hide Line Markers'
        triggered = self.switch
        checkable = True
        super(ShowLineMarksAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self.setChecked(show_markers)

    def switch(self, checked=False):
        try:
            self.plot.switch_line_markers(checked)
        except AttributeError:
            """"""

    def _setChecked(self, checked):
        # self.switch(checked)
        self.setChecked(checked)


class CenterLinesAction(PlotAction):
    def __init__(self, plot, parent=None):
        icon = qt.QIcon()
        text = 'Reset'
        tooltip = 'Center Markers & X coordinates'
        triggered = self.reset
        checkable = False
        super(CenterLinesAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)

    # noinspection PyUnusedLocal
    def reset(self, checked=False):
        try:
            self.plot.reset_markers()
        except AttributeError:
            """"""


class PylostCustomToolbar(qt.QToolBar):
    """"""
    def __init__(self, parent=None, plot=None, title=None, show_profiles=True, apply_fit=True, verbose=False):
        super(PylostCustomToolbar, self).__init__(title, parent)
        self.verbose = verbose
        assert plot is not None
        self._plotRef = weakref.ref(plot)

        self.show_profiles_widget = ShowProfileAction(plot, show_profiles=show_profiles)
        self.addAction(self.show_profiles_widget)

        self.plane_fit_widget = PlaneFitAction(plot, apply_fit=apply_fit)
        self.addAction(self.plane_fit_widget)

        self.show_stats_widget = ShowStatsAction(plot)
        self.addAction(self.show_stats_widget)

        if 'PylostMulti2D' in str(type(plot)):
            self.show_noise_widget = NoiseAction(plot)
            self.addAction(self.show_noise_widget)


class ColormapAction(PlotAction):
    def __init__(self, plot, params, parent=None, auto=True):
        icon = qt.QIcon()
        text = 'Colors Auto' if auto else 'Fixed Colors'
        tooltip = 'Quick Colormap settings'
        triggered = self._actionTriggered
        checkable = True
        super(ColormapAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self.setChecked(not auto)
        self._checked = not auto
        self._params = params

    # noinspection PyUnusedLocal
    def _actionTriggered(self, checked=True):
        self._checked = not self._checked

        if self._checked:
            self.setIconText('Fixed Colors')
            self._params.fill_elements(**self.plot.cmap_prms)
        else:
            self.setIconText('Colors Auto')
            self.plot.update_colormap()
        self._params.show_elements(self._checked)


class ColormapEditorAction(qt.QWidgetAction):

    sigeditingFinished = qt.Signal()

    params = ('zmin', 'z_lo', 'z_hi', 'zmax')

    def __init__(self, parent=None):
        super(ColormapEditorAction, self).__init__(parent)
        self.zmin = None
        self.z_lo = None
        self.z_hi = None
        self.zmax = None

    def show_elements(self, show=True):
        for prm in self.params:
            wid = getattr(self, prm)
            wid.show() if show else wid.hide()

    def fill_elements(self, **prms):
        for attribute in self.params:
            wid = getattr(self, attribute)
            wid.setValue(prms[attribute])

    def get_elements(self):
        return [getattr(self, prm).value() for prm in self.params]

    # noinspection PyArgumentList,PyUnresolvedReferences
    def createWidget(self, parent):
        """Inherit the method to create a new editor"""
        widget = qt.QWidget(parent)
        layout = qt.QHBoxLayout(widget)
        if isinstance(parent, qt.QMenu):
            margins = layout.contentsMargins()
            layout.setContentsMargins(margins.left(), 0, margins.right(), 0)
        else:
            layout.setContentsMargins(0, 0, 0, 0)

        lbl = qt.QLabel("   ")
        layout.addWidget(lbl)

        self.zmin = qt.QDoubleSpinBox()
        self.zmin.setRange(-1000, 1000)
        # self.zmin.setValue(1)
        self.zmin.setSingleStep(0.1)
        # self.zmin.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self.zmin)
        self.zmin.editingFinished.connect(self.sigeditingFinished)

        self.z_lo = qt.QDoubleSpinBox()
        self.z_lo.setRange(-1000, 1000)
        # self.z_lo.setValue(1)
        self.z_lo.setSingleStep(0.1)
        # self.z_lo.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self.z_lo)
        self.z_lo.editingFinished.connect(self.sigeditingFinished)

        self.z_hi = qt.QDoubleSpinBox()
        self.z_hi.setRange(-1000, 1000)
        # self.z_hi.setValue(1)
        self.z_hi.setSingleStep(0.1)
        # self.z_hi.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self.z_hi)
        self.z_hi.editingFinished.connect(self.sigeditingFinished)

        self.zmax = qt.QDoubleSpinBox()
        self.zmax.setRange(-1000, 1000)
        # self.zmax.setValue(1)
        self.zmax.setSingleStep(0.1)
        # self.zmax.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self.zmax)
        self.zmax.editingFinished.connect(self.sigeditingFinished)

        self.show_elements(False)

        return widget


class PylostColorToolbar(qt.QToolBar):
    """"""
    def __init__(self, parent=None, plot=None, title=None, auto=True, verbose=False):
        super(PylostColorToolbar, self).__init__(title, parent)
        self.verbose = verbose
        assert plot is not None
        self._plotRef = weakref.ref(plot)

        self.colormap_params = ColormapEditorAction(self)
        self.colormap_widget = ColormapAction(plot, self.colormap_params, auto=auto)

        self.colormap_params.sigeditingFinished.connect(plot.update_colormap)

        self.addAction(self.colormap_widget)
        self.addAction(self.colormap_params)

    def is_fixed(self):
        return self.colormap_widget._checked

    def get_elements(self):
        return self.colormap_params.get_elements()

    def fill_elements(self, **prms):
        return self.colormap_params.fill_elements(**prms)


class ProfileRangeAction(PlotAction):
    def __init__(self, plot, params, parent=None, auto=True):
        icon = qt.QIcon()
        text = 'Range Auto' if auto else 'Fixed Y Range'
        tooltip = 'Y axis Range settings'
        triggered = self._actionTriggered
        checkable = True
        super(ProfileRangeAction, self).__init__(plot, icon, text, tooltip, triggered, checkable, parent)
        self.setChecked(not auto)
        self._checked = not auto
        self._params = params

    # noinspection PyUnusedLocal
    def _actionTriggered(self, checked=True):
        self._checked = not self._checked

        if self._checked:
            self.setIconText('Fixed Y Range')
            self._params.fill_elements(**dict(zip(('ymin', 'ymax'), self.plot.getGraphYLimits())))
        else:
            self.setIconText('Range Auto')
            self.plot.resetZoom()
        self._params.show_elements(self._checked)


class ProfileRangeEditorAction(qt.QWidgetAction):

    sigeditingFinished = qt.Signal()

    params = ('ymin', 'ymax')

    def __init__(self, parent=None):
        super(ProfileRangeEditorAction, self).__init__(parent)
        self.ymin = None
        self.ymax = None

    def show_elements(self, show=True):
        for prm in self.params:
            wid = getattr(self, prm)
            wid.show() if show else wid.hide()

    def fill_elements(self, **prms):
        for attribute in self.params:
            wid = getattr(self, attribute)
            wid.setValue(prms[attribute])

    def get_elements(self):
        return [getattr(self, prm).value() for prm in self.params]

    # noinspection PyArgumentList,PyUnresolvedReferences
    def createWidget(self, parent):
        """Inherit the method to create a new editor"""
        widget = qt.QWidget(parent)
        layout = qt.QHBoxLayout(widget)
        if isinstance(parent, qt.QMenu):
            margins = layout.contentsMargins()
            layout.setContentsMargins(margins.left(), 0, margins.right(), 0)
        else:
            layout.setContentsMargins(0, 0, 0, 0)

        lbl = qt.QLabel("   ")
        layout.addWidget(lbl)

        self.ymin = qt.QDoubleSpinBox()
        self.ymin.setRange(-1000, 1000)
        # self.ymin.setValue(1)
        self.ymin.setSingleStep(0.1)
        # self.ymin.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self.ymin)
        self.ymin.editingFinished.connect(self.sigeditingFinished)

        self.ymax = qt.QDoubleSpinBox()
        self.ymax.setRange(-1000, 1000)
        # self.ymax.setValue(1)
        self.ymax.setSingleStep(0.1)
        # self.ymax.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self.ymax)
        self.ymax.editingFinished.connect(self.sigeditingFinished)

        self.show_elements(False)

        return widget


class PylostProfileRangeToolbar(qt.QToolBar):
    """"""
    def __init__(self, parent=None, plot=None, title=None, auto=True, verbose=False):
        super(PylostProfileRangeToolbar, self).__init__(title, parent)
        self.verbose = verbose
        assert plot is not None
        self._plotRef = weakref.ref(plot)

        self.range_params = ProfileRangeEditorAction(self)
        self.range_widget = ProfileRangeAction(plot, self.range_params, auto=auto)

        self.range_params.sigeditingFinished.connect(plot.update_range)

        self.addAction(self.range_widget)
        self.addAction(self.range_params)

    def is_fixed(self):
        return self.range_widget._checked

    def get_elements(self):
        return self.range_params.get_elements()


class _PylostImageProfileRoiEditor(_DefaultImageProfileRoiEditor):
    """customized _DefaultImageProfileRoiEditor"""
    def __init__(self, parent=None, scale=1, unit=None, verbose=False):
        self.verbose = verbose
        self._scale = scale
        self._unit = unit
        super().__init__(parent=parent)

    def setEditorData(self, roi):
        # update roi linewidth with the doublespin value
        roi.setProfileLineWidth(self._lineWidth.value())
        # remove profile aera slow display
        if roi._DefaultImageProfileRoiMixIn__area:
            try:
                roi.removeItem(roi._DefaultImageProfileRoiMixIn__area)
            except ValueError:
                """"""
        super().setEditorData(roi)

    def setRoiData(self, roi):
        # update roi linewidth with the doublespin value
        roi.setProfileLineWidth(self._lineWidth.value())
        # remove profile aera slow display
        super().setRoiData(roi)
        if roi._DefaultImageProfileRoiMixIn__area:
            try:
                roi.removeItem(roi._DefaultImageProfileRoiMixIn__area)
            except ValueError:
                """"""

    def update_unit(self, unit):
        self._unit = unit
        self._lineWidthUnit.setToolTip(f'Line width in {self._unit}')
        self._lbl_unit.setText(str(unit))

    # noinspection PyUnresolvedReferences
    def _initLayout(self, layout):
        if self.verbose:
            print('customizing roi toolbar')
        if self._unit is not None:
            self._lineWidthUnit = qt.QDoubleSpinBox(self)
            self._lineWidthUnit.setRange(self._scale, 1000 * self._scale)
            self._lineWidthUnit.setValue(round(1 / self._scale) * self._scale)
            self._lineWidthUnit.setSingleStep(self._scale)
            self._lineWidthUnit.setToolTip(f'Line width in {self._unit}')
            self._lineWidthUnit.valueChanged[float].connect(self._widthChanged)

        self._lineWidth = qt.QSpinBox(self)
        self._lineWidth.setRange(1, 1000)
        self._lineWidth.setValue(1)
        self._lineWidth.setToolTip("Line width in pixels")
        self._lineWidth.valueChanged[int].connect(self._widgetChanged)

        self._methodsButton = ProfileOptionToolButton(parent=self, plot=None)
        self._methodsButton.sigMethodChanged.connect(self._widgetChanged)

        label = qt.QLabel(' width:')
        label.setToolTip("Line width")
        layout.addWidget(label)
        if self._unit is not None:
            layout.addWidget(self._lineWidthUnit)
            self._lbl_unit = qt.QLabel(self._unit)
            layout.addWidget(self._lbl_unit)
        layout.addWidget(self._lineWidth)
        layout.addWidget(self._methodsButton)
        if self._unit is not None:
            self._lineWidth.hide()

    def _widgetChanged(self, value=None):
        self._lineWidthUnit.blockSignals(True)
        self._lineWidthUnit.setValue(self._lineWidth.value() * self._scale)
        self._lineWidthUnit.blockSignals(False)
        self.commitData()

    def _widthChanged(self, value):
        if self._unit is None:
            return
        self._lineWidth.setValue(int(round(value / self._scale)))


class PylostProfileRoiEditorAction(ProfileRoiEditorAction):
    """Customized ProfileRoiEditorAction"""
    def __init__(self, parent=None, verbose=False):
        self.verbose = verbose
        self._scale = 1
        self._unit = None
        super(PylostProfileRoiEditorAction, self).__init__(parent)

    def setUnits(self, scale, unit):
        self._scale = scale
        self._scale = unit.SI_unit(self._scale, unit) * 1e3  # mm
        self._unit = 'mm'
    #     self._lineWidthUnit = None
    #
    # def createWidget(self, parent):
    #     wid = super().createWidget(parent)
    #
    #     lbl = qt.QLabel("Profile width: ")
    #     wid.layout().addWidget(lbl)
    #     self._lineWidthUnit = qt.QDoubleSpinBox(wid)
    #     self._lineWidthUnit.setRange(self._scale, 1000 * self._scale)
    #     self._lineWidthUnit.setValue(round(1 / self._scale) * self._scale)
    #     self._lineWidthUnit.setSingleStep(self._scale)
    #     self._lineWidthUnit.setToolTip(f'Line width in {self._unit}')
    #     # self._lineWidthUnit.valueChanged[float].connect(self._widthChanged)
    #     if self._unit is not None:
    #         wid.layout().addWidget(self._lineWidthUnit)
    #         unit = qt.QLabel(self._unit)
    #         wid.layout().addWidget(unit)
    #
    #     return wid

    # def _widthChanged(self, value):
    #     if self._unit is None:
    #         return
    #     self._lineWidth.setValue(int(round(value / self._scale)))

    def getEditorClass(self, roi):
        """Inject the customized _DefaultImageProfileRoiEditor"""
        editorClass = super().getEditorClass(roi)
        if issubclass(editorClass, _DefaultImageProfileRoiEditor):
            return _PylostImageProfileRoiEditor
        return editorClass

    def _updateWidgets(self):
        """Update the kind of editor to display, according to the selected profile ROI."""
        parent = self.parent()
        editorClass = self.getEditorClass(self.__roi)
        for widget in self.createdWidgets():
            if issubclass(editorClass, _PylostImageProfileRoiEditor):
                if self.verbose:
                    print(f'switch to PylostImageProfileRoiEditor')
                editor = editorClass(parent, scale=self._scale, unit=self._unit, verbose=self.verbose)
                self.__roi.setProfileLineWidth(int(round(1 / self._scale)))
            else:
                editor = editorClass(parent)
            editor.setEditorData(self.__roi)
            self.__setEditor(widget, editor)

    # name mangling
    @property
    def __roi(self):
        return self._ProfileRoiEditorAction__roi

    # def __roiManager(self):
    #     return self._ProfileRoiEditorAction__roiManager

    def __setEditor(self, widget, editor):
        self._ProfileRoiEditorAction__setEditor(widget, editor)


# class PylostProfileManager(_CustomProfileManager):
class PylostProfileManager(manager.ProfileManager):
    def __init__(self, parent=None, plot=None, profilewidget=None, verbose=False):
        self.verbose = verbose
        self.__profileWindow = None
        super().__init__(parent=parent, plot=plot, roiManager=None)
        self.setProfileWindowClass(profilewidget)
        self.setSingleProfile(False)

        # ProfileWidthEditorAction

    def createEditorAction(self, parent):
        action = PylostProfileRoiEditorAction(parent=parent, verbose=self.verbose)
        roimanager = self.getRoiManager()
        action.setRoiManager(roimanager)
        return action

    def setProfileWindow(self, profileWindow):
        """seen as a _CustomProfileManager"""
        self.__profileWindow = profileWindow

    # noinspection PyUnusedLocal
    def initProfileWindow(self, profileWindow, roi):
        """inhibit window creation

        called when profile action is triggered
        """
        self.__profileWindow.roi_active(True)

    # noinspection PyUnusedLocal
    def clearProfileWindow(self, profileWindow):
        """called when roi action is stopped"""
        self.__profileWindow.roi_active(False)
        # profileWindow.hide()

    # def requestUpdateProfile(self, profileRoi):
    #     super(PylostProfileManager, self).requestUpdateProfile(profileRoi)
    #     self.__profileWindow.setProfile(update_all=True, silx_roi=True)


# class ShowProfileAction(PlotAction):
#     def __init__(self, plot, parent=None):
#         super(ShowProfileAction, self).__init__(
#             plot, icon=qt.QIcon(), text='Show Profiles',
#             tooltip='Show profiles widget',
#             triggered=self._actionTriggered,
#             checkable=True, parent=parent)
#
#     def _actionTriggered(self, checked=False):
#         if checked:
#             self.plot._profileswidget.show()
#         else:
#             self.plot._profileswidget.hide()

class ProfileWidthEditorAction(qt.QWidgetAction):

    sigWidthChanged = qt.Signal(float)

    def __init__(self, parent=None):
        super(ProfileWidthEditorAction, self).__init__(parent)
        self._scale = 0.01
        self._unit = 'mm'
        self._lineWidth = None
        self._lbl_unit = None

    # noinspection PyArgumentList,PyUnresolvedReferences
    def createWidget(self, parent):
        """Inherit the method to create a new editor"""
        widget = qt.QWidget(parent)
        layout = qt.QHBoxLayout(widget)
        if isinstance(parent, qt.QMenu):
            margins = layout.contentsMargins()
            layout.setContentsMargins(margins.left(), 0, margins.right(), 0)
        else:
            layout.setContentsMargins(0, 0, 0, 0)

        lbl = qt.QLabel("  Profile width:")
        layout.addWidget(lbl)

        self._lineWidth = qt.QDoubleSpinBox()
        self._lineWidth.setRange(0.0, 1000)
        self._lineWidth.setValue(1)
        self._lineWidth.setSingleStep(0.01)
        self._lineWidth.setToolTip(f'Line width in {self._unit}')
        layout.addWidget(self._lineWidth)
        self._lineWidth.editingFinished.connect(lambda: self._widthChanged(self._lineWidth.value()))

        # if self._unit is not None:
        self._lbl_unit = qt.QLabel('mm')
        # else:
        #     unit = qt.QLabel('pix')
        layout.addWidget(self._lbl_unit)

        return widget

    # def setUnits(self, scale, unit):
    #     self._scale = scale
    #     # self._scale = unit.SI_unit(self._scale, unit) * 1e3  # mm
    #     self._unit = 'mm'

    def update_unit(self, unit):
        self._unit = unit
        self._lineWidth.setToolTip(f'Line width in {self._unit}')
        self._lbl_unit.setText(str(unit))

    def getWidth(self):
        if self._lineWidth:
            return self._lineWidth.value()
        return 1

    def _widthChanged(self, value):
        self.sigWidthChanged.emit(value)
        # if self._unit is None:
        #     return
        # return value


class PylostProfileToolBar(ProfileToolBar):
    def __init__(self, parent=None, plot=None, profilewidget=None, show_markers=False, verbose=False):
        self.verbose = verbose
        self._profilewidget = profilewidget
        super().__init__(parent=parent, plot=plot, profileWindow=profilewidget)

        self._ProfileToolBar__multiAction.setVisible(False)
        self.clearAction.setVisible(False)

        # add width action in the toolbar
        self._profilewidthAction = ProfileWidthEditorAction(self)
        self._profilewidthAction.sigWidthChanged.connect(lambda: self._profilewidget.setProfile(update_all=True))
        self.addAction(self._profilewidthAction)

        self.show_line_marks = ShowLineMarksAction(plot, show_markers)
        self.addAction(self.show_line_marks)

        self.center_line_marks = CenterLinesAction(plot)
        self.addAction(self.center_line_marks)

    def getProfileWidthAction(self):
        return self._profilewidthAction

    def getWidth(self):
        return self._profilewidthAction.getWidth()

    def createProfileManager(self, parent, plot):
        return PylostProfileManager(parent=parent, plot=plot, profilewidget=self._profilewidget, verbose=self.verbose)

    def getProfilePlot(self):
        return self._profilewidget
