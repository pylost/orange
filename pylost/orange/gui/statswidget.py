# coding=utf-8
"""
https://www.w3.org/TR/SVG11/types.html#ColorKeywords
https://stackoverflow.com/questions/36196988/color-individual-horizontal-headers-of-qtablewidget-in-pyqt
"""

from pylost.orange.gui import *

from AnyQt.QtGui import QBrush, QColor, QFont, QPalette


class ColoredTableHeaderStyle(QtWidgets.QProxyStyle):
    def drawControl(self, element, option, painter, widget=None):
        if element == QtWidgets.QStyle.ControlElement.CE_HeaderSection and isinstance(widget, QHeaderView):
            fill = option.palette.brush(QPalette.ColorRole.Light)  # the Qt implementation actually sets the background brush on the Window color role, the default Windows style simply ignores it
            painter.fillRect(option.rect, fill)  # fill the header section with the background brush
            # border = option.palette.brush(QPalette.ColorRole.Window)  # the Qt implementation actually sets the background brush on the Window color role, the default Windows style simply ignores it
            # painter.drawRect(option.rect)
        else:
            self.baseStyle().drawControl(element, option, painter, widget)  # use the default implementation in all other cases


# noinspection PyArgumentList,PyTypeChecker
class StatsTable(QTableWidget):

    # sigEntriesChanged = QtCore.Signal()
    # self.sigEntriesChanged.emit()
    boldfont = QFont()
    boldfont.setBold(True)

    def __init__(self, *args, **kwargs):
        super(StatsTable, self).__init__(*args, **kwargs)
        self.noise_data = False

        # self.setStyleSheet(stylesheet)

        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.setFocusPolicy(Qt.NoFocus)

        self.setRowCount(0)
        self.setColumnCount(6)

        # color = QColor('lightskyblue')
        for col, name in enumerate(['Measurement', 'type', 'rms', 'pv', 'radius', 'radius (sag)']):
            item = QtWidgets.QTableWidgetItem(name)
            item.setFont(self.boldfont)
            # item.setBackground(QBrush(QColor(color)))
            self.setHorizontalHeaderItem(col, item)
        # self.horizontalHeader().setStyleSheet("QHeaderView::section { border-bottom: 1px gray; "
        #                                       "background-color:lightskyblue;}")

        self.horizontalHeader().setDefaultSectionSize(100)
        self.verticalHeader().setDefaultSectionSize(50)

    def add_noise_full_stats(self, full_rms, full_pv, unit):
        self.noise_data = True

        self.insertRow(0)

        item = QtWidgets.QTableWidgetItem('Noise')
        item.setFont(self.boldfont)
        self.setVerticalHeaderItem(0, item)

        legend = '    All visible data'
        datatype = '                 global noise statistics'
        rms = f'{full_rms:.3f} {unit}'
        pv = f'{full_pv:.3f} {unit}'

        color = QColor('lightyellow')
        for col, val in enumerate((legend, datatype, rms, pv)):
            item = QTableWidgetItem(val)
            item.setFont(self.boldfont)
            item.setBackground(QBrush(color))
            self.setItem(0, col, item)

        self.removeColumn(4)
        self.removeColumn(5)

    def fill_row(self, index, curve, image, width):
        if self.rowCount() <= index:
            self.insertRow(index)

        unit_coo = str(curve.profile.coords_unit)
        unit_val = str(curve.profile.values_unit)
        unit_roc = str(curve.profile.radius_unit)

        legend = curve.legend.split(': ')[-1]

        if image is not None and not image.surface.from_profile:
            size = image.surface.length
            unit = image.surface.coords_unit
            datatype = f'Full surface  [{size[0]:.3f} {unit} x {size[1]:.3f} {unit}]\n' \
                       f'Profile (Y = {image.marker.offset:.2f} {unit_coo}, width = {width:.2f} {unit_coo})'
        else:
            datatype = f'Profile  [{curve.profile.length:.3f} {curve.profile.coords_unit}]'

        rms = ''
        if unit_val not in ('unitless',):
            rms = f'{curve.profile.rms:.3f} {unit_val}'
            if image is not None and not image.surface.from_profile:
                rms = f'{image.surface.rms:.3f} {unit_val}' + '\n' + rms

        pv = ''
        if unit_val not in ('unitless',):
            pv = f'{curve.profile.pv:.3f} {unit_val}'
            if image is not None and not image.surface.from_profile:
                pv = f'{image.surface.pv:.3f} {unit_val}' + '\n' + pv

        radius = ''
        if curve.profile.radius is not None:
            radius = f'{curve.profile.radius:.3f} {unit_roc}'
            if image is not None and not image.surface.from_profile:
                if abs(image.surface.radius) > 1e6:
                    radius = 'inf'
                else:
                    radius = f'{image.surface.radius:.3f} {unit_roc}' + '\n' + radius

        radius_sag = ''
        if image is not None and not image.surface.from_profile:
            if image.surface.radius_sag is not None:
                if abs(image.surface.radius_sag) > 1e6:
                    radius_sag = 'inf'
                else:
                    radius_sag = f'{image.surface.radius_sag:.3f} {unit_roc}'

        color = QColor('cyan') if index % 2 == 0 else QColor('mistyrose')#'lavender'

        # item = QtWidgets.QTableWidgetItem(str(curve.legend))
        item = QtWidgets.QTableWidgetItem(f'plot {curve.index}\n ({curve.link}.{curve.subelement})')
        item.setFont(self.boldfont)
        # item.setBackground(QBrush(color))
        self.setVerticalHeaderItem(index, item)

        values = (legend, datatype, rms, pv) if self.noise_data else (legend, datatype, rms, pv, radius, radius_sag)
        for col, val in enumerate(values):
            item = QTableWidgetItem(val)
            item.setBackground(QBrush(color))
            self.setItem(index, col, item)

    def resizeColumns(self):
        self.resizeColumnsToContents()
        for col in range(self.columnCount()):
            self.setColumnWidth(col, self.columnWidth(col) + 21)

    def removeAllRows(self):
        for row in range(self.rowCount()):
            self.removeRow(row)

    def removeColumns(self):
        for row in range(self.rowCount()):
            self.removeRow(row)


class StatsWindow(QtWidgets.QMainWindow):
    """"""
    sigShowEvent = pyqtSignal(bool)

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        # self.setStyle(ColoredTableHeaderStyle(self.style()))
        self.stats = StatsTable()
        self.setWindowTitle('Statistics')
        self.setCentralWidget(self.stats)

        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

    def sizeHint(self):
        return QSize(850, 350)

    def showEvent(self, *args, **kwargs):
        super().showEvent(*args, **kwargs)
        self.sigShowEvent.emit(True)

    def hideEvent(self, *args, **kwargs):
        super().hideEvent(*args, **kwargs)
        self.sigShowEvent.emit(False)

    def _spacebar_hit(self):
        self.hide()

    def keyPressEvent(self, event):
        key = event.key()
        if key in (32,):
            self._spacebar_hit()
            return
        super().keyPressEvent(event)
