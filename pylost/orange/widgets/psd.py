# coding=utf-8

from pylost.orange.widgets import *

from pylost.utils.psd import WINDOWS, psd_1d, calc_csp, psd_superflat, psd_2d_linewise, windowing, get_window
from pylost.orange.gui.psdviewer import PylostPlotCanvas, PSDPlotCanvas, PSDPlotToolbar


# OPTIONS = ['PSD 1D curves average', 'Superflat script (hgt->slp in fourier space)',
#            'Superflat script (PSD on slopes)', 'PSD 2D linewise average']#, 'Areal PSD 2D']
OPTIONS = ['PSD 1D curves average', 'Superflat script', 'PSD 2D linewise average']


class OWPowerSpectralDensity(PylostWidgets):
    name = 'PSD & CSP'
    description = ''
    icon = "../icons/psd.svg"
    priority = 63

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = True
    settings_version = 1

    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    psd_method:int = Setting(1)
    windowing:int = Setting(6)
    tukey_alpha:float = Setting(0.2)
    kaiser_beta:float = Setting(10)
    flip_csp:bool = Setting(False)
    freq_min:float = Setting(0.0)
    freq_max:float = Setting(0.0)
    plot_display:int = Setting(0)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        data = Output('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    # class Error(widget.OWWidget.Error):
    #     default = Msg('{}')
    #     not_implemented = Msg('{}')
    #     scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(limit_size=False, *args, **kwargs)
        self.spacebar_action = '_calc'
        self.replace = True
        self.freq = []
        self.psd = []
        self.csp = []
        self.window_applied = []

    def sizeHint(self):
        return QSize(800, 700)

    def clear_outputs(self):
        self.Outputs.data.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self.canvas.clear_plots()
        self.rb_display.setEnabled(True)
        if dataset.is_slopes:
            self.plot_display = 1
            self.rb_display.setEnabled(False)
        self._update_window()
        if self.autocalc:
            self._calc()

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)

    def _window_label(self):
        label = self.window_combo.currentText()
        if self.windowing == 0:
            label = 'no window'
        elif self.windowing == 5:
            label = label + f' (beta={self.kaiser_beta})'
        elif self.windowing == 6:
            label = label + f' (alpha={self.tukey_alpha})'
        return label

    def _update_window(self):
        self.alpha.hide()
        self.beta.hide()
        if self.windowing == 5:
            self.beta.show()
        elif self.windowing == 6:
            self.alpha.show()
        # elif self.windowing == 0:
        #     if self.dataflow.is_2D:
        #         self.canvas_window.clear_plots()
        #         return
        coords = np.linspace(0, 1, 100) - 0.5
        if self.dataflow is not None:
            x_unit = self.dataflow.coords_unit
            y_unit = self.dataflow.values_unit
            data = self.dataflow.stitched if self.dataflow.is_stitched else self.dataflow.scans[0]
            if self.dataflow.is_1D:
                coords = data.coords
                values, w = windowing(self.dataflow.scans[0].values, self.windowing,
                                    tukey_alpha=self.tukey_alpha, kaiser_beta=self.kaiser_beta)
            else:
                profile = data.extract_profile()
                coords = profile.coords
                values, w = windowing(profile.values, self.windowing,
                                    tukey_alpha=self.tukey_alpha, kaiser_beta=self.kaiser_beta)
            legend = self.dataflow.title + f'  (RMS: {np.nanstd(values):.3f} {y_unit})'
            self.canvas_window.draw_data(coords, w * np.nanmax(values), clear=True, legend=self._window_label())
            self.canvas_window.draw_data(coords, values, clear=False, legend=legend, units=(x_unit, y_unit))
            return
        w = get_window(self.windowing, len(coords), tukey_alpha=self.tukey_alpha, kaiser_beta=self.kaiser_beta)
        self.canvas_window.draw_data(coords, w)

    def _replace(self):
        self.replace = True
        self._calc()

    def _add(self):
        self.replace = False
        self._calc()

    def _calc(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        try:
            data = self.dataflow.stitched if self.dataflow.is_stitched else self.dataflow.scans[0]
            psd_algo = self.psd_combo.currentIndex()
            legend = self._window_label()
            if psd_algo == 1:
                legend = 'Superflat - ' + legend
                freq, h_psd, s_psd = psd_superflat(data, self.windowing,
                                                   tukey_alpha=self.tukey_alpha, kaiser_beta=self.kaiser_beta)
            elif psd_algo == 2:
                legend = '2D average - ' + legend
                freq, h_psd, s_psd = psd_2d_linewise(data, self.windowing,
                                                     tukey_alpha=self.tukey_alpha, kaiser_beta=self.kaiser_beta)
            else:
                legend = '1D average - ' + legend
                freq, h_psd, s_psd = psd_1d(data, self.windowing,
                                            tukey_alpha=self.tukey_alpha, kaiser_beta=self.kaiser_beta)

            h_csp = calc_csp(freq, h_psd,
                             freq_min=self.freq_min, freq_max=self.freq_max,
                             flip_integration=self.flip_csp)
            s_csp = calc_csp(freq, s_psd,
                             freq_min=self.freq_min, freq_max=self.freq_max,
                             flip_integration=self.flip_csp)
            if self.replace:
                self.freq = [freq, ]
                self.psd = [(h_psd, s_psd),]
                self.csp = [(h_csp, s_csp),]
                self.window_applied = [legend,]
            else:
                self.freq.append(freq)
                self.psd.append((h_psd, s_psd))
                self.csp.append((h_csp, s_csp))
                self.window_applied.append(legend)

            self._update_plots()
            # self.canvas.draw_plots(self.freq, psd, csp, self.dataflow.units, legend, replace=self.replace)
            # self.btnExport.setEnabled(True)
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default(f'Error in PSD/CSP calculation.', e)

    def _update_plots(self):
        self.canvas.clear_plots()
        for freq, both_psd, both_csp, legend in zip(self.freq, self.psd, self.csp, self.window_applied):
            psd, csp = both_psd[self.plot_display], both_csp[self.plot_display]
            if psd is None:
                continue
            units = {'coords': 'mm', 'values': 'nm' if self.plot_display == 0 else 'urad'}
            factor = 1e9 if self.plot_display == 0 else 1e6
            self.canvas.draw_plots(freq * 1e-3, psd * factor**2, csp * factor, units, legend, replace=self.replace)
        self.btnExport.setEnabled(True)

    # noinspection PyArgumentList
    def _export(self):
        if self.dataflow is None or not self.freq:
            return
        save_folder = self.dataflow.folder
        if save_folder is None:
            save_folder = Path.home()
        savepath, _ = QFileDialog.getSaveFileName(None, 'Export PSD/CSP to csv', str(save_folder), 'CSV format (*.csv)')
        if not savepath:
            return
        header = ''
        arr = []
        for freq, both_psd, both_csp, legend in zip(self.freq, self.psd, self.csp, self.window_applied):
            psd, csp = both_psd[self.plot_display], both_csp[self.plot_display]
            units = 'nm' if self.plot_display == 0 else 'urad'
            factor = 1e9 if self.plot_display == 0 else 1e6
            header = header + 'Spatial frequency (mm-1), ' + f'PSD ({units}^2 / mm-1) - {legend}, ' + f'CSP ({units}) - {legend}, '
            arr.append(freq * 1e-3)
            arr.append(psd * factor**2)
            arr.append(csp * factor)
        np.savetxt(savepath, np.asfarray(arr).T, header=header, delimiter=',')

    # def change_plot(self):
    #     """"""

    # noinspection PyAttributeOutsideInit,PyArgumentList
    def set_ui(self):
        self.prepare_control_area()
        self.toggle_control_area()

        # main area
        main_box = gui.vBox(self.mainArea)
        main_box.setMinimumSize(800, 700)
        main_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        # # actions
        # actions = gui.hBox(main_box, box='Actions', spacing=10,
        #                    sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        # actions.layout().setAlignment(Qt.AlignRight | Qt.AlignTop)
        # self.btnApply = gui.button(actions, self, 'Calculation', stretch=1, autoDefault=False, callback=self._calc)
        # self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        # PSD options
        middle_hbox = gui.hBox(main_box, sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        inner_middle_vbox = gui.vBox(middle_hbox, sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        hbox = gui.hBox(inner_middle_vbox, 'PSD Options', sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        options_box = gui.vBox(hbox, spacing=20)
        options_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.psd_combo = gui.comboBox(options_box, self, 'psd_method', label='Type of PSD:', items=OPTIONS,
                                      orientation=QtHorizontal, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))

        window_box = gui.hBox(options_box, box='', spacing=30, sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        window_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.window_combo = gui.comboBox(window_box, self, 'windowing', label='Window:', items=WINDOWS,
                                         orientation=QtHorizontal, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed),
                                         callback=self._update_window)
        self.lbl_prm = gui.label(window_box, self, '')
        self.lbl_prm.hide()
        self.alpha = gui.doubleSpin(window_box, self, value='tukey_alpha', minv=0, maxv=1, step=0.01,
                                    sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed), callback=self._update_window)
        self.beta = gui.doubleSpin(window_box, self, value='kaiser_beta', minv=0, maxv=1000, step=0.01,
                                   sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed), callback=self._update_window)
        hbox = gui.hBox(options_box, box='', spacing=20, sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        self.btnReplace = gui.button(hbox, self, 'Replace', autoDefault=False, callback=self._replace)
        self.btnAdd = gui.button(hbox, self, 'Add to plots', autoDefault=False, callback=self._add)
        self.btnExport = gui.button(hbox, self, 'Export PSD/CSP plots', autoDefault=False, callback=self._export)
        self.btnExport.setEnabled(False)

        # CSP options
        vbox = gui.vBox(inner_middle_vbox, box='CSP options', spacing=20,
                        sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        hbox = gui.hBox(vbox, box='cumulation direction')
        self.direction = gui.radioButtons(hbox, self, 'flip_csp', addSpace=True,
                                          btnLabels=['left --> right', 'left <-- right'], orientation=QtHorizontal)
        hbox = gui.hBox(vbox, box='cutoff frequencies', spacing=20, sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        hbox.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        gui.doubleSpin(hbox, self, value='freq_min', label='low limit:', minv=0, maxv=10000, step=0.0001,
                       sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        gui.doubleSpin(hbox, self, value='freq_max', label='high limit:', minv=0, maxv=10000, step=0.0001,
                       sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))

        # PSD type
        vbox = gui.vBox(inner_middle_vbox, box='PSD display', spacing=20,
                        sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        self.rb_display = gui.radioButtons(vbox, self, 'plot_display', addSpace=True,
                                           btnLabels=['Heights', 'Slopes'], orientation=QtHorizontal,
                                           callback=self._update_plots)

        # window plot
        infos_box = gui.vBox(middle_hbox, box='Window', sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        infos_box.setMaximumHeight(300)
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        self.canvas_window = PylostPlotCanvas(scroll, self)
        scroll.setWidget(self.canvas_window)
        self.toolbar_window = PSDPlotToolbar(self.canvas_window, self.canvas_window, coordinates=True)
        infos_box.layout().addWidget(self.toolbar_window)
        infos_box.layout().addWidget(scroll)
        # self.infos = gui.label(infos_box, self, '')  figsize=(6.32, 6.32)

        # plot
        plot_box = gui.vBox(main_box, box='', sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding))
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        self.canvas = PSDPlotCanvas(scroll, self)
        scroll.setWidget(self.canvas)
        self.toolbar = PSDPlotToolbar(self.canvas, self.canvas, coordinates=True)
        plot_box.layout().addWidget(self.toolbar)
        plot_box.layout().addWidget(scroll)

        self._update_window()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
