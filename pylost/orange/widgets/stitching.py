# coding=utf-8

from pylost.stitching import *
from pylost.orange.widgets import *

from pylost.user.settings import AUTOSTITCH, ALGORITHMS

# builtin algorithms
from pylost.stitching.globaloptimize import GlobalOptimize

from pylost.utils.image_processing import scikit_present

# load users algorithms
algorithms = [GlobalOptimize]
if ALGORITHMS.exists():
    try:
        module = import_module('stitching_algorithms')
        if VERBOSE:
            print(f"    loading plugins: {module.__name__}")
        for algoname, algoclass in getmembers(module, isclass):
            if issubclass(algoclass, StitchingAlgorithm):
                locals()[algoname] = algoclass
                algorithms.append(algoclass)
                if VERBOSE:
                    print(f"      algorithm '{algoname}' loaded.")
    except Exception as exception:
        print(f'error when importing plugins.stitching_algorithms: \'{exception}\'')


class OWStitching(PylostWidgets):
    name = 'Stitching'
    description = ''
    icon = "../icons/stitch.svg"
    priority = 14

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    timeout:int = Setting(120)
    last_algorithm:str = Setting('')
    algorithm_parameters:dict = Setting({})
    autostitch:bool = Setting(AUTOSTITCH)
    shifts:tuple = Setting((0, 0))
    force_step:bool = Setting(False)
    optimize_xz:bool = Setting(False)
    return_overlapped:bool = Setting(False)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        patches = Input('Stitching', StitchingDataset)
        # motors = Input('Motors', Motors, explicit=True)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        stitched = Output('Stitched scan', StitchingDataset)
        reference = Output('Reference', StitchingDataset)
        # overlaps = Output('Overlap regions', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        not_implemented = Msg('{}')
        scans_not_found = Msg('No scan data is available.')
        stitched_is_none = Msg('Algorithm has return None or False.')
        invalid_dataset = Msg('Invalid dataset')

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    class Warning(widget.OWWidget.Warning):
        aborted = Msg('{}')
        no_data = Msg('No data')
        no_algorithm = Msg('No algorithm set.')
        no_motors = Msg('No motors informations: please load motors informations or set a constant step.')
        already_stitched = Msg('Dataset already stitched.')

    sigRetrieveReference = pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        self.algorithm = None
        self.motors_updated = None  # TODO
        super().__init__(limit_size=False, *args, **kwargs)
        self.sigRetrieveReference.connect(self._send_ref)
        self.spacebar_action = 'begin_stitch'

    def hideEvent(self, event):
        super().hideEvent(event)
        self.algorithm_parameters[self.algorithm.name] = self.algorithm.get_params()

    @staticmethod
    def _postprocessing(reference, stitched, **kwargs):
        reload = kwargs.pop('reload', False)
        auto_units = kwargs.pop('auto_units', True)
        verbose = kwargs.pop('verbose', True)
        slopes = kwargs.pop('slopes', False)
        if reference is not None:
            reference.center_coordinates()
            func = 'level_data' if slopes else 'mean_removal'
            getattr(reference, func)(reload=reload, auto_units=auto_units, verbose=verbose)
        if stitched is not None:
            stitched.center_coordinates()
            func = 'level_data' if slopes else 'plane_removal'
            getattr(stitched, func)(reload=reload, auto_units=auto_units, verbose=verbose)
        return reference, stitched

    def _handle_results(self, stitching_result:dict):
        if not stitching_result:
            self.clear_outputs()
            return
        super()._handle_results([self.dataflow.scans])
        try:
            slopes = stitching_result.get('slopes', False)
            stitched = stitching_result.get('x_slopes', None) if slopes else stitching_result.get('stitched', None)
            if stitched is None or stitched is False:
                self.clear_all_messages()
                self.Error.stitched_is_none()
                self.clear_outputs()
                return

            units = self.algorithm.patches_units
            stitched_sampling = stitching_result.get('stitched_sampling', self.algorithm.pixel_size)
            source = self.dataflow.scans[0].source
            source = 'stitched_' + source if source is not None else 'stitched data'
            x = np.arange(stitched.shape[0]) * stitched_sampling[0]
            y = np.arange(stitched.shape[1]) * stitched_sampling[1]
            stitched = Surface([x, y], stitched, units, source)
            stitched.copy_attributes(self.dataflow.scans[0], source=source)
            result = self.dataflow.copy_attributes_only()
            result.scans = self.dataflow.scans
            result.set_stitched_data(stitched)
            result.set_title(stitched.source)
            result.shrinkdata()

            if slopes:
                result.stitched.x_slopes = Surface([x, y], stitching_result.get('x_slopes', None), units, source)
                result.stitched.x_slopes.copy_attributes(self.dataflow.scans[0].x_slopes, source=source)
                result.stitched.x_slopes.remove_invalid_lines()
                result.stitched.y_slopes = Surface([x, y], stitching_result.get('y_slopes', None), units, source)
                result.stitched.y_slopes.copy_attributes(self.dataflow.scans[0].y_slopes, source=source)
                result.stitched.y_slopes.remove_invalid_lines()

            reference = stitching_result.get('reference', None)
            if reference is not None:
                source = 'ref_' + stitched.source
                x = np.arange(reference.shape[0]) * self.algorithm.pixel_size[0]
                y = np.arange(reference.shape[1]) * self.algorithm.pixel_size[1]
                reference = Surface([x, y], reference, units, source)
                reference.copy_attributes(self.dataflow.scans[0], source=source)
                reference.remove_invalid_lines()
                if slopes:
                    reference.x_slopes = Surface([x, y], stitching_result.get('x_ref', None), units, source)
                    reference.x_slopes.copy_attributes(self.dataflow.scans[0], source=source)
                    reference.x_slopes.remove_invalid_lines()
                    reference.y_slopes = Surface([x, y], stitching_result.get('y_ref', None), units, source)
                    reference.y_slopes.copy_attributes(self.dataflow.scans[0], source=source)
                    reference.y_slopes.remove_invalid_lines()

            # TODO: post processing temporarly exclude from multithreading (critical crash)
            self.algorithm.sigStatus.emit('Postprocessing...')
            reference, stitched = self._postprocessing(reference, stitched, verbose=self.verbose, slopes=slopes)
            if slopes:
                self._postprocessing(result.stitched.x_slopes, result.stitched.y_slopes, verbose=self.verbose, slopes=slopes)
                if reference:
                    self._postprocessing(reference.x_slopes, reference.y_slopes, verbose=self.verbose, slopes=slopes)
            result.set_reference_extracted(reference)

            caption = self.last_algorithm + '\n'
            representation = repr(result)
            datatype, size = representation.split(':')
            caption = caption + f'   size: {size}\n'
            caption = caption + 'XY Optimization\n' if self.optimize_xz else ''
            caption = caption + stitching_result.get('caption', '')
            runtime = stitching_result['runtime']
            caption = caption + '\n' + f'     runtime: {runtime * 1e-9:.3f}s'
            result.add_history_entry('- Stitching\n   ' + caption)

            self.send_and_close(self.Outputs.stitched, result)
            self._send_ref(reference)
            self._update_caption(caption)

            if self.return_overlapped:
                overlapped = result.copy_attributes_only()
                overlapped.scans = []
                for patch in self.algorithm.get_overlapped_regions():
                    x = np.arange(patch.shape[0]) * self.algorithm.pixel_size[0]
                    y = np.arange(patch.shape[1]) * self.algorithm.pixel_size[1]
                    p = Surface([x, y], patch, self.algorithm.patches_units, source='overlap')
                    overlapped.scans.append(p)
                # overlapped.shrinkdata()
                # noinspection PyUnresolvedReferences
                self.Outputs.overlaps.send(overlapped)

        except Exception as e:
            # self.create_traceback()
            self.Information.clear()
            self.Error.default('error when stitching.', e)

    def _handle_interrupt(self, interrupt):
        self.progressBarFinished()
        super()._handle_interrupt(interrupt)
        self._invalidate_outputs()

    def _handle_errors(self, error):
        self.progressBarFinished()
        super()._handle_errors(error)
        self._invalidate_outputs()

    def _thread_complete(self):
        super(OWStitching, self)._thread_complete()
        self.algorithm.enable_parameters(True)
        self.btnAbort.hide()

    def _invalidate_outputs(self):
        self.clear_outputs()

    def _send_ref(self, reference):
        if not reference:
            self.Outputs.reference.send(None)
            return
        print('sending extracted reference')
        self.Outputs.reference.send(StitchingDataset(reference, sender=self, title=reference.source))

    def _update_steps(self):
        self.shifts = (self.step_x.value(), self.step_y.value())

    def clear_outputs(self):
        self.Outputs.stitched.send(None)
        self.Outputs.reference.send(None)
        super().clear_outputs()

    @Inputs.patches
    def get_dataset(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            self.dataflow = None
            return
        if dataset.is_1D:
            self.Error.not_implemented('Profile stitching not implemented yet.')
            return
        if len(dataset) < 2:
            self.Error.invalid_dataset()
            return
        if not dataset:
            return
        # if self.verbose:
        #     print('OWStitching: data received:', len(datalist) if datalist else None, 'items')

        self.acknowledge_reception(dataset)
        if dataset.is_stitched:
            self.Warning.already_stitched()
        self.check_motors()
        if self.autostitch:
            self.begin_stitch()

    # @Inputs.motors
    # def get_motors(self, motors):
    #     if not motors:
    #         return
    #     if self.verbose:
    #         print('OWStitching: motors inputs received:', motors)

    def check_motors(self):
        if self.dataflow.has_motors:
            self.chkForce.show()
            if not self.force_step: # or sum(self.shifts) == 0:
                self.shifts = self.dataflow.get_motors_steps()
        else:
            self.chkForce.hide()
            self.chkForce.setChecked(True)
        self.step_x.setValue(self.shifts[0])
        self.step_y.setValue(self.shifts[1])

    def begin_stitch(self):
        self.clear_outputs()
        if self.dataflow is None:
            self.Warning.no_data()
            return
        self.clear_all_messages()
        if self.algorithm is None:
            self.Warning.no_algorithm()
            return
        try:
            if sum(np.abs(self.shifts)) == 0 and not self.force_step:
                self.Information.clear()
                self.Warning.no_motors()
                self.force_step = True
                self.force_constant_step()
                return
            motors = self.dataflow.motors_to_list()
            if self.force_step or not self.dataflow.has_motors:
                pos = np.arange(0, len(self.dataflow))
                motors = pos * self.shifts[0], pos * self.shifts[1]
            self.algorithm_parameters[self.algorithm.name] = self.algorithm.get_params()
            self.algorithm.set_data(self.dataflow, motors)
            self.algorithm.enable_parameters(False)
            self.btnAbort.show()
            self.algorithm.return_overlapped = self.return_overlapped
            self.start_threaded_function(self.algorithm._stitch)
            # import time
            # time.sleep(5)
            # self._abort()
        except Exception as e:
            # self.create_traceback()
            self.Information.clear()
            self.Error.default('error when stitching.', e)
            self.clear_outputs()

    def set_algorithm(self, idx=None):
        self.last_algorithm = self.algo_combo.currentText()
        if self.verbose and idx is not None:
            print('switching to stitching algorithm: ', self.last_algorithm)
        current_algorithm = algorithms[self.algo_combo.currentIndex()]
        self.algorithm = current_algorithm(verbose=self.verbose,
                                           sigCancel=self.sigAbort,
                                           sigProgress=self.sigProgress,
                                           sigStatus=self.sigCaptionChanged,
                                           optimize_xy=self.optimize_xz,
                                           # sigFinished=self.sigf
                                           return_overlapped=self.return_overlapped,
                                           )
        self.algorithm.clear_widget(self.options_area)
        self.algorithm.set_widget(self.options_area,
                                  self.algorithm_parameters.get(self.algorithm.name, self.algorithm_parameters))

    def force_constant_step(self):
        self.step_x.setEnabled(self.force_step)
        self.step_y.setEnabled(self.force_step)

    def _find_algorithm(self, algorithm:str = None):
        if algorithm is None:
            algorithm = self.last_algorithm
        idx = self.algo_combo.findText(algorithm)
        idx = 0 if idx == -1 else idx
        return idx

    def _set_overlapped(self, algorithm:str = None):
        if algorithm is None:
            return
        self.algorithm.return_overlapped = True

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def set_ui(self):
        """"""
        # controlArea
        items = {'checkBox': {'args': ['autostitch', 'Autostitch']}}
        self.prepare_control_area(items=items)
        self.autocalc_cb.hide()

        # hide control_area at creation
        # self.toggle_control_area()

        algo_list = []
        for algo in algorithms:
            algo_list.append(algo.name)

        # main area
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(500, 250)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        # buttons
        actions = gui.hBox(main, box='Actions', spacing=10,
                           sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Stitch', stretch=1, autoDefault=False, callback=self.begin_stitch)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.btnAbort = gui.button(actions, self, 'Abort', stretch=1, autoDefault=False, callback=self._abort)
        self.btnAbort.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.btnAbort.hide()

        algorithm = gui.hBox(main, spacing=10, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        # common = gui.vBox(algorithm, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        # algorithm.setMinimumSize(240, 400)
        algorithm.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        # left box
        general = gui.vBox(algorithm, box='General options', spacing=10,
                           sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        general.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.algo_combo = gui.comboBox(general, self, None, label='Stitching algorithm:', orientation=Qt.Horizontal)
        self.algo_combo.addItems(algo_list)
        self.algo_combo.setCurrentIndex(self._find_algorithm())
        self.algo_combo.currentIndexChanged.connect(self.set_algorithm)
        gui.widgetLabel(general, '')

        steps = QWidget(parent=general)
        layout = QGridLayout()
        layout.addWidget(gui.widgetLabel(None, 'Constant steps:'), 0, 0, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'X (mm)'), 0, 1, alignment=Qt.AlignHCenter)
        layout.addWidget(gui.widgetLabel(None, 'Y (mm)'), 0, 2, alignment=Qt.AlignHCenter)
        self.step_x = FloatLineEdit(steps, value=self.shifts[0], fmt='.6f', maxwidth=61, callback=self._update_steps)
        self.step_x.setEnabled(self.force_step)
        layout.addWidget(self.step_x, 1, 1, alignment=Qt.AlignHCenter)
        self.step_y = FloatLineEdit(steps, value=self.shifts[1], fmt='.6f', maxwidth=61, callback=self._update_steps)
        self.step_y.setEnabled(self.force_step)
        layout.addWidget(self.step_y, 1, 2, alignment=Qt.AlignHCenter)
        steps.setLayout(layout)
        general.layout().addWidget(steps)

        self.chkForce = gui.checkBox(general, self, 'force_step', 'Force constant steps', callback=self.force_constant_step)

        self.chkoptim_xz = gui.checkBox(general, self, 'optimize_xz', 'Patches phase cross-correlation',
                                        callback=self.set_algorithm)
        self.chkoptim_xz.setEnabled(scikit_present)

        # self.chkoverlapped = gui.checkBox(general, self, 'return_overlapped', 'Send overlapped regions',
        #                                   callback=self._set_overlapped)

        # right box
        self.options_area = gui.vBox(algorithm, box='Algorithm options', spacing=10,
                                     sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        self.options_area.layout().setAlignment(Qt.AlignTop | Qt.AlignLeft)

        self.set_algorithm()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
