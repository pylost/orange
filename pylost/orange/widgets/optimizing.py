# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils.image_processing import optimize_XY


class OWOptimXY(PylostWidgets):
    name = 'Optimizing XY'
    description = ''
    icon = "../icons/view.svg"
    priority = 62

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = True
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    pixel_size_correction:bool = Setting(False)
    new_pixel_size:list = Setting(0)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Stitching dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        optimized = Output('Stitching dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sigProgress.connect(self._update_progress)
        self.spacebar_action = 'optimize'

    def sizeHint(self):
        return QSize(450, 450)

    def clear_outputs(self):
        self.Outputs.optimized.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        if self.autocalc:
            self.optimize()

    def _handle_results(self, results):
        if not results:
            return

        corrections, new_pix_shift, new_pixel_size = results

        unit = self.dataflow.motors[0]['unit']
        pixel_size = unit(self.dataflow.pixel_size, self.dataflow.coords_unit)
        motors = self.dataflow.motors_to_list()
        new_motors = np.asfarray([new_pix_shift[0] * pixel_size[0] + motors[0][0],
                                  new_pix_shift[1] * pixel_size[1] + motors[1][0]]).T
        new = self.dataflow.copy_attributes_only()
        new.scans = self.dataflow.scans
        new.set_motors(new_motors, str(unit))

        self.float_newres.setValue(new_pixel_size[0])
        self.new_pixel_size = [new_pixel_size[0], new_pixel_size[0]]
        if self.pixel_size_correction:
            new.scans = new.copypatches()
            new.change_pixel_size(self.new_pixel_size)

        self._update_caption(f'success.')
        history = f'- XY position optimized.'
        self.send_and_close(self.Outputs.optimized, new, history=history)

    def optimize(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()

        if self.dataflow.motors is None:
            return

        motors = self.dataflow.motors_to_list()
        pix_shift = self.dataflow.motors_to_pixel_shift(motors)
        unit = self.dataflow.motors[0]['unit']
        pixel_size = unit(self.dataflow.pixel_size, self.dataflow.coords_unit)
        self.start_threaded_function(optimize_XY, self.dataflow.scans, pix_shift, pixel_size,
                                     progress_signal=self.sigProgress)

    # noinspection PyArgumentList,PyAttributeOutsideInit
    def set_ui(self):
        self.prepare_control_area()

        # main area
        self.mainArea.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(350, 120)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Optimize sitiching steps', stretch=1, autoDefault=False, callback=self.optimize)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        params = gui.widgetBox(main, 'Optimizing', stretch=1)
        grid_layout = QGridLayout()
        self.cb_pixel = gui.checkBox(None, self, 'pixel_size_correction', 'Pizel size correction')
        float_newres_lbl = QLabel('Estimated corrected pixel size', None)
        self.float_newres = FloatLineEdit(self, value=0, fmt='.6f')
        self.btnNewres = gui.button(actions, self, 'Correction', stretch=1, autoDefault=False, callback=self.optimize)
        self.btnNewres.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        grid_layout.addWidget(self.cb_pixel, 0, 0, Qt.AlignVCenter)
        grid_layout.addWidget(float_newres_lbl, 1, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.float_newres, 1, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.btnNewres, 1, 2, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(grid_layout)
        params.layout().addWidget(grid)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
