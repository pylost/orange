# coding=utf-8

from pylost.orange.widgets import *
from pylost.user.settings import MATERIALS
from pylost.data.pyopticslab import gravity


class OWGravity(PylostWidgetsViewerMixin):
    """ each widget in Pylost should be a subclass of 'PylostWidgets' """
    name = 'Gravity'
    description = ''
    icon = "../icons/gravity.svg"
    priority = 22

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    length:float = Setting(0)
    thickness:float = Setting(0)
    separation:float = Setting(0)
    material:dict = Setting({'material':'Si-100', 'rho':2330, 'young':1.30e11})
    orientation:int = Setting(0)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Data', StitchingDataset)
        # params = Input(Fit parameters', dict, explicit=True)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        corrected = Output('gravity correction', StitchingDataset)
        model = Output('gravity model', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        missing_parameters = Msg('Missing parameters.')
        incompatible_distance = Msg('Missing parameters.')

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    # class Warning(widget.OWWidget.Warning):
    #     aborted = Msg('{}')

    def __init__(self, *args, **kwargs):
        super().__init__(multiview=True, no_caption=True, view_threading=False, *args, **kwargs)
        self.spacebar_action = 'gravity_correction'

    # def sizeHint(self):
    #     return QSize(0, 720)

    def clear_outputs(self):
        self.Outputs.corrected.send(None)
        self.Outputs.model.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self.data_to_viewer(dataset, link=0, threading=False)
        valid = self.length * self.thickness * self.separation > 1e-3
        if valid and self.autocalc:
            self.gravity_correction()

    def _update_materials_list(self):
        self.material_combo.clear()
        for material, prm in MATERIALS.items():
            self.material_combo.addItem(material)

    def _update_material_params(self):
        material = self.material_combo.currentText()
        if material == 'custom':
            return
        rho, young = MATERIALS.get(material, (0, 0))
        self.float_rho.setValue(rho, block_signal=True)
        self.float_young.setValue(young, block_signal=True)
        self._update_params()

    def _customized(self):
        self.material_combo.blockSignals(True)
        self.material_combo.setCurrentIndex(0)
        self.material_combo.blockSignals(False)
        self._update_params()

    def _update_params(self):
        self.length = self.float_length.value()
        self.thickness = self.float_thickness.value()
        self.separation = self.float_separation.value()
        self.material['material'] = self.material_combo.currentText()
        self.material['rho'] = self.float_rho.value()
        self.material['young'] = self.float_young.value()
        self.orientation = self.orientation_combo.currentIndex()

    def finalize(self, processed: list[Surface, Profile], model):
        if not processed:
            return
        super()._handle_results(processed)
        model = StitchingDataset([model], title=model.source)
        self.data_to_viewer(model, link=1, threading=False)
        material = self.material['material']
        status = ['   added \n', '   subtracted \n'][self.orientation]
        rho, young = self.material['rho'], self.material['young']
        status = status + f'   material: {material} ({rho:.0f}kg/m3, {young:.2e})\n' \
                          f'   length: {self.length} mm\n' \
                          f'   thickness: {self.thickness} mm\n   separation: {self.separation} mm'
        self._update_caption(status)
        history = '- Gravity correction:\n'
        history = history + status
        self.send_and_close(self.Outputs.corrected, self.create_output_dataset(processed), history)
        self.send_and_close(self.Outputs.model, model, history)

    def _calc_model(self, data):
        return gravity.model(
            data,
            length=self.length,
            thickness=self.thickness,
            distance=self.separation,
            material=self.material['material'],
            rho=self.material['rho'],
            young=self.material['young'],
            orientation=('faceup', 'facedown')[self.orientation]
        )

    def gravity_correction(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        if self.length * self.thickness * self.separation == 0:
            self.Error.missing_parameters()
            self._update_caption('Missing parameters')
            return
        if self.length < self.separation:
            self.Error.incompatible_distance()
            self._update_caption('Contacts separation greater than mirror length !')
            return
        self.toc = perf_counter_ns()
        res = self.prepare_dataset(self.dataflow)
        model = self._calc_model(res[0])
        if ('faceup', 'facedown')[self.orientation] == 'facedown':
            model.values = np.negative(model.values)
        for scan in res:
            model.change_values_unit(scan.values_unit)
            scan.values += model.values
            if isinstance(scan, Surface):
                if scan.is_slopes and scan.has_slopes:
                    scan.x_slopes.values += model.values
        self.finalize(res, model)

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        """method called by super().__init__(*args, **kwargs)"""

        # add options box
        self.prepare_control_area(options=True, no_history=True)

        grid_layout = QGridLayout()
        model_box = gui.widgetBox(self.controlArea, box='Gravity model', spacing=10,
                                  orientation=grid_layout,
                                  sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        # hb1 =
        # size_box = gui.hBox(None)
        length_lbl = QLabel('Length (mm)', None)
        self.float_length = FloatLineEdit(None, self.length, maxwidth=61, fmt='.3f', callback=self._update_params)
        # self.float_length.setToolTip("Distance from source in meter")
        thickness_lbl = QLabel('Thickness (mm)', None)
        self.float_thickness = FloatLineEdit(None, self.thickness, maxwidth=61, fmt='.3f', callback=self._update_params)
        # self.float_thickness.setToolTip("Distance from source in meter")
        separation_lbl = QLabel('Contacts separation (mm)', None)
        self.float_separation = FloatLineEdit(None, self.separation, maxwidth=61, fmt='.3f', callback=self._update_params)
        # self.float_separation.setToolTip("Distance from source in meter")
        grid_layout.addWidget(length_lbl, 0, 0, Qt.AlignCenter)
        grid_layout.addWidget(self.float_length, 1, 0, Qt.AlignCenter)
        grid_layout.addWidget(thickness_lbl, 0, 1, Qt.AlignCenter)
        grid_layout.addWidget(self.float_thickness, 1, 1, Qt.AlignCenter)
        grid_layout.addWidget(separation_lbl, 0, 2, Qt.AlignCenter)
        grid_layout.addWidget(self.float_separation, 1, 2, Qt.AlignCenter)

        material_lbl = QLabel('Material', None)
        self.material_combo = QComboBox(self, sizeAdjustPolicy=QComboBox.AdjustToContents)
        self._update_materials_list()
        try:
            index = list(MATERIALS.keys()).index(self.material['material'])
        except ValueError:
            index = 6  # Si-100 by default
        self.material_combo.setCurrentIndex(index)
        self.material_combo.currentIndexChanged.connect(self._update_material_params)
        rho_lbl = QLabel('Density (kg/m3)', None)
        self.float_rho = FloatLineEdit(None, self.material['rho'], maxwidth=61, fmt='.3f', callback=self._customized)
        young_lbl = QLabel('Young\'s modulus (N/m)', None)
        self.float_young = FloatLineEdit(None, self.material['young'], maxwidth=61, fmt='.3e',
                                         auto_dtype=False, callback=self._customized)
        grid_layout.addWidget(material_lbl, 2, 0, Qt.AlignCenter)
        grid_layout.addWidget(self.material_combo, 3, 0, Qt.AlignCenter)
        grid_layout.addWidget(rho_lbl, 2, 1, Qt.AlignCenter)
        grid_layout.addWidget(self.float_rho, 3, 1, Qt.AlignCenter)
        grid_layout.addWidget(young_lbl, 2, 2, Qt.AlignCenter)
        grid_layout.addWidget(self.float_young, 3, 2, Qt.AlignCenter)

        self.orientation_combo = QComboBox(self, sizeAdjustPolicy=QComboBox.AdjustToContents)
        self.orientation_combo.addItems(('Add to data', 'Subtract to data'))
        self.orientation_combo.currentIndexChanged.connect(self._update_params)
        grid_layout.addWidget(self.orientation_combo, 3, 3, Qt.AlignCenter)

        self.controlArea.setMinimumWidth(301)

        self.export_button = gui.button(None, self, 'Gravity\ncompensation', autoDefault=False,
                                        callback=self.gravity_correction)
        grid_layout.addWidget(self.export_button, 1, 3, Qt.AlignRight)

        model_box.setLayout(grid_layout)

        # add only history box on the bottom
        self.prepare_control_area(options=False, no_history=False)

        # Data viewer in mainArea
        main = gui.vBox(self.mainArea)
        box = gui.vBox(main, 'Silx dataviewer', stretch=1)
        box.setMinimumSize(800, 600)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().addWidget(self._plotviewer)


# global variable set in the user settings.ini file
if VERBOSE:
    print(f"  module '{__name__}' loaded.")
