# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils.methods import integrate


class OWIntegrate(PylostWidgets):
    name = 'Integration'
    description = ''
    icon = "../icons/integral.svg"
    priority = 42

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = True
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    timeout:int = Setting(120)
    method_2d:str = Setting('Frankot-Chellappa')  # 'Frankot-Chellappa', 'Grad2Surf'

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        data = Output('integrated', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        not_implemented = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'integrate_data'

    def clear_outputs(self):
        self.Outputs.data.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return

        self.acknowledge_reception(dataset)
        if self.autocalc:
            self.integrate_data()

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)
        caption = '1D profile integrated using cubic B-splines.'
        if self.dataflow.is_2D:
            method_2d = ['Frankot-Chellappa', 'Grad2Surf (Sylvester)'][self.combo.currentIndex()]
            caption = f'2D slopes integrated using {method_2d} algorithm.'
        self._update_caption(caption)
        history = '- ' + caption

        self.send_and_close(self.Outputs.data, self.create_output_dataset(processed, ellipse_errors=True), history)

    def integrate_data(self):
        self.clear_outputs()
        if self.dataflow is None:
            return

        self.clear_all_messages()
        if not self.dataflow.is_slopes:
            self.Error.not_implemented('Integration of non slopes surface data is not available.')
            return
        if self.dataflow.is_2D:
            if not self.dataflow.scans[0].has_slopes:
                self.Error.not_implemented('At least one gradient field not present.')
                return

        dialog = False
        if self.dataflow.is_2D:
            limit = 32768
            if self.dataflow.is_stitched:
                dialog = self.dataflow.stitched.values.size > limit
            else:
                dialog = self.dataflow.scans[0].values.size > limit or len(self.dataflow.scans) > 10

        if dialog and self.combo.currentIndex() == 1:
            msg = 'Large data size for 2D integration.\n' \
                  'Consider binning or interpolation before integrating.\n\n' \
                  'Continue anyway ?'
            if not self.show_warning_box('2D integration Warning', msg):
                self._update_caption('aborted.')
                return

        try:
            method_1d = 'cubicbspline' # 'cubicbspline', 'simpson'
            method_2d = ['Frankot-Chellappa', 'Grad2Surf (Sylvester)'][self.combo.currentIndex()]
            self.start_executor(integrate, self.prepare_dataset(self.dataflow),
                                method_1D=method_1d, method_2D=method_2d, keep_slopes=True)
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default('Error when integrating data.', e)

    # noinspection PyAttributeOutsideInit
    def set_ui(self):
        self.prepare_control_area()
        # self.toggle_control_area()

        main = gui.vBox(self.mainArea)
        main.setMaximumSize(350, 200)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        action = gui.hBox(main, 'Action', stretch=1)
        gui.button(action, self, 'Integrate data', autoDefault=False, callback=self.integrate_data)

        box = gui.hBox(main, '2D integration algorithm', stretch=1)
        box.setMaximumSize(350, 150)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.combo = gui.comboBox(box, self, 'method_2d', label='',
                                  items=('Frankot-Chellappa', 'Grad2Surf (Sylvester)'))


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
