# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils.methods import filtering


class OWFilter(PylostWidgets):
    name = 'Filter'
    description = ''
    icon = "../icons/filter.svg"
    priority = 44

    settings_version = 1

    combo:int = Setting(0)
    chk_x:bool = Setting(True)
    chk_y:bool = Setting(True)
    win_x:int = Setting(1)
    win_y:int = Setting(1)
    low_w:float = Setting(0.0)
    high_w:float = Setting(0.0)
    sigma:float = Setting(1.0)
    filter_size:float = Setting(1.0)

    class Inputs:
        data = Input('Data', StitchingDataset)

    class Outputs:
        filtered = Output('Filtered data', StitchingDataset)

    def __init__(self, *args, **kwargs):
        self.unit = ''
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'filter_data'

    def sizeHint(self):
        return QSize(250, 350)

    def clear_outputs(self):
        self.Outputs.filtered.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self.unit = dataset.coords_unit
        self._update()
        if self.autocalc:
            self.filter_data()

    @staticmethod
    def _filter(data:[Surface, Profile], func:str, **kwargs):
        data = filtering(data, func=func, **kwargs)
        return data

    def _build_caption(self):
        method = self.combo_filter.currentText()
        params = ('\nno filter applied.',
                  f'\n{self.win_x:d} by {self.win_y:d} pixels',
                  f'\nmin wavelength: {self.low_w:.3f} mm\nmax wavelength: {self.high_w:.3f} {self.unit}',
                  f'\nsigma: {self.sigma:.1f}',
                  f'\nfilter size: {self.filter_size:.3f} {self.unit}')
        return method + params[self.combo_filter.currentIndex()]

    def _handle_results(self, filtered:list):
        if None in filtered:
            self.Error.default('One element is None')
            self.clear_outputs()
            return
        super()._handle_results(filtered)
        filtered = self.create_output_dataset(filtered)
        status = self._build_caption()
        self._update_caption(status)
        history = '- ' + status
        self.send_and_close(self.Outputs.filtered, filtered, history=history)

    def _get_method(self):
        idx = self.combo_filter.currentIndex()
        func, kwargs = None, None
        if idx == 0:
            return None, None
        elif idx == 1:
            func = 'moving_average'
            kwargs = {'win_x':self.win_x, 'win_y':self.win_y}
        elif idx == 2:
            func = 'fft_filtering'
            pixel_size = self.dataflow.pixel_size[0] if self.dataflow.is_2D else self.dataflow.pixel_size
            low_wavelength = abs(self.low_w)
            low_wavelength = low_wavelength if low_wavelength > 1e-9 else -1
            high_wavelength = abs(self.high_w)
            high_wavelength = high_wavelength if high_wavelength > 1e-9 else 2 * pixel_size
            kwargs = {'low_freq':1 / low_wavelength, 'high_freq':1 / high_wavelength}
        elif idx == 3:
            func = 'gaussian_filter'
            kwargs = {'sigma': self.sigma}
        elif idx == 4:
            func = 'median_filter'
            kwargs = {'filter_size': self.filter_size}
        return func, kwargs

    def filter_data(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        func, kwargs = self._get_method()
        if func is None:
            self.send_and_close(self.Outputs.filtered, self.dataflow.copydataset())
            return
        try:
            self.start_executor(self._filter, self.prepare_dataset(self.dataflow), func=func, **kwargs)
        except Exception as e:
            self.Error.default(repr(e))

    # noinspection PyUnresolvedReferences
    def _update(self):
        self.low_wavelength_lbl.setText(f'Low wavelength ({self.unit})')
        self.high_wavelength_lbl.setText(f'High wavelength ({self.unit})')
        idx = self.combo_filter.currentIndex()
        self.combo = idx
        self.low_w = self.low_wavelength.value()
        self.high_w =self.high_wavelength.value()
        self.sigma = self.sigma_f.value()
        self.filter_size = self.filter_size_f.value()
        self.move_ave_box.setVisible(idx == 1)
        self.fft_fixed_box.setVisible(idx == 2)
        self.gauss_box.setVisible(idx == 3)
        self.median_box.setVisible(idx == 4)
        if self.dataflow is not None:
            is_2d = self.dataflow.is_2D
            self.x_lbl.setVisible(is_2d)
            self.cby.setVisible(is_2d)
            self.spy.setVisible(is_2d)
            scan = self.dataflow.stitched if self.dataflow.is_stitched else self.dataflow.scans[0]
            length = scan.length[0] if is_2d else scan.length
            pixel_size = scan.pixel_size[0] if is_2d else scan.pixel_size
            self.low_w = length if self.low_w == 0.0 else self.low_w
            self.high_w = pixel_size * 2 if self.high_w == 0.0 else self.high_w
            self.low_wavelength.setValue(self.low_w, block_signal=True)
            self.high_wavelength.setValue(self.high_w, block_signal=True)

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        self.prepare_control_area()
        self.toggle_control_area()\

        # main area
        self.mainArea.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(400, 300)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Filter data', stretch=1, autoDefault=False, callback=self.filter_data)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        params = gui.hBox(main, 'Filter', stretch=1)
        main.setMaximumSize(360, 250)
        params.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.params = params
        params.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        combox_wid = gui.vBox(params)
        # method_lbl = QLabel('Method', combox_wid)
        self.combo_filter = gui.comboBox(combox_wid, self, 'combo', label='Method',
                                         items=('None', 'Moving Average', 'FFT fixed', 'Gauss Filter', 'Median Filter'),
                                         callback=self._update)
        self.combo_filter.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.move_ave_box = gui.widgetBox(params, '', stretch=1)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.cbx = gui.checkBox(None, self, 'chk_x', 'X axis')
        self.cby = gui.checkBox(None, self, 'chk_y', 'Y axis')
        self.spx = gui.spin(None, self, 'win_x', 1, 8192, 1, label='Window X', controlWidth=61)
        self.spy = gui.spin(None, self, 'win_y', 1, 8192, 1, label='Window Y', controlWidth=61)
        self.x_lbl = gui.label(None, params, ' x ')
        layout.addWidget(self.cbx, 0, 0, Qt.AlignVCenter)
        layout.addWidget(self.cby, 0, 2, Qt.AlignVCenter)
        layout.addWidget(self.spx, 1, 0, Qt.AlignVCenter)
        layout.addWidget(self.x_lbl, 1, 1, Qt.AlignVCenter)
        layout.addWidget(self.spy, 1, 2, Qt.AlignVCenter)
        layout.addWidget(gui.label(None, params, 'pix'), 1, 3, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(layout)
        self.move_ave_box.layout().addWidget(grid)

        self.fft_fixed_box = gui.widgetBox(params, '', stretch=1)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.low_wavelength_lbl = QLabel('Low wavelength', None)
        self.low_wavelength = FloatLineEdit(self, value=self.low_w, maxwidth=121, callback=self._update)
        self.high_wavelength_lbl = QLabel('High wavelength', None)
        self.high_wavelength = FloatLineEdit(self, value=self.high_w, maxwidth=121, callback=self._update)
        layout.addWidget(self.low_wavelength_lbl, 0, 2, Qt.AlignVCenter)
        layout.addWidget(self.low_wavelength, 1, 2, Qt.AlignVCenter)
        layout.addWidget(self.high_wavelength_lbl, 0, 3, Qt.AlignVCenter)
        layout.addWidget(self.high_wavelength, 1, 3, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(layout)
        self.fft_fixed_box.layout().addWidget(grid)

        self.gauss_box = gui.vBox(params, '', stretch=1)
        # self.sigma_lbl = QLabel('Sigma', None)
        self.gauss_box.layout().addWidget(QLabel('Sigma', None), Qt.AlignVCenter)
        self.sigma_f = FloatLineEdit(self, value=self.sigma, maxwidth=61, callback=self._update)
        self.gauss_box.layout().addWidget(self.sigma_f, Qt.AlignVCenter)
        self.gauss_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        self.median_box = gui.vBox(params, '', stretch=1)
        # self.filter_size_lbl = QLabel('Filter size (mm)', None)
        self.median_box.layout().addWidget(QLabel('Filter size (mm)', None), Qt.AlignVCenter)
        self.filter_size_f = FloatLineEdit(self, value=self.filter_size, maxwidth=61, callback=self._update)
        self.median_box.layout().addWidget(self.filter_size_f, Qt.AlignVCenter)
        self.median_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        self._update()

    def _print_size(self):
        super()._print_size()
        size_str = f'\n                       params: {self.params.frameGeometry().width()} {self.params.frameGeometry().height()}'
        size_str = size_str + f'\n                       move_ave_box: {self.move_ave_box.frameGeometry().width()} {self.move_ave_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       fft_fixed_box: {self.fft_fixed_box.frameGeometry().width()} {self.fft_fixed_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       gauss_box: {self.gauss_box.frameGeometry().width()} {self.gauss_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       median_box: {self.median_box.frameGeometry().width()} {self.median_box.frameGeometry().height()}'
        print(size_str)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
