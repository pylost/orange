# coding=utf-8

from pylost.orange.widgets import *


class OWSubtracting(PylostWidgetsOperators):
    name = 'Subtract'
    description = ''
    icon = "../icons/minus.svg"
    priority = 32
    title = 'data subtracted'

    want_main_area = False
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    # anchored:list = Setting([False, False])

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        minuend = Input('Minuend', StitchingDataset)
        subtrahend = Input('Subtrahend', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        result = Output('Difference', StitchingDataset)

    # option to keep anchors in place
    # sink_anchors_in_place = True
    sink_anchors_positions = [1/3, 2/3]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entries = [None, None]

    # noinspection PyTypeChecker
    @Inputs.minuend
    def set_minuend(self, entry:StitchingDataset):
        # print('set_minuend', entry)
        self.sink_anchors_in_place = True

        if not entry:
            self.entries[0] = None
            self.clear_outputs()
            self.refresh_layout()
            return

        self.sink_anchors_positions = [2/3, 1/3] if entry.sender is not self.get_sources_widget()[0] else [1/3, 2/3]
        self.refresh_layout()

        # valid link
        self.entries[0] = entry
        self.acknowledge_reception(entry)
        self.clear_all_messages()
        if self.entries[1] is not None:
            self.check_entries(compute=self.autocalc)

    # noinspection PyTypeChecker
    @Inputs.subtrahend
    def set_subtrahend(self, entry:StitchingDataset):
        # print('set_subtrahend', entry)
        self.sink_anchors_in_place = True

        if not entry:
            self.entries[1] = None
            self.clear_outputs()
            self.refresh_layout()
            return

        self.sink_anchors_positions = [2/3, 1/3] if entry.sender is self.get_sources_widget()[0] else [1/3, 2/3]
        self.refresh_layout()

        if not entry.is_stitched and len(entry) > 1:
            self.Error.subtrahend_only_single()
            return

        # valid link
        self.entries[1] = entry
        # self.acknowledge_reception(entry)
        self.clear_all_messages()
        if self.entries[0] is not None:
            self.check_entries(compute=self.autocalc)

    @staticmethod
    def compute(datalist:list[Surface, Profile], operand:[Surface, Profile] = None):
        if operand is None:
            return

        res = []
        for data in datalist:
            valid_mask = np.all(np.isfinite([data.values, operand.values]), axis=0)
            item = data.duplicate()
            item.values = np.nansum(np.dstack((data.values, np.negative(operand.values))), axis=2).reshape(data.values.shape)
            item.values[~valid_mask] = np.nan
            res.append(item)
        return res


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
