# coding=utf-8

from pylost.orange.widgets import *


class OWMerge(PylostWidgetsOperators):
    name = 'Merge dataset'
    description = ''
    icon = "../icons/merge.svg"
    priority = 51

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = False
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    # startfolder = Setting('', schema_only=True)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = MultiInput('Dataset', StitchingDataset, filter_none=True, auto_summary=False)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        result = Output('Merged Dataset', StitchingDataset)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.check_dims = False

    @Inputs.data
    def set_entry(self, link:int, entry:StitchingDataset):
        super().set_entry(link, entry)

    @Inputs.data.insert
    def insert_entry(self, link:int, entry:StitchingDataset):
        super().insert_entry(link, entry)

    @Inputs.data.remove
    def remove_entry(self, link:int):
        super().remove_entry(link)

    def check_entries(self, compute=True):
        self.clear_outputs()
        self.clear_all_messages()

        entrylist = [[entry.stitched] if entry.is_stitched else entry.scans for entry in self.valid_entries]

        datalist = self.check_data(entrylist)
        if datalist is None:
            return

        if len(entrylist) == 0:
            self.Error.scans_not_found()
            return

        if compute:
            self.count = len(datalist)
            try:
                self.finalize(datalist)
            except Exception as e:
                self.Error.default(e)

    def set_ui(self):
        self.prepare_control_area(timeout=False)
        # self.toggle_control_area()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
