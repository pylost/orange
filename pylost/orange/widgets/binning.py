# coding=utf-8
from pylost.orange.widgets import *
from pylost.utils.methods import bin_data


class OWBinning(PylostWidgets):
    name = 'Binning'
    description = ''
    icon = "../icons/reduce.svg"
    priority = 53

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = True
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    bin_x:bool = Setting(True)
    bin_y:bool = Setting(True)
    val_x:int = Setting(2)
    val_y:int = Setting(2)
    statistic:int = Setting(0)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        binned = Output('binned data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'bin_data'

    def sizeHint(self):
        return QSize(450, 450)

    def clear_outputs(self):
        self.Outputs.binned.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self._manage_bins()
        if self.autocalc:
            self.bin_data()

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)
        val_x = self.val_x if self.bin_x else 1
        val_y = self.val_y if self.bin_y else 1
        average = ('mean', 'median')[self.statistic]
        self._update_caption(f'{val_x}x{val_y} binning successful.\n{average} average')
        history = f'- {val_x}x{val_y} binning, {average} average'
        self.send_and_close(self.Outputs.binned, self.create_output_dataset(processed), history)

    def bin_data(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        try:
            if self.dataflow.is_1D:
                bins = self.val_x if self.bin_x else 1
            else:
                bins = (self.val_x if self.bin_x else 1, self.val_y if self.bin_y else 1)
            stat_method = ('nanmean', 'nanmedian')[self.statistic]
            self.start_executor(bin_data, self.prepare_dataset(self.dataflow), bins=bins, stat_method=stat_method)
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default('Error when derivating data.', e)

    def _manage_bins(self):
        if self.dataflow is None:
            self.spx.setDisabled(True)
            self.cbx.setDisabled(True)
            self.spy.setDisabled(True)
            self.cby.setDisabled(True)
            return

        is_1D = self.dataflow.is_1D
        shape = self.dataflow.shape
        vmax = int(shape[1] // self.val_x)
        vmax = vmax - 1 if vmax % 2 == 1 else vmax
        self.spx.setMaximum(max(vmax, 256))
        self.spy.setDisabled(is_1D)
        self.cby.setDisabled(is_1D)
        if is_1D:
            self.spy.setMaximum(2)
            return
        vmax = int(shape[2] // self.val_y)
        vmax = vmax - 1 if vmax % 2 == 1 else vmax
        self.spy.setMaximum(max(vmax, 256))

    # noinspection PyArgumentList,PyAttributeOutsideInit
    def set_ui(self):
        self.prepare_control_area()
        # hide control_area at creation
        # self.toggle_control_area()

        # main area
        self.mainArea.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(260, 140)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Bin data', stretch=1, autoDefault=False, callback=self.bin_data)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        params = gui.widgetBox(main, 'Binning', stretch=1)
        grid_layout = QGridLayout()
        self.cbx = gui.checkBox(None, self, 'bin_x', 'X axis')
        self.cby = gui.checkBox(None, self, 'bin_y', 'Y axis')
        self.spx = gui.spin(None, self, 'val_x', 2, 256, 1, label='Bin X value', controlWidth=61)
        self.spy = gui.spin(None, self, 'val_y', 2, 256, 1, label='Bin Y value', controlWidth=61)
        self.combo = gui.comboBox(None, self, 'statistic', label='Statistic', items=('Mean', 'Median'))

        grid_layout.addWidget(self.cbx, 0, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cby, 0, 2, Qt.AlignVCenter)
        grid_layout.addWidget(self.spx, 1, 0, Qt.AlignVCenter)
        grid_layout.addWidget(gui.label(None, params, ' x '), 1, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.spy, 1, 2, Qt.AlignVCenter)
        grid_layout.addWidget(Spacer(), 1, 3, Qt.AlignVCenter)
        grid_layout.addWidget(gui.label(None, params, 'Average'), 0, 4, Qt.AlignVCenter)
        grid_layout.addWidget(self.combo, 1, 4, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(grid_layout)
        params.layout().addWidget(grid)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
