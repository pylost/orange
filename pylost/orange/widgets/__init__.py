# coding=utf-8
"""
pylost orange3 main widgets

https://orange-widget-base.readthedocs.io/en/latest/widget.html

TODO widgets: calibration grid, etc.
TODO canvas: link colors,
"""
# import sys
# import traceback
from time import sleep, perf_counter_ns

# Qt5 imports
from AnyQt.QtCore import QCoreApplication, QThreadPool#, pyqtSignal
# orange3 imports
from Orange.widgets import gui
from orangewidget.widget import Msg
from orangewidget.utils.filedialogs import format_filter #, RecentPathsWComboMixin
from orangecanvas.canvas.items.nodeitem import escape, parse_format_fields
from orangecanvas.scheme.events import WorkflowEvent
from orangewidget.utils.signals import get_widget_inputs

# pylost imports
from ._items import *
from pylost.data import *
from pylost.orange.gui import *
from pylost.orange.gui.dataviewers import PylostSilxPlotViewer, PylostSilxComparisonViewer
from pylost.utils.multithreading import WorkerThread, Worker

# pylost user settings
from pylost.user.settings import INFONICON, MAX_RECENT_FILE, get_user_history, TIMEOUT


# base widget classes
class PylostWidgets(widget.OWWidget, openclass=True):
    """Base class for all orange pylost widgets"""

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    # want_main_area = False

    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    verbose:bool = Setting(VERBOSE)
    autocalc:bool = Setting(True)
    autoclose:bool = Setting(True)
    last_size:tuple = Setting((0, 0))
    timeout:int = Setting(TIMEOUT)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    # class Inputs:
    #     input = widget.Input('', object, auto_summary=False)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    # class Outputs:
    #     sent_data = widget.Output('data', object, auto_summary=False)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        not_implemented = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    class Warning(widget.OWWidget.Warning):
        aborted = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget informations
    # -------------------------------------------------------------------------
    class Information(widget.OWWidget.Information):
        general = Msg('{}')
        success = Msg('Data successfully processed')

    spacebar_action = None

    def __new__(cls, *args, captionTitle=None, **kwargs):
        widget_class = super().__new__(cls, *args, captionTitle=cls.name, **kwargs)
        # signalmanager = widget_class.signalManager
        signalmanager = kwargs.get('signal_manager', None)
        if signalmanager is not None:
            scheme_widget = signalmanager.workflow().parent().scheme_widget # workflow controls
            scene = scheme_widget.scene()  # can be used to change link color, anchor position, etc.
            # inputs will not move according to the source position
            if not isinstance(scene.anchor_layout(), AnchorLayoutStatic):
                cls._anchor_layout = AnchorLayoutStatic()
                scene.set_anchor_layout(cls._anchor_layout)
                scene.addItem(cls._anchor_layout)
                scene.node_item_removed.connect(widget_class._remove_node_item)
            else:
                # custom AnchorLayout already injected
                cls._anchor_layout = scene.anchor_layout()
            try:
                # add the possibility to remove raise canvas behaviour
                if 'Comparison' in widget_class.name:
                    signalmanager.workflow().widget_manager._WidgetManager__on_activate_parent = widget_class.ctrl_up
                # cls.scheme_widget._SchemeEditWidget__raiseWidgetsAction.triggered.disconnect(
                #     cls.scheme_widget._SchemeEditWidget__raiseToFont)
                # cls.scheme_widget._SchemeEditWidget__raiseWidgetsAction.triggered.connect(widget_class.ctrl_down)
            except Exception:
                """ already disconnected """
            # change the titles dynamically at any widget creation to remove annoying auto increment in naming
            for node in signalmanager.workflow().nodes:
                if node.title.startswith(cls.name) and len(node.title) > len(cls.name):
                    node.title = cls.name
        return widget_class

    # signals
    sigAbort = pyqtSignal()  # must be implemented in the subclasses for aborting multithreading/Qthreading tasks
    sigProgress = pyqtSignal(float)
    sigCaptionChanged = pyqtSignal(str)

    # option to keep anchors in place
    sink_anchors_in_place = False
    sink_anchors_positions = None # auto positions if None

    def __init__(self, limit_size=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if limit_size:
            self.setMaximumSize(self.sizeHint())
        self.toc = 0
        self.dataflow = None
        self.threadpool = QThreadPool()
        self.worker = None
        self.sigAbort.connect(self._abort)
        self.sigProgress.connect(self._update_progress)
        self.sigCaptionChanged.connect(self._update_caption)
        self.processingStateChanged.connect(self._progress_status)
        self.set_ui()

        # change nodeItem signals to capture status update
        self._node = None
        self._progress = False
        if self.node_from_scheme is not None:
            self.node_from_scheme.title_changed.connect(self._node_item_setTitle)
            self.node_from_scheme.progress_changed.connect(self._node_item_setProgress)
            self.node_from_scheme.processing_state_changed.connect(self._node_item_setProcessingState)
            self.node_from_scheme.status_message_changed.connect(self._node_item_setStatusMessage)

        self._anchor_layout.sigUpdate.connect(self.refresh_layout)

        self._thread_active = False

        # self.workflow.link_added.connect(self._link_added)
        # self.scene.link_item_added.connect(self.link_item_added)

    def sizeHint(self):
        return QSize(0, 400)

    def _print_size(self):
        if not self.verbose:
            return
        size_str = f'DEBUG: widget size -->  {self.frameGeometry().width()} {self.frameGeometry().height()}  ({self})'
        size_str = size_str + f'\n                        {self.mainArea.frameGeometry().width()} {self.mainArea.frameGeometry().height()}'
        print(size_str)

    def show_warning_box(self, title, msg):
        self._update_caption('... Warning dialog awaiting response ...')
        dlg = QMessageBox(self)
        dlg.setWindowTitle(title)
        dlg.setText(msg)
        dlg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        dlg.setIcon(QMessageBox.Warning)
        button = dlg.exec()
        if button == QMessageBox.Yes:
            return True
        self._update_caption('')
        # sleep(0.5)
        return False

    # noinspection PyArgumentList
    def ctrl_up(self, *args, **kwargs):
        event = WorkflowEvent(WorkflowEvent.ActivateParentRequest)
        QCoreApplication.sendEvent(self.signalManager.workflow(), event)
        # self.workflow.widget_manager._WidgetManager__on_activate_parent()

    # # noinspection PyArgumentList
    # def ctrl_down(self, *args, **kwargs):
    #     print('ctrl_down')
    #     # event = WorkflowEvent(WorkflowEvent.ActivateParentRequest)
    #     # QCoreApplication.sendEvent(self.workflow, event)
    #     # # self.workflow.widget_manager._WidgetManager__on_activate_parent()

    # def event(self, event):
    #     print(event.type())
    #     return True

    # def set_basic_layout(self):
    #     """if one wants to change default layout"""

    # @property
    # def item(self):
    #     return self.workflow.widget_manager._OWWidgetManager__item_for_widget(self)

    @property
    def node_from_scheme(self):
        # if self._node is not None:
        #     return self._node
        node = self.signalManager.workflow().node_for_widget(self)
        if node is not None:
            self._node = node
        return node

    @property
    def scheme_widget(self):
        return self.signalManager.workflow().parent().scheme_widget

    @property
    def scene(self):
        return self.scheme_widget.scene()

    @property
    def node_item(self):
        if self.node_from_scheme is None:
            return None
        return self.scene.item_for_node(self.node_from_scheme)

    @property
    def node_input_links(self):
        if self.node_item is not None:
            return self.scene.node_input_links(self.node_item)
        return []

    def get_source_channel(self, link):
        return self.scene.link_for_item(self.node_input_links[link]).source_channel

    def get_source_node(self, link):
        try:
            return self.scene.node_for_item(self.node_input_links[link].sourceItem)
        except Exception:
            return None

    def get_source_widget(self, link):
        try:
            return self.signalManager.workflow().widget_for_node(self.get_source_node(link))
        except Exception:
            return None

    def get_sources_widget(self):
        return [self.get_source_widget(link) for link, _ in enumerate(self.node_input_links)]

    def get_link(self, sender):
        try:
            return self.get_sources_widget().index(sender)
        except ValueError:
            return None

    def get_source_state(self, link):
        try:
            name = self.get_source_channel(link).name
        except IndexError:
            return False
        for output in self.get_source_widget(link).Outputs.__dict__.values():
            if output.name == name:
                return output.is_invalid
        return True

    def get_sources_state(self):
        return [self.get_source_state(link) for link, _ in enumerate(self.node_input_links)]

    def get_inputs(self):
        return get_widget_inputs(self).get('Results', [])

    def refresh_layout(self):
        # only useful when fixing sink anchors in fixed positions
        if self.sink_anchors_in_place and self.node_item is not None:
            self.scene._on_position_change(self.node_item)

    @staticmethod
    def findChildByClassName(qtwidget, classname:str):
        for child in qtwidget.children():
            if classname == child.__class__.__name__:
                return child

    # def link_item_added(self, link_item):
    #     print(link_item)
        # def __updatePen(self):
        #     # type: () -> None
        #     self.prepareGeometryChange()
        #     self.__boundingRect = None
        #     if self.__dynamic:
        #         if self.__dynamicEnabled:
        #             color = QColor(0, 150, 0, 150)
        #         else:
        #             color = QColor(150, 0, 0, 150)
        #
        #         normal = QPen(QBrush(color), 2.0)
        #         hover = QPen(QBrush(color.darker(120)), 2.0)
        #     else:
        #         normal = QPen(QBrush(QColor("#9CACB4")), 2.0)
        #         hover = QPen(QBrush(QColor("#959595")), 2.0)
        #
        #     if self.__state & LinkItem.Empty:
        #         pen_style = Qt.DashLine
        #     else:
        #         pen_style = Qt.SolidLine
        #
        #     normal.setStyle(pen_style)
        #     hover.setStyle(pen_style)
        #
        #     if self.hover or self.isSelected():
        #         pen = hover
        #     else:
        #         pen = normal
        #
        #     self.curveItem.setPen(pen)

    @staticmethod
    def prepare_dataset(dataset:StitchingDataset, update_roc=True):
        """
            prepare dataset as entry for an executor
            return either a copy of stitched data as a list if present or a copy of the scans list
        """
        return [dataset.copystitched(update_roc)] if dataset.is_stitched else dataset.copypatches(update_roc)

    def create_output_dataset(self, processed:[list, Profile, Surface], ellipse_errors=False):
        """  create a new StitchingDataset according to input and return it """

        if ellipse_errors:
            if isinstance(processed, list):
                for integral in processed:
                    integral.ellipse_errors = True

        new = self.dataflow.copy_attributes_only()
        if self.dataflow.is_stitched:
            if isinstance(processed, list):
                processed = processed[0]
            new.scans = self.dataflow.scans
            new.set_stitched_data(processed)
            new.set_reference_extracted(self.dataflow.reference)
        else:
            if isinstance(processed, list):
                new.scans = processed
            else:
                new.scans = [processed]
        new.unify_units()
        new.shrinkdata()
        return new

    def send_and_close(self, output, data, history:str = ''): #, force=False):
        if isinstance(data, StitchingDataset):
            # to be used in MultiInputs to keep track of the links (Orange3 mix it sometimes)
            data.set_sender(self)
        if history:
            data.add_history_entry(history)
        # if self.autocalc or force:
        output.send(data)
        # else:
        #     self.clear_outputs()
        if self.autoclose:
            self.close()

    def acknowledge_reception(self, data:StitchingDataset):
        self.clear_all_messages()
        self.clear_outputs()
        if data is None:
            data = self.dataflow
        if not isinstance(data, StitchingDataset):
            return
        try:
            self.update_history(data)
        except AttributeError:
            """ no historybox present """
        if self.verbose:
            other = f'{len(data)} patches'
            if data.is_stitched:
                other = f'stitched data ({other})'
            print(f'{self.name} widget - data received: ' + other)

    def clear_outputs(self):
        self._update_caption('')

    def clear_all_messages(self):
        self.Information.clear()
        self.Warning.clear()
        self.Error.clear()

    def start_threaded_function(self, fn, *args, **kwargs):
        """
        Single Qthread for widget long running tasks.
        Should not be used for direct data processing, use start_executor instead
        """
        self.progressBarInit()  # avoid UserWarning
        self.worker = WorkerThread(self, fn, *args, **kwargs)
        self.worker.signals.past.connect(self._set_progress)
        self.worker.signals.error.connect(self._handle_errors)
        self.worker.signals.result.connect(self._handle_results)
        self.worker.signals.progress.connect(self._update_progress)
        self.worker.signals.interrupt.connect(self._handle_interrupt)
        self.worker.signals.finished.connect(self._thread_complete)
        self.toc = perf_counter_ns()
        self.worker.start()
        # from threading import Thread
        # threaded = Thread(target=fn, args=args, kwargs=kwargs, daemon=True)
        # threaded.start()

    def start_executor(self, fn, data:[StitchingDataset, list], *args, **kwargs):
        """
        multithreaded executor for widget parallel tasks

        data can be a StitchingDataset or a list

        """
        if data is None:
            return
        if isinstance(data, StitchingDataset):
            if data.is_stitched:
                data = [data.stitched]
            else:
                data = data.scans
        if not isinstance(data, list):
            return

        if not kwargs.pop('keep_slopes', False):
            if isinstance(data[0], Surface):
                if data[0].is_slopes and data[0].has_slopes:
                    arr = []
                    for i in range(len(data)):
                        surf = data.pop(0)
                        arr.append(surf.x_slopes)
                        arr.append(surf.y_slopes)
                    data = arr

        self.progressBarInit()
        self.toc = perf_counter_ns()
        self.worker = Worker(fn, data, timeout=self.timeout, *args, **kwargs)
        self.worker.signals.past.connect(self._set_progress)
        self.worker.signals.error.connect(self._handle_errors)
        self.worker.signals.result.connect(self._handle_results)
        self.worker.signals.progress.connect(self._update_progress)
        self.worker.signals.interrupt.connect(self._handle_interrupt)
        self.worker.signals.finished.connect(self._thread_complete)
        self.threadpool.start(self.worker)

    def _abort(self):
        """stop worker by pressing abort button"""
        try:
            self.worker.cancel()
        except Exception as e:
            if self.verbose:
                print(e)

    def _update_caption(self, caption):
        self.setCaption(caption)
        self.setStatusMessage(caption)
        # sleep(0.1)

    def _remove_node_item(self):
        """ disconnect custom signals at widget deletion """
        try:
            self._node.title_changed.disconnect(self._node_item_setTitle)
            self._node.progress_changed.disconnect(self._node_item_setProgress)
            self._node.processing_state_changed.disconnect(self._node_item_setProcessingState)
            self._node.status_message_changed.disconnect(self._node_item_setStatusMessage)
        except TypeError:
            """ signals not connected """

    def _node_item_setTitle(self, title):
        """ redirect  """
        self._updateTitleText('title', title)

    def _node_item_setProgress(self, progress):
        self._updateTitleText('progress', progress)

    def _node_item_setProcessingState(self, state):
        self._updateTitleText('processing', state)

    def _node_item_setStatusMessage(self, message):
        """ little tempo to get custom method processed after default methods """
        sleep(0.1)
        self._updateTitleText('status', message)

    def _updateTitleText(self, signal:str, status:str):
        """ Update the title text item allowing multilines based on \n char in the status string """
        origin = {signal: status}
        if not origin:
            return

        node_item = self.node_item

        if node_item is None:
            return

        if node_item.captionTextItem.isEditing():
            return
        text = ['<div align="center">%s' % escape(node_item.title())]

        status_text = []

        progress_included = False
        if node_item._NodeItem__statusMessage:
            msg = escape(node_item._NodeItem__statusMessage)
            status_text = msg.split('\n')  # nodeitem.NodeItem.__updateTitleText() TWEAK
            format_fields = dict(parse_format_fields(msg))
            if "progress" in format_fields and len(format_fields) == 1:
                # Insert progress into the status text format string.
                spec, _ = format_fields["progress"]
                if spec is not None:
                    progress_included = True
                    progress_str = "{0:.0f}%".format(node_item.progress())
                    status_text.append(msg.format(progress=progress_str))
            # else:
            #     status_text.append(msg)

        if node_item.progress() >= 0 and not progress_included:
            status_text.append("%i%%" % int(node_item.progress()))

        if status_text:
            text += ["<br/>",
                     '<span style="font-style: italic">',
                     "<br/>".join(status_text),
                     "</span>"]
        text += ["</div>"]
        text = "".join(text)
        if node_item._NodeItem__renderedText != text:
            node_item._NodeItem__renderedText = text
            # The NodeItems boundingRect could change.
            node_item.prepareGeometryChange()
            node_item._NodeItem__boundingRect = None
            node_item.captionTextItem.setHtml(text)
            node_item._NodeItem__layoutCaptionTextItem()

    def _progress_status(self, status):
        self._progressbar_status = status

    def _update_progress(self, value):
        if self._progressbar_status == 1:
            self.progressBarAdvance(value)
        else:
            self.progressBarFinished()

    def _set_progress(self, value):
        if self._progressbar_status == 1:
            self.progressBarSet(value)
        else:
            self.progressBarFinished()

    def _handle_results(self, processed):
        self.tic = perf_counter_ns()

        if processed:
            if self.verbose:
                print(f'{self.name}: {len(processed)} elements processed in {(self.tic-self.toc) * 1e-6:.3f}ms')
            if isinstance(processed[0], Surface):
                if processed[0].is_slopes:
                    arr = []
                    for _ in range(len(processed) // 2):
                        x_slopes = processed.pop(0)
                        y_slopes = processed.pop(0)
                        surf = x_slopes.duplicate(update_roc=False)
                        surf.x_slopes = x_slopes
                        surf.y_slopes = y_slopes
                        arr.append(surf)
                    for _ in range(len(arr)):
                        processed.append(arr.pop(0))

        self.Error.clear()
        # self.Warning.clear()
        if INFONICON:
            self.Information.success()
        return processed

    def _handle_interrupt(self, interrupt):
        self.Information.clear()
        self.Warning.aborted(interrupt)

    def _handle_errors(self, error):
        """tuple(type, class, string) """
        self.Information.clear()
        # if self.verbose:
        #     self.Error.default(repr(error[-1]))
        # else:
        self.Error.default(repr(error[1]))

    def _thread_complete(self):
        self._thread_active = False
        self.progressBarFinished()
        try:
            del self.worker
        except AttributeError:
            """"""

    @property
    def _splitter(self):
        return self.findChildByClassName(self, '_Splitter')

    # useful methods from base class (exemple)
    def onDeleteWidget(self):
        """
        Invoked by the canvas to notify the widget it has been deleted
        from the workflow.

        If possible, subclasses should gracefully cancel any currently
        executing tasks.
        """

    def handleNewSignals(self):
        """
        Invoked by the workflow signal propagation manager after all
        signals handlers have been called.

        Reimplement this method in order to coalesce updates from
        multiple updated inputs.
        """

    def add_history_entry(self, entry:str, *args, **kwargs):
        """"""
        self.dataflow.add_history_entry(entry)

    def update_history(self, dataset:StitchingDataset = None):
        """"""
        if dataset is None:
            dataset = self.dataflow
        for w in self.historybox.children():
            if isinstance(w, QWidget):
                self.historybox.layout().removeWidget(w)
                w.deleteLater()
        for item in dataset.history:
            # if len(item) > 30:
            #
            gui.widgetLabel(self.historybox, item)

    # noinspection PyAttributeOutsideInit
    def prepare_control_area(self, **kwargs):
        options = kwargs.get('options', True)
        no_history = kwargs.get('no_history', False)
        timeout = kwargs.get('timeout', True)
        items = kwargs.get('items', {}) # dict: {'qobject':{'args':args, 'kwargs':kwargs]}

        self.controlArea.setMinimumWidth(320)

        if options:
            self.optionsbox = gui.hBox(self.controlArea, 'Widget Options',
                                       margin=10, addSpace=True, stretch=1,
                                       sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
            self.optionsbox.setMinimumWidth(320)

            gui.checkBox(self.optionsbox, self, 'autoclose', 'Auto Close')
            self.autocalc_cb = gui.checkBox(self.optionsbox, self, 'autocalc', 'Auto Process')
            # gui.checkBox(self.optionsbox, self, 'verbose', 'Verbose')
            for key, val in items.items():
                getattr(gui, key)(self.optionsbox, self, *val.get('args', []), **val.get('kwargs', {}))

        if timeout:
            gui.spin(self.optionsbox, self, 'timeout', minv=1, maxv=999, label='Timeout (s)')

        if no_history:
            return

        self.historybox = gui.vBox(self.controlArea, 'History',
                                   margin=10, addSpace=False, stretch=1,
                                   sizePolicy=(QSizePolicy.Minimum, QSizePolicy.MinimumExpanding))

        self.historybox.setMinimumWidth(320)
        self.historybox.setMinimumHeight(480)
        layout = self.historybox.layout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        # gui.widgetLabel(self.historybox, 'nothing to show.')

    def _spacebar_hit(self):
        if self._thread_active:
            self._abort()
            return
        if self.spacebar_action is not None:
            getattr(self, self.spacebar_action)()

    # ----Qt Events----
    def keyPressEvent(self, event):
        key = event.key()
        # if key in (32, 16777220, 16777221):  # Enter keys and spacebar launch reload action
        if key in (32,):  # spacebar launch reload action
            self._spacebar_hit()
        if key == 16777248: # shift keys (debug: show current window size)
            self._print_size()

    def set_ui(self):
        """default widget"""
        # control area on the left
        self.prepare_control_area()

        # main area
        box = gui.vBox(self.mainArea, '<Main area>', stretch=1)
        box.setMinimumSize(800, 600)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

    #     # hide control_area at creation
    #     self.toggle_control_area()

    def toggle_control_area(self):
        if self._splitter is not None:
            self._splitter.handleClicked.emit()

    # @staticmethod
    # def create_traceback(error_msg:str = None):
    #     traceback.print_exc()
    #     exctype, value = sys.exc_info()[:2]
    #     if error_msg is not None:
    #         """"""
    #         # value = error_msg
    #     return exctype, value, traceback.format_exc()

    # def resizeEvent(self, event):
    #     super().resizeEvent(event)
    #     if self.verbose:
    #         print(self.size())


# noinspection PyTypeChecker,PyUnresolvedReferences
class PylostWidgetsOperators(PylostWidgets, openclass=True):

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    # settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    # register:dict = Setting({})

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    # class Inputs:
    #     data = MultiInput('Operands', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        result = Output('Result', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        subtrahend_only_single = Msg('Subtrahend could not be a dataset containing multiple data.')
        scans_not_found = Msg('No scan data is available.')
        inconsistent_inputs = Msg('All dataset should have the same number of data.')
        only_single = Msg('No dataset containing multiple data is allowed.')
        dims_differ = Msg('All Inputs must be either 1D or 2D.')
        kind_differ = Msg('All Inputs must be heights or slopes.')
        shapes_differ = Msg('Inputs must have the same size.  {}')
        samplings_differ = Msg('Inputs must have the same sampling.  {}')

    title = 'unknown'

    spacebar_action = 'check_entries'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sources_widget = self.get_sources_widget()
        self.sources_state = self.get_sources_state()
        self.entries = [None] * len(self.sources_widget)
        self.count = 0
        self.has_operand = False
        self.check_dims = True
        self._caption_end = ''

    def clear_outputs(self):
        self.Outputs.result.send(None)
        self.clear_all_messages()
        super().clear_outputs()

    def set_entry(self, link:int, entry:StitchingDataset):
        # link updated
        self.clear_outputs()

        lnk = self.get_link(entry.sender)

        if lnk >= len(self.entries):
            extend = max(1, len(self.entries) - lnk)
            self.entries.extend([None] * extend)

        self.entries[lnk] = entry

        self.check_entries(compute=self.autocalc)

    def insert_entry(self, link:int, entry:StitchingDataset):
        # new link added
        self.sources_widget = self.get_sources_widget()
        self.sources_state = self.get_sources_state()

        if entry is not None:
            self.set_entry(-1, entry)

    def remove_entry(self, link:int):
        # existing link removed or became None
        self.clear_outputs()

        widgets = self.get_sources_widget()
        if not widgets:
            return

        for lnk, wid in enumerate(self.sources_widget):
            invalidated = self.source_invalidated(lnk)
            if wid not in widgets or invalidated:
                self.entries[lnk] = None
                if wid not in widgets:
                    self.entries.pop(lnk)
                self.check_entries()
                self.sources_widget = self.get_sources_widget()
                self.sources_state = self.get_sources_state()
                return

    def source_invalidated(self, link):
        return self.get_source_state(link) != self.sources_state[link]

    @property
    def valid_entries(self):
        entries = []
        for entry in self.entries:
            if entry is not None:
                entries.append(entry)
        return entries

    def check_entries(self, compute=True):
        self.clear_outputs()
        self.clear_all_messages()

        entrylist = [[entry.stitched] if entry.is_stitched else entry.scans for entry in self.valid_entries]
        singles = [len(entry) == 1 for entry in entrylist]

        datalist = self.check_data(entrylist)
        if datalist is None:
            return

        operand = None
        self.has_operand = False
        if len(entrylist) == 0:
            self.Error.scans_not_found()
            return
        elif len(entrylist) == 1:
            if singles[0]:
                return
        elif len(entrylist) == 2:
            if 'Average' in self.name:
                if not all(singles):
                    self.Error.only_single()
                    return
            else:
                if singles[1]:
                    operand = datalist.pop()
                    self.has_operand = True
                else:
                    self.Error.inconsistent_inputs()
                    return
        else:
            if not all(singles):
                self.Error.only_single()
                return

        if compute:
            self.count = len(datalist)
            try:
                res = self.compute(datalist, operand=operand)
                if operand is not None:
                    if operand.is_slopes:
                        if operand.is_2D:
                            for item in ('x_slopes', 'y_slopes'):
                                slopeslist = self.compute([getattr(data, item) for data in datalist],
                                                          operand=getattr(operand, item))
                                [setattr(new, item, slopes) for new, slopes in zip(res, slopeslist)]
                self.finalize(res)
            except Exception as e:
                self.Error.default(e)

    def check_data(self, entries:list[StitchingDataset]):
        # check the data consistency before computation
        if not entries:
            return

        datalist = []
        for entry in entries:
            for data in entry:
                datalist.append(data)

        is_1D = [data.is_1D for data in datalist]
        is_2D = [data.is_2D for data in datalist]
        if not all(is_1D) and not all(is_2D):
            self.Error.dims_differ()
            return

        is_slopes = [data.is_slopes for data in datalist]
        is_heights = [data.is_heights for data in datalist]
        if not all(is_slopes) and not all(is_heights):
            self.Error.kind_differ()
            return

        datalist = self.unify_units(datalist)

        pixel_size = np.array([np.round(data.pixel_size, 3) for data in datalist])
        if not np.all(pixel_size == pixel_size[0]):
            min_val, max_val = np.min(pixel_size, axis=0), np.max(pixel_size, axis=0)
            # min_y, max_y = np.min(pixel_size, axis=), np.max(pixel_size, axis=1)
            self.Error.samplings_differ(f'{min_val[0]:.6f} vs {max_val[0]:.6f} {datalist[0].x_unit}')
            return

        if self.check_dims:
            shapes = np.array([data.shape for data in datalist])
            if not np.all(shapes == shapes[0]):
                min_val, max_val = np.min(shapes, axis=0), np.max(shapes, axis=0)
                self.Error.shapes_differ(f'X: {min_val[0]} vs {max_val[0]}, Y: {min_val[1]} vs {max_val[1]}')
                return

        return datalist

    @staticmethod
    def unify_units(datalist: list[Surface, Profile]):
        if not datalist:
            return None
        coo_unit = datalist[0].x_unit
        val_unit = datalist[0].z_unit
        for data in datalist[1:]:
            data.change_coords_unit(coo_unit)
            data.change_values_unit(val_unit)
        return datalist

    @staticmethod
    def compute(datalist, operand=None):
        """ in the overloaded method, put math here... """

    def finalize(self, result):
        if not result:
            self.Error.default('error occured during computation')
            return

        if not self.has_operand and self.count == len(self.valid_entries):
            caption = f'{self.count} items successfully processed.'
        else:
            caption = f'{self.count} items in the dataset successfully processed, given the operand.'
            if not self.has_operand:
                caption = f'{self.count} items in the dataset successfully processed.'
            elif self.count == 1:
                caption = f'{self.count} items successfully processed, given the operand.'
        caption = caption + self._caption_end
        self._update_caption(caption)
        history = f'{self.name}: {self.count} items processed'

        # 1st linked dataset will be the canvas for the output
        if self.valid_entries[0].is_stitched and self.count == 1 and 'Merge' not in self.name:
            output = self.valid_entries[0].copydataset()
            output.set_stitched_data(result[0])
        else:
            output = self.valid_entries[0].copy_attributes_only()
            output.scans = result

        self.send_and_close(self.Outputs.result, output, history=history)

    def prepare_control_area(self, **kwargs):
        super().prepare_control_area(timeout=False)

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def set_ui(self):
        # controlArea
        self.prepare_control_area()
        # hide control_area at creation
        self.toggle_control_area()


class PylostWidgetsViewerMixin(PylostWidgets, openclass=True):

    values:list = Setting([])
    pixels:list = Setting([])
    rectangle:list = Setting([])
    is_pixel:bool = Setting(False)
    is_relative:bool = Setting(True)
    unit:float = Setting(1.0)

    class Outputs:
        data = Output('Masked data', StitchingDataset)

    class Warning(widget.OWWidget.Warning):
        aborted = Msg('{}')
        no_data = Msg('No data to show')

    def __init__(self, multiview=False, mask=True, custom_toolbar=True, *args, **kwargs):
        if multiview:
            self._plotviewer = PylostSilxComparisonViewer(self,
                                                          mask=mask,
                                                          verbose=self.verbose,
                                                          no_caption=kwargs.get('no_caption', False),
                                                          )
        else:
            self._plotviewer = PylostSilxPlotViewer(self,
                                                    mask=mask,
                                                    custom_toolbar=custom_toolbar,
                                                    verbose=self.verbose)
        super().__init__(*args, **kwargs)

    def data_to_viewer(self, dataset:StitchingDataset, **kwargs):
        if not dataset:
            self.Warning.no_data()
            return
        try:
            self.clear_outputs()
            self._plotviewer.setData(dataset, **kwargs)
        except Exception as e:
            # self.create_traceback()
            self.Information.clear()
            self.Error.default('Error occured in silx viewer.', e)

    def build_params_dict(self):
        dico = {}
        for attrname in Mask.params_list:
            dico[attrname] = getattr(self, attrname)
        return dico

    def send_noise(self, noise):
        """"""

    def set_mask_attr(self, params:dict):
        for attrname, value in params.items():
            setattr(self, attrname, value)

    def send_mask_params(self, mask_params:dict):
        """all masks must be sent only from here"""
        if mask_params:
            try:
                masked = self.dataflow.masking(self.rectangle)
                if not masked:
                    status = 'invalid'
                    self.clear_outputs()
                else:
                    status = self._create_status(mask_params, self.dataflow.is_1D)
                    history = '- Mask applied:\n' + status
                    self.send_and_close(self.Outputs.data, masked, history) #, force=True)
                self._update_caption(status)
            except Exception as e:
                # self.create_traceback()
                self.Information.clear()
                self.Error.default('error when masking', e)

    def _create_status(self, params:dict, is_1D=False):
        dtype = 'values'
        fmt = ['.4f', '.4f']
        if params['is_pixel']:
            dtype = 'pixels'
            fmt = ['d', '.3f']
        center, size = params[dtype]
        unit = params['unit']
        if is_1D:
            full = self.dataflow.size[-1] if 'values' in dtype else self.dataflow.shape[-1]
            size = min(full, size[0])
            status = f'   size: {size:{fmt[0]}} {unit}\n   center: {center[0]:{fmt[1]}} {unit}'
        else:
            full = self.dataflow.size if 'values' in dtype else self.dataflow.shape
            size = [min(full[axis - 2], size[axis]) for axis in (0, 1)]
            status = f'   size: {size[0]:{fmt[0]}} {unit} x {size[1]:{fmt[0]}} {unit}\n' \
                     f'   center: X={center[0]:{fmt[1]}} {unit}, Y={center[1]:{fmt[1]}} {unit}\n'
        if params['is_relative']:
            status = status + '   (relative to data)'
        else:
            status = status + '   (relative to detector)'
        return status


class PylostWidgetsFileManagementMixin(PylostWidgets, openclass=True):

    max_recent:int = Setting(MAX_RECENT_FILE, schema_only=False)

    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        not_implemented = Msg('{}')
        no_data = Msg('No data')
        invalid_data = Msg('Invalid data: {}')
        file_not_found = Msg('File not found')
        reader_missing = Msg('Missing reader')
        reader_error = Msg('Error reader: {}')
        hdf5_multi = Msg('Only one HDF5 file can be load at time.')

    class Warning(widget.OWWidget.Warning):
        aborted = Msg('Files task aborted')

    def __init__(self, *args, **kwargs):
        self.file_combo = QComboBox(self, sizeAdjustPolicy=QComboBox.AdjustToContents)
        super().__init__(*args, **kwargs)
        self.readers = PylostReader.readers

    @staticmethod
    def get_history():
        return [[int(entry[0]), entry[1:]] for entry in get_user_history()]

    @property
    def last_path(self):
        history = self.get_history()
        if not history:
            return Path.home()
        last_entry = history[0]
        return Path(last_entry[1][0]).parent

    @property
    def last_folder(self):
        folder = self.last_path
        if folder is None:
            return Path.home()
        if folder.is_file():
            folder = folder.parent
        return folder

    def get_reader(self, filename):
        reader = PylostReader.get_reader(str(filename))
        if reader is None:
            Exception(self.Error.reader_missing)
            return None
        if 'unknown' == reader.DATATYPE:
            reader = None
            Exception(self.Error.reader_error('DATATYPE attribute not set'))
        if 'unknown' == reader.NDIM:
            reader = None
            Exception(self.Error.reader_error('NDIM attribute not set'))
        return reader

    # noinspection PyArgumentList
    def open_files_dialog(self, start_dir=None, title='Open...'):
        filters = ['Defaults (*' + ' *'.join(PYLOST_FILE_EXTENSIONS) + ')']
        for reader in self.readers.values():
            filter_str = format_filter(reader)
            if filter_str not in filters:
                filters.append(filter_str)
        if start_dir is None:
            start_dir = str(self.last_folder)
        filepaths, _ = QFileDialog.getOpenFileNames(None, title, start_dir, ';;'.join(filters))
        return filepaths

    def prepare_control_area(self, **kwargs):
        if kwargs.get('options', True):
            kwargs['items'] = {'spin': {'args': ['max_recent',], 'kwargs': {'minv':0, 'maxv':99, 'label':'Max recent history',}}}
        super().prepare_control_area(**kwargs)
