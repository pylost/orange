# coding=utf-8

from pylost.orange.widgets import *


class OWDerivative(PylostWidgets):
    name = 'Derivatives'
    description = ''
    icon = "../icons/derivative.png"
    priority = 41

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = False
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    # startfolder = Setting('', schema_only=True)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        dx = Output('dz/dx', StitchingDataset)
        dy = Output('dz/dy', StitchingDataset)
        dxdy = Output('dz/dx and dz/dx', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'derivate_data'

    def clear_outputs(self):
        self.Outputs.dx.send(None)
        self.Outputs.dy.send(None)
        self.Outputs.dxdy.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        if self.autocalc:
            self.derivate_data()

    def collate_slopes(self, dx, dy):
        new = self.dataflow.copy_attributes_only()
        if self.dataflow.is_stitched:
            dx = dx.scans[0]
            dy = dy.scans[0]
            new.scans = self.dataflow.scans
            stitched = dx.duplicate(update_roc=False)
            stitched.x_slopes = dx
            stitched.y_slopes = dy
            new.set_stitched_data(stitched)
            new.set_reference_extracted(self.dataflow.reference)
        else:
            new.scans = []
            for x_slopes, y_slopes in zip(dx, dy):
                scan = x_slopes.duplicate(update_roc=False)
                scan.x_slopes = x_slopes
                scan.y_slopes = y_slopes
                new.scans.append(scan)
        new.unify_units()
        new.shrinkdata()
        return new

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)
        self._update_caption(f'{len(processed)} items in the dataset successfully derived.')
        history = f'- Derivative using gradient ({len(processed)} items)'
        if self.dataflow.is_2D:
            dx, dy = list(zip(*processed))
            dx = self.create_output_dataset(list(dx))
            dy = self.create_output_dataset(list(dy))
            self.send_and_close(self.Outputs.dx, dx, history)
            self.send_and_close(self.Outputs.dy, dy, history)
            self.send_and_close(self.Outputs.dxdy, self.collate_slopes(dx, dy), history)
        else:
            self.send_and_close(self.Outputs.dx, self.create_output_dataset(processed, ellipse_errors=True), history)
            self.send_and_close(self.Outputs.dy, None)
            self.send_and_close(self.Outputs.dxdy, None)

    @staticmethod
    def _derivate(data):
        return data.derivative(method='cubicbspline', copy=False) # 'cubicbspline', 'gradient'

    def derivate_data(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        try:
            self.start_executor(self._derivate, self.prepare_dataset(self.dataflow))
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default('Error when derivating data.', e)

    def set_ui(self):
        self.prepare_control_area()
        # hide control_area at creation
        # self.toggle_control_area()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
