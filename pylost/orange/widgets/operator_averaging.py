# coding=utf-8

from pylost.orange.widgets import *


class OWAveraging(PylostWidgetsOperators):
    name = 'Average'
    description = ''
    icon = "../icons/avg.svg"
    priority = 33
    title = 'data averaged'

    want_main_area = True

    method_average:int = Setting(0)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = MultiInput('Operands', StitchingDataset, filter_none=True, auto_summary=False)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        result = Output('Average', StitchingDataset)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @Inputs.data
    def set_entry(self, link:int, entry:StitchingDataset):
        super().set_entry(link, entry)

    @Inputs.data.insert
    def insert_entry(self, link:int, entry:StitchingDataset):
        super().insert_entry(link, entry)

    @Inputs.data.remove
    def remove_entry(self, link:int):
        super().remove_entry(link)

    # @staticmethod
    def compute(self, datalist:list[Surface, Profile], **kwargs):
        func = np.nanmean if self.method_average == 0 else np.nanmedian
        self._caption_end = '\nMean average' if self.method_average == 0 else '\nMedian value'
        res = datalist[0].duplicate()
        res.values = func([data.values for data in datalist], axis=0)
        return [res]

    def _update(self):
        self.method_average = self.combo_method.currentIndex()

    # noinspection PyAttributeOutsideInit
    def set_ui(self):
        super().set_ui()

        self.mainArea.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(400, 300)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Average data', stretch=1, autoDefault=False, callback=self.check_entries)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        params = gui.hBox(main, 'Method', stretch=1)
        main.setMaximumSize(360, 250)
        params.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.params = params
        params.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        combox_wid = gui.vBox(params)
        self.combo_method = gui.comboBox(combox_wid, self, None, label=None, items=('Mean Average', 'Median Value'),)
        self.combo_method.setCurrentIndex(self.method_average)
        self.combo_method.currentIndexChanged.connect(self._update)
        self.combo_method.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
