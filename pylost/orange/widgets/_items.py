# coding=utf-8
"""
various helping/customized classes
"""
from AnyQt.QtCore import pyqtSignal

from Orange.widgets import widget
from orangewidget.settings import Setting as DefaultSetting

from orangecanvas.canvas.layout import (
    attrgetter, angle, sip, QGraphicsObject, QApplication, QRectF, QEvent,
    NodeItem, LinkItem, NodeAnchorItem, SourceAnchorItem, SinkAnchorItem,
    invert_permutation_indices, argsort, composition, linspace_trunc
)

# from orangecanvas.scheme.scheme import Scheme, SchemeNode, check_arg, NodeEvent, QCoreApplication, log


class Motors:
    """class for motors parameters, used as specific conveyor in the workflow"""
    # params_dict = {'x':x_pos, 'y':y_pos, 'unit':unit}

    def __init__(self, params:dict):
        self.params = params


class Mask:
    """simple class for mask parameters view as dict, used as specific conveyor in the workflow"""
    params_list = ('values', 'pixels', 'rectangle', 'is_pixel', 'is_relative')
    values = None
    pixels = None
    rectangle = None
    is_pixel = None
    is_relative = None
    unit = None

    def __init__(self, params:dict):
        self.params = params
        for attrname, value in params.items():
            setattr(self, attrname, value)

    def __repr__(self):
        pix_str = f'{self.pixels[1][0]} x {self.pixels[1][1]} pixels'
        pix_ctr = f'{self.pixels[0][0]} x {self.pixels[0][1]} pixels'
        if self.is_pixel:
            return 'mask size: ' + pix_str + ', centre: ' + pix_ctr
        string = f'mask size: {self.values[1][0]:.3f} x {self.values[1][1]:.3f} {self.unit}, '
        string = string + f'centre: {self.values[0][0]:.3f} x {self.values[0][1]:.3f} {self.unit}'
        return string + f'  ({pix_str}, {pix_ctr})'

    def as_dict(self):
        return self.params


class Setting(DefaultSetting):
    """set schema*only=True as default"""
    def __init__(self, *args, schema_only=True, **kwargs):
        super(Setting, self).__init__(*args, schema_only=schema_only, **kwargs)


class MultiInput(widget.MultiInput):
    """set auto_summary=False as default"""
    def __init__(self, *args, filter_none=False, auto_summary=False, **kwargs):
        super(MultiInput, self).__init__(*args, filter_none=filter_none, auto_summary=auto_summary, **kwargs)


class Input(widget.Input):
    """set auto_summary=False as default"""
    def __init__(self, *args, auto_summary=False, **kwargs):
        super(Input, self).__init__(*args, auto_summary=auto_summary, **kwargs)


class Output(widget.Output):
    """set auto_summary=False as default"""
    def __init__(self, *args, auto_summary=False, **kwargs):
        super(Output, self).__init__(*args, auto_summary=auto_summary, **kwargs)
        self.is_invalid = True

    def send(self, value, *args, **kwargs):
        self.is_invalid = True if value is None else False
        super().send(value, *args, **kwargs)

# # customizing scheme class
# class PylostScheme(Scheme):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         # if scheme is not None:
#         #     scheme_load()
#         #     self.__title = scheme.title
#         #     self.__description = scheme.description
#         #     self.__annotations = scheme.annotations
#         #     self.__nodes = scheme.nodes
#         #     self.__links = scheme.links
#         #     self.__loop_flags = scheme.loop_flags()
#         #     self.__env = scheme.runtime_env()
#
#     def insert_node(self, index, node):
#         """
#         Insert `node` into self.nodes at the specified position `index`
#         """
#         assert isinstance(node, SchemeNode)
#         check_arg(node not in self.nodes,
#                   "Node already in scheme.")
#         self._Scheme__nodes.insert(index, node)
#
#         ev = NodeEvent(NodeEvent.NodeAdded, node, index)
#         QCoreApplication.sendEvent(self, ev)
#
#         log.info("Added node %r to scheme %r." % (node.title, self.title))
#         self.node_added.emit(node)
#         self.node_inserted.emit(index, node)
#
#
# noinspection PyUnresolvedReferences, PyArgumentList
class AnchorLayoutStatic(QGraphicsObject):
    """
    lazy replacement (because of name mangling) of AnchorLayout
    to have an individual option in the widget to keep anchors in place
    """

    sigUpdate = pyqtSignal() # layout update signal

    def __init__(self, parent=None, **kwargs):
        # type: (Optional[QGraphicsItem], Any) -> None
        super().__init__(parent, **kwargs)
        self.setFlag(QGraphicsObject.ItemHasNoContents)

        self.__layoutPending = False
        self.__isActive = False
        self.__invalidatedAnchors = []  # type: List[NodeAnchorItem]
        self.__enabled = True

    def boundingRect(self):  # type: () -> QRectF
        return QRectF()

    def activate(self):  # type: () -> None
        """
        Immediately layout all anchors.
        """
        if self.isEnabled() and not self.__isActive:
            self.__isActive = True
            try:
                self._doLayout()
            finally:
                self.__isActive = False
                self.__layoutPending = False

    def isActivated(self):  # type: () -> bool
        """
        Is the layout currently activated (in :func:`activate()`)
        """
        return self.__isActive

    def _doLayout(self):  # type: () -> None
        """ called each time an anchor is modified """
        if not self.isEnabled():
            return

        scene = self.scene()
        items = scene.items()
        links = [item for item in items if isinstance(item, LinkItem)]
        point_pairs = [(link.sourceAnchor, link.sinkAnchor)
                       for link in links
                       if link.sourceAnchor is not None
                       and link.sinkAnchor is not None]
        point_pairs += [(a, b) for b, a in point_pairs]
        to_other = dict(point_pairs)

        anchors = set(self.__invalidatedAnchors)

        for anchor_item in anchors:
            if sip.isdeleted(anchor_item):
                continue

            # keep anchors in place according to pylost widget option
            positions = anchor_item.anchorPositions()
            override = False
            if isinstance(anchor_item, SinkAnchorItem):
                node_item = anchor_item.parentNodeItem()
                if node_item is not None:
                    try:
                        node_widget = scene.scheme.widget_for_node(scene.node_for_item(node_item))
                    except KeyError:  # non initialized widget deletion
                        continue
                    if node_widget is not None:
                        try:
                            if node_widget.sink_anchors_in_place:
                                if node_widget.sink_anchors_positions is None:
                                    # auto placement
                                    lim = anchor_item.count() + 1
                                    positions = [p / lim for p in range(1, lim)]
                                else:
                                    positions = node_widget.sink_anchors_positions[:anchor_item.count() + 1]
                                # updatepositions only if needed to avoid critical deletion error
                                if len(anchor_item._NodeAnchorItem__uniformPointPositions) != len(positions):
                                    continue
                                override = True
                        except AttributeError:
                            """ non pylost widget """

            points = anchor_item.anchorPoints()
            anchor_pos = anchor_item.mapToScene(anchor_item.pos())
            others = [to_other[point] for point in points]

            if isinstance(anchor_item, SourceAnchorItem):
                others_angle = [-angle(anchor_pos, other.anchorScenePos())
                                for other in others]
            else:
                others_angle = [angle(other.anchorScenePos(), anchor_pos)
                                for other in others]

            indices = argsort(others_angle)
            # Invert the indices.
            indices = invert_permutation_indices(indices)

            if not override:
                positions = list(linspace_trunc(len(points)))
                positions = [positions[i] for i in indices]
            anchor_item.setAnchorPositions(positions)

        self.__invalidatedAnchors = []

        self.sigUpdate.emit()

    def invalidateLink(self, link):
        # type: (LinkItem) -> None
        """
        Invalidate the anchors on `link` and schedule an update.

        Parameters
        ----------
        link : LinkItem
        """
        if link.sourceItem is not None:
            self.invalidateAnchorItem(link.sourceItem.outputAnchorItem)
        if link.sinkItem is not None:
            self.invalidateAnchorItem(link.sinkItem.inputAnchorItem)

    def invalidateNode(self, node):
        # type: (NodeItem) -> None
        """
        Invalidate the anchors on `node` and schedule an update.

        Parameters
        ----------
        node : NodeItem
        """
        self.invalidateAnchorItem(node.inputAnchorItem)
        self.invalidateAnchorItem(node.outputAnchorItem)

        self.scheduleDelayedActivate()

    def invalidateAnchorItem(self, anchor):
        # type: (NodeAnchorItem) -> None
        """
        Invalidate the all links on `anchor`.

        Parameters
        ----------
        anchor : NodeAnchorItem
        """
        self.__invalidatedAnchors.append(anchor)

        scene = self.scene()  # type: CanvasScene
        node = anchor.parentNodeItem()
        if node is None:
            return
        if isinstance(anchor, SourceAnchorItem):
            links = scene.node_output_links(node)
            getter = composition(attrgetter("sinkItem"),
                                 attrgetter("inputAnchorItem"))
        elif isinstance(anchor, SinkAnchorItem):
            links = scene.node_input_links(node)
            getter = composition(attrgetter("sourceItem"),
                                 attrgetter("outputAnchorItem"))
        else:
            raise TypeError(type(anchor))

        self.__invalidatedAnchors.extend(map(getter, links))

        self.scheduleDelayedActivate()

    # noinspection PyArgumentList
    def scheduleDelayedActivate(self):
        # type: () -> None
        """
        Schedule an layout pass
        """
        if self.isEnabled() and not self.__layoutPending:
            self.__layoutPending = True
            QApplication.postEvent(self, QEvent(QEvent.LayoutRequest))

    def __delayedActivate(self):
        # type: () -> None
        if self.__layoutPending:
            self.activate()

    def event(self, event):
        # type: (QEvent)->bool
        if event.type() == QEvent.LayoutRequest:
            self.activate()
            return True
        return super().event(event)
