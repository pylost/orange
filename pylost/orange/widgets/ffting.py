# coding=utf-8

# why bother ? Import all what you need for pylost widget management, not more
from pylost.orange.widgets import *


class OWFFT(PylostWidgets):
    """ each widget in Pylost should be a subclass of 'PylostWidgets' """
    name = 'FFT'
    description = ''
    # icon = "../icons/flip.svg"
    priority = 72

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings: not used yet
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters: need to be updated when necessary (by user interaction)
    # -------------------------------------------------------------------------
    shift_freq_x:bool = Setting(True)

    # -------------------------------------------------------------------------
    # Widget inputs: can be one or more (MultiInput class is different and needs more care, see: PylostWidgetsOperators)
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs: can be one or more (should send different class to be easier to handle)
    # -------------------------------------------------------------------------
    class Outputs:
        fft = Output('FFT 2D', StitchingDataset)
        # reconstructed = Output('reconstructed data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors: override the default error messages
    # -------------------------------------------------------------------------
    # class Error(widget.OWWidget.Error):
    #     generic = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget warnings: override the default warning messages
    # -------------------------------------------------------------------------
    # class Warning(widget.OWWidget.Warning):
    #     aborted = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget informations: override the default information messages
    # -------------------------------------------------------------------------
    # class Information(widget.OWWidget.Information):
    #     succes = Msg('42')

    def __init__(self, *args, **kwargs):
        #   !!MANDATORY!!
        # Inherit all the methods and objects from the pylost widget canvas.
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'calculate'

    def sizeHint(self):
        return QSize(250, 250)

    def clear_outputs(self):
        #   !!MANDATORY!!
        # Sending nothing to others, clearing the following widgets and their data
        # Must be adapted to each of the objects in the 'Outputs' container.
        self.Outputs.fft.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):

        # this method should be overload to send no data
        self.clear_outputs()

        #   !!MANDATORY!!
        # incoming data must be set as 'self.dataflow'
        self.dataflow = dataset

        # incoming data: could be 'None' if empty link or in instance of removal\insertion upstream
        if not dataset:
            return

        if dataset.is_1D:
            return

        # clear all messages and fill the log
        self.acknowledge_reception(dataset)

        # user option to decide what to do just after linking in the workflow
        if self.autocalc:
            self.calculate()

    @staticmethod
    def _fft2d(data:[Surface], shift_freq_x=True):
        """
            --> Put math, or anything, in this method  <--

            method to be called in the 'Executor', copy should have been made beforehand by calling 'self.prepare_dataset'
            Must return the active processed data.

            '@staticmethod' because the executor doesn't know nothing about self...as AI...for now...
         """
        return data.calc_fft2d(shift=shift_freq_x, plot=False, return_cls=True)

    def _handle_results(self, ffts:list):
        """
            Override this method to retrieve the results of the data processing from the 'Executor'.
            Called after all threads have been completed successfully.
            Passed argument is a list of results, formatted in a valid StitchingDataset by 'self.create_output_dataset'
        """

        # shared method between pylost widgets: log and clear messages, basically
        super()._handle_results(ffts)

        #   !!MANDATORY!  ...if using multiprocessing ('self.start_executor' or 'self.start_threaded_function').
        # fill the processed data ('self.dataflow') accordingly to the input (stitched or only subapertures list)
        # see: 'self.prepare_dataset'
        ffts = self.create_output_dataset(ffts)

        # status shown in the titlebar and in the visible dataflow scheme.
        # In the later, use '\n' to split into lines for visual appealing.
        # self._update_caption(status)

        # text added to history
        history = '- '# + status

        # send the result through the workflow and fill the history box.
        self.send_and_close(self.Outputs.fft, ffts, history=history)

    def calculate(self):

        # Ouptut send None
        self.clear_outputs()

        # sanity
        if self.dataflow is None:
            return

        # clear messages before processing
        self.clear_all_messages()

        #   !!MANDATORY!!
        # Copy only the data before any alteration.
        # Only stitched (and reference extracted) object if present, otherwise copy the list of subapertures.
        dataset = self.prepare_dataset(self.dataflow)

        # 'self.start_executor' is a pylost widget method which use a multiprocessing 'Executor' in a separate Qthread.
        # Only process stitched data if present, else process all the scans in the 'StitchingDataset' or in a list.
        self.start_executor(self._fft2d, dataset, shift_freq_x=self.shift_freq_x)

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        """
            GUI method called everytime at creation by 'super().__init__(*args, **kwargs)'
         """

        # add options box in the left control area
        self.prepare_control_area()

        # Hide the left control area, if wanted. It's a simple switch.
        self.toggle_control_area()

        # GUI qt5 management (see the doc:https://doc.qt.io/qtforpython-5/)
        main = gui.hBox(self.mainArea)
        main.setMaximumSize(250, 140)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        box = gui.vBox(main, '', stretch=1)
        box.setMaximumSize(200, 200)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        gui.button(box, self, 'Calculate FFT 2D', autoDefault=False, callback=self.calculate)
        self.cb_shift = gui.checkBox(box, self, 'shift_freq_x', 'Zero-freq at center')


# global variable set in the user 'settings.ini' file, here for logging
if VERBOSE:
    print(f"  module '{__name__}' loaded.")
