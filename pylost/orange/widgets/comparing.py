# coding=utf-8

from pylost.orange.widgets import *
from silx.gui.plot.LegendSelector import RenameCurveDialog
from pylost.user.settings import DIALOG_MULTIPLOT


class OWComparing(PylostWidgetsViewerMixin):
    name = 'Comparison'
    description = ''
    icon = "../icons/visstitch.svg"
    priority = 21

    mainArea_width_height_ratio = 2.0

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1

    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    legend_dialog:bool = Setting(DIALOG_MULTIPLOT)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = MultiInput('Results', StitchingDataset, filter_none=True, auto_summary=False)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        noise = Output('noise', StitchingDataset)
    #     # mask_params = Output('Mask', Mask)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------

    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        missing_sources = Msg('Comparison widget corrupt. Please remove and replace widget.')
        type_mismatch = Msg('Cannot compare heights with slopes.')
        fft_input = Msg('FFT data linked. Cannot compare.')

    def __init__(self, *args, **kwargs):
        super().__init__(limit_size=False, multiview=True, mask=False, *args, **kwargs)
        self.sources_widget = self.get_sources_widget()
        self.sources_state = self.get_sources_state()
        self.noise_data = False
        self.has_fft = False
        self.set_sink_anchors_order()

    def showEvent(self, event):
        super().showEvent(event)
        if self._plotviewer._plot is None:
            return
        if not self._plotviewer._plot.initialized:
            self._plotviewer._plot.resetZoom()

    def _spacebar_hit(self, method=None):
        is_checked = self._plotviewer._plot._customToolbar.show_stats_widget.isChecked()
        action = 'hide' if is_checked else 'show'
        getattr(self._plotviewer._plot.getstatswindow(), action)()

    def ctrl_up(self, *args, **kwargs):
        if self._plotviewer._plot:
            active = self._plotviewer._plot.getActiveImage()
            self._plotviewer._plot.shift_profile(active, 'up')
            self._plotviewer._plot.update_marker(active)
        else:
            super().ctrl_up(*args, **kwargs)

    def set_sink_anchors_order(self):
        lim = len(self.node_input_links) + 1 if self.node_input_links else 2
        positions = [p / lim for p in range(1, lim)]
        self.sink_anchors_positions = positions
        self.refresh_layout()

    def open_legend_dialog(self, dataset:StitchingDataset):
        self._update_caption('... Legend dialog awaiting response ...')
        current_legend = dataset.stitched.source if dataset.is_stitched else dataset.scans[0].source
        dialog = RenameCurveDialog(self._plotviewer, current_legend, ('',))
        dialog.resize(300, 50)
        ret = dialog.exec()
        if ret:
            updated_legend = dialog.getText()
            updated = not updated_legend == current_legend
            if not updated:
                return
            if not dataset.is_stitched and len(dataset) > 1:
                split = updated_legend[::-1].rpartition('.')
                fmt = f'0{int(np.log10(len(dataset))) + 1}'
                for s, scan in enumerate(dataset.scans):
                    scan.source = split[2][::-1] + f'_{s + 1:{fmt}}' + split[1] + split[0][::-1]
            elif dataset.is_stitched:
                dataset.stitched.source = updated_legend
            else:
                dataset.scans[0].source = updated_legend

    @Inputs.data
    def update_plot(self, link:int, dataset:StitchingDataset):
        if self.verbose:
            print('update_plot', link, dataset)

        self.send_noise(None)

        if link is None:
            self.clear_all_messages()
            self.Error.missing_sources()
            self._update_caption('CORRUPT WIDGET')
            return

        if dataset.scans[0].is_fft:
            self.has_fft = True
        if self.has_fft and len(self.get_sources_widget()) > 1:
            self.Error.fft_input()
            self._update_caption('! FFT linked !')
            return

        self.noise_data = dataset.noise_data

        if not dataset.is_stitched and len(dataset) > 10 and not dataset.noise_data:
            ans = self.show_warning_box('Comparison Warning', 'Input contains many entries.\nDo you wish to continue?')
            if not ans:
                self._update_caption('aborted.')
                dataset.sender.clear_outputs()
                return

        if self.legend_dialog and not self.noise_data:
            self.open_legend_dialog(dataset)

        self.sink_anchors_in_place = True
        self.set_sink_anchors_order()

        self._update_caption('wait...')

        lnk = self.get_link(dataset.sender)
        if lnk is None:
            self.clear_all_messages()
            self.Error.missing_sources()
            self._update_caption('CORRUPT WIDGET')
            return

        self.data_to_viewer(dataset, link=lnk)

        self._update_caption(self.check_type())

    @Inputs.data.insert
    def insert_dataset(self, link:int, dataset:StitchingDataset):
        if self.verbose:
            print('insert_dataset', link, dataset)

        # new link added
        self.sources_widget = self.get_sources_widget()
        self.sources_state = self.get_sources_state()

        if dataset is not None:
            # option to keep anchors in place, must be set at the first valid connection to avoid critical error
            self.sink_anchors_in_place = True
            self.update_plot(-1, dataset)

        self.set_sink_anchors_order()

    @Inputs.data.remove
    def remove_dataset(self, link:int):
        # existing link removed or became None
        if self.verbose:
            print('remove_dataset', link)

        self.set_sink_anchors_order()

        self._update_caption(self.check_type())

        if self.emptied():
            self._plotviewer.clear()
            return

        widgets = self.get_sources_widget()
        for lnk, wid in enumerate(self.sources_widget):
            invalidated = self.source_invalidated(lnk)
            if wid not in widgets or invalidated:
                if self.verbose:
                    print(f'link {lnk} invalidated.' if invalidated else f'link {lnk} has been removed.')
                self._plotviewer.remove_link(lnk, invalidated)
                self.sources_widget = self.get_sources_widget()
                self.sources_state = self.get_sources_state()
                return

    def source_invalidated(self, link):
        return self.get_source_state(link) != self.sources_state[link]

    def check_type(self):
        self.clear_all_messages()
        is_slopes = []
        for _, entry in self.get_inputs():
            if not isinstance(entry, StitchingDataset):
                continue
            is_slopes.append(entry.is_slopes)
        if not is_slopes:
            return ''
        if not min(is_slopes) == max(is_slopes):
            self.Error.type_mismatch()
            return '! Heights & Slopes comparison !'
        return 'Multiview complete'

    def emptied(self):
        empty = []
        widgets = self.get_sources_widget()
        for lnk, wid in enumerate(self.sources_widget):
            empty.append(wid not in widgets or self.get_source_state(lnk))
        return np.all(empty)

    def send_noise(self, noise):
        if noise is None:
            self.Outputs.noise.send(None)
            return
        noise.set_sender(self)
        self.Outputs.noise.send(noise)

    def send_mask_params(self, params:dict):
        """ inhibit mask output """

    def set_ui(self):
        # Data viewer in mainArea
        box = gui.vBox(self.mainArea, 'Silx dataviewer', stretch=1)
        box.setMinimumSize(800, 600)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().addWidget(self._plotviewer)

        # controlArea
        items = {'checkBox': {'args': ['legend_dialog', 'Dialog']}}
        self.prepare_control_area(items=items)
        # gui.checkBox(self.optionsbox, self, 'legend_dialog', 'Dialog')

        # hide control_area at creation
        self.toggle_control_area()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
