# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils.simulation import generate_surface, generate_subapertures


mirror_prms = ['size_x', 'size_y', 'res_x', 'res_y', 'shape', 'roc', 'roc_x', 'roc_y', 'p', 'q', 'theta', 'equ_shape',
               'errors', 'rms_errors', 'psd_slope', 'hurst_exp', 'roll_off', 'roll_off_exp',
               'low_x', 'low_y', 'cut_off', 'cut_off_exp', 'equ_errors', 'noise_errors']
mirror_vald = [94.0, 5.0, 50.0, 50.0, 1, 123.0, 247.0, 847.0, 118.425, 0.145, 4.5, '',
               1, 0.3, -2.2, 0.81, 66.3, 0.85,
               69.1, 12.3, 0.0, 1.2, 'cos(2*pi*x/3.5) + sin(2*pi*y/0.77)', 0.0] # 0, 0.723, -3.15, 0.642, 15.486, '', 0.0]
sub_prms = ['sub_x', 'sub_y', 'overlap_x', 'overlap_y', 'random_x', 'random_y', 'sub_noise']
sub_vald = [0.0, 0.0, 80.0, 80.0, 0.0, 0.0, 0.0]
ref_prms = ['pv_ref', 'ref', 'path_ref', 'equ_ref', 'noise_ref']
ref_vald = [0.0, 0, '', '', 0.0]


class OWSimulating(PylostWidgetsFileManagementMixin):
    name = 'Surface Simulation'
    description = ''
    icon = "../icons/mirror.svg"
    priority = 71

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    mirror_prms:dict = Setting(dict(zip(mirror_prms, mirror_vald)))
    sub_prms:dict = Setting(dict(zip(sub_prms, sub_vald)))
    ref_prms:dict = Setting(dict(zip(ref_prms, ref_vald)))
    generate_sub:bool = Setting(False)
    reference_path:str = Setting('')
    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    # class Inputs:
    #     data = Input('data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        simulated = Output('Simulated data', StitchingDataset)
        fft = Output('Simulated FFT', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    # class Error(widget.OWWidget.Error):
    #     unknown = widget.Msg("{}")
    #     scans_not_found = widget.Msg("No scan data is available.")

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    class Warning(widget.OWWidget.Warning):
        no_file = Msg('No file to load.')

    def __init__(self, *args, **kwargs):
        self._default_parameters()
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'generate'
        self.file_combo.setVisible(False)
        self.autoclose = False

    def create_dataset(self, surface:Surface, subapertures:[Surface], motors=None):
        if not subapertures:
            return StitchingDataset([surface], None, folder=self.last_folder)
        dataset = StitchingDataset(subapertures, motors, folder=self.last_folder)
        dataset.set_stitched_data(surface)
        return dataset

    def generate(self):
        self._update_params()
        try:
            self.Error.clear()
            surface, fft = generate_surface(**self.mirror_prms)
            if surface is None:
                self.Information.success()
                return
            subapertures, motors = None, None
            if self.generate_sub:
                subapertures, motors = generate_subapertures(**{**self.sub_prms, **self.ref_prms})
            self.send_and_close(self.Outputs.simulated, self.create_dataset(surface, subapertures))
            if fft is not None:
                self.send_and_close(self.Outputs.fft, self.create_dataset(fft, None))
            self.Information.success()
        except Exception as e:
            msg = repr(e) if self.verbose else 'Error during generation'
            self.Error.default(msg)

    def load_reference(self):
        filepaths = self.open_files_dialog(start_dir=str(self.last_path))
        if not filepaths:
            self.Warning.no_file()
            return
        self.reference_path = str(filepaths[0])

    def _default_parameters(self):
        for i, key in enumerate(mirror_prms):
            if key not in self.mirror_prms:
                self.mirror_prms[key] = mirror_vald[i]

        for i, key in enumerate(sub_prms):
            if key not in self.sub_prms:
                self.sub_prms[key] = sub_vald[i]

        for i, key in enumerate(ref_prms):
            if key not in self.ref_prms:
                self.ref_prms[key] = ref_vald[i]

    def _update_size(self):
        self.res_y.setValue(self.res_x.value(), block_signal=True)
        try:
            px = int(round(1e3 * self.size_x.value() / self.res_x.value(), 0))
            px = px - 1 if px % 2 == 1 else px
            py = int(round(1e3 * self.size_y.value() / self.res_y.value(), 0))
            py = py - 1 if py % 2 == 1 else py
            length = px * self.res_x.value() * 1e-3
            width = py * self.res_y.value() * 1e-3
            text = f'   ({int(round(1e3 * length / self.res_x.value(), 0))} x {int(round(1e3 * width / self.res_y.value(), 0))} pix)'
            self.pixel_size_str.setText(text)
            self.size_x.blockSignals(True)
            self.size_y.blockSignals(True)
            self.size_x.setValue(length)
            self.size_y.setValue(width)
            self.size_x.blockSignals(False)
            self.size_y.blockSignals(False)
            self._update_params()
        except ZeroDivisionError:
            return

    def _update_shape(self):
        shape = self.shape.currentIndex()
        self.cyl_params.setVisible(shape==1)
        self.ell_params.setVisible(shape==2)
        self.sph_params.setVisible(shape==3)
        self.equ_params.setVisible(shape==4)
        self._update_params()

    def _update_errors(self):
        self.rand_box.setVisible(self.errors.currentIndex()==1)
        self.gen_box.setVisible(self.errors.currentIndex()==2)
        self._update_params()

    def _update_ref(self):
        self.path_box.setVisible(self.ref.currentIndex()==1)
        self.equ_box.setVisible(self.ref.currentIndex()==2)
        self._update_params()

    def _update_params(self):
        for prms, prms_name in zip((mirror_prms, sub_prms, ref_prms), ('mirror_prms', 'sub_prms', 'ref_prms')):
            for attr_name in prms:
                try:
                    attr = getattr(self, attr_name)
                    if isinstance(attr, FloatLineEdit):
                        func = 'value'
                    elif isinstance(attr, QPlainTextEdit):
                        func = 'toPlainText'
                    else:
                        func = 'currentIndex'
                    getattr(self, prms_name)[attr_name] = getattr(attr, func)()
                except AttributeError:
                    continue
        try:
            ref_path = 'No reference file loaded.'
            if self.reference_path:
                ref_path = './' + Path(self.reference_path).name
            self.path_ref.setText(ref_path)
        except AttributeError:
            ...

    def _enable_sub(self):
        self.sub_box.setEnabled(self.generate_sub)
        self.ref_box.setEnabled(self.generate_sub)

    def _update_sub(self):
        try:
            step_x = self.sub_x.value() * (1 - self.overlap_x.value() * 0.01)
            step_y = self.sub_y.value() * (1 - self.overlap_y.value() * 0.01)
            num_x = int(np.ceil(self.size_x.value() / step_x)) if step_x > 0 else 1
            num_y = int(np.ceil(self.size_y.value() / step_y)) if step_y > 0 else 1
            self.step_x.setText(f'{step_x:.3f}')
            self.step_y.setText(f'{step_y:.3f}')
            self.num_x.setText(f'{num_x}')
            self.num_y.setText(f'{num_y}')
            self._update_params()
        except AttributeError:
            return

    # noinspection PyAttributeOutsideInit,PyArgumentList
    def set_ui(self):
        """"""
        # hide control_area at creation
        self.toggle_control_area()

        main = gui.vBox(self.mainArea)
        main.setMinimumSize(720, 510)
        main.layout().setAlignment(Qt.AlignRight | Qt.AlignTop)

        # buttons
        actions = gui.hBox(main, box='Actions', spacing=10,
                           sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignRight | Qt.AlignTop)
        self.btnRandom = gui.button(actions, self, 'Generate random parameters', stretch=1, autoDefault=False, callback=self.generate)
        self.btnRandom.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        self.btnApply = gui.button(actions, self, 'Simulate', stretch=1, autoDefault=False, callback=self.generate)
        self.btnApply.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        params_box = gui.hBox(main, box='', spacing=10,
                           sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding))
        params_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        surf_box = gui.vBox(params_box, box='Surface parameters', spacing=10,
                            sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        surf_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.surf_box = surf_box
        self._set_ui_size(surf_box)
        self._set_ui_shape(surf_box)
        self._set_ui_errors(surf_box)

        subref_box = gui.vBox(params_box, spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        subref_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.subref_box = subref_box
        gui.checkBox(subref_box, self, 'generate_sub', 'Generate subapertures', callback=self._enable_sub)
        self._set_ui_subapertures(subref_box)
        self._set_ui_reference(subref_box)
        self._enable_sub()

    # noinspection PyAttributeOutsideInit,PyArgumentList
    def _set_ui_subapertures(self, subref_box):
        self.sub_box = gui.vBox(subref_box, box='Subapertures', spacing=0, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.sub_box.setFixedWidth(340)
        sub_gen_box = gui.vBox(subref_box, box='', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        self.sub_gen_box = sub_gen_box

        sub_size = QWidget(parent=self.sub_box)
        sub_size.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)

        line = 0
        layout.addWidget(gui.widgetLabel(None, 'X axis'), line, 1, alignment=Qt.AlignCenter | Qt.AlignBottom)
        layout.addWidget(gui.widgetLabel(None, 'Y axis'), line, 3, alignment=Qt.AlignCenter | Qt.AlignBottom)

        line = 1
        layout.addWidget(gui.widgetLabel(None, 'Subaperture size:'), line, 0, alignment=Qt.AlignLeft)
        self.sub_x = FloatLineEdit(None, value=self.sub_prms['sub_x'], fmt='.3f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.sub_x, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' x '), line, 2, alignment=Qt.AlignCenter)
        self.sub_y = FloatLineEdit(None, value=self.sub_prms['sub_y'], fmt='.3f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.sub_y, line, 3, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' mm '), line, 4, alignment=Qt.AlignLeft)

        line = 2
        layout.addWidget(gui.widgetLabel(None, 'Overlap in both axis:'), line, 0, alignment=Qt.AlignLeft)
        self.overlap_x = FloatLineEdit(None, value=self.sub_prms['overlap_x'], fmt='.2f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.overlap_x, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, '%     '), line, 2, alignment=Qt.AlignLeft)
        self.overlap_y = FloatLineEdit(None, value=self.sub_prms['overlap_y'], fmt='.2f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.overlap_y, line, 3, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, '%'), line, 4, alignment=Qt.AlignLeft)

        line = 3
        layout.addWidget(gui.widgetLabel(None, '# of subapertures:'), line, 0, alignment=Qt.AlignLeft)
        self.num_x = gui.widgetLabel(None, '0')
        layout.addWidget(self.num_x, line, 1, alignment=Qt.AlignRight)
        self.num_y = gui.widgetLabel(None, '0')
        layout.addWidget(self.num_y, line, 3, alignment=Qt.AlignRight)

        line = 4
        layout.addWidget(gui.widgetLabel(None, 'Constant step size:'), line, 0, alignment=Qt.AlignLeft)
        self.step_x = gui.widgetLabel(None, '0.0')
        layout.addWidget(self.step_x, line, 1, alignment=Qt.AlignRight)
        layout.addWidget(gui.widgetLabel(None, 'mm'), line, 2, alignment=Qt.AlignLeft)
        self.step_y = gui.widgetLabel(None, '0.0')
        layout.addWidget(self.step_y, line, 3, alignment=Qt.AlignRight)
        layout.addWidget(gui.widgetLabel(None, 'mm'), line, 4, alignment=Qt.AlignLeft)

        line = 5
        layout.addWidget(gui.widgetLabel(None, 'Random variation:'), line, 0, alignment=Qt.AlignLeft)
        self.random_x = FloatLineEdit(None, value=self.sub_prms['random_x'], fmt='.3f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.random_x, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' x '), line, 2, alignment=Qt.AlignCenter)
        self.random_y = FloatLineEdit(None, value=self.sub_prms['random_y'], fmt='.3f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.random_y, line, 3, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' mm '), line, 4, alignment=Qt.AlignLeft)

        line = 6
        layout.addWidget(gui.widgetLabel(None, 'Noise added:'), line, 0, alignment=Qt.AlignLeft)
        self.sub_noise = FloatLineEdit(None, value=self.sub_prms['sub_noise'], fmt='.3f', maxwidth=61, callback=self._update_sub)
        layout.addWidget(self.sub_noise, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'nm '), line, 2, alignment=Qt.AlignLeft)

        sub_size.setLayout(layout)

        self.sub_box.layout().addWidget(sub_size)

        self._update_sub()

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def _set_ui_reference(self, subref_box):
        self.ref_box = gui.vBox(subref_box, box='Reference errors', spacing=10,
                                sizePolicy=(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        self.ref_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.ref_box.setFixedWidth(340)
        self.ref_box.setFixedHeight(190)

        pv_box = QWidget(parent=self.ref_box)
        layout = QHBoxLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Peak to Valley:'), alignment=Qt.AlignLeft)
        self.pv_ref = FloatLineEdit(None, value=self.ref_prms['pv_ref'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.pv_ref, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'nm'), alignment=Qt.AlignLeft)
        pv_box.setLayout(layout)
        self.ref_box.layout().addWidget(pv_box)

        items = ('None', 'Load from file', 'Enter equation')
        self.ref = gui.comboBox(self.ref_box, self, None, orientation=Qt.Horizontal, label='', items=items)
        self.ref.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.ref.setCurrentIndex(self.ref_prms['ref'])
        self.ref.currentIndexChanged.connect(self._update_ref)

        self.path_box = QWidget(parent=self.ref_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        load_button = gui.button(None, self, '...', autoDefault=False, callback=self.load_reference)
        load_button.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))
        load_button.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
        layout.addWidget(load_button, 0, 0)
        self.path_ref = gui.widgetLabel(None, '')
        layout.addWidget(self.path_ref, 2, 0, alignment=Qt.AlignLeft)
        self.path_box.setLayout(layout)
        self.ref_box.layout().addWidget(self.path_box)

        self.equ_box = QWidget(parent=self.ref_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Equation (x,y):'), 0, 0, alignment=Qt.AlignLeft)
        self.equ_ref = QPlainTextEdit(self.ref_prms['equ_ref'])
        self.equ_ref.textChanged.connect(self._update_params)
        self.equ_ref.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.equ_ref.setMinimumWidth(165)
        layout.addWidget(self.equ_ref, 0, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'Noise percentage:'), 1, 0, alignment=Qt.AlignLeft)
        self.noise_ref = FloatLineEdit(None, value=self.ref_prms['noise_ref'], fmt='.1f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.noise_ref, 1, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, '%'), 1, 2, alignment=Qt.AlignLeft)
        self.equ_box.setLayout(layout)
        self.ref_box.layout().addWidget(self.equ_box)

        self._update_ref()

    # noinspection PyAttributeOutsideInit,PyArgumentList
    def _set_ui_size(self, surf_box, surf_width=351):
        surf_size_box = gui.vBox(surf_box, box='Size', spacing=0, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        surf_size_box.setFixedWidth(surf_width)
        self.surf_size_box = surf_size_box

        surf_size = QWidget(parent=surf_size_box)
        surf_size.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)

        layout.addWidget(gui.widgetLabel(None, 'Full size:'), 0, 0, alignment=Qt.AlignLeft)
        self.size_x = FloatLineEdit(None, value=self.mirror_prms['size_x'], fmt='.3f', maxwidth=61, callback=self._update_size)
        layout.addWidget(self.size_x, 0, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' x '), 0, 2, alignment=Qt.AlignLeft)
        self.size_y = FloatLineEdit(None, value=self.mirror_prms['size_y'], fmt='.3f', maxwidth=61, callback=self._update_size)
        layout.addWidget(self.size_y, 0, 3, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' mm '), 0, 4, alignment=Qt.AlignLeft)
        self.pixel_size_str = gui.widgetLabel(None, '')
        layout.addWidget(self.pixel_size_str, 0, 5, alignment=Qt.AlignLeft)

        layout.addWidget(gui.widgetLabel(None, 'Pixel size:'), 1, 0, alignment=Qt.AlignLeft)
        self.res_x = FloatLineEdit(None, value=self.mirror_prms['res_x'], fmt='.3f', maxwidth=61, callback=self._update_size)
        layout.addWidget(self.res_x, 1, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' x '), 1, 2, alignment=Qt.AlignLeft)
        self.res_y = FloatLineEdit(None, value=self.mirror_prms['res_y'], fmt='.3f', maxwidth=61, callback=self._update_size)
        layout.addWidget(self.res_y, 1, 3, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, ' um '), 1, 4, alignment=Qt.AlignLeft)
        self.res_y.setEnabled(False)

        surf_size.setLayout(layout)

        surf_size_box.layout().addWidget(surf_size)

        self._update_size()

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def _set_ui_shape(self, surf_box, surf_width=351):
        surf_shape_box = gui.vBox(surf_box, box='Global shape', spacing=0, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        surf_shape_box.setFixedWidth(surf_width)
        surf_shape_box.setMinimumHeight(133)
        surf_shape_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.surf_shape_box = surf_shape_box

        items = ('Flat', 'Cylinder', 'Tangential Ellipse', 'Sphere', 'Equation')#, 'Parabolic', 'Aspherical')
        self.shape = gui.comboBox(surf_shape_box, self, None, orientation=Qt.Horizontal, label='', items=items)
        self.shape.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.shape.setCurrentIndex(self.mirror_prms['shape'])
        self.shape.currentIndexChanged.connect(self._update_shape)

        # cylinder
        self.cyl_params = QWidget(parent=surf_shape_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Radius of curvature longitudinal:'), 0, 0, alignment=Qt.AlignLeft)
        self.roc_x = FloatLineEdit(None, value=self.mirror_prms['roc_x'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.roc_x, 0, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'm'), 0, 2, alignment=Qt.AlignLeft)

        layout.addWidget(gui.widgetLabel(None, 'Radius of curvature sagital:'), 1, 0, alignment=Qt.AlignLeft)
        self.roc_y = FloatLineEdit(None, value=self.mirror_prms['roc_y'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.roc_y, 1, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'm'), 1, 2, alignment=Qt.AlignLeft)
        self.cyl_params.setLayout(layout)
        surf_shape_box.layout().addWidget(self.cyl_params)

        # ellipse
        self.ell_params = QWidget(parent=surf_shape_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Source distance p:'), 0, 0, alignment=Qt.AlignLeft)
        self.p = FloatLineEdit(None, value=self.mirror_prms['p'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.p, 0, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'm'), 0, 2, alignment=Qt.AlignLeft)

        layout.addWidget(gui.widgetLabel(None, 'Focus distance q:'), 1, 0, alignment=Qt.AlignLeft)
        self.q = FloatLineEdit(None, value=self.mirror_prms['q'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.q, 1, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'm'), 1, 2, alignment=Qt.AlignLeft)

        layout.addWidget(gui.widgetLabel(None, 'Incidence angle theta:'), 2, 0, alignment=Qt.AlignLeft)
        self.theta = FloatLineEdit(None, value=self.mirror_prms['theta'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.theta, 2, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'mrad'), 2, 2, alignment=Qt.AlignLeft)
        self.ell_params.setLayout(layout)
        surf_shape_box.layout().addWidget(self.ell_params)

        # sphere
        self.sph_params = QWidget(parent=surf_shape_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Radius of curvature:'), 0, 0, alignment=Qt.AlignLeft)
        self.roc = FloatLineEdit(None, value=self.mirror_prms['roc'], fmt='.3f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.roc, 0, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'm'), 0, 2, alignment=Qt.AlignLeft)
        self.sph_params.setLayout(layout)
        surf_shape_box.layout().addWidget(self.sph_params)

        # equation
        self.equ_params = QWidget(parent=surf_shape_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Equation (x,y):'), 0, 0, alignment=Qt.AlignLeft)
        self.equ_shape = QPlainTextEdit(self.mirror_prms['equ_shape'])
        self.equ_shape.textChanged.connect(self._update_params)
        self.equ_shape.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.equ_shape.setMinimumWidth(239)
        layout.addWidget(self.equ_shape, 0, 1, alignment=Qt.AlignLeft)
        self.equ_params.setLayout(layout)
        surf_shape_box.layout().addWidget(self.equ_params)

        self._update_shape()

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def _set_ui_errors(self, err_box, surf_width=351):
        surf_err_box = gui.vBox(err_box, box='Figure errors', spacing=0, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        surf_err_box.setFixedWidth(surf_width)
        surf_err_box.setMinimumHeight(165)
        surf_err_box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.surf_err_box = surf_err_box

        items = ('Perfect shape', 'Generate randomly', 'Enter equation')
        self.errors = gui.comboBox(surf_err_box, self, None, orientation=Qt.Horizontal, label='', items=items)
        self.errors.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.errors.setCurrentIndex(self.mirror_prms['errors'])
        self.errors.currentIndexChanged.connect(self._update_errors)

        self.rand_box = QWidget(parent=surf_err_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)

        line = 0
        layout.addWidget(gui.widgetLabel(None, 'RMS roughness:'), line, 0, alignment=Qt.AlignLeft)
        self.rms_errors = FloatLineEdit(None, value=self.mirror_prms['rms_errors'], fmt='.2f', maxwidth=61,
                                        callback=self._update_params)
        layout.addWidget(self.rms_errors, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'nm'), line, 2, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'target value'), line, 3, alignment=Qt.AlignLeft)

        line = 1
        layout.addWidget(gui.widgetLabel(None, 'Fractal power:'), line, 0, alignment=Qt.AlignLeft)
        self.psd_slope = FloatLineEdit(None, value=self.mirror_prms['psd_slope'], fmt='.2f', maxwidth=61,
                                       callback=self._update_params)
        layout.addWidget(self.psd_slope, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'PSD slope'), line, 3, alignment=Qt.AlignLeft)

        line = 2
        layout.addWidget(gui.widgetLabel(None, 'Hurst exponent:'), line, 0, alignment=Qt.AlignLeft)
        self.hurst_exp = FloatLineEdit(None, value=self.mirror_prms['hurst_exp'], fmt='.2f', maxwidth=61,
                                       callback=self._update_params)
        layout.addWidget(self.hurst_exp, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, '[0..1]'), line, 2, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'roughness'), line, 3, alignment=Qt.AlignLeft)

        line = 3
        layout.addWidget(gui.widgetLabel(None, 'Roll-Off period:'), line, 0, alignment=Qt.AlignLeft)
        self.roll_off = FloatLineEdit(None, value=self.mirror_prms['roll_off'], fmt='.2f', maxwidth=61,
                                      callback=self._update_params)
        layout.addWidget(self.roll_off, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'mm'), line, 2, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'exponent:'), line, 3, alignment=Qt.AlignRight)
        self.roll_off_exp = FloatLineEdit(None, value=self.mirror_prms['roll_off_exp'], fmt='.2f', maxwidth=61,
                                          callback=self._update_params)
        layout.addWidget(self.roll_off_exp, line, 4, alignment=Qt.AlignLeft)

        # line = 3
        # layout.addWidget(gui.widgetLabel(None, 'Roll-Off exponent:'), line, 0, alignment=Qt.AlignLeft)
        # self.roll_off_exp = FloatLineEdit(None, value=self.mirror_prms['roll_off_exp'], fmt='.2f', maxwidth=61,
        #                                   callback=self._update_params)
        # layout.addWidget(self.roll_off_exp, line, 1, alignment=Qt.AlignLeft)
        # layout.addWidget(gui.widgetLabel(None, '0 for'), line, 2, alignment=Qt.AlignLeft)
        # layout.addWidget(gui.widgetLabel(None, 'roll-off'), line, 3, alignment=Qt.AlignLeft)
        #
        # line = 4
        # layout.addWidget(gui.widgetLabel(None, 'Low freq period:'), line, 0, alignment=Qt.AlignLeft)
        # self.low_x = FloatLineEdit(None, value=self.mirror_prms['low_x'], fmt='.2f', maxwidth=61,
        #                            callback=self._update_params)
        # layout.addWidget(self.low_x, line, 1, alignment=Qt.AlignLeft)
        # layout.addWidget(gui.widgetLabel(None, 'mm'), line, 2, alignment=Qt.AlignLeft)
        # self.low_y = FloatLineEdit(None, value=self.mirror_prms['low_y'], fmt='.2f', maxwidth=61,
        #                            callback=self._update_params)
        # layout.addWidget(self.low_y, line, 3, alignment=Qt.AlignLeft)
        # layout.addWidget(gui.widgetLabel(None, 'mm'), line, 4, alignment=Qt.AlignLeft)

        line = 5
        layout.addWidget(gui.widgetLabel(None, 'Cut-Off period:'), line, 0, alignment=Qt.AlignLeft)
        self.cut_off = FloatLineEdit(None, value=self.mirror_prms['cut_off'], fmt='.2f', maxwidth=61,
                                     callback=self._update_params)
        layout.addWidget(self.cut_off, line, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'mm'), line, 2, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'exponent:'), line, 3, alignment=Qt.AlignRight)
        self.cut_off_exp = FloatLineEdit(None, value=self.mirror_prms['cut_off_exp'], fmt='.2f', maxwidth=61,
                                         callback=self._update_params)
        layout.addWidget(self.cut_off_exp, line, 4, alignment=Qt.AlignLeft)

        self.rand_box.setLayout(layout)
        surf_err_box.layout().addWidget(self.rand_box)

        self.gen_box = QWidget(parent=surf_err_box)
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(gui.widgetLabel(None, 'Equation (x,y):\n\ncoordinates in mm\n heights in nm'), 0, 0, alignment=Qt.AlignLeft)
        # self.equ_errors = gui.lineEdit(None, self, self.mirror_prms['equ_errors'], callback=self._update_params)
        self.equ_errors = QPlainTextEdit(self.mirror_prms['equ_errors'])
        self.equ_errors.textChanged.connect(self._update_params)
        self.equ_errors.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.equ_errors.setMinimumWidth(239)
        layout.addWidget(self.equ_errors, 0, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, 'Noise percentage:'), 1, 0, alignment=Qt.AlignLeft)
        self.noise_errors = FloatLineEdit(None, value=self.mirror_prms['noise_errors'], fmt='.1f', maxwidth=61, callback=self._update_params)
        layout.addWidget(self.noise_errors, 1, 1, alignment=Qt.AlignLeft)
        layout.addWidget(gui.widgetLabel(None, '%'), 1, 2, alignment=Qt.AlignLeft)
        self.gen_box.setLayout(layout)
        surf_err_box.layout().addWidget(self.gen_box)

        self._update_errors()

    def _print_size(self):
        super()._print_size()
        size_str = f'\n                       main: {self.mainArea.frameGeometry().width()} {self.mainArea.frameGeometry().height()}'
        size_str = size_str + f'\n                       surf_box: {self.surf_box.frameGeometry().width()} {self.surf_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       surf_size_box: {self.surf_size_box.frameGeometry().width()} {self.surf_size_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       surf_shape_box: {self.surf_shape_box.frameGeometry().width()} {self.surf_shape_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       surf_err_box: {self.surf_err_box.frameGeometry().width()} {self.surf_err_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       equ_errors: {self.equ_errors.frameGeometry().width()} {self.equ_errors.frameGeometry().height()}'
        size_str = size_str + f'\n                       sub_box: {self.sub_box.frameGeometry().width()} {self.sub_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       sub_gen_box: {self.sub_gen_box.frameGeometry().width()} {self.sub_gen_box.frameGeometry().height()}'
        size_str = size_str + f'\n                       sub_ref_box: {self.ref_box.frameGeometry().width()} {self.ref_box.frameGeometry().height()}'
        print(size_str)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
