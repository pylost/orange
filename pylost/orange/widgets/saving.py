# coding=utf-8

from pylost.orange.widgets import *


class OWSaving(PylostWidgetsFileManagementMixin, PylostWidgetsViewerMixin):
    """ each widget in Pylost should be a subclass of 'PylostWidgets' """
    name = 'Saving'
    description = ''
    icon = "../icons/save.svg"
    priority = 24

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    default_folder:str = Setting('')
    include_patches:bool = Setting(False)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('export', StitchingDataset)
        # params = Input(Fit parameters', dict, explicit=True)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    # class Outputs:
    #     exported = Output('saved data', StitchingDataset)
    #     # params = Output('Fit parameters', dict)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    # class Error(widget.OWWidget.Error):
    #     generic = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    # class Warning(widget.OWWidget.Warning):
    #     aborted = Msg('{}')

    def __init__(self, *args, **kwargs):
        super().__init__(limit_size=False, *args, **kwargs)
        self.spacebar_action = 'open_saving_dialog'
        # widget specific attributes
        self.savepath = None

    def sizeHint(self):
        return QSize(0, 720)

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        """if some Inputs are defined, each one MUST have an specific method to handle the reception"""
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            self._plotviewer.clear()
            return
        self.acknowledge_reception(dataset)
        self.data_to_viewer(dataset)
        self.include_subapertures.setEnabled(len(dataset) > 1 and dataset.is_stitched)
        self.include_subapertures.setChecked(len(dataset) > 1 and self.include_patches or not dataset.is_stitched)

    def change_default_folder(self):
        # noinspection PyArgumentList
        self.default_folder = QFileDialog.getExistingDirectory(None, 'Choose default folder', self.default_folder,)
        # QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)

    # noinspection PyArgumentList
    def open_saving_dialog(self, default_name=''):
        if self.dataflow is None:
            return
        filters = ['PyLost format (*.plst)'][::-1]
        if self.dataflow.is_1D:
            if self.dataflow.is_slopes:
                filters.append('ESRF LTP format (*.slp)')
            else:
                filters.append('Zygo dat format (*.dat)')
                filters.append('ESRF HGT format (*.hgt)')
        elif self.dataflow.is_2D and self.dataflow.is_heights:
            filters.append('Zygo datx format (*.datx)')
            filters.append('OPD Veeco format (*.opd)')
            filters.append('Zygo dat format (*.dat)')
        filters = ';;'.join(filters[::-1])
        # filters = ';;'.join(filters)
        if not default_name:
            default_name = Path(self.dataflow.scans[0].source)
        if not self.default_folder:
            self.default_folder = str(self.dataflow.folder)
        # extension = default_name.suffix
        default_name = Path(self.default_folder, default_name.stem)
        self.savepath, _ = QFileDialog.getSaveFileName(None, 'Export data to file', str(default_name), filters)
        if not self.savepath:
            self.savepath = None
            return

    def export(self):
        self.open_saving_dialog()
        if self.dataflow is None or self.savepath is None:
            return
        self.clear_all_messages()
        self.clear_outputs()
        new_path = Path(self.savepath)
        self.default_folder = str(new_path.parent)
        reader = self.get_reader(new_path)
        if reader is None:
            return
        try:
            assert isinstance(self.dataflow, StitchingDataset)
            if isinstance(reader, PylostFormat):
                reader.export(new_path, self.dataflow, include_subapertures=self.include_patches)
                caption = 'saved in Pylost format'
            elif self.dataflow.is_stitched:
                data = self.dataflow.stitched
                reader.export(str(new_path), data)
                caption = f'Stitched data exported as: \n{new_path.name}'
                if self.include_patches and len(self.dataflow) > 1:
                    subfolder = self.save_patches(new_path, reader)
                    caption = caption + f'\n({len(self.dataflow)} subapertures exported in\n ./{subfolder.name})'
            else:
                if len(self.dataflow) > 1:
                    subfolder = self.save_patches(new_path, reader)
                    caption = f'\n{len(self.dataflow)} data exported in\n ./{subfolder.name}.'
                else:
                    reader.export(str(new_path), self.dataflow.scans[0])
                    caption = f'Single data exported as: \n{new_path.name}'
            self._update_caption(caption)
            self.Information.success()
            if self.autoclose:
                self.close()
        except Exception as e:
            self.Error.default(f'Error when exporting data: {e}')
            self.file_combo.insertItem(0, new_path.name)
            self.file_combo.setCurrentIndex(0)

    def save_patches(self, new_path:Path, reader):
        subfolder = Path(new_path.parent, new_path.stem)
        if not subfolder.exists():
            subfolder.mkdir()
        fmt = f'0{int(np.log10(len(self.dataflow))) + 1}'
        for i, scan in enumerate(self.dataflow.scans):
            savepath = Path(subfolder, f'{new_path.stem}_{i + 1:{fmt}}{reader.EXTENSIONS[0]}')
            reader.export(savepath, scan)
        return subfolder

    # noinspection PyArgumentList,PyAttributeOutsideInit
    def set_ui(self):
        """method called by super().__init__(*args, **kwargs)"""

        # add options box
        self.prepare_control_area(options=True, no_history=True, timeout=False)

        self.controlArea.setMinimumWidth(201)

        export = gui.vBox(self.controlArea, box='Export results', spacing=10,
                          sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        # layout = QGridLayout()
        # layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        # export = gui.widgetBox(self.controlArea, orientation=layout)

        selection = gui.hBox(export, box=None, spacing=10, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        gui.widgetLabel(selection, 'Output file: ')
        self.file_combo.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        selection.layout().addWidget(self.file_combo, Qt.AlignLeft)
        # self.file_button = gui.button(selection, self, '...', autoDefault=False, callback=self.change_default_folder)
        # self.file_button.setToolTip("Set default folder")
        # self.file_button.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))
        # self.file_button.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
        # selection.layout().addWidget(self.file_button, Qt.AlignLeft)

        self.export_button = gui.button(selection, self, 'Export', autoDefault=False, callback=self.export)

        multi = gui.hBox(export, box=None, spacing=10, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        self.include_subapertures = gui.checkBox(multi, self, 'include_patches', 'Includes subapertures')

        # resize = gui.hBox(export, box=None, spacing=10, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        # self.include_subapertures = gui.checkBox(resize, self, 'include_patches', 'Includes subapertures')

        # add only history box on the bottom
        self.prepare_control_area(options=False, no_history=False, timeout=False)

        # Data viewer in mainArea
        main = gui.vBox(self.mainArea)
        # actions = gui.hBox(main, 'Saving')
        # actions.layout().setAlignment(Qt.AlignRight | Qt.AlignTop)
        # gui.button(actions, self, 'Save', callback=self.save_input, autoDefault=False)

        box = gui.vBox(main, 'Silx dataviewer', stretch=1)
        box.setMinimumSize(800, 600)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().addWidget(self._plotviewer)


# global variable set in the user settings.ini file
if VERBOSE:
    print(f"  module '{__name__}' loaded.")
