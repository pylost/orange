# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils import methods


cartesians = {'piston': True, 'plane': True, 'tiltX': True, 'tiltY': True, 'sphere': False,
              'cylinder': False, 'cylX': True, 'cylY': True, 'twist': False}
zernike_std = {'pst': True, 'tlt': True, 'pwr': False, 'ast': False, 'cma': False, 'sa3': False}
polynomials = {'poly_selection': 0, 'poly_deg_max': 0, 'poly_deg_min': 0,
               'poly_x_axis': True, 'poly_y_axis': True}
ellipse = {'ell_optim': True, 'ell_rotate': False,
           'ell_p_optim':False, 'ell_p': 0.0,
           'ell_q_optim':True, 'ell_q': 0.0,
           'ell_theta_optim':True, 'ell_theta': 0.0}
additionals = {'act_bin_x': False, 'act_bin_y': False, 'val_x': 2, 'val_y': 2, 'statistic': 0, 'filtering': False}


class OWfitting(PylostWidgets):
    """ each widget in Pylost should be a subclass of 'PylostWidgets' """
    name = 'Fit'
    description = ''
    icon = "../icons/fit.svg"
    priority = 13

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    timeout:int = Setting(300)
    fit_params:dict = Setting({})
    radio:int = Setting(3)
    autofit:bool = Setting(True)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('data', StitchingDataset)
        # params = Input(Fit parameters', dict, explicit=True)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        fitted = Output('fitted data', StitchingDataset)
        # params = Output('Fit parameters', dict)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    # class Error(widget.OWWidget.Error):
    #     generic = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    class Warning(widget.OWWidget.Warning):
        aborted = Msg('{}')
        not_implemented = Msg('{}')
        # no_method = Msg('No method set.')

    # sigRetrieveFitComplete = pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        self._load_params()
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'remove_fit'
        self._float_update()
        self._validate_terms()
        # self._float_update()
        # self.sigRetrieveFitComplete.connect(self.send_fitted)

    def clear_outputs(self):
        self.Outputs.fitted.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        """if some Inputs are defined, each one MUST have an specific method to handle the reception"""
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self.dataflow = dataset
        self._manage_terms()
        if self.autofit:
            self.remove_fit()

    def remove_fit(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        self._update_caption('wait...')
        method, terms, options = self._setmethod()
        if method is None:
            self.send_fitted(self.dataflow.copydataset())
            return
        try:
            self.start_executor(self._fit, self.prepare_dataset(self.dataflow, update_roc=False),
                                method=method, terms=terms, **options)
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default('Error when fitting data.', e)

    def send_fitted(self, dataset:StitchingDataset):
        if not dataset:
            return
        status = self._set_status(dataset)
        self._fill_infos(dataset)
        self.info_coefs.setText(status)
        history = '- Fit applied:\n' + status
        self.send_and_close(self.Outputs.fitted, dataset, history)
        self._update_caption(status)

    def _set_status(self, dataset:StitchingDataset):
        data = dataset.stitched if dataset.is_stitched else dataset.scans[0]
        multi = not dataset.is_stitched and len(dataset) > 1
        stats = ''
        if not multi:
            unit_val, units_roc = data.values_unit, data.radius_unit
            radius = 'inf' if abs(data.radius) > 1e6 else f'{data.radius:.3f}{units_roc}'
            stats = f'\n  RMS: {data.rms:.3f}{unit_val}, PV: {data.pv:.3f}{unit_val}, ' \
                    f'radius: {radius}'

        method, terms, options = self._setmethod()
        if method == 'cartesian_removal':
            string = '   Cartesians terms:      \n     '
            string = string + 'piston, ' if terms['piston'] else string
            if dataset.is_1D:
                string = string + 'tilt, ' if terms['tiltX'] else string
            else:
                string = string + 'plane, ' if terms['tiltX'] and terms['tiltY'] else string
                string = string + 'tilt x, ' if terms['tiltX'] and not terms['tiltY'] else string
                string = string + 'tilt y, ' if not terms['tiltX'] and terms['tiltY'] else string
            string = string + 'sphere, ' if terms['sphere'] else string
            string = string + 'cylinder, ' if terms['cylX'] and terms['cylY'] else string
            string = string + 'cylinder x, ' if terms['cylX'] and not terms['cylY'] else string
            string = string + 'cylinder y, ' if not terms['cylX'] and terms['cylY'] else string
            string = string + 'twist, ' if terms['twist'] else string
            return string[:-2] + stats
        elif method == 'zernike_std_removal':
            string = '   Zernike standard:      \n     '
            string = string + 'piston, ' if terms[0] else string
            string = string + 'tilt, ' if terms[1] else string
            string = string + 'power, ' if terms[2] else string
            string = string + 'astigmatism, ' if terms[3] else string
            string = string + 'coma, ' if terms[4] else string
            string = string + 'spherical aberration, ' if terms[5] else string
            return string[:-2] + stats
        elif self.radio == 3:
            string = '   Cartesian polynomials:      \n     '
            if method == 'poly_legendre_removal':
                string = '   Legendre polynomials:      \n     '
            elif method == 'poly_zernike_removal':
                string = '   Zernike polynomials:      \n     '
            deg_min = terms['deg_min']
            deg_max = terms['deg_max']
            if deg_min != 0:
                string = string + f'from degree: {deg_min}\n to degree: {deg_max}'
            else:
                string = string + f'degree: {deg_max}'
            x_axis = terms['x_axis']
            y_axis = terms['y_axis']
            if not (x_axis and y_axis):
                string = string + '\n         on x axis only' if x_axis else string + '\n         on y axis only'
            return string + stats
        elif method == 'ellipse_removal':
            string = '   Ellipse'
            if data.is_slopes:
                string = string + ' / slopes \n      '
            if data.is_heights:
                string = string + ' / heights \n      '
            optimized = options['optimize']
            if optimized:
                if 'p' in data.ellipse.optimized.optimization:
                    string = string + 'p, '
                if 'q' in data.ellipse.optimized.optimization:
                    string = string + 'q, '
                if 'theta' in data.ellipse.optimized.optimization:
                    string = string + 'theta, '
                if multi:
                    string = string[:-2] + f' optimized over {len(dataset)} elements.      '
                else:
                    string = string[:-2] + ' optimized: \n      '
            if not multi:
                string = string + f'p: {terms[0]:.6f}'
                string = string + f' --> {data.ellipse.p:.6f} m \n      ' if optimized else string + ' m \n      '
                string = string + f'q: {terms[1]:.6f}'
                string = string + f' --> {data.ellipse.q:.6f} m \n      ' if optimized else string + ' m \n      '
                string = string + f'theta: {terms[2]:.5f}'
                string = string + f' --> {data.ellipse.theta * 1e3:.6f} mrad ' if optimized else string + ' mrad '
                string = string + f'\n      rotation minimized ({data.ellipse.rotation:.3f}deg)' if options['rotation'] else string
            else:
                string = string + f'\n      rotation minimized' if options['rotation'] else string
            return string + stats
        return 'No fit applied'

    # additionals = {'binning': False, 'bin_x': 1, 'bin_y': 1, 'filtering': False}

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)
        self.send_fitted(self.create_output_dataset(processed))

    @staticmethod
    def _fit(scan:[Profile, Surface], method, terms, **options):
        # call to the appropriate fitting method
        return getattr(methods, method)(scan, terms, **options)

    # def _preprocessing(self):
    #     if self.filtering:    # TODO

    def _setmethod(self):
        if self.radio == 1:
            # cartesians
            func = 'cartesian_removal'
            terms = {'piston':self.piston, 'plane':self.plane,
                     'tiltX':self.plane and self.tiltX, 'tiltY':self.plane and self.tiltY,
                     'sphere':self.sphere,
                     'cylinder':self.cylinder, 'cylX':self.cylinder and self.cylX, 'cylY':self.cylinder and self.cylY,
                     'twist':self.cylinder and self.twist and False}
            kwargs = {'center_coordinates':True, 'auto_units':True}
        elif self.radio == 2:
            # zernike
            # func = 'zernike_std_removal'
            # terms = (self.pst, self.tlt, self.pwr, self.ast, self.cma, self.sa3)
            # kwargs = {'center_coordinates':True, 'auto_units':True}
            self.Warning.not_implemented('zernike_std_removal not implemented yet.')
            return None, None, None
        elif self.radio == 3:
            # cartesian terms by default
            func = 'poly_cartesians_removal'
            if self.poly_selection == 1:
                # legendre
                # func = 'poly_legendre_removal'
                self.Warning.not_implemented('poly_legendre_removal not implemented yet.')
                return None, None, None
            if self.poly_selection == 2:
                # zernike
                # func = 'poly_zernike_removal'
                self.Warning.not_implemented('poly_zernike_removal not implemented yet.')
                return None, None, None
            terms = {'deg_max':self.poly_deg_max, 'deg_min':self.poly_deg_min,
                     'x_axis':self.poly_x_axis, 'y_axis':self.poly_y_axis}
            kwargs = {'center_coordinates':True, 'auto_units':True}
        elif self.radio == 4:
            # ellipse
            func = 'ellipse_removal'
            terms = (self.ell_p, self.ell_q, self.ell_theta)
            optimization = ()
            if self.ell_p_optim:
                optimization = optimization + ('p',)
            if self.ell_q_optim:
                optimization = optimization + ('q',)
            if self.ell_theta_optim:
                optimization = optimization + ('theta',)
            rotation = self.ell_rotate and self.dataflow.is_2D
            kwargs = {'optimize':self.ell_optim, 'optimization':optimization, 'rotation':rotation}
        else:
            return None, None, None
        more_options = {'binning':self.act_bin_x or self.act_bin_y,
                        'bins':(self.val_x if self.act_bin_x else 1, self.val_y if self.act_bin_y else 1),
                        'stat_method':('nanmean', 'nanmedian')[self.statistic]}
        for key, value in more_options.items():
            kwargs[key] = value
        return func, terms, kwargs

    def _load_params(self):
        for rb in ('cartesians', 'zernike_std', 'polynomials', 'ellipse', 'additionals'):
            for attr, default in globals()[rb].items():
                setattr(self, attr, default)
                try:
                    setattr(self, attr, self.fit_params[rb][attr])
                except KeyError:
                    """new fitting widget added to workflow"""
                    # print('unknow attribute', attr)

    def _fill_infos(self, dataset:StitchingDataset):
        """"""
        self.info_coefs.clear()
        self.fit_errors.clear()
        # method, terms, options = self._setmethod()
        # for data in dataset:
        #     fit = data.fit
        #     # terms = data.fit_terms
        #     # coefs = data.fit_coefs

    def _fill_fit_params(self):
        self.ell_p = self.float_p.value()
        self.ell_q = self.float_q.value()
        self.ell_theta = self.float_theta.value()
        if self.dataflow is not None and self.poly_deg_max == 0:
            self.fit_params['polynomials']['poly_deg_max'] = self.poly_deg_max = 1 if self.dataflow.is_slopes else 2
        for rb in ('cartesians', 'zernike_std', 'polynomials', 'ellipse', 'additionals'):
            self.fit_params[rb] = {}
            for attr in globals()[rb].keys():
                self.fit_params[rb][attr] = getattr(self, attr)

    def _validate_terms(self):
        qobjects = [['cb_cpst', 'cb_cpln', 'cb_ctx', 'cb_cty', 'cb_csph', 'cb_ccyl', 'cb_ccx', 'cb_ccy', 'cb_twt'],
                    ['cb_zpst', 'cb_ztlt', 'cb_zpwr', 'cb_zast', 'cb_zcma', 'cb_zsa3'],
                    ['poly_combo', 'poly_max', 'poly_min', 'poly_x', 'poly_y'],
                    ['cb_optim', 'cb_rotate', 'cb_p', 'float_p', 'cb_q', 'float_q', 'cb_theta', 'float_theta']]
        for i in range(4):
            for qobject in qobjects[i]:
                getattr(self, qobject).setEnabled(self.radio == i+1)
        self._manage_terms()

    def _manage_bins(self):
        if self.dataflow is None or self.radio == 4:
            self.cb_bin_x.setDisabled(True)
            self.sp_bin_x.setDisabled(True)
            self.cb_bin_y.setDisabled(True)
            self.sp_bin_y.setDisabled(True)
            return

        is_2D = self.dataflow.is_2D
        self.cb_bin_x.setEnabled(True)
        self.sp_bin_x.setEnabled(self.act_bin_x)
        self.cb_bin_y.setEnabled(is_2D)
        self.sp_bin_y.setEnabled(self.act_bin_y and is_2D)
        shape = self.dataflow.shape
        vmax = int(shape[1] // self.val_x)
        vmax = vmax - 1 if vmax % 2 == 1 else vmax
        self.sp_bin_x.setMaximum(max(vmax, 256))
        if not is_2D:
            self.sp_bin_y.setMaximum(2)
            return
        vmax = int(shape[2] // self.val_y)
        vmax = vmax - 1 if vmax % 2 == 1 else vmax
        self.sp_bin_y.setMaximum(max(vmax, 256))

    def _manage_terms(self):
        self._fill_fit_params()
        profile = False
        if self.dataflow is not None:
            profile = self.dataflow.is_1D
        self.cb_rotate.setDisabled(profile or self.radio != 4)
        if self.radio == 1:
            if profile:
                self.cb_cpln.setText('Tilt')
            else:
                self.cb_cpln.setText('Plane')
            if profile and self.cylinder:
                self.cb_csph.blockSignals(True)
                self.cb_csph.setChecked(True)
                self.sphere = True
                self.cb_csph.blockSignals(False)
                self.cb_ccyl.blockSignals(True)
                self.cb_ccyl.setChecked(False)
                self.cylinder = False
                self.cb_ccyl.blockSignals(False)
            self.cb_ctx.setEnabled(self.plane and not profile)
            self.cb_cty.setEnabled(self.plane and not profile)
            self.cb_csph.setEnabled(not self.cylinder)
            self.cb_ccyl.setEnabled(not self.sphere and not profile)
            self.cb_ccx.setEnabled(self.cylinder and not self.sphere and not profile)
            self.cb_ccy.setEnabled(self.cylinder and not self.sphere and not profile)
            self.cb_twt.setEnabled(self.cylinder and not self.sphere and not profile)
        if self.radio == 2:
            if profile:
                self.radio = 0
                self._validate_terms()
                return
            self.cb_ztlt.setEnabled(not self.cma)
            if self.cma:
                self.tlt = True
            self.cb_zpwr.setEnabled(not (self.ast or self.sa3))
            if self.ast or self.sa3:
                self.pwr = True
        for cb in ('cb_csph', 'cb_ccyl', 'cb_twt', 'cb_ztlt', 'cb_zpwr'):
            cb = getattr(self, cb)
            if not cb.isEnabled():
                cb.blockSignals(True)
                cb.setChecked(False)
                cb.blockSignals(False)
        self._manage_bins()

    def _float_update(self):
        self.float_p.setValue(self.ell_p, block_signal=True)
        self.float_q.setValue(self.ell_q, block_signal=True)
        self.float_theta.setValue(self.ell_theta, block_signal=True)

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        """method called by super().__init__(*args, **kwargs)"""
        # control area on the left
        items = {'checkBox': {'args': ['autofit', 'Autofit']}}
        self.prepare_control_area(items=items)

        # hide control_area at creation
        self.toggle_control_area()

        # main area
        main = gui.vBox(self.mainArea)
        layout = QGridLayout()

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignRight | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Remove fit', stretch=1, autoDefault=False, callback=self.remove_fit)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        general = gui.widgetBox(main, box='Fit Functions', spacing=16, addSpace=True, stretch=8,
                                orientation=layout, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum))
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        rb = gui.radioButtons(None, self, 'radio', box=True, addSpace=True, addToLayout=False,
                              callback=self._validate_terms)

        # no fit
        rb0 = gui.appendRadioButton(rb, 'None', addToLayout=False)
        layout.addWidget(rb0, 0, 0, Qt.AlignVCenter)

        # classical cartesian terms
        rb1 = gui.appendRadioButton(rb, 'Cartesian', addToLayout=False)
        layout.addWidget(rb1, 1, 0, Qt.AlignVCenter)
        grid_layout = QGridLayout()
        self.cb_cpst = gui.checkBox(None, self, 'piston', 'Piston', callback=self._manage_terms)
        self.cb_cpln = gui.checkBox(None, self, 'plane', 'Plane', callback=self._manage_terms)
        self.cb_ctx = gui.checkBox(None, self, 'tiltX', 'along X-axis', callback=self._manage_terms)
        self.cb_cty = gui.checkBox(None, self, 'tiltY', 'along Y-axis', callback=self._manage_terms)
        self.cb_csph = gui.checkBox(None, self, 'sphere', 'Sphere', callback=self._manage_terms)
        self.cb_ccyl = gui.checkBox(None, self, 'cylinder', 'Cylinder', callback=self._manage_terms)
        self.cb_ccx = gui.checkBox(None, self, 'cylX', 'along X-axis', callback=self._manage_terms)
        self.cb_ccy = gui.checkBox(None, self, 'cylY', 'along Y-axis', callback=self._manage_terms)
        self.cb_twt = gui.checkBox(None, self, 'twist', 'Twist', callback=self._manage_terms)
        grid_layout.addWidget(self.cb_cpst, 0, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_cpln, 1, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_ctx, 1, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_cty, 1, 2, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_csph, 2, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_ccyl, 3, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_ccx, 3, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_ccy, 3, 2, Qt.AlignVCenter)
        # grid_layout.addWidget(self.cb_twt, 4, 0, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(grid_layout)
        layout.addWidget(grid, 1, 1, 1, 3)

        # standard zernike terms
        rb2 = gui.appendRadioButton(rb, 'Zernike Standard', addToLayout=False)
        layout.addWidget(rb2, 2, 0, Qt.AlignVCenter)
        grid_layout = QGridLayout()
        self.cb_zpst = gui.checkBox(None, self, 'pst', 'Piston', callback=self._manage_terms)
        self.cb_zpst.setToolTip("Removes Z0")
        self.cb_ztlt = gui.checkBox(None, self, 'tlt', 'Tilt', callback=self._manage_terms)
        self.cb_ztlt.setToolTip("Removes Z1 & Z2")
        self.cb_zpwr = gui.checkBox(None, self, 'pwr', 'Power', callback=self._manage_terms)
        self.cb_zpwr.setToolTip("Removes Z3")
        self.cb_zast = gui.checkBox(None, self, 'ast', 'Astigmatism', callback=self._manage_terms)
        self.cb_zast.setToolTip("Removes Z4 & Z5 (includes power)")
        self.cb_zcma = gui.checkBox(None, self, 'cma', 'Coma', callback=self._manage_terms)
        self.cb_zcma.setToolTip("Removes Z6 & Z7 (includes Tilts)")
        self.cb_zsa3 = gui.checkBox(None, self, 'sa3', 'Spherical Aberration', callback=self._manage_terms)
        self.cb_zsa3.setToolTip("Removes Z8 (includes Power)")
        grid_layout.addWidget(self.cb_zpst, 0, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_ztlt, 0, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_zpwr, 0, 2, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_zast, 1, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_zcma, 1, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.cb_zsa3, 1, 2, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(grid_layout)
        layout.addWidget(grid, 2, 1, 1, 3)

        sep = gui.separator(general, height=2)
        layout.addWidget(sep, 3, 0, 1, 2)

        # polynomial tables
        rb4 = gui.appendRadioButton(rb, 'Polynomial', addToLayout=False)
        layout.addWidget(rb4, 4, 0, Qt.AlignVCenter)
        box_up = gui.hBox(None)
        self.poly_combo = gui.comboBox(box_up, self, 'poly_selection', sendSelectedValue=False,
                                       items=['Cartesian coefficients', 'Legendre polynomials', 'Zernike polynomials'],
                                       callback=self._manage_terms,
                                       sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.poly_max = gui.spin(box_up, self, 'poly_deg_max', minv=1, maxv=21, label='     Degree max:', labelWidth=75,
                                 callback=self._manage_terms,
                                 orientation=Qt.Horizontal, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.poly_min = gui.spin(box_up, self, 'poly_deg_min', minv=0, maxv=21, label='     Degree min:', labelWidth=75,
                                 callback=self._manage_terms,
                                 orientation=Qt.Horizontal, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        box_dw = gui.hBox(None)
        box_dw.layout().setAlignment(Qt.AlignRight)
        self.poly_x = gui.checkBox(box_dw, self, 'poly_x_axis', 'along X-axis', callback=self._manage_terms)
        self.poly_y = gui.checkBox(box_dw, self, 'poly_y_axis', 'along Y-axis', callback=self._manage_terms)
        box = gui.vBox(None)
        box.layout().addWidget(box_up)
        gui.separator(box, height=1)
        box.layout().addWidget(box_dw)
        layout.addWidget(box, 4, 1, 1, 2)

        sep = gui.separator(general, height=2)
        layout.addWidget(sep, 5, 0, 1, 2)

        # best ellipse
        rb6 = gui.appendRadioButton(rb, 'Ellipse', addToLayout=False)
        layout.addWidget(rb6, 6, 0, Qt.AlignVCenter | Qt.AlignTop)
        box = gui.vBox(None)
        options = gui.hBox(None)
        self.cb_optim = gui.checkBox(options, self, 'ell_optim', 'Optimization', callback=self._manage_terms)
        self.cb_optim.setToolTip("LSQ best ellipse parameters")
        self.cb_rotate = gui.checkBox(options, self, 'ell_rotate', 'Auto Rotation', callback=self._manage_terms)
        self.cb_rotate.setToolTip("Find optimal orientation along X axis")
        box.layout().addWidget(options, Qt.AlignVCenter)
        grid_layout = QGridLayout()
        hb1 = gui.hBox(None)
        self.cb_p = gui.checkBox(hb1, self, 'ell_p_optim', '', callback=self._manage_terms)
        float_p_lbl = QLabel('p (m)', None)
        hb1.layout().addWidget(float_p_lbl, Qt.AlignRight)
        self.float_p = FloatLineEdit(None, 0, maxwidth=75, fmt='.6f', callback=self._manage_terms)
        self.float_p.setToolTip("Distance from source in meter")
        hb2 = gui.hBox(None)
        self.cb_q = gui.checkBox(hb2, self, 'ell_q_optim', '', callback=self._manage_terms)
        float_q_lbl = QLabel('q (m)', None)
        hb2.layout().addWidget(float_q_lbl, Qt.AlignRight)
        self.float_q = FloatLineEdit(None, 0, maxwidth=75, fmt='.6f', callback=self._manage_terms)
        self.float_q.setToolTip("Distance from focus in meter")
        hb3 = gui.hBox(None)
        self.cb_theta = gui.checkBox(hb3, self, 'ell_theta_optim', '', callback=self._manage_terms)
        float_theta_lbl = QLabel('theta (mrad)', None)
        hb3.layout().addWidget(float_theta_lbl, Qt.AlignRight)
        self.float_theta = FloatLineEdit(None, 0, maxwidth=75, fmt='.6f', callback=self._manage_terms)
        self.float_theta.setToolTip("Incidence angle in milliradian")
        grid_layout.addWidget(hb1, 0, 0, Qt.AlignTop | Qt.AlignCenter)
        grid_layout.addWidget(hb2, 0, 1, Qt.AlignTop | Qt.AlignCenter)
        grid_layout.addWidget(hb3, 0, 2, Qt.AlignTop | Qt.AlignCenter)
        grid_layout.addWidget(self.float_p, 1, 0, Qt.AlignTop | Qt.AlignCenter)
        grid_layout.addWidget(self.float_q, 1, 1, Qt.AlignTop | Qt.AlignCenter)
        grid_layout.addWidget(self.float_theta, 1, 2, Qt.AlignTop | Qt.AlignCenter)
        grid = QWidget(None)
        grid.setLayout(grid_layout)
        box.layout().addWidget(grid, Qt.AlignVCenter | Qt.AlignTop)
        layout.addWidget(box, 6, 1, 1, 2, Qt.AlignVCenter | Qt.AlignTop)

        pre_layout = QGridLayout()
        gui.widgetBox(main, box='Fitting Preprocessing Options', spacing=16, addSpace=True, stretch=8,
                      orientation=pre_layout, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum))
        pre_layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        binning_widget = gui.widgetBox(None)
        binning_layout = QGridLayout()
        self.cb_bin_x = gui.checkBox(None, self, 'act_bin_x', 'X axis', callback=self._manage_terms)
        self.cb_bin_x.setToolTip("Fit coefficients are determined from a binned copy of dataset\n"
                                 "then the fit is removed from the non-binned data.")
        self.sp_bin_x = gui.spin(None, self, 'val_x', minv=0, maxv=256, step=2, label='     along X-axis:', labelWidth=77,
                                 callback=self._manage_terms,
                                 orientation=Qt.Horizontal, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.cb_bin_y = gui.checkBox(None, self, 'act_bin_y', 'Y axis', callback=self._manage_terms)
        self.cb_bin_y.setToolTip("Fit coefficients are determined from a binned copy of dataset\n"
                                 "then the fit is removed from the non-binned data.")
        self.sp_bin_y = gui.spin(None, self, 'val_y', minv=0, maxv=256, step=2, label='     along Y-axis:', labelWidth=77,
                                 callback=self._manage_terms,
                                 orientation=Qt.Horizontal, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.combo_bin = gui.comboBox(None, self, 'statistic', label='Statistic', items=('mean', 'median'))
        binning_layout.addWidget(self.cb_bin_x, 0, 0, Qt.AlignVCenter)
        binning_layout.addWidget(self.cb_bin_y, 0, 2, Qt.AlignVCenter)
        binning_layout.addWidget(self.sp_bin_x, 1, 0, Qt.AlignVCenter)
        binning_layout.addWidget(gui.label(None, binning_widget, ' x '), 1, 1, Qt.AlignVCenter)
        binning_layout.addWidget(self.sp_bin_y, 1, 2, Qt.AlignVCenter)
        binning_layout.addWidget(gui.label(None, binning_widget, '    '), 1, 3, Qt.AlignVCenter)
        binning_layout.addWidget(gui.label(None, binning_widget, 'Average'), 0, 4, Qt.AlignVCenter)
        binning_layout.addWidget(self.combo_bin, 1, 4, Qt.AlignVCenter)
        binning_grid = QWidget(None)
        binning_grid.setLayout(binning_layout)
        binning_widget.layout().addWidget(binning_grid)
        pre_layout.addWidget(binning_widget, 0, 0)

        box = gui.hBox(None)
        self.cbfilter = gui.checkBox(box, self, 'filtering', 'Filtering', callback=self._manage_terms)
        self.cbfilter.setDisabled(True)
        pre_layout.addWidget(box, 1, 0)
        # filtering.setToolTip("")

        grid_layout = QGridLayout()
        gui.widgetBox(main, box='Fit Results', spacing=16, addSpace=True, stretch=8,
                      orientation=grid_layout, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum))
        grid_layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        lbl1 = gui.widgetLabel(None, 'Fit parameters: ')
        grid_layout.addWidget(lbl1, 0, 0)
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        self.info_coefs = gui.widgetLabel(None, '')
        scroll.setWidget(self.info_coefs)
        grid_layout.addWidget(scroll, 0, 1)
        lbl1 = gui.widgetLabel(None, 'Fit residuals: ')
        grid_layout.addWidget(lbl1, 1, 0)
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        self.fit_errors = gui.widgetLabel(None, '')
        scroll.setWidget(self.fit_errors)
        grid_layout.addWidget(scroll, 1, 1)


# global variable set in the user settings.ini file
if VERBOSE:
    print(f"  module '{__name__}' loaded.")
