# coding=utf-8

from pylost.orange.widgets import *


class OWAdding(PylostWidgetsOperators):
    name = 'Add'
    description = ''
    icon = "../icons/plus.svg"
    priority = 31
    title = 'data added'

    want_main_area = False

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = MultiInput('Operands', StitchingDataset, filter_none=True, auto_summary=False)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        result = Output('Sum', StitchingDataset)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @Inputs.data
    def set_entry(self, link:int, entry:StitchingDataset):
        super().set_entry(link, entry)

    @Inputs.data.insert
    def insert_entry(self, link:int, entry:StitchingDataset):
        super().insert_entry(link, entry)

    @Inputs.data.remove
    def remove_entry(self, link:int):
        super().remove_entry(link)

    @staticmethod
    def compute(datalist:list[Surface, Profile], operand:[Surface, Profile] = None):
        if operand is None:
            res = datalist[0].duplicate()
            res.values = np.nansum([data.values for data in datalist], axis=0)
            return [res]

        res = []
        for data in datalist:
            valid_mask = np.all(np.isfinite([data.values, operand.values]), axis=0)
            item = data.duplicate()
            item.values = np.nansum(np.dstack((data.values, operand.values)), axis=2).reshape(data.values.shape)
            item.values[~valid_mask] = np.nan
            res.append(item)
        return res


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
