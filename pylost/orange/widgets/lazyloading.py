# coding=utf-8

# TODO: show files view selector + relevant infos, add inputs: motors, sampling, data title, etc.

from pylost.orange.widgets import *
from pylost.user.settings import save_user_history, SEQUENCE_STYLES
from pylost.orange.gui import Regex_Dialog

class OWLoadMeasurement(PylostWidgetsFileManagementMixin, PylostWidgetsViewerMixin):
    name = 'Simple loader'
    description = 'Open measurement files from various common format.'
    icon = "../icons/import.svg"
    priority = 11

    settings_version = 2

    timeout:int = Setting(120)
    history:list[dict] = Setting([])
    regex:str = Setting('')

    class Outputs:
        data = Output('Measurement', StitchingDataset)
        mask_params = Output('Mask', Mask)

    class Warning(widget.OWWidget.Warning):
        aborted = Msg('Files loading aborted.')
        no_file = Msg('No file to load.')
        no_data = Msg('No data to show.')
        invalid_csv = Msg('Cannot load motors positions from file.')
        invalid_sequence = Msg('Invalid sequence, only the selected file is loaded.')
        file_missing = Msg('Sequence contains missing files.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.spacebar_action = '_reload'
        self.rebuild_history()
        self.build_file_combo()
        self.reader = None
        self.samplingbox = None
        self.sampling_unit = None
        self.resx = None
        self.resy = None
        self.shiftx = 0
        self.shifty = 0
        self.postprocessing = True
        self.filepaths = []
        self._last_regex = 0

    def sizeHint(self):
        return QSize(1024, 720)

    def _load_file(self, filepath):  # used in ThreadPoolExecutor
        try:
            return self.reader.loadfile(filepath)
        except Exception as e:
            self.clear_all_messages()
            self.Error.reader_error('Error reading data.', e)
            return None

    def _load_files(self):
        # if not self.Warning.active:
        self.clear_outputs()
        self.clear_all_messages()
        self._plotviewer.clear()
        if not self.filepaths:
            self.Warning.no_file()
            return
        self.test_sequence.hide()
        self.btnAbort.show()
        self._thread_active = True
        self.reader = self.get_reader(self.filepaths[0])
        self.start_executor(self._load_file, self.filepaths, verbose=self.verbose)

    def _handle_results(self, processed):
        for data in processed:
            if data is None:
                self._plotviewer.clear()
                self.clear_outputs()
                self.clear_all_messages()
                self.Error.invalid_data('at least one one element is None.')
                return

        super()._handle_results(processed)
        try:
            if isinstance(self.reader, PylostFormat) or isinstance(self.reader, NexusFormat):
                self.dataflow = processed[0]
                self.dataflow.scans[0].center_coordinates()
                self._update_data()
                filename = Path(self.filepaths[0]).name
                self._update_caption(f'Pylost file {filename} loaded.')
                return
            if len(processed) == 1:
                datalist, motors, extra = processed[0]
            else:
                datalist, motors, extra = list(zip(*processed))
                datalist = [data[0] for data in datalist]
                motors = [motor[0] for motor in motors]
            motors = None if None in motors else motors
            dataset = StitchingDataset(datalist, motors, extra=extra, folder=self.last_folder)
            # if isinstance(dataset.scans[0], MxData):
            #     dataset.Mx_std_canvas_size()
            dataset.shrinkdata()
            self.dataflow = dataset
            self.add_history_entry()
            self._update_data()
        except Exception as e:
            # self.create_traceback()
            self._plotviewer.clear()
            self.clear_outputs()
            self.Information.clear()
            if self.verbose:
                print(f'{self.name}: {e}')
            self.Error.invalid_data(e)

    def _handle_errors(self, error):
        """tuple(type, class, string) """
        if isinstance(error[1], ValueError):
            self.Information.clear()
            self.Error.invalid_data(str(error[1]))
        else:
            super()._handle_errors(error)

    def _thread_complete(self):
        super()._thread_complete()
        self.btnAbort.hide()
        self.test_sequence.show()

    def _update_data(self, include_viewer=True):
        self.update_infos()
        if include_viewer:
            self.data_to_viewer(self.dataflow)
        self.send_and_close(self.Outputs.data, self.dataflow)
        self._update_caption(self.dataflow.history[-1][2:])

    def _change_pixel_size(self):
        self.clear_outputs()
        xy_unit = self.dataflow.scans[0].coords_unit
        new_sampling = xy_unit(np.array([self.resx.value(), self.resy.value()]), self.sampling_unit)
        self.dataflow.change_pixel_size(new_sampling)
        autoclose = self.autoclose
        self.autoclose = False
        self._update_data()
        self.autoclose = autoclose

    def _generate_from_constant_steps(self):
        self.clear_outputs()
        self.dataflow.set_motors_from_steps(self.step_x.value(), self.step_y.value())
        self._update_data(include_viewer=False)

    def _load_motors_csv(self):
        self.Warning.clear()
        # noinspection PyArgumentList
        filepath, _ = QFileDialog.getOpenFileName(None, 'Load motors positions', str(self.last_folder), 'text file (*.csv *.txt)')
        try:
            positions = np.loadtxt(filepath, delimiter=',')
            self.dataflow.set_motors(positions)
            self.clear_outputs()
            self.update_infos()
            self.send_and_close(self.Outputs.data, self.dataflow)
        except Exception:
            self.Warning.invalid_csv()

    def _export_motors_positions(self):
        # noinspection PyArgumentList
        savepath = QFileDialog.getSaveFileName(None, 'Export motors positions', str(self.last_folder) + '_motors_positions',
                                               filter='text file (*.csv *.txt)')
        if not savepath[0]:
            return
        motors_pos = np.asfarray(self.dataflow.motors_to_list()).T
        if motors_pos.size < 1:
            return
        np.savetxt(Path(savepath[0]), motors_pos, delimiter=',')

    def _clean_plst(self):
        self.dataflow.stitched = None
        self._update_data()
        self._update_caption(f'Stitched data removed.')

    def clear_outputs(self):
        self.Outputs.data.send(None)
        super().clear_outputs()

    def open_dialog(self):
        self.filepaths = self.open_files_dialog(start_dir=str(self.current_folder))
        if not self.filepaths:
            return
        return Path(self.filepaths[0])

    def load_manual(self):
        ret = self.open_dialog()
        if ret is None:
            return
        self.clear_all_messages()
        stype = 1 if len(self.filepaths) > 1 else 0
        self.save_selection(stype)
        self._load_files()

    def load_sequence(self):
        first_file = self.open_dialog()
        if first_file is None:
            return
        self.clear_all_messages()
        self.filepaths = self.create_sequence(first_file)
        if not self.filepaths:
            self.Warning.invalid_sequence()
            stype = 1 if len(self.filepaths) > 1 else 0
            self.save_selection(stype)
            self._load_files()
            return
        if len(self.filepaths) == 1:
            self.save_selection(0)
        else:
            self.save_selection(2)
        self._load_files()

    def add_history_entry(self, *args, **kwargs):
        last = self.history[0]
        stype = ['- Single file loaded:', '- Multiple files loaded from folder:', '- Sequence loaded from folder:']
        etype = last['type']
        entry = stype[etype]
        name = 'noname'
        if etype == 0:
            name = last['name'].split(':')[-1]
            entry = entry + '\n   ' + name
        elif etype == 1:
            name = Path(last['value'][0]).name
            parent = Path(last['value'][0]).parent.name
            entry = entry + '\n   ...\\' + parent + '\\' + f'\n    {name},...'
        elif etype == 2:
            name = Path(last['value'][0]).name
            parent = Path(last['value'][0]).parent.name
            entry = entry + '\n   ...\\' + parent + '\\' + f'\n    {name},...'
        self.dataflow.add_history_entry(entry)
        self.dataflow.set_title(name)
        return entry

    def save_selection(self, stype):
        first_file = str(self.filepaths[0])
        if stype !=0 and first_file.split('.')[-1] in ('plst',): #('nx5', 'hdf5', 'h5'):
            self.clear_outputs()
            self.Error.hdf5_multi()
            return
        history = [[entry['type'], entry['value'][0]] for entry in self.history]
        entry = [stype, first_file]
        try:
            self.history.pop(history.index(entry))
        except ValueError:
            """"""
        history_entry = {'name': self.create_selection_text(stype, self.filepaths),
                         'type': stype,
                         'value': self.filepaths if stype == 1 else [first_file,]}
        self.history = [history_entry,] + self.history
        self.history = self.history[:self.max_recent]
        save_user_history(self.merge_history())
        self.build_file_combo()

    @property
    def current_selection(self):
        history = self.merge_history()
        if history and self.file_combo.currentIndex() < len(history):
            return history[self.file_combo.currentIndex()]
        return {}

    @property
    def current_folder(self):
        history = self.merge_history()
        if history and self.file_combo.currentIndex() < len(history):
            folderpath = Path(history[self.file_combo.currentIndex()][1][0])
            if not folderpath.is_dir():
                folderpath = folderpath.parent
            return folderpath
        return self.last_folder

    def _try_relative(self, current_selection):
        if not Path(current_selection[1][0]).exists():
            parent = Path(self.scheme_widget.path()).parent
            roots = list(parent.rglob(Path(current_selection[1][0]).name))
            if len(roots) == 0:
                return None
            selection = [str(Path(roots[0].parent, Path(rel_path).name)) for rel_path in current_selection[1]]
            current_selection[1] = selection
        return current_selection

    def _reload(self):
        current_selection = self._try_relative(self.current_selection)
        if current_selection is None:
            self.Information.clear()
            self.Error.file_not_found()
            self.clear_outputs()
            return
        try:
            if current_selection[0] == 2:
                self.filepaths = self.create_sequence(Path(current_selection[1][0]))
            else:
                self.filepaths = current_selection[1]
        except AttributeError:
            self.Information.clear()
            self.Error.no_data()
            self.clear_outputs()
            return
        if self.filepaths:
            self.save_selection(current_selection[0])
        self._load_files()

    def clean_history(self):
        updated = []
        for entry in self.merge_history():
            first_file = entry[1][0]
            try:
                if Path(first_file).exists():
                    stype = int(entry[0])
                    filepaths = entry[1]
                    history_entry = {'name': self.create_selection_text(stype, filepaths),
                                     'type': stype,
                                     'value': filepaths if stype == 1 else [first_file, ]}
                    updated.append(history_entry)
                    break # keep only the last valid entry
            except PermissionError:
                continue
        msg = f'Widget history cleaned: {len(self.history) - len(updated[:self.max_recent])} entries removed.'
        self.Information.clear()
        self.Information.general(msg)
        if self.verbose:
            print(msg)
        self.history = updated[:self.max_recent]
        self.build_file_combo()

    def build_file_combo(self):
        self.file_combo.clear()
        for i, entry in enumerate(self.merge_history()):
            if i == self.max_recent:
                break
            stype = int(entry[0])
            filepaths = entry[1]
            selname = self.create_selection_text(stype, filepaths)
            self.file_combo.addItem(selname)

    def merge_history(self):
        history = [[entry['type'],entry['value']] for entry in self.history]
        if not history:
            return self.get_history()
        for e, entry in enumerate(self.get_history()):
            if entry not in history:
                history.append(entry)
            if e == self.max_recent:
                break
        return history

    def rebuild_history(self):
        for entry in self.history:
            if entry['type'] == 2:
                entry['value'] = [entry['value'][0],]
        self.history = self.history[:self.max_recent]

    @staticmethod
    def _find_sequence_style(stem:str):
        for style, regex in SEQUENCE_STYLES.items():
            match = re.search(regex.lower(), stem.lower())
            if match:
                return style, regex, match
        return None, None, None

    def create_sequence(self, filename:Path):
        root_folder = filename.parent
        style, regex, match = OWLoadMeasurement._find_sequence_style(filename.stem)
        if match is None:
            self.Warning.invalid_sequence()
            return None
        if self.verbose:
            print('Found sequence style: ' + f'\'{style}\'')
        start = match.start()
        root_name = filename.stem[:start]
        lis = root_folder.glob(root_name + '*' + filename.suffix)
        filenames = []
        idx = []
        for f in lis:
            match = re.search(regex.lower(), f.stem.lower())
            if match is None:
                continue
            if len(f.stem[:match.start()]) != len(root_name):
                continue
            filenames.append(str(f))
            idx_str = str(match.group())
            idx_match = re.search(r'\d+', idx_str)
            if idx_match is None:
                continue
            idx.append(int(idx_match.group()))
        try:
            idx = np.array(idx) - min(idx)
        except ValueError:
            self.Warning.file_missing()
            return None
        return list(np.array(filenames)[np.argsort(idx)])

    @staticmethod
    def create_selection_text(stype:int, filepaths):
        list_type = ['', 'multi: ', 'seq: ']
        first_file = filepaths[0]
        if stype == 2:
            return list_type[stype] + Path(first_file).name
        elif stype == 1:
            return list_type[stype] + Path(first_file).parent.name
        return Path(first_file).name

    @staticmethod
    def _convert_sampling(data, sampling):
        if data.is_1D:
            sampling = np.asfarray([sampling, 0])
        sampling_unit = data.units.get('coords', u.unitless)
        if str(sampling_unit) not in ('unitless', 'pix'):
            _, new_unit = sampling_unit.auto(max(sampling))
            sampling = new_unit(sampling, sampling_unit)
            sampling_unit = new_unit
        return sampling, sampling_unit

    # noinspection PyArgumentList,PyUnresolvedReferences,PyAttributeOutsideInit
    def update_infos(self):
        dataset = self.dataflow
        self.infobox.layout().removeWidget(self.infos)
        self.infos.deleteLater()

        # infos box
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.infos = gui.widgetBox(self.infobox, orientation=layout)

        layout.addWidget(gui.widgetLabel(None, ' '), 0, 0)
        representation = repr(dataset)
        datatype, size = representation.split(':')
        single = len(dataset) == 1
        if single:
            datatype = 'Single measurement'
            size = size.split('x', 1)[-1]

        lbl = gui.widgetLabel(None, 'Type: ')
        layout.addWidget(lbl, 1, 0)
        lbl = gui.widgetLabel(None, datatype)
        layout.addWidget(lbl, 1, 1)

        layout.addWidget(self.clean_plst, 1, 2)
        if isinstance(self.reader, PylostFormat) and dataset.is_stitched:
            self.clean_plst.show()
        else:
            self.clean_plst.hide()

        lbl = gui.widgetLabel(None, 'Reader: ')
        layout.addWidget(lbl, 2, 0)
        lbl = gui.widgetLabel(None, self.reader.DESCRIPTION)
        layout.addWidget(lbl, 2, 1)

        layout.addWidget(gui.widgetLabel(None, ' '), 6, 0)
        lbl = gui.widgetLabel(None, 'Sampling: ')
        lbl.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(lbl, 7, 0)
        sampling, sampling_unit = self._convert_sampling(dataset, dataset.mean_step)
        if dataset.is_1D:
            string = f'     {sampling[0]:.3f} {sampling_unit}'
        else:
            string = f'     {sampling[0]:.3f} {sampling_unit} x {sampling[1]:.3f} {sampling_unit}'
        if dataset.is_oversampled:
            string = string + f'\n  (oversampling: {dataset.pixel_size:0.3f} {sampling_unit})'
        lbl = gui.widgetLabel(None, string)
        layout.addWidget(lbl, 7, 1)

        layout.addWidget(gui.widgetLabel(None, ' '), 3, 0)
        lbl = gui.widgetLabel(None, 'Size: ')
        layout.addWidget(lbl, 4, 0)
        lbl = gui.widgetLabel(None, '    ' + size)
        layout.addWidget(lbl, 4, 1)
        shape = dataset.shape
        string = f'     {shape[0]}  x  ({shape[1]}' if dataset.is_2D else f'     {shape[0]}  x  {shape[1]}'
        if single:
            string = f'     {shape[1]}'
        if dataset.is_1D:
            string = string + f' points'
        else:
            string = string + f' x {shape[2]}'
            if not single:
                string = string + ')'
        lbl = gui.widgetLabel(None, 'Shape: ')
        layout.addWidget(lbl, 5, 0)
        lbl = gui.widgetLabel(None, string)
        layout.addWidget(lbl, 5, 1)

        if not single:
            layout.addWidget(gui.widgetLabel(None, ' '), 8, 0)
            lbl = gui.widgetLabel(None, 'Motors: ')
            lbl.setAlignment(Qt.AlignLeft | Qt.AlignTop)
            layout.addWidget(lbl, 9, 0)
            if dataset.has_motors:
                grid_layout = QGridLayout()
                grid_layout.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
                grid = gui.widgetBox(None, orientation=grid_layout)
                unit = dataset.units.get('coords', u.unitless)
                string = gui.widgetLabel(None, 'Pos     ')
                grid_layout.addWidget(string, 0, 0)
                string = gui.widgetLabel(None, f'  X ({unit})     ')
                grid_layout.addWidget(string, 0, 1)
                string = gui.widgetLabel(None, f'  Y ({unit})     ')
                grid_layout.addWidget(string, 0, 2)
                x, y = dataset.motors_to_list()
                size = len(x)
                pos = list(np.arange(0, size))
                if size > 7:
                    pos = list(np.arange(0, 3)) + [np.nan] + list(np.arange(size - 3, size))
                    x = x[:3] + [np.nan] + x[-3:]
                    y = y[:3] + [np.nan] + y[-3:]
                for i, motor in enumerate(zip(pos, x, y)):
                    p, px, py = motor
                    p = '' if np.isnan(p) else f'#{int(p) + 1}  '
                    px = '...     ' if np.isnan(px) else f'{px:.3f}  '
                    py = '  ...   ' if np.isnan(py) else f'  {py:.3f}'
                    string = gui.widgetLabel(None, p)
                    grid_layout.addWidget(string, i+1, 0)
                    string = gui.widgetLabel(None, px)
                    grid_layout.addWidget(string, i+1, 1)
                    string = gui.widgetLabel(None, py)
                    grid_layout.addWidget(string, i+1, 2)
                layout.addWidget(grid, 9, 1)
            else:
                lbl = gui.widgetLabel(None, 'no motors informations')
                layout.addWidget(lbl, 9, 1)
            layout.addWidget(gui.widgetLabel(None, ' '), 10, 0)
            self.update_motors_button = gui.button(None, self, 'Update from csv file', autoDefault=False,
                                                   callback=self._load_motors_csv)
            self.update_motors_button.setIcon(self.style().standardIcon(QStyle.SP_FileIcon))
            self.update_motors_button.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
            self.update_motors_button.setToolTip('Load motors positions from a 2 columns csv file (values in mm).')
            layout.addWidget(self.update_motors_button, 11, 1)
            self.create_motors_button = gui.button(None, self, 'Generate constant steps', autoDefault=False,
                                                   callback=self._generate_from_constant_steps)
            # self.create_motors_button.setIcon(self.style().standardIcon(QStyle.SP_DialogRetryButton))
            self.create_motors_button.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
            self.create_motors_button.setToolTip('Generate motors positions from constant steps.')
            layout.addWidget(self.create_motors_button, 12, 1)
            self.shiftx, self.shifty = dataset.get_motors_steps()
            coords_unit = dataset.coords_unit
            steps_widget_lbl = gui.hBox(self.infobox)
            # steps_widget_lbl.layout().setAlignment(Qt.AlignHCenter)
            gui.widgetLabel(steps_widget_lbl, f' X ({coords_unit}) ')
            # gui.widgetLabel(steps_widget_lbl, '   ')
            gui.widgetLabel(steps_widget_lbl, f' Y ({coords_unit}) ')
            layout.addWidget(steps_widget_lbl, 11, 2)
            steps_widget = gui.hBox(self.infobox)
            self.step_x = FloatLineEdit(None, value=self.shiftx, fmt='.3f', maxwidth=61)
            steps_widget.layout().addWidget(self.step_x, alignment=Qt.AlignHCenter)
            self.step_y = FloatLineEdit(None, value=self.shifty, fmt='.3f', maxwidth=61)
            steps_widget.layout().addWidget(self.step_y, alignment=Qt.AlignHCenter)
            layout.addWidget(steps_widget, 12, 2)

            layout.addWidget(gui.button(None, self, 'Export to csv file', autoDefault=False,
                                        callback=self._export_motors_positions), 11, 3, alignment=Qt.AlignRight)

        # sampling box
        if self.samplingbox is not None:
            self.controlArea.layout().removeWidget(self.samplingbox)
            self.samplingbox.deleteLater()
        self.samplingbox = gui.hBox(self.controlArea, 'Change sampling', stretch=0,
                                    sizePolicy=(QSizePolicy.Expanding, QSizePolicy.Minimum))
        layout = self.samplingbox.layout()
        layout.setAlignment(Qt.AlignLeft| Qt.AlignTop)
        sampling, sampling_unit = self._convert_sampling(dataset, dataset.pixel_size)
        self.sampling_unit = sampling_unit
        gui.widgetLabel(self.samplingbox, f' X ({sampling_unit}) ')
        self.resx = FloatLineEdit(None, value=sampling[0], fmt='.6f')#, maxwidth=101)
        layout.addWidget(self.resx)
        gui.widgetLabel(self.samplingbox, '       ')
        gui.widgetLabel(self.samplingbox, f' Y ({sampling_unit}) ')
        self.resy = FloatLineEdit(None, value=sampling[1], fmt='.6f')#, maxwidth=101)
        layout.addWidget(self.resy)
        if dataset.is_1D:
            self.resy.setDisabled(True)
        gui.widgetLabel(self.samplingbox, '       ')
        self.apply_res_button = gui.button(self.samplingbox, self, 'Apply', autoDefault=False,
                                           callback=self._change_pixel_size)

        # self.controlArea.setMinimumWidth(400)

    def _get_last_regex(self, regex_idx):
        self._last_regex = regex_idx

    def open_regex_dialog(self):
        dialog = Regex_Dialog(self._last_regex, Path(self.current_selection[1][0]).stem)
        dialog.sigClose.connect(self._get_last_regex)
        if not dialog.exec():
            return False
        dialog.close()
        dialog.deleteLater()

    # noinspection PyAttributeOutsideInit,PyArgumentList
    def set_ui(self):
        # Data viewer in mainArea
        box = gui.vBox(self.mainArea, 'Silx dataviewer', stretch=1)
        box.setMinimumSize(950, 630)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().addWidget(self._plotviewer)

        # controlArea
        self.prepare_control_area(no_history=True)
        self.autocalc_cb.setEnabled(False)

        layout = QGridLayout()
        layout.setAlignment(Qt.AlignLeft)
        gui.widgetBox(self.controlArea, 'Load', margin=10, orientation=layout, addSpace=True, stretch=0,
                      sizePolicy=(QSizePolicy.Minimum, QSizePolicy.Fixed))
        lbl = gui.widgetLabel(None, 'Input file: ')
        layout.addWidget(lbl, 0, 0)
        self.file_combo.setMaximumSize(171, 21)
        # self.controlArea.setMinimumWidth(171)
        self.file_combo.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        layout.addWidget(self.file_combo, 0, 1)

        self.file_button = gui.button(None, self, '...', autoDefault=False, callback=self.load_manual)
        self.file_button.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))
        self.file_button.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
        layout.addWidget(self.file_button, 0, 2)

        self.reload_button = gui.button(None, self, 'Reload', autoDefault=False, callback=self._reload)
        self.reload_button.setIcon(self.style().standardIcon(QStyle.SP_BrowserReload))
        self.reload_button.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        layout.addWidget(self.reload_button, 0, 3)

        self.clean_history = gui.button(None, self, 'Clean history', autoDefault=False, callback=self.clean_history)
        self.clean_history.setIcon(self.style().standardIcon(QStyle.SP_DialogResetButton))
        self.clean_history.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        layout.addWidget(self.clean_history, 1, 1)
        self.clean_history.setEnabled(True)

        self.stitching_folder = gui.button(None, self, 'Auto Seq', autoDefault=False, callback=self.load_sequence)
        self.stitching_folder.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))
        self.stitching_folder.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        layout.addWidget(self.stitching_folder, 1, 2)

        self.btnAbort = gui.button(None, self, 'Abort', stretch=1, autoDefault=False, callback=self._abort)
        self.btnAbort.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        layout.addWidget(self.btnAbort, 1, 3)
        self.btnAbort.hide()

        self.test_sequence = gui.button(None, self, 'Test sequence', autoDefault=False, callback=self.open_regex_dialog)
        self.test_sequence.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        layout.addWidget(self.test_sequence, 1, 3)

        self.infobox = gui.widgetBox(self.controlArea, 'Info', stretch=1,
                       sizePolicy=(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.infos = gui.widgetLabel(self.infobox, 'No data loaded.')
        # self.warnings = gui.widgetLabel(self.infobox, '')

        self.clean_plst = gui.button(None, self, 'Keep only subapertures', autoDefault=False, callback=self._clean_plst)
        self.clean_plst.setIcon(self.style().standardIcon(QStyle.SP_DialogResetButton))
        self.clean_plst.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
