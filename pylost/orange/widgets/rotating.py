# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils.methods import rotate


class OWRotate(PylostWidgets):
    """ each widget in Pylost should be a subclass of 'PylostWidgets' """
    name = 'Rotate'
    description = ''
    icon = "../icons/rotate.png"
    priority = 34

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    rotation_angle:float = Setting(0)

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        rotated = Output('Rotated data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        generic = Msg('{}')
        profile_not_supported = Msg('Only 2D data can be rotated')

    # -------------------------------------------------------------------------
    # Widget warnings
    # -------------------------------------------------------------------------
    # class Warning(widget.OWWidget.Warning):
    #     aborted = Msg('{}')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'rotate_data'

    def sizeHint(self):
        return QSize(400, 500)

    def clear_outputs(self):
        self.Outputs.rotated.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        if not dataset:
            self.dataflow = None
            return
        if dataset.is_1D:
            self.Error.profile_not_supported()
            self.dataflow = None
            return
        self.dataflow = dataset
        self.acknowledge_reception(dataset)
        if self.autocalc:
            self.rotate_data()

    def rotate_data(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        self.start_executor(rotate, self.prepare_dataset(self.dataflow), rotation_angle=self.rotation_angle)

    def _handle_results(self, rotated:list):
        if not rotated:
            return
        super()._handle_results(rotated)
        status = f'Data rotated by an angle of {self.rotation_angle} degree clockwise.'
        history = '- ' + status
        self.send_and_close(self.Outputs.rotated, self.create_output_dataset(rotated), history=history)
        self._update_caption(status)

    def _update_params(self):
        # to store the value in the *.ows file when saving
        angle_deg = self.float_angle.value() % 360
        if angle_deg > 180:
            angle_deg -= 360
        self.float_angle.setValue(angle_deg, block_signal=True)
        self.rotation_angle = self.float_angle.value()

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        """method called by super().__init__(*args, **kwargs)"""

        # add options box
        self.prepare_control_area()
        self.toggle_control_area()

        # qt5
        main = gui.hBox(self.mainArea)
        main.setMaximumSize(350, 500)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        box = gui.hBox(main, 'Rotation', stretch=1)
        box.setMaximumSize(300, 50)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        angle_lbl = QLabel('Rotation angle (deg)', None)
        self.float_angle = FloatLineEdit(None, self.rotation_angle, maxwidth=61, fmt='.3f', callback=self._update_params)
        self.float_angle.setToolTip("Clockwise rotation angle")
        box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        box.layout().addWidget(angle_lbl, Qt.AlignLeft | Qt.AlignTop)
        box.layout().addWidget(self.float_angle, Qt.AlignLeft | Qt.AlignTop)
        gui.button(box, self, 'Rotate data', autoDefault=False, callback=self.rotate_data)


# global variable set in the user settings.ini file
if VERBOSE:
    print(f"  module '{__name__}' loaded.")
