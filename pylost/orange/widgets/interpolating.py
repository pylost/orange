# coding=utf-8

from pylost.orange.widgets import *
from pylost.utils.methods import interpolate, spline_interpolation


class OWInterpolation(PylostWidgets):
    name = 'Interpolate subpixels'
    description = 'Interpolate subapertures (a) to nearest pixel shift or (b) to a smaller pixel size.'
    icon = "../icons/spline.svg"
    priority = 43

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    timeout:int = Setting(180)
    type:int = Setting(0)
    algorithm:int = Setting(0)
    pixel_size:list = Setting([])
    length:float = Setting(0)
    force_len:bool = Setting(False)
    unit:str = Setting('')

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        interpolated = Output('Interpolated', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sampling_unit = None
        self.spacebar_action = 'interpolate_data'

    def clear_outputs(self):
        self.Outputs.interpolated.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self._init_params()
        self._update_ui()
        if self.autocalc:
            self.interpolate_data()

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)
        func = ['scipy interpn', 'scipy griddata', 'scipy map_coordinates'][self.algorithm]
        if self.type == 1:
            caption = f'NaN interpolation using {func}.'
            history = f'- NaN interpolation using {func}'
        elif self.type == 2:
            if processed[0].is_2D:
                pixel_size = f'[{self.pixel_size[0]:.3f}, {self.pixel_size[1]:.3f}]'
            else:
                pixel_size = f'{self.pixel_size[0]:.3f}'
            caption = f'Interpolation using {func}.\n' \
                      'pixel size: ' + pixel_size + f' {self.unit}'
            history = f'- Interpolation using {func}:\n' \
                      '    new pixel size: ' + pixel_size + f' {self.unit}'
            if self.cb_length.isChecked():
                caption = caption + f'\nlength forced to {self.length} mm'
                history = history + f'\nlength forced to {self.length} mm'
        else:
            caption = 'no interpolation'
            history = None
        self._update_caption(caption)
        self.send_and_close(self.Outputs.interpolated, self.create_output_dataset(processed), history)

    def force_length(self):
        if self.cb_length.isChecked() and self.float_length.value() != self.dataflow.size[1]:
            return self.float_length.value()
        return None
        # new_length = self.float_length.value()
        # self._init_params()
        # factor = new_length / self.float_length.value()
        # self.float_length.setValue(new_length)
        # self.resx.setValue(self.resx.value() * factor)
        # self.resy.setValue(self.resy.value() * factor)

    @staticmethod
    def _interpolate_new_pixel_size(data, new_sampling, func, **kwargs):
        """"""
        return interpolate(data, new_sampling=new_sampling, func=func, **kwargs)

    def interpolate_data(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        self.pixel_size = [self.resx.value(), self.resy.value()]
        self.unit = str(self.sampling_unit)
        self.length = self.float_length.value()
        self._update_ui()
        try:
            if self.type == 2:
                new_sampling = self.dataflow.units['coords'](self.resx.value(), self.sampling_unit)
                if self.dataflow.is_2D:
                    new_sampling = [new_sampling, self.dataflow.coords_unit(self.resy.value(), self.sampling_unit)]
                func = ['scipy_interpn', 'scipy_griddata', 'map_coordinates'][self.algorithm]
                self.start_executor(self._interpolate_new_pixel_size, self.prepare_dataset(self.dataflow),
                                    new_sampling=new_sampling, func=func, force_length=self.force_length(), print_warning=self.verbose)
            elif self.type == 1:
                self.start_executor(spline_interpolation, self.prepare_dataset(self.dataflow), print_warning=self.verbose)
            else:
                caption = 'no interpolation'
                self._update_caption(caption)
                self.send_and_close(self.Outputs.interpolated, self.dataflow.copydataset())
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default('Error when interpolating data.', e)

    def set_method(self, idx=None):
        self.algorithm = idx

    def _init_params(self):
        if not self.pixel_size:
            sampling = self.dataflow.pixel_size
            sampling_unit = self.dataflow.units.get('coords', u.unitless)
        else:
            sampling = self.pixel_size
            sampling_unit = getattr(u, self.unit)
        if self.dataflow.is_1D:
            if isinstance(sampling, float):
                sampling = np.asfarray([sampling, 0])
        if sampling_unit is not u.unitless:
            _, new_unit = sampling_unit.auto(max(sampling))
            sampling = new_unit(sampling, sampling_unit)
            sampling_unit = new_unit
        self.sampling_unit = sampling_unit
        self.lbl_resx.setText(f' X ({sampling_unit}) ')
        self.resx.setValue(sampling[0])
        self.lbl_resy.setText(f' Y ({sampling_unit}) ')
        self.resy.setValue(sampling[1])
        self.pixel_size = [sampling[0], sampling[1]]
        self.unit = str(sampling_unit)
        if abs(self.length) < 0.01 or self.cb_length.isChecked():
            self.float_length.setValue(self.dataflow.size[1])
        else:
            self.float_length.setValue(self.length)

    def _update_ui(self):
        # self.combo.setDisabled(self.type != 2)
        if not self.dataflow:
            return
        if self.dataflow.is_1D:
            self.resy.setDisabled(True)
            self.combo.setDisabled(True)
            self.combo.setCurrentIndex(0)
        else:
            self.combo.setDisabled(False)
            # self.combo.setCurrentIndex(2)
        self.float_length.setEnabled(self.cb_length.isChecked())

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        self.prepare_control_area()
        # hide control_area at creation
        # self.toggle_control_area()

        # Hide the left control area, if wanted. It's a simple switch.
        self.toggle_control_area()

        # GUI qt5 management (see the doc:https://doc.qt.io/qtforpython-5/)
        main = gui.vBox(self.mainArea)
        main.setMaximumSize(420, 300)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Interpolate data', stretch=1, autoDefault=False,
                                   callback=self.interpolate_data)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        box = gui.vBox(main, 'Interpolation options', stretch=1, spacing=20)
        box.setMaximumSize(421, 201)
        box.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        self.combo = gui.comboBox(box, self, 'algorithm', label='',
                                  items=('Scipy interpn', 'Scipy griddata', 'Scipy map_coordinates'),#, 'Bspline (Superflat)'),
                                  )
        self.combo.currentIndexChanged.connect(self.set_method)
        self.combo.setMaximumSize(151, 31)
        self.combo.setDisabled(self.type != 2)

        layout = QGridLayout()
        gui.widgetBox(box, box=None, spacing=10, addSpace=True,# stretch=8,
                      orientation=layout, sizePolicy=(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum))

        rb = gui.radioButtons(None, self, 'type', box=True, addSpace=True, addToLayout=False, callback=self._update_ui)
        rb1 = gui.appendRadioButton(rb, 'No interpolation.', addToLayout=False)
        rb2 = gui.appendRadioButton(rb, 'Interpolate NaN and Inf.', addToLayout=False)
        rb3 = gui.appendRadioButton(rb, 'Interpolate to new pixel size:', addToLayout=False)
        layout.addWidget(rb1, 0, 0, Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(rb2, 1, 0, Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(rb3, 2, 0, Qt.AlignLeft | Qt.AlignTop)

        sampling_unit = 'um'
        sampling = [0.0, 0.0]
        self.lbl_resx = gui.widgetLabel(None, f' X ({sampling_unit}) ')
        self.resx = FloatLineEdit(None, value=sampling[0], fmt='.6f', maxwidth=101)
        self.lbl_resy = gui.widgetLabel(None, f'    Y ({sampling_unit}) ')
        self.resy = FloatLineEdit(None, value=sampling[1], fmt='.6f', maxwidth=101)
        layout.addWidget(self.lbl_resx, 2, 1, Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(self.resx, 2, 2, Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(self.lbl_resy, 2, 3, Qt.AlignLeft | Qt.AlignTop)
        layout.addWidget(self.resy, 2, 4, Qt.AlignLeft | Qt.AlignTop)

        length_box = gui.hBox(box)
        length_box.setMaximumSize(200, 50)
        self.cb_length = gui.checkBox(box, self, 'force_len', '', callback=self._update_ui)
        length_lbl = QLabel('Force longitudinal length (mm)', None)
        self.float_length = FloatLineEdit(None, value=self.length, maxwidth=61, fmt='.3f')
        length_box.layout().addWidget(self.cb_length, Qt.AlignLeft | Qt.AlignTop)
        length_box.layout().addWidget(length_lbl, Qt.AlignLeft | Qt.AlignTop)
        length_box.layout().addWidget(self.float_length, Qt.AlignLeft | Qt.AlignTop)


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
