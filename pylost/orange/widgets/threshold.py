# coding=utf-8

from pylost.orange.widgets import *


class OWThreshold(PylostWidgets):
    name = 'Threshold'
    description = ''
    icon = "../icons/threshold.svg"
    priority = 54

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    want_main_area = True
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    method:dict = Setting({})

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs
    # -------------------------------------------------------------------------
    class Outputs:
        data = Output('Dataset', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        self.unit = ''
        super().__init__(*args, **kwargs)
        self.spacebar_action = 'apply_threshold'

    def sizeHint(self):
        return QSize(360, 180)

    def clear_outputs(self):
        self.Outputs.data.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            return
        self.acknowledge_reception(dataset)
        self.unit = dataset.values_unit
        self._update()
        if self.autocalc:
            self.apply_threshold()

    def _handle_results(self, processed):
        if not processed:
            return
        super()._handle_results(processed)
        meth = ('std', 'min_max')[self.method['idx']]
        caption = ''
        if meth == 'std':
            factor = abs(self.method['factor'])
            caption = f'{factor} sigma'
        elif meth == 'min_max':
            minval, maxval = self.method['min'], self.method['max']
            caption = f'min: {minval} {self.unit}\n max: {maxval} {self.unit}'
        self._update_caption(caption)
        history = f'- {caption} threshold applied'
        self.send_and_close(self.Outputs.data, self.create_output_dataset(processed), history)

    @staticmethod
    def _apply_threshold(data:[Surface, Profile], method):
        meth = ('std', 'min_max')[method.get('idx', 0)]
        if meth == 'std':
            factor = abs(method.get('factor', 3))
            mean = data.mean
            rms = np.nanstd(data.values - mean) + mean
            data.values = np.where(np.logical_and(data.values > -factor * rms, data.values < factor * rms),
                                   data.values, np.nan)
        elif meth == 'min_max':
            mean = 0 # data.mean
            pv = data.pv
            threshold_min = method.get('min', -0.9 * pv) + mean
            threshold_max = method.get('max', 0.9 * pv) + mean
            data.values = np.where(np.logical_and(data.values > threshold_min, data.values < threshold_max),
                                   data.values, np.nan)
        return data

    def apply_threshold(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()
        try:
            self.start_executor(self._apply_threshold, self.prepare_dataset(self.dataflow), method=self.method)
        except Exception as e:
            # self.create_traceback(str(e))
            self.Information.clear()
            self.Error.default('Error when applying threshold to data.', e)

    def _update(self):
        self.min_lbl.setText(f'Min ({self.unit})')
        self.max_lbl.setText(f'Max ({self.unit})')
        idx = self.combo.currentIndex()
        self.method['idx'] = idx
        if idx == 0:
            self.factor_lbl.show()
            self.factor.show()
            self.min_lbl.hide()
            self.min.hide()
            self.max_lbl.hide()
            self.max.hide()
            self.method['factor'] = self.factor.value()
        elif idx == 1:
            self.factor_lbl.hide()
            self.factor.hide()
            self.min_lbl.show()
            self.min.show()
            self.max_lbl.show()
            self.max.show()
            self.method['idx'] = idx
            self.method['min'] = self.min.value()
            self.method['max'] = self.max.value()

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        self.prepare_control_area()
        # hide control_area at creation
        self.toggle_control_area()

        # main area
        self.mainArea.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(275, 200)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        main.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.btnApply = gui.button(actions, self, 'Apply threshold', stretch=1, autoDefault=False, callback=self.apply_threshold)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        params = gui.widgetBox(main, 'Threshold', stretch=1)
        grid_layout = QGridLayout()
        method_lbl = QLabel('Method', None)
        self._tmp = self.method.get('idx', 0)
        self.combo = gui.comboBox(None, self, '_tmp', label='Method',
                                  items=('standard deviation', 'min/max'),
                                  callback=self._update)
        self.combo.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.factor_lbl = QLabel('Factor', None)
        self.factor = FloatLineEdit(self, value=self.method.get('factor', 3), maxwidth=61, callback=self._update)
        self.min_lbl = QLabel('Min', None)
        self.min = FloatLineEdit(self, value=self.method.get('min', -1), maxwidth=61, callback=self._update)
        self.max_lbl = QLabel('Max', None)
        self.max = FloatLineEdit(self, value=self.method.get('max', 1), maxwidth=61, callback=self._update)
        grid_layout.addWidget(method_lbl, 0, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.combo, 1, 0, Qt.AlignVCenter)
        grid_layout.addWidget(self.factor_lbl, 0, 2, Qt.AlignVCenter)
        grid_layout.addWidget(Spacer(), 1, 1, Qt.AlignVCenter)
        grid_layout.addWidget(self.factor, 1, 2, Qt.AlignVCenter)
        grid_layout.addWidget(self.min_lbl, 0, 2, Qt.AlignVCenter)
        grid_layout.addWidget(self.min, 1, 2, Qt.AlignVCenter)
        grid_layout.addWidget(self.max_lbl, 0, 3, Qt.AlignVCenter)
        grid_layout.addWidget(self.max, 1, 3, Qt.AlignVCenter)
        grid = QWidget(None)
        grid.setLayout(grid_layout)
        params.layout().addWidget(grid)

        self._update()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
