# coding=utf-8

from pylost.orange.widgets import *


class OWMasking(PylostWidgetsViewerMixin):
    name = 'Mask'
    description = ''
    icon = "../icons/mask.svg"
    priority = 12

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters
    # -------------------------------------------------------------------------
    # last:dict = Setting({})

    # -------------------------------------------------------------------------
    # Widget inputs
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Masking', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors
    # -------------------------------------------------------------------------
    class Error(widget.OWWidget.Error):
        default = Msg('{}')
        not_implemented = Msg('{}')
        scans_not_found = Msg('No scan data is available.')

    def __init__(self, *args, **kwargs):
        super().__init__(limit_size=False, mask=True, custom_toolbar=False, *args, **kwargs)
        # if self.verbose and self.values:
        #     print('mask params', self.values, self.pixels, self.rectangle, self.is_pixel, self.is_relative)

    def clear_outputs(self):
        self.Outputs.data.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):
        self.clear_outputs()
        self.dataflow = dataset
        if not dataset:
            self._plotviewer.clear()
            return
        self.acknowledge_reception(dataset)
        # self.prepare_dataset(dataset)  a copy of data is made in 'self.send_mask_params'
        self.data_to_viewer(dataset)
        self._plotviewer.show_mask_widget()
        mask_params = self.check_params()
        if mask_params:
            # update mask with current parameters
            self._plotviewer.mask_widget.update_from_dict(mask_params)
        self._plotviewer.mask_widget.applyBtn.setText('Apply mask')
        if self.autocalc:
            # send masked data and mask parameters
            self.send_mask_params(mask_params)

    def check_params(self):
        mask_params = self.build_params_dict()
        if not self.rectangle: # or not self.dataflow.masking(mask_params['rectangle']):
            return {}
        return mask_params

    # noinspection PyAttributeOutsideInit,PyArgumentList,PyUnresolvedReferences
    def set_ui(self):
        # Data viewer in mainArea
        box = gui.vBox(self.mainArea, 'Silx dataviewer', stretch=1)
        box.setMinimumSize(800, 600)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().addWidget(self._plotviewer)

        # controlArea
        self.prepare_control_area(timeout=False)
        # hide control_area at creation
        self.toggle_control_area()


if VERBOSE:
    print(f"  module '{__name__}' loaded.")
