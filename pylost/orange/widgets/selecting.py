# coding=utf-8

from pylost.orange.widgets import *
from AnyQt.QtGui import QBrush, QColor

Brush = QBrush(QtCore.Qt.white), QBrush(QColor('lightskyblue'))


class OWSelect(PylostWidgets):
    """ each widget in Pylost should be a subclass of 'PylostWidgets' """
    name = 'Select sub-apertures'
    description = ''
    icon = "../icons/filter-subap.svg"
    priority = 52

    # -------------------------------------------------------------------------
    # Widget GUI Layout Settings: not used yet
    # -------------------------------------------------------------------------
    settings_version = 1
    # -------------------------------------------------------------------------
    # Widget saved parameters: need to be updated when necessary (by user interaction)
    # -------------------------------------------------------------------------
    removed:list = Setting([])

    # -------------------------------------------------------------------------
    # Widget inputs: can be one or more (MultiInput class is different and needs more care, see: PylostWidgetsOperators)
    # -------------------------------------------------------------------------
    class Inputs:
        data = Input('Data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget outputs: can be one or more (should send different class to be easier to handle)
    # -------------------------------------------------------------------------
    class Outputs:
        selected = Output('Selected data', StitchingDataset)

    # -------------------------------------------------------------------------
    # Widget errors: override the default error messages
    # -------------------------------------------------------------------------
    # class Error(widget.OWWidget.Error):
    #     generic = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget warnings: override the default warning messages
    # -------------------------------------------------------------------------
    # class Warning(widget.OWWidget.Warning):
    #     aborted = Msg('{}')

    # -------------------------------------------------------------------------
    # Widget informations: override the default information messages
    # -------------------------------------------------------------------------
    # class Information(widget.OWWidget.Information):
    #     succes = Msg('42')

    header = ['source', 'type', 'kind', 'shape']

    spacebar_action = 'select'

    def __init__(self, *args, **kwargs):
        #   !!MANDATORY!!
        # Inherit all the methods and objects from the pylost widget canvas.
        self.select_all = True
        super().__init__(limit_size=False, *args, **kwargs)
        self.initialized = False
        self.spacebar_action = 'select'

    def sizeHint(self):
        return QSize(0, 0)

    def clear_outputs(self):
        self.tree_widget.clear()
        self.header[0] = 'source'
        self.tree_widget.setHeaderLabels(self.header)
        self.Outputs.selected.send(None)
        super().clear_outputs()

    @Inputs.data
    def get_data(self, dataset:StitchingDataset):

        # this method should be overload to send no data
        self.clear_outputs()

        #   !!MANDATORY!!
        # incoming data must be set as 'self.dataflow'
        self.dataflow = dataset

        # incoming data: could be 'None' if empty link or in instance of removal\insertion upstream
        if not dataset:
            if self.initialized:
                self.removed = []
            return
        self.initialized = True

        # clear all messages and fill the log
        self.acknowledge_reception(dataset)

        self.treeview()
        self.select()

    def treeview(self):
        self.clear_outputs()
        if self.dataflow is None:
            return
        self.clear_all_messages()

        self.tree_widget.clear()
        self.tree_widget.setColumnCount(4)
        self.header[0] = str(self.dataflow.folder)
        self.tree_widget.setHeaderLabels(self.header)
        scans = []
        max_size = 10
        for i, scan in enumerate(self.dataflow.scans):
            strings = [f'  {i}:  {scan.source}', 'Profile' if scan.is_1D else 'Surface', scan.kind, str(scan.shape)]
            child = QTreeWidgetItem(None, strings)
            max_size = max(max_size, len(scan.source) + 10)
            state = Qt.Unchecked if i in self.removed else Qt.Checked
            color = Brush[0] if i in self.removed else Brush[1]
            self._change_row_color(child, color)
            child.setFlags(child.flags() | Qt.ItemIsUserCheckable | Qt.ItemIsAutoTristate)
            child.setCheckState(0, state)
            scans.append(child)
        self.tree_widget.addTopLevelItems(scans)
        self.tree_widget.setColumnWidth(0, int(max_size * self.logicalDpiX() / 10))

    def select(self):
        data_out = self.dataflow.copydataset()
        history = '- Sub-apertures removed:'
        removed = []
        for idx in self.removed[::-1]:
            scan = data_out.scans.pop(idx)
            if data_out.has_motors:
                data_out.motors.pop(idx)
            removed.append(scan.source)
        for source in removed[::-1]:
            history = history + '\n    ' + source
        caption = f'{len(self.removed)} elements removed.'
        self._update_caption(caption)
        self.send_and_close(self.Outputs.selected, data_out, history=history)

    def _change_row_color(self, item, color):
        self.tree_widget.blockSignals(True)
        for column in range(len(self.header)):
            item.setBackground(column, color)
        self.tree_widget.blockSignals(False)

    @staticmethod
    def _double_clicked(item):
        state = Qt.Unchecked if item.checkState(0) == Qt.Checked else Qt.Checked
        item.setCheckState(0, state)  # self._item_state called just after

    def _item_state(self, item):
        idx = self.tree_widget.indexFromItem(item).row()
        if item.checkState(0) == Qt.Unchecked:
            if idx not in self.removed:
                self.removed.append(idx)
                self._change_row_color(item, Brush[0])
        else:
            try:
                self.removed.pop(self.removed.index(idx))
                self._change_row_color(item, Brush[1])
            except ValueError:
                """"""
        self.removed = sorted(self.removed)

    def _select_all(self):
        self.tree_widget.blockSignals(True)
        for idx in range(self.tree_widget.topLevelItemCount()):
            item = self.tree_widget.topLevelItem(idx)
            item.setCheckState(0, 2 if self.select_all else 0)
            self._item_state(item)
        self.tree_widget.blockSignals(False)

    # noinspection PyArgumentList,PyAttributeOutsideInit,PyUnresolvedReferences
    def set_ui(self):
        # add options box in the left control area
        self.prepare_control_area(timeout=False)

        # Hide the left control area, if wanted. It's a simple switch.
        self.toggle_control_area()

        # GUI qt5 management (see the doc:https://doc.qt.io/qtforpython-5/)
        # main area
        main = gui.vBox(self.mainArea)
        main.setMinimumSize(480, 240)
        main.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)

        # buttons
        actions = gui.hBox(main, box='Actions', spacing=10, sizePolicy=(QSizePolicy.Fixed, QSizePolicy.Fixed))
        actions.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        gui.checkBox(actions, self, 'select_all', 'de/select all', callback=self._select_all)
        # manual input

        # apply
        self.btnApply = gui.button(actions, self, 'Apply selection', stretch=1, autoDefault=False, callback=self.select)
        self.btnApply.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        box = gui.hBox(main, 'Sub-apertures list', stretch=1)
        box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        box.layout().setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.tree_widget = QTreeWidget(None)
        self.tree_widget.setColumnCount(4)
        self.tree_widget.setHeaderLabels(self.header)
        self.tree_widget.itemChanged[QTreeWidgetItem, int].connect(self._item_state)
        self.tree_widget.itemDoubleClicked[QTreeWidgetItem, int].connect(self._double_clicked)
        box.layout().addWidget(self.tree_widget)


# global variable set in the user 'settings.ini' file, here for logging
if VERBOSE:
    print(f"  module '{__name__}' loaded.")
