# coding=utf-8
"""
pylost base module
"""

import time
from functools import wraps

# default settings
from pylost.user.settings import *


__version__ = "2.0rc0"


VERBOSE = check_user_settings()
if VERBOSE:
    print('\nloading pylost...')

# user settings
check_user_folder()
check_user_history()
check_user_materials()
check_user_instruments()
check_user_regex()

def timeit(method):
    @wraps(method)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = method(*args, **kwargs)
        end_time = time.time()
        print(f"{method.__name__} => {(end_time-start_time)*1000:.3f} ms")
        return result
    return wrapper

def catch_errors(method):
    @wraps(method)
    def wrapper(*args, **kwargs):
        try:
            return method(*args, **kwargs)
        except Exception as e:
            print(f"Error occured: {method.__name__} => {e}")
    return wrapper
