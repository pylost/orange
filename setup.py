# coding=utf-8
from setuptools import setup
from pathlib import Path
import sys

# entry_points = {"orange.widgets": ("PyLOSt = pylost.orange.widgets",)}#, "PyLOSt.model = pylost.orange.learning")},
user_widgets = Path.home()
for path in ('.pylost', 'plugins', 'custom_widgets'):
    user_widgets = Path(user_widgets, path)
    if not user_widgets.exists():
        print('\n--- PyLOSt User folders creation ---')
        user_widgets.mkdir()
        print('    ' + str(user_widgets) + ' created.')
        if 'custom_widgets' in path:
            path = Path(user_widgets, '__init__.py')
            buf = "# coding=utf-8\n"
            with open(path, 'w') as init:
                init.write(buf)
                print('    ' + 'writing __init__.py in ' + str(user_widgets) + '\n')
sys.path.append(str(user_widgets))
entry_points = {"orange.widgets": ("PyLOSt = pylost.orange.widgets", f"PyLOSt User = custom_widgets")}


setup(
      name='pylost',
      version='2.0.rc0',
      description='This is an Orange3 stitching/analysis add-on for synchrotron x-ray mirrors metrology measurements.',
      url='https://gitlab.esrf.fr/pylost/orange',
      license='MIT',
      package_data={'': ['*.ico', '*.png', '*.rst', '*.svg', '*.txt', '*.yml']},
      python_requires='>=3.9',
      install_requires=['silx==1.1.2', 'orange3==3.36.2', 'sympy==1.12', 'pyFAI==2023.9.0', 'scikit-image==0.22.0',], # conda-forge
      entry_points=entry_points
      )
