**Descripton**

    This is an Orange3 stitching/analysis add-on for synchrotron x-ray mirrors metrology measurements.

        :current version: 2.0.rc0 (python 3.9)


**Installation**

    - Create new conda environment:

        With the conda environment file located in **/doc/installation** (recommended):

            **conda env create -f pylost_(plaform)_(version).yml**

                https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file

        or

        Manual conda environment creation:

            **conda create --name pylost -c conda-forge python=3.9 powershell_shortcut silx=1.1.2 orange3=3.36.2 sympy=1.12 pyFAI=2023.9.0 scikit-image=0.22.0**


    - **conda activate pylost**

    - **pip install git+https://gitlab.esrf.fr/pylost/orange.git@main  --no-deps**


**Documentation**

    Documentation soon available

**Samples**

    data samples can be found on the git: https://gitlab.esrf.fr/pylost/data-samples
